title: Cell Phone Stipend  
author: Julie Storm, Chief People Officer  
published: 2019-07-01  
policy level: Annual General  
data classification: Internal  
approver(s): Julie Storm - Chief People Officer, Jeff Cryder - EVP  
applicable Locations: All  
effective date: 2020-07-01  

---

# Program Overview

The Cell Phone Policy is designed to provide a stipend to eligible team members who regularly use their personal cellular service(s) for ConstructConnect dba iSqFt Inc., BidClerk Inc., CDC Publishing Holdings, and CMD (the company) business due to their position and/or their responsibilities.

# Program Eligibility

Eligibility in the cell phone stipend program is determined by the Vice President of each division.  To participate in the program all of the following criteria must be met:  

1. Your role and responsibilities must designate the need for the use of your cellular service to be used for business related purposes as listed below.
    - Need to contact the team member at all times for work-related matters, 
    - Requirement that the team member be available to speak with clients at times when the team member is away from the office, and 
    - Need to speak with clients located in other time zones at times outside the team member's normal workday. 
1. Written approval from your team Vice President must be forwarded to the Payroll Manager.
1. The Cell Phone Agreement Form for the cell phone stipend program must be completed and returned to the Payroll Manager.
1. You must have an active working cellular service to be eligible for the stipend.  Service stipend must not exceed the cost of active services.    

The company reserves the right to cancel your eligibility to participate in this program at any time.  Should your position and/or responsibilities change within the organization, it will be evaluated and determined if you will continue to be eligible for a stipend. 

# Equipment
Each team member may use their personal cellular equipment or port their personal cell phone number to the company’s “Friends and Family” plan, if applicable to take advantage of the corporate discounted rates.  Team members using their personal cellular equipment must use hardware, software and services that work with IT Operations existing infrastructure.  The stipend for cellular services from the company is the same for any carrier.  Your cellular services must be active and remain active to be eligible for stipend.  The costs associated with the activation, termination and/or any personal fees involved with your cellular service are the responsibility of the team member and may not be submitted as an expense to the company for stipend, unless the action is mandated by the company.

In the event that your phone is stolen or compromised, you may request that we wipe your phone of information. The owner of the device must request/provide permission for the phone to be wiped via email or no action will be taken. The team member is responsible for having an up-to-date backup copy of any data or applications on their phone.  The company is not responsible for any data or applications lost due to a remote wipe.
 
# Leave of Absence

Team members entering a leave of absence may continue use of the cell phone stipend program for up to 90 days.  After 90 days, the company reserves the right to remove the Team Member from the cell phone stipend program.

# Cellular Use While Driving

The company recommends pulling safely to the side of the road prior to engaging in any cellular phone conversations, texting or any other activity relating to cellular use.  Team Members are responsible for knowing and following all state, local, and federal laws regarding the use of cellular phones while operating a motor vehicle, including the use of hands free devices where required.

# Team Member Stipend

Team members approved for the company Cell Phone Stipend will receive a monthly stipend for the services for which they have been approved.  

# Employer-Provided Cell Phones (IRS Guidelines, Publication 15-B)

The value of the employer-provided cell phone stipend, is primarily for noncompensatory business reasons, is excludable from an team member’s income as a working condition fringe benefit. 
