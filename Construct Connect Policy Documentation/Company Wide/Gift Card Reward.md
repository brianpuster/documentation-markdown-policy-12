title: GIFT CARD REWARD  
author: Finance Operations  
published: 2018-01-18  


# Purpose

The purpose of this process implementation is to streamline the administrative activities supporting gift card incentive awards and harmonize the tax treatment for the benefit of all award recipients.

To achieve these objectives the tracking of gift card awards is being centralized in Payroll and the ordering/fulfillment of gift cards in Accounting.

# Process

- Requestor will complete [request form](https://theloop.constructconnect.com/Interact/Pages/Content/Document.aspx?id=6777&SearchId=41878&), obtain proper signatures for approval via electronic signature and submit form for tracking and fulfillment.
	- Amazon is our only vendor and ALL gift cards for any reason will be going through this process.
- Gift card orders will be submitted to finance and processed on or about the 10th & 25th of each month. These are the only dates that gift card orders will be processed so please plan accordingly. No one-off cards in between orders.
- Gift cards purchased individually and submitted as an expense will no longer be approved.
- Gift cards are for internal use only - not for outside vendors, clients, etc.
