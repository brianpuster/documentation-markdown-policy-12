title: Documentation of Approvals – General Requirements  
author: Steve Testa - VP Strategic Initiatives and Continuous Improvement  
published: 2020-02-05  
effective date: 2020-02-05  
policy level: Important Not Urgent
approver(s): Dave Storer - Controller
applicable locations: All  
updated: 2021-02-26    

---
 
This policy covers the general requirements for documentation of any an all types of approvals.
 
- All approvals must be documented in writing/electronic text
- There are no verbal approvals
- If a request for approval is not responded to, approval cannot be implied. 
- Approvals must be attached to the content (communication, policy, etc..) being approved and stored in a central location that is appropriate for the team and/or content. (Shared drive, group email box, etc.)
    - Best Practice: For approvals that existing in an email string it is best to "print to PDF" the entire email string and save it and/or attach it to any purchase request.
- We have a number of policies that layout specific documentation for approvals. Examples are [Software Purchasing Policy](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Software%20Purchasing) and the [Consulting and Professional Services Policy](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Consulting%20and%20Professional%20Services%20Purchasing). To submit these policies for approval simply copy the bullet points from the policy directly into the body of an email. Please also include a link to the goverining policy. Fill out the requested item within the body of the email. By leaving these items in the body of the email, rather than an attached file, the request is easily visible in the body of the email as questions are asked by approvers. This approval request should be sent to the approvers indicated in the policy.


---
