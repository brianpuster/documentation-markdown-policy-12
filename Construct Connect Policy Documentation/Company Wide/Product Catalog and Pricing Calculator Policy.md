title: Product Catalog and Pricing Calculator Policy  
author: Beth Patrick, Accounting Operations Manager   
published: 2021-06-01  
policy level: Very Important  
approver(s): Buck Brody EVP Finance, Jim Hill EVP Trade Contractor, Howard Atkins EVP BPM, Jon Kost EVP GC  
applicable locations: All locations  
effective date: 2021-06-01  



# Policy text:

The ConstructConnect Product Catalog is housed within Zuora and contains all current and expired product offerings. Active products are available in Salesforce during the quoting experience, based on specific Team permission settings. All products have a list price within the Product Catalog, and a pricing calculator is utilized to obtain approved discounting. 

Per the [Delegation of Authority](https://guidebook.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Delegation%20of%20Authority%20-%20ConstructConnect#2-11), changes or additions to the current Product Catalog and pricing calculator shall be vetted and approved by Controllership prior to implementation. This includes a current product being sold by a new segment.

All requests are to be entered as a [Jira ticket](jira.constructconnect.atlassian.net). A memo is required within the ticket that outlines:

- What is the product?  
- What will it be utilized for?  
- Who will be utilizing it, or whom should be blocked from      utilizing it?
- Is it a stand-alone or an add-on feature?  
- Is it a one-time or recurring charge?  
- Is it renewable or not?  
- What is the requested list price?  
- What uplift percentage, if any, should be included in renewal?  
- Is it quantity based, volume pricing, or flat fee?  
- What rate plans should be available?  
- Is it provisioned immediately or is there a delay for fulfillment?  
- Confirm ownership and method of provisioning.  
- Are any changes needed in our terms of service?  
- Provide an example of how the product is sold and utilized if possible.  

Upon receipt, Back Office will work with Accounting to arrange a 30 minute vetting meeting with the necessary parties. This time will serve as a review, Q&A, and will commence with appropriate approval or denial of the product and pricing structure via Controllership. Accounting will decide the appropriate accounting and tax codes, as well as the revenue recognition schedule.
