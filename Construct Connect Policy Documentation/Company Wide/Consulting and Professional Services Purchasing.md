title: Consulting and Professional Services Purchasing  
author: Dave Storer - Controller  
published: 2019-08-16  
policy level: Very Important  
approver(s): Bob Ven, CTO, Buck Brody EVP Finance  
applicable locations: All locations, all teams  
effective date: 2019-08-15

---



It is the responsibility of the requestor to complete the proper forms and obtain the appropriate approvals.  

All purchases made under the `Microsoft Dynamics 365` Item ID category: `Consulting and Professional Services`, AND that

1. Directly interacts with one of our customer-facing products regardless of price
1. Directly interacts with any internal systems managed by IT 
1. Are for services not rendered in the ordinary course of business

A memo is required that outlines:

- The business issue
- How this solution solves that issue
- Reason for selection of this vendor
- List of existing systems impacted
- Project and program management of the vendor relationship
- Estimated timeline for SOW or vendor relationship
- Completed Vendor Privacy and Security Checklist [See Below](#vendor-privacy-and-security-checklist)
- If a new vendor, complete the Extended Vendor Setup Checklist [See Below](#extended-vendor-setup-checklist)
- Vendors vetting procedures and/or qualification of its consultant
- Cost and Payment terms - How much, how often (annual, monthly, quarterly) and terms (Net 30, Net 60)
- Contract start and end dates and
- Cancellation requirements
- Is this budgeted

This memo will be reviewed and approved by the following:

- <Buck.Brody@ConstructConnect.com>, <Dana.Oney@ConstructConnect.com> and, if over $100k, <Bob.Ven@ConstructConnect.com>
- Security review by CISO
    - email to <Suits@versprite.com> with cc to Tony UV <mando@versprite.com>
- Architecture Review Board for purchases that interact with customer-facing products
- <jeff.cryder@constructconnect.com>
- CC <Dave.storer@constructconnect.com> and <eric.bodge@constructconnect.com>

The memo should simply go in the body of the email request. There is no need to create a separate word doc.

The preceding memo should be updated for relevant changes each time there is a new SOW, increase of cost of 10% over the initial requisition or other material changes to terms and conditions.

All purchases must be supported by a `Microsoft Dynamics 365` requisition before purchase even if payment method is a credit card. All purchases made before August 15, 2019 will not be subject to new purchase requirements, however renewal requirements would apply.  

## Additional Documentation 

### Extended Vendor Setup Checklist  

- US based legal entity?  
- Written code of ethics?  
- ISO or equivalent certifications?  
- Background checks on employees?  
- Will sub-contractors be used? If so, describe vetting process?  
- Are there any other relevant certifications?  


### Vendor Privacy and Security Checklist  

- Incident response plan?  
- Business continuity plan?  
- Employee cyber and privacy awareness training?  
- Do they have a CISO or IT Security leader?  
- Do they have IT Security personnel or a managed service?  
- Do they have a Data Protection Officer or similar role?  
- Do they use managed services to conduct incident response activities?  
- Does the application support MFA for administrators or native accounts?  
- Do their systems require native identities?  
- Do the app use LDAP integration?  
- Does the app support Single Sign-On?  
- What are the supported methods of authentication?  
- Does the app use Data encryption and what type of encryption?  
- What security method is used during data transfers?  
- Where will the data be geographically located?  
- What approach is used to isolate multiple customers?  
- What are the data retention policies?  
- Is our data exposed in non-production environments? How is it protected?  
- What is the type of data center they use?  
- Does the data center have physical security?  
- Where is the data center located?  
- Who is the data center provider?  
- Are IPS/IDS systems in place?  
- Is there DDoS protection in place?  
- Is there WAF protection in place?  
- Do they conduct regular external vulnerability and Pen-Tests? Who does it? and how often?  
- How often do they perform internal vulnerability assessments?  
- How often do they perform a comprehensive IT Security audit?  
- Do they have a Security Operations Center? How is this staffed?  
- Do they have Managed detection and protection services?  
- Do they conduct Security audits? And how often?  
- Are there privacy policies in place?  
- Does a privacy program CPO or similar role exists?  
- Is the service GDPR compliant?  
- Does it meet Datashield guidelines?  
- Does it meet Truste guidelines?  
- Audits and certifications  
- Are they SOC compliant?    
- Are they ISO 27001 compliant?    
- Are they NIST CSF compliant?  
- Are they GDPR compliant?  
- What is the security model for the APIs?  
- What methods of integration are available?  
- What method of security is used?  
- What method of encryption is used?  
- What are the RTO and RPO for their service?  
- What are the SLAs for their solution?  
- What are the support SLAs?  
- Incident resolution and customer notification?  
- Application description  
- Does the application federate access?  
- Who can access the application  
- Does application have audit trail capability for application access?  
- Does the service have logs to identify who has accessed data stored in databases and files?  
- For how long are the audit trail logs retained?  
- Is there a formal process in place for security training of all personnel?   
- Are security and privacy incorporated in the product development process?  
- Are code scans performed as regular part of delivery process?  
