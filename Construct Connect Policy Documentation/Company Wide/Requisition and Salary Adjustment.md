title: Requisition and Salary Adjustment    
author: Julie Storm, Chief People Officer    
published: 2019-11-26    
policy level: Very Important   
data classification: Internal  
approver(s): Senior Finance, Julie Storm, Chief People Officer  
applicable Locations: All  
effective date: 2019-11-26  
last Update: 2021-06-03    


# Hiring Requisitions  
This policy applies to all open positions. An open position is defined as

- Addition: A position that is newly created.  An addition is incremental to the existing team headcount and must be approved by Finance before an offer is made.  
- Backfill: A team member that fills an open position left by another team member who has been moved to another role, will not be returning from a leave of absence or has resigned or was terminated from the role.  An open position will only be considered an approved backfill for 90 days.  If it has not been filled within 90 days of becoming vacant, it will then be viewed as an addition to headcount.    
- Promotion – A promotion is not an “automatic” or “scheduled” change in title and salary.  A promotion is a result of a team member having  been interviewed  and selected for a new role, whether on their own team or another team or a team member who  earned the new title and salary and/or commission/incentive plan through a change in their job responsibilities which involved a larger scope of work, i.e. managing someone or changing their responsibilities altogether. A promotion is must go through a defined approval process, which includes a pay analysis, updated or new JD and final approval by the appropriate parties.   
	- If the role into which the team member is being promoted is a role that was not previously a role, it is considered an addition and must be approved by Finance.
	- If the role was vacated prior to the team member being promoted into it, it is considered a backfill for 90 days. This applies to both scenarios described above.  
- Independent Contractor – A position that will be filled by an outside agency or independent contractor for a temporary period of time.  This does not apply to the approved headcount for each team and must have prior approval of both P&C and Finance via a Position Requisition before executing an agreement. Independent contractor status will only be available if a preponderance of the following conditions are met:  
	- The nature of the work to be performed is documented in a Statement of Work with deliverables, timing, responsibilities of the parties, acceptance criteria, etc…;  
	- Contractor is able to perform work for others during the engagement;  
	- Supervision will be more oversight than supervision;  
	- Contractor has ability to secure professional liability insurance (as may be required); and  
	- Work to be performed by the Contractor is unlike work being performed by a team member.       

Open positions are generally filled with the following candidates

- New Hire: A newly hired team member that hasn’t previously been employed by the company.  A new hire may be hired to fill a newly created position (addition) or to backfill an open position. This does not include those previously employed as either employees or contractors.  
- Rehire – A former team member that is returning to fill a newly created position or to backfill an open position.  Eligibility for rehire must be determined through P&C before an offer is made  
- Role Transfer - A team member is moved to a new role either on their own team or a new team. If the role into which the team member is being moved is a role that was not previously a role, it is considered an addition and must be approved by Finance.  If the role was vacated prior to the team member being transferred into it, it is considered a backfill for 90 days. This applies to both scenarios described above. A transfer is considered to be an “automatic” or “scheduled” change in title and salary that does not require an interview and selection process and is expected by the team member once they meet the criteria for the change.  A transfer can also be a title change or change in JD that does not result in an increase in salary or substantial change in commission/incentive plan.  Transfers should not require a pay analysis or final approval by appropriate parties per the Requisition and Salary Adjustment Policy.  
- Independent Contractor – A position that will be filled by an outside agency or independent contractor for a temporary period of time.  This does not apply to the approved headcount for each team and must have prior approval of both P&C and Finance via a Position Requisition before executing an agreement 
- Full Time Temporary Employee – A position that will be filled by an individual who is not through an agency or does not meet the conditions described above for independent contractors. Full time temporary employees will remain temporary for a defined period of time.  
- Conversion of Temporary to Team Member – A position may be filled by someone from an employment agency or who has been classified as a Full Time Temporary Employee for a temporary period of time and then can be converted to a ConstructConnect team member. All temporary positions must be approved by P&C prior to signing with an agency or hiring an individual, whether as a new employee or as a returning former team member. If the team member is to be converted, this too must be approved by P&C. The position will be posted internally and the temporary person must be interviewed along with any other internal candidates. If there is no internal interest, the temporary will go through the normal interview and hiring process with P&C before being converted.    

## Requisition Completion  

Requisitions for all open positions will be completed by the hiring manager or the ELT leader using the [Requisition Form](https://theloop.constructconnect.com/Interact/Pages/Content/Document.aspx?id=4579&SearchId=0) found on the Loop  

- The requisition must be completely filled out, and must indicate the salary range for the position as well as the open position category  (addition, backfill, promotion, contractor). In addition, indicate whether the position is included in the most recent forecast. In the situation where a team member is backfilling a team member that has moved to another team, the manager requesting the backfill must identify the team member that is being backfilled, the date that the position became vacant, and the previous team member's salary.    
- If it is known at the time the requisition is completed, it should also be noted as to the nature of the person who will fill that position (new hire, rehire, or existing team member).    

Once completed, the requisition goes to Talent Acquisition, who will validate the requested salary range with the Director, P&C via PayFactors. If the salary range is in question, Talent Acquisition will have a discussion with the hiring manager to reach an agreeable range. Talent Acquisition then sends an e-mail to the EVP Finance and CEO with the ELT leader cc’d to approve the request. Requisitions will not be posted until all pertinent approvals are obtained. Once obtained, the requisition will be posted within 24 hours.  

If a requisition needs more clarification, the CEO or EVP Finance will reach out to all included on the approval e-mail. If the requisition is not approved, this will also be communicated by both the CEO and EVP, Finance via e-mail to those on the approval e-mail.  

A previously approved requisition may be cancelled by either the CEO, EVP Finance, applicable ELT Leader or VP Level Leader or the hiring manager **at any time**. In the event this occurs, all relevant parties will be notified via email by the person cancelling in a timely manner.  

### Independent Contractors and Temps  

A requisition will be completed and the approval process will consist of the following  

- Independent Contractors
	- A proposed agreement with the independent contractor or agency from which the contractor is being provided should be included with the requisition and should include the proposed timeframe and cost structure. 
	- Once P&C receives the request, the requisition and proposal will be sent to Finance for approval. Finance will then contact the manager completing the request for any further information needed, recommend policy limits on any professional liability insurance required and will either approve or disapprove the request. Finance will then notify P&C and the manager of the decision. 
	- Once the independent contractor is approved if through an agency, Jeff Cryder will provide the final executed document before the contractor begins. If the person is an independent contractor without agency and the situation meets the preponderance of the conditions outlined above, the contractor must complete an agreement created by Jeff Cryder before work begins. P&C and Payroll will be copied on this agreement and will execute accordingly. Independent Contractors should be paid by A/P with 1099 reporting.  If a preponderance of the independent contractor conditions are not met and there is no agency involved, then the independent contractor will be treated as a full-time temporary employee and P&C will issue an Offer Letter specific to these situations. 
	- Independent Contractors are not onboarded in the same manner as regular full-time team members. Please consult P&C for details on this topic. 
	- Full Time Temporary Employees – Once the requisition is approved, P&C will create an offer letter for approval.  Once approved, the offer letter process will continue as outlined below. Full-time temporary employees are eligible to consider Medical and Pharmacy benefits and may be onboarded as a regular full time employee.   
- Temporary Help
	- Once the requisition is received, P&C will contact the manager to get further details on the request and will contact the agency to secure the person. P&C will also sign the contract with the agency. Once completed, the manager will be notified and the temp may begin. In the case of filling a position with a temp conversion, the normal approval process will be followed.  

Reference [Selection and Hiring of Candidates](http://localhost:5000/pages%2Fpolicy%2FConstruct%20Connect%20Policy%20Documentation%2FCompany%20Wide%2F2019%20ConstructConnect%20Employment%20Handbook#selection-and-hiring-of-candidates) in the Employee Handbook.

# Salary Adjustments

All requests for salary adjustments (i.e. not part of the annual salary increase process) must follow the guidelines outlined below. All salary adjustment requests must also align with the most recent financial forecast. [See Headcount Forecasting Approval Policy](/pages/finance/Finance/FP&A%20Processes/Headcount%20Approval%20Process)

This includes market adjustments, off-cycle increases, transitions to new roles and promotions.  

Reference [Salary Increases](http://localhost:5000/pages%2Fpolicy%2FConstruct%20Connect%20Policy%20Documentation%2FCompany%20Wide%2F2019%20ConstructConnect%20Employment%20Handbook#salary-increases) in the Employee Handbook.  

## In Cycle Adjustments (preferred approach / timing)

The process begins by receiving team-specific pay adjustment workbooks from Finance in mid-February of each year. Team members with a last pay adjustment or hire date after October 1st of the prior year will not be eligible for a current year In Cycle adjustment. The workbooks will include an allowance amount for each senior leader to allocate across their respective team members. By March end, the returned workbooks will be reviewed and validated to be within the assigned allowance. Pay adjustments resulting in a team exceeding their assigned allowance will need to apply the Off Cycle approval process described below to the team member(s) receiving the largest adjustment. This includes market adjustments, role transitions or promotions. A promotion is defined as a change in the duties and/or level of responsibility resulting in a new job description and job title. A job title change does not qualify as a promotion.  

All In Cycle Adjustments will be posted to Ultipro in early April with an effective date of April 1st to be included in the April 15th pay period.

## Off Cycle Adjustments (only to be used in exceptional business cases)

Off-cycle adjustments refer to pay adjustments not otherwise covered by the normal in-cycle process and timing. Generally, this Off Cycle Adjustment section applies to all promotions, market adjustments and role transitions during the period April 1 thru December 31 of each year.  As noted above, this section also applies to all promotions and role transitions regardless of time-of-year.  A promotion is defined as a change in the duties and/or level of responsibility resulting in a new job description and job title.  Simply changing a job title does not qualify as a promotion.  

The process begins with the team member’s manager submitting a request by completing the Status Change form found on the Loop. The Director, P&C will validate the change in salary request through the use of Payfactors(1) and/or internal equity analysis. If the salary range is in question, Talent Acquisition will have a discussion with the hiring manager to reach an agreeable range. Once a range is agreed upon, the form will then be sent to the ELT Leader and Jeff Cryder. The hiring manager is copied. During the final approval process, additional information may be requested. Once final approvals are received by Talent Acquisition, they will notify the hiring manager to proceed with the verbal offer. Talent Acquisition is to be notified when this has been completed. If the request is made during the normal in-cycle process, the salary change effective date will be the first day of the pay period in which final approval is received. 

Salary adjustments related to role transitions or promotions will require a new offer letter. Talent Acquisition will create the offer letter once the hiring manager confirms a verbal offer has been extended to the affected team member. The offer letter will be sent via DocuSign by Talent Acquisition to the team member and hiring manager.

Please Note: The entire salary adjustment approval cycle must be completed before the hiring manager is authorized to inform the affected team member of any potential adjustment.  In keeping with our value of respect, there is to be no expectation set or promise made regarding the approval of the promotion, role transition or market adjustment to the team member prior to or during the approval cycle.

# Offer Letters

Internal offer letters will be handled as described above. External offer letters will be created by Talent Acquisition and, if financial terms (salary, bonus, relocation and/or recruiting fee) associated with the offer exceeds the range authorized by the approved requisition, an email will be sent to EVP and EVP Finance seeking approval of all deviations. To assist them in considering approval of the proposed financial terms, a copy of the signed requisition will be sent. The EVP and EVP, Finance will then either approve, deny, seek additional information or propose changes to the request via email to Talent Acquisition. Once the financial terms are approved, Talent Acquisition will send the offer letter to the candidate and copy the hiring manager. Talent Acquisition will liaise with the candidate to arrive at agreed-upon terms within the approved salary range. Talent Acquisition will coordinate with the hiring manager and the EVP and EVP, Finance if additional approvals are required to successfully complete the hire. Further, CEO approval will be required for all external offer letters to successful candidates who are also former team members where prior employment with the Company termed within the prior 24 months.

For external offer letters with financial terms within the ranges of the approved requisition, Talent Acquisition does not need to obtain approval by the Executive Vice President.  

**Under no circumstances, for any reason, and regardless of any “verbal approvals” by any member of the ELT will an offer letter be sent by Talent Acquisition to a candidate.**

(1) PayFactors is a third-party application used by ConstructConnect to validate salary & other pay levels. Use of Payfactors is guided by the Company’s practice to benchmark.  
