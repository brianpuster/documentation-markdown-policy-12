title: Software Purchasing  
author: Dave Storer - Controller  
published: 2019-08-08  
policy level: Very Important  
approver(s): Bob Ven, CTO, Buck Brody EVP Finance 
applicable locations: All locations, all teams 
effective date: 2019-08-15   

---

 

It is the responsibility of the requestor to complete the proper forms and obtain the appropriate approvals.

All purchases made under the Reqlogic Item ID categories: software licenses, IT maintenance and support and custom analytics direct costs require approval by VP of IT Operations.

Additionally, for new purchases greater than $5,000 inclusive of sales tax (based on Annual Contract Value) OR that 

1. Directly interacts with one of our customer-facing products regardless of price
1. Interface or store Personally Identifiable Information (PII).  This applies to both customer-facing and internal systems (such as backoffice systems)
1. Interface or store other red data (including customer/prospect lists, trade secrets, or company confidential material). This applies to both customer-facing and internal systems (such as backoffice systems)

All contracts MUST have AccountsPayable@constructconnect.com as the billing/invoice contact email.

A memo is required that outlines:

- The business issue
- How this solution solves that issue
- Listing of competitors contacted
- Reason for selection of this vendor
- List of existing systems impacted
- Who will serve as internal system admin
- Estimated timeline for implementation
- Completed Vendor Privacy and Security Checklist [See Below](#vendor-privacy-and-security-checklist)  
- Updated Privacy Documents from CISO (including updated PII data flow diagrams and DSAR compliance)
- Completed technical feasibility checklist from EAG (for architecture and infrastructure governance)
- Cost and Payment terms - How much, how often (annual, monthly, quarterly) and terms (Net 30, Net 60)
- Date to complete the purchase
- Contract start and end date
- Cancellation requirements
- Is this budgeted

This memo will be reviewed and approved by the following:

- EVP Finance, VP of IT Operations and, if over $100k, the CTO
    - EVP Finance <buck.brody@constructconnect.com>, VP of IT Operations <dana.oney@constructconnect.com>, CTO <bob.ven@constructconnect.com>
- Security review by CISO
    - email to <suits@versprite.com> with cc to Tony UV <mando@versprite.com>
- EVP Legal
    - email to <jeff.cryder@constructconnect.com>
- CC: Controller and Director of Accounting
    - email to <dave.storer@constructconnect.com> and <eric.bodge@constructconnect.com>

The memo should simply go in the body of the email request. There is no need to create a separate word doc.

In most cases, the approvers will require all sections listed above to be included in the memo to approve the purchase. At their discretion, the approvers can waive specific sections from being included in the memo.


For renewal purchases with Annual Contract Value changes greater than 5 percent where the total purchase is greater than $5,000 OR 

1. Directly interacts with one of our customer-facing products regardless of price
1. Interface or store Personally Identifiable Information (PII).  This applies to both customer-facing and internal systems (such as backoffice systems)
1. Interface or store other red data (including customer/prospect lists, trade secrets, or company confidential material). This applies to both customer-facing and internal systems (such as backoffice systems)


A memo is required that outlines:

- Completed UPDATED vendor checklist on security and privacy
- Any key changes to Terms and Conditions

All purchases must be supported by a ReQlogic requisition before purchase even if payment method is a credit card. All purchases made before August 15, 2019 will not be subject to new purchase requirements, however renewal requirements would apply.  

## Additional Documentation 

### Vendor Privacy and Security Checklist  

- Incident response plan?  
- Business continuity plan?  
- Employee cyber and privacy awareness training?  
- Do they have a CISO or IT Security leader?  
- Do they have IT Security personnel or a managed service?  
- Do they have a Data Protection Officer or similar role?  
- Do they use managed services to conduct incident response activities?  
- Does the application support MFA for administrators or native accounts?  
- Do their systems require native identities?  
- Do the app use LDAP integration?  
- Does the app support Single Sign-On?  
- What are the supported methods of authentication?  
- Does the app use Data encryption and what type of encryption?  
- What security method is used during data transfers?  
- Where will the data be geographically located?  
- What approach is used to isolate multiple customers?  
- What are the data retention policies?  
- Is our data exposed in non-production environments? How is it protected?  
- What is the type of data center they use?  
- Does the data center have physical security?  
- Where is the data center located?  
- Who is the data center provider?  
- Are IPS/IDS systems in place?  
- Is there DDoS protection in place?  
- Is there WAF protection in place?  
- Do they conduct regular external vulnerability and Pen-Tests? Who does it? and how often?  
- how often do they perform internal vulnerability assessments?  
- How often do they perform a comprehensive IT Security audit?  
- Do they have a Security Operations Center? How is this staffed?  
- Do they have Managed detection and protection services?  
- Do they conduct Security audits? And how often?  
- Are there privacy policies in place?  
- Does a privacy program CPO or similar role exists?  
- IS the service GDPR compliant?  
- Does it meet Datashield guidelines?  
- Does it meet Truste guidelines?  
- Audits and certifications  
- Are they SOC compliant?    
- Are they ISO 27001 compliant?    
- Are they NIST CSF compliant?  
- Are they GDPR compliant?  
- What is the security model for the APIs?  
- What methods of integration are available?  
- What method of security is used?  
- What method of encryption is used?  
- What are the RTO and RPO for their service?  
- What are the SLAs for their solution?  
- What are the support SLAs?  
- Incident resolution and customer notification?  
- Application description  
- Does the application federate access?  
- Who can access the application  
- Does application have audit trail capability for application access?  
- Does the service have logs to identify who has accessed data stored in databases and files?  
- For how long are the audit trail logs retained?  
- Is there a formal process in place for security training of all personnel?   
- Are security and privacy incorporated in the product development process?  
- Are code scans performed as regular part of delivery process?  
  
### Example Memo  

- **Vendor Name:** Cloudingo

- **New Vendor?** Yes

- **Purchase Type:** Software

- **Purchase Criteria:** New Purchase

- **The Business Issue:** As part of SDP 1.0 we need to automate the
    deduping of records in Salesforce. Data duplication causes
    significant issues for email automation tools that rely on contacts
    existing in the CRM only once to ensure that we are providing the
    correct automated messaging to move them through the sales cycle.
    Additionally it is important that accounts are not duplicated for
    proper roll up reporting on customer accounts. Today this process is
    completely manual and requires significant man hours to run reports
    looking for accounts and/or contacts that should be merged into one
    record. This also created confusion for reps when engaging with
    prospects/customers and they do not see all relevant information
    because it was written to a duplicate record that they have not
    looked at.

- **How this solution solves that issue:** This solution automates the
    deduplication on records daily providing several options for
    handling based on provided rules. 1.) Automated merges without user
    interaction 2.) Automated reports on fuzzy logic matches providing
    direct access to a merge screen for the designated representatives.
    3.) Provides logic to help enforce data standards preventing end
    users from entering know bad data into the CRM to shortcut process
    4.) Additional address verification via USPS.com api making it
    easier to identify duplicates based on address verified address
    information

- **Listing of Competitors contacted:** Demand Tools - this tool is
    used today for manual deduplication

- **Reason for selection of this Vendor:** Cloudingo was reviewed by
    several members of the SDP 1.0 team and it was agreed to provide the
    needed features: Data Standards Enforcement Address Verification
    Automation of record deduping Job Reporting for oversight Fuzzy
    logic for identifying harder to find duplicates Multiple seats for
    Sales Coordinator access Cloudingo includes the ability to look at
    custom objects in Salesforce which most other tools do not allow.
    The automated reporting also enables us to distribute the deduping
    of more complicated records to sales coordinators who are better
    equipped to handle situations where records should not be merged but
    need to be identified correctly so they do not cause confusion
    (typically parent\\child situations where a separate account is
    needed). Pricing for cloudingo is based on number of records in the
    system which means used properly cost will only go up as new unique
    records are added. Cloudingo API allows external systems to dedupe
    against Salesforce and bring over Salesforce ID\'s for better data
    matching between systems. Demand Tools is an excellent solution but
    the process is manual and the licensing is per seat. Based on number
    of seats that would be required to enable Sales Coordinators the
    pricing would be similar with less ability to automate. Demand Tools
    does provide other features that admins find useful but they would
    not enable SDP 1.0.

- **List of existing systems impacted:** Salesforce

- **Who will serve as internal system administrator?** Jim McCabe

- **Estimated timeline for implementation:** Less than 30 days for
    direct automation. 60-90 days to get Sales Coordinators full
    engaged.

- **Completed Vendor Checkist on Security and Privacy:** Yes

- **Updated Privacy Documents from CISO (including updated PII data
    flow diagrams and DSAR compliance):** Please see attached zip file
    from vendor

- **Completed technical feasibility checklist from EAG (for
    architecture and infrastructure governance):** See attached - also
    have a zip file with additional technical documents for review but
    cannot attach to request. I will send to Alex Hart after submission.

- **Payment Terms:** $X0,000 Annual Net 30 They only offer annual
    payments

- **Date to complete purchase:** 12/31/2020  

- **Contract Start and End Date:** 365 days from date of close. Currently targeting start date of 1/1/2021 - 12/31/2021   

- **Cancellation Requirements:** Notification of non-renewal must be received 30-days prior to the end of the current subscription term.  

- **Is this budgeted?** No  

- **Additional Information:** None  
