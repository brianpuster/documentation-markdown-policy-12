title: 2021 ConstructConnect Employment Handbook    
published: 2021-05-25    
author: Jennifer Manguiat  
status: draft


# TEAM MEMBER EMPLOYMENT HANDBOOK ACKNOWLEDGEMENT FORM

The employment handbook describes important information about ConstructConnect.  I understand that I am responsible for reading the handbook, familiarizing myself with its contents and adhering to all of the policies and procedures of ConstructConnect whether set forth in this handbook or elsewhere.  I further understand that I may consult with People & Culture (HR) regarding any questions I may have about such policies and procedures.  

Since the information, policies and benefits described here are necessarily subject to change, I acknowledge that revisions to the handbook may occur except to the policy of employment-at-will, which may only be modified by written agreement by the Chief Executive Officer (CEO) of ConstructConnect.  All such changes will be communicated through official notices, and I understand that revised information may supersede, modify or eliminate existing policies.  

Furthermore, I acknowledge and understand that employment with ConstructConnect is not for a specified term and is at the mutual consent of the team member and the Company which is classified as “At-Will” employment.   Accordingly, either I or ConstructConnect can terminate the relationship at will, with or without cause, at any time, so long as there is not a violation of applicable federal law or state law.  

I acknowledge that this handbook is neither a contract of employment nor a legal document. I have received the handbook, and I understand that it is my responsibility to read, understand and comply with the policies contained in this handbook & state supplements and any revisions made to it.  

The [Employment Handbook](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/2021%20Employment%20Handbook#2021-constructconnect-employment-handbook-draft){:target="_blank"} & [State Supplements](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/2021%20Employment%20Handbook#separation-of-employment){:target="_blank"} can be found on the ConstructConnect Intranet.  If there are questions regarding accessing the Employment Handbook or State Supplements, contact People & Culture (HR).
ned in this handbook & state supplements and any revisions made to it.

NOTE: This document is signed electronically. Contact People & Culture with any questions.

TEAM MEMBER'S NAME (printed):

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  

TEAM MEMBER'S SIGNATURE:

*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_* 

DATE: *\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_* 



# INTRODUCTION

Team –

If you’re new to the team, we’re so glad you’ve chosen to join us. I say this often and each time I mean it more and more – it’s never been a better time to be part of ConstructConnect.    

Each of you have the opportunity to have an impact on our strategy and to serve our customers through the unique experiences and perspective you bring to our company. We’re very excited and hope you are too!  

> **To be THE ONE PLACE where PEOPLE CONNECT amd CONFIDENTLY EVALUATE, CHOOSE, and PREPARE PROJECTS BEFORE THEY BUILD**    

Of equal importance is your growth, both professional and personal, as well as your wellbeing and feeling of connection to our company.   One of the ways we prioritize this is by creating a culture focused on engagement and inclusion that’s built on our five values:  Respect, People First, Empowerment, Collaboration, Drive.   

When we each make it a priority to live out our values day-in and day-out, we ensure everyone has the opportunity to achieve their potential and find success.      

Please take time to review the Handbook in detail.  If you have any questions, reach out to our People & Culture team.  

We are better together –  

**Dave Conway**    
**President & CEO, ConstructConnect**  

## Forward

This Employment Handbook has been developed to get you acquainted with our Company and answer many of your questions.  ConstructConnect believes that it is important to keep you informed about its policies, procedures, practices and benefits, and what you can expect from the Company and the obligations you assume as a team member of the Company. These practices are designed to provide consistent and equal treatment. It is your responsibility to read, understand and comply with the policies contained in this handbook and any revisions made to it.  If you have any questions on any matter pertaining to your employment, please contact your manager or the Chief People Officer.

This Employment Handbook is intended to provide members of the Company with basic information. Your employment is at will. This means that you have the right to leave at any time for any reason or for no reason at all and the Company has the right to separate you from employment at any time for any lawful reason or no reason at all.  Only the CEO of the Company or that person’s authorized representative has the authority to enter into an employment agreement that alters the fact that employment with the Company is at-will, and any such agreement must be in writing signed by the CEO of the Company or an authorized representative.

The policies and practice statements, which are included in this Handbook, reflect a great deal of care and concern for the people who make it possible for ConstructConnect to exist…ITS TEAM MEMBERS. These policies may change with time or they may need to be clarified, amended, supplemented or revoked. The Company maintains the responsibility and the right to make these changes at any time with or without notice when this happens. All such revisions, deletions, or additions must be in writing. No oral statements or representations can change the provisions of this Employee Handbook. 

The descriptions of various fringe benefits such as group insurance are summaries only. Should the description in this Handbook differ with formal agreements or documents involved, the formal and complete plans are to be considered correct. As noted above, policies and practices are subject to change.

Please consult with your manager or People & Culture (HR) if you have questions related to any of the information supplied in this Employment Handbook. The information in this Handbook replaces any of the same previously issued information.  

## Company Values  

**Respect**
: Embracing the talent and uniqueness of each person, we practice trust, transparency and integrity in our words, actions and commitment to one another and our customers.  

**People First**
: Family, community and each individual's personal and professional well-being and growth is the foundation of our culture - profit follows.  

**Empowerment**
: Trusting in and developing the abilities and ideas of our people to create and maintain a culture in which the organization continuously operates, innovates and flourishes.  

**Collaboration**
: With humility, holding each other accountable to be passionate and engaged to be focused on our customers' success.  

**Drive**
: Fearlessly and energetically pursuing excellence and results.  

## Strategy Statement  

To invest in products, partnerships and capabilities to be THE ONE PLACE where people connect and confidently evaluate, choose, and prepare projects before they build.  

## Customer Relations Statement

Customers are among the Company’s most valuable assets.  Every team member represents the Company to our customers and the public. The way we do our jobs presents an image of our entire Company. Customers judge all of us by how they are treated during each team member interaction. Therefore, one of our first business priorities is to assist any customer or potential customer. Nothing is more important than being courteous, friendly, helpful, and prompt in the attention you give to customers.  Our personal contact with the public, our manners on the telephone, and the communications we send to customers are a reflection not only of ourselves, but also of the professionalism of the Company. Positive customer relations not only enhance the public’s perception or image of ConstructConnect, but also pay off in greater customer loyalty.  

## Internal Communications  

We value open, consistent, quality communications with our team members.  ConstructConnect’ s business is all about effective communications and we know firsthand how essential internal communication is to success.  We aim to ensure that all team members understand our values, mission and strategy, as well as our products, services and the markets in which we do business.

All team members share the responsibility of staying informed so that we, as a company, can gain a competitive advantage.  We encourage you to discuss with your managers ways to stay abreast of the rapidly changing business climate, including any resources you may need to excel in your position or to keep current on new company developments.  Team members are also encouraged to contribute news or ideas to our company Intranet.  

## Open Door Initiative

At ConstructConnect, we believe that communication is the heart of good team member relations. Team Members should share their concerns, seek information, provide input and resolve work-related issues by discussing them with their managers until they are fully resolved.

The problem-solving procedure whereby management will seek answers for questions or problems a team member may have is the following:  

1.  If any questions, concerns or difficulties arise, a team member is advised to discuss them with their immediate manager.
1.  If the issue cannot be satisfied by the manager, a team member is advised to bring it to the attention of the Team Director/Vice President.
1.  If the issue still has not been resolved or needs further clarification, the team member is advised to speak with a member of People & Culture (HR).  
1.  If the issue still has not been resolved by a member of People & Culture (HR), the team member is advised to speak with the Chief Executive Officer (CEO).  

In certain circumstances a team member may contact People & Culture (HR) and forgo Steps 1 & 2.  

Regardless of the situation, team members should be able to openly discuss any work-related problems and concerns without fear of retaliation.  

If a team member has a concern about discrimination and/or harassment, the Company has set up special procedures to report and address those issues. The proper reporting procedures are set forth in the Company's Workplace Harassment Policy.  

# EMPLOYMENT

## At-Will

Nothing contained in this Handbook or in any other materials or information distributed by the Company creates a contract of employment between an individual and ConstructConnect. Employment is on an at-will basis. This means that individuals are free to resign their employment at any time, for any or no reason, with or without notice, and ConstructConnect retains the same right. No statements to the contrary, written or oral made either before or during an individual’s employment can change this. No individual, manager or other officer can make a contrary agreement, except the Chief Executive Officer (CEO), and even then, such an agreement must be set forth in a written employment contract with the individual, signed by the Chief Executive Officer (CEO).  

## Employment Eligibility

ConstructConnect, as an Equal Opportunity Employer, is committed to employing individuals legally eligible to work in the United States without regard to citizenship, ethnic background or place of national origin.

Federal regulations require the Company to comply with the Immigration Reform and Control Act of 1986. All new team members must complete an I-9 form and provide proof of their identity and ability to work in this country. People & Culture (HR) is responsible for ensuring the completion of the I-9 form and verifying the eligibility to work in the United States. Team members will be expected to complete the I-9 form during their first three (3) days of work. 

Additionally, any team member younger than eighteen (18) years of age will be required to present proof of age and work permit as required by law.  

## Orientation Period

All new, rehired, and transferring team members work on an orientation period of 90 days following the date of hire or rehire date, and some teams may adhere to longer orientation periods.

The orientation period is intended to give new team members the opportunity to demonstrate their ability to achieve a satisfactory level of performance and to determine whether the new position meets their expectations. ConstructConnect uses this period to evaluate team members’ capabilities and overall performance.

If the Company determines that the orientation period does not allow sufficient time to thoroughly evaluate the team member’s performance, the orientation period may be extended for a specified time.  Upon successful completion of the orientation period team members will enter the “regular” employment classification.  If team members are unable to demonstrate their ability to achieve a satisfactory level of performance during their orientation period their employment may be terminated.  

During the orientation period and regular employment, team members’ status continues to be at will.  

## Team Member Classification

It is the intent of ConstructConnect to clarify the definitions of employment classifications so that team members understand their compensation and benefit eligibility. These classifications do not guarantee employment for any specified period of time.  Accordingly, the right to terminate the relationship “At-Will” at any time, for any or no reason or cause with or without notice is retained and granted to both the team member and the Company.

Each team member is designated as either “exempt”, “salary non-exempt” or “hourly,” consistent with federal and state wage and hour laws. “Exempt team members” are not subject to the minimum wage and overtime requirements of state and federal law. “Salary non-exempt team members” and “hourly team members” are subject to the state and federal wage and hour laws and are paid at least the minimum wage and one and one-half times their hourly rate for all work in excess of forty (40) hours in a work week or as otherwise required by applicable state law.

In addition to the above categories, each team member will belong to one category:  

**Temporary/Contractors Team Members**
: are hired for a limited period of time and are ineligible to participate in the company's benefit program, training programs, and events.  

**Interns/Co-ops**
: are enrolled in a school/academic institution with a minimum of a part-time status and are participating in the company's college program in an effort to gain experience in their specific field of interest.  

**Full-Time Regular Team Members**
: are not hired for a specified limited period of time and who are regularly scheduled to work a minimum of thirty (30) hours (not including unpaid meal time) per week. Generally, they are eligible for the company's benefit package and subject to the terms, conditions and limitations of each benefit program.  

**Part-Time Team Members**
: work less than thirty (30) hours (not including unpaid meal time) per week. While they may receive all legally mandated benefits (such as social security and worker's compensation insurance) they are ineligible for all other Company benefit programs.  

## Selection and Hiring of Candidates

**Job Postings** – Most job opportunities will be posted internally to allow qualified internal applicants the opportunity to express interest and apply. However, promotions can be made within teams that do not require the position to be posted. At the discretion of the Talent Acquisition Team, open opportunities may be posted externally simultaneously. Opportunities will remain posted internally and externally until the position is filled or at the discretion of People & Culture (HR).  
 
**Internal Opportunities** – Team members may apply for or be promoted to career opportunities within their current team after six (6) months in their current role and outside of their current team after one (1) year.  Any exception to the tenure requirement needs to be approved by the Chief People Officer.  Team members must have a satisfactory performance record, and have not been issued any improvement plans in the previous six-month period to be eligible for internal career opportunities.   

Team members must complete the “Internal Application” in its entirety and return it to People & Culture (HR). All applicants will be considered on the basis of their qualifications as they relate to the job description and performance in their current position. Only internal candidates who meet the qualifications and are in good performance standing will be interviewed. Candidates who are interviewed but not selected for a transfer will be provided feedback by People & Culture (HR) regarding their interview.  

## Team Member Referral Program

Our team members are a critical element to our ongoing recruiting success through our Team Member Referral Program.  We value candidate referrals and reward our team members for helping the Company find the right talent for our business needs.   

### Referral Eligibility

- Active, regular, full-time team members are eligible to participate in the program.    
- Members of the management team and People & Culture (HR) are not eligible for referral rewards.   
- Referral of any applicant who has previously worked for the Company (including per diem) will not qualify for a reward.    
- Positions such as internships, temporary positions or summer positions are not eligible for rewards.    

### Referral Award

- The award is $500 for team members who refer a qualified candidate, who is hired into a full-time position with the Company.    
- The candidate must indicate on their application that they were referred by a team member and the team member must complete and return the Team Member Referral form to the Talent Acquisition team.     
- The candidate must be actively employed with the Company for the award to be paid.  The award will be paid once the candidate successfully completes 90 days of continuous employment.   
- For the award to be paid, the referring team member must be an active team member of the Company at the time of hire and 90-day anniversary of the candidate.    

The People & Culture (HR) Team will make the final decision in determining eligibility for a referral reward.  

## Employment of Relatives

ConstructConnect does not permit members of the same family to work at the Company. The Company has the right and will determine in all cases if a familial relationship exists. Relatives are defined as: parent, spouse, child, sibling, grandparent, grandchild, aunt, uncle, cousin, in-law or step relative, or any person with whom the team member has a close personal relationship such as a domestic partner, romantic partner or cohabitant. Team members who enter into a romantic relationship with another team member should disclose it to their manager and it will be reviewed accordingly. See the Relationships in the Workplace policy for additional information.  

## Request to Work from Another State  

On occasion, a team member may, at their own request, want to relocate to another city/state other than their current Company home state location.  These requests will be reviewed and approved in accordance with the policy guidelines.  The Company reserves the right to reject relocation requests based on the needs of the business.  Any team member who would like more information should contact their manager.    

## Reinstatement After Break In Service

On occasion, team members are rehired who have previously been employed by ConstructConnect. When a team member departs from the company, his/her manager will indicate the team member’s rehire eligibility based on their rehire status indicated in the payroll system. 

Team members re-hired one (1) year or less than after separation will be restored to their original hire date.  All applicants are required to complete the interview process as outlined in the interview and selection policy. With regards to eligibility for all other benefits, the plan documents for each benefit will govern whether previous service can be used to establish eligibility.  

## Team Member Records

ConstructConnect is required to keep accurate, up-to-date employment records, to include electronic records, on all team members to ensure compliance with state and federal regulations and to keep benefits information current and to make certain that important mailings reach all team members. The Company considers the information in employment records to be confidential.

The Company maintains employment records on each team member. The personnel file includes such information as the team member’s job application, resume, records of training, documentation of performance appraisals and salary increases, and other employment records. Current team members will be permitted to review their personnel files by contacting People & Culture (HR). With reasonable notice the files may be reviewed in People & Culture (HR) in the presence of a member of People & Culture (HR). A team member may also make a request to have a copy of a document from their personnel file.  

## Changes in Personal/Employment Status

Team members must inform the Company of any necessary updates to their personnel records. Information such as change of address, changed telephone numbers, emergency contact, marital status, number of dependents or names of covered beneficiaries and training/education should be changed in the HRIS system.  

## Payroll Policies

### Deductions

The Company adheres to a policy of strict compliance with the Fair Labor Standards Act. The Company will make deductions from the wages of its team members only as permitted by law. Among these are applicable federal, social security, Medicare, state and local income taxes, child support and garnishments. Making improper deductions from the wages of team members is strictly prohibited.  

Despite our best efforts to prevent improper deductions, it is possible that mistakes may be made.  

If you have questions about deductions from your pay, if you believe you have been subject to any improper deductions, or if your pay does not accurately reflect your hours worked, please contact the Payroll Manager.  

If you have not received a satisfactory response within five business days after reporting an incident, please immediately contact the Chief People Officer.  

Every report will be fully investigated and if the Company determines that a deduction was taken improperly, the Company will reimburse the affected team member for the improper deduction.  

The Company will not allow any form of retaliation against individuals who make good faith reports of alleged violations of this policy, or who cooperate in the Company’s investigation of such reports, even if the reports do not reveal any errors or wrongdoing. Retaliation is unacceptable, and any form of retaliation in violation of this policy will result in disciplinary action, up to and including discharge. If the Company concludes that a team member has violated this Policy, corrective action will be taken, where appropriate, up to and including discharge.  

*Note to Exempt Team members*

Team members who are classified as exempt must record absences from work for reasons such as leaves of absence, sick leave or vacation. As a general matter, exempt team members are not subject to partial-day deductions from salary.  However, exempt team members should be aware that deductions from salary may occur under certain circumstances, including but not limited to the following:  

- Absences from work for one or more full days for personal reasons other than sickness or disability.    
- Absences from work for one or more full days due to sickness or disability.  
- Offsets to amounts received by team members for jury duty, attendance as a witness, or temporary military leave;  
- Penalties imposed in good faith for infractions of safety rules of major significance;  
- Unpaid disciplinary suspensions of one or more full days imposed in good faith for infractions of workplace conduct rules;  
- Proportionate deductions from an exempt team member’s initial or final week of employment in order to pay the team member only for the time actually worked during those weeks; and  
- Unpaid leave under the Family and Medical Leave Act.     

The Company may require an exempt team member to use available vacation or sick time, as a replacement for salary, when the team member takes less than a full-day off from work.  

An exempt team member’s salary will not be reduced when the team member works part of a week and misses part of a week due to service as a juror, witness or in the military or for lack of work, though deductions may be made to offset amounts an team member receives as jury or witness fees, or for military pay.  The Company may also make lawful deductions from an team member’s salary for penalties imposed in good faith for infractions of safety rules of major significance.  

It is company policy to comply with the salary basis requirements of the Fair Labor Standards Act (FLSA) and applicable state law. The Company prohibits any deductions from pay that violate the FLSA or applicable state law.  

If an exempt team member believes that an improper deduction has been made to their salary, the team member should immediately report this information to Human Resources. Reports of improper deductions will be promptly investigated. If it is determined that an improper deduction has occurred, the team member will be promptly reimbursed for any improper deduction made.  

### Payroll Frequency

Team members are paid by direct deposit semi-monthly.  Scheduled hours worked through the pay date will be included on the payroll check.  In the event that a regularly scheduled payday falls on a day off such as a holiday, team members will be paid on the work day prior to the regularly scheduled payday.  

### Direct Deposit

For the sake of convenience and efficiency for both the team member and the Company, team members will have their payroll checks processed through electronic direct deposit or a payroll card in accordance with applicable law.  Not reporting bank account changes or closings could result in a delay in receiving a team member’s direct deposit.  

## Standards of Conduct  

As an integral member of the Company, you are expected to accept certain responsibilities, adhere to acceptable business practices and exhibit a high degree of personal integrity at all times. This involves respecting the rights and feelings of others and refraining from any behavior that might be harmful to you, your coworkers or the Company. You are encouraged to observe the highest standards of professionalism at all times.   

Since it is impossible to list in this Handbook every situation inconsistent with these principles, the absence of an illustration from this handbook will not prohibit the Company from taking action.   

Issues related to sub-standard performance, attendance and conduct will be handled through the use of an Improvement Agreement, which is a tool designed for the manager and team member to identify the cause of the issue and create an action plan to improve/mitigate the issue.  If a satisfactory improvement cannot be obtained and sustained, further action, up to and including immediate termination can occur when the Company believes, in its sole discretion, such action is warranted.  Note: Some teams have team specific attendance policies and performance metrics that each team member is expected to adhere to.    

The standard progression for an Improvement Plan is:  

- Stage 1 Performance Improvement Plan  
- Stage 2 Performance Improvement Plan  
- Termination  

Note: Performance improvement policies may vary by team. Please ensure you are aware of your team’s policy.  

The Company reserves the right to alter this progression on a case-by-case basis, taking all facts into consideration, including the severity and impact of the infraction.  

# BENEFITS  

Eligible team members at ConstructConnect are provided a range of benefits.  A number of programs, such as social security, worker’s compensation and unemployment insurance, cover all team members in the manner prescribed by law.  A team member’s eligibility for other benefits is dependent upon a variety of factors, including team member classification.  This handbook provides a summary of team member benefits.

A change in employment status that would result in loss of eligibility to participate in the health, dental or vision insurance plan may qualify a team member for benefits continuation under the Consolidated Omnibus Budget Reconciliation Act (COBRA). 

During the annual open enrollment period, all benefit eligible team members may change insurance coverage options and level of coverage.  Except under certain circumstances, once you have elected an insurance option and level of coverage, your election cannot be changed for the remainder of the year.  

Team members are subject to all terms and conditions of the agreements between the Company and the insurance carriers. More detailed information about the Company benefits can be found in the summary plan description provided by the insurance carriers.   

## Paid Time Off (PTO)  

**NOTE TO TEAM MEMBERS:  Where applicable state laws or local ordinances (including Paid Sick Leave laws or ordinances) provide greater benefits than those described in this policy, the Company will comply with the state law/local ordinance.**  

Paid Time Off (PTO) is an all-purpose time off policy for eligible team members to use for vacation, illness or injury and personal business. Team members are accountable and responsible for managing their own PTO hours to allow for adequate reserves should the need arise. For your convenience, your PTO balance will show on the People & Culture (HR) Information System.   

Annual PTO entitlement is based on your length of service. Each year, a regular full-time team member working 30 or more hours may take PTO depending upon his or her length of service, according to the PTO accrual table. Part-time, temporary, contract and co-op positions are not eligible for PTO.  Length of service accrual increases will be done in the pay period of your service anniversary.    

Our PTO year runs concurrently with the calendar year, from January 1 to December 31. We urge you to take all of your PTO time during each calendar year.  

The amount of PTO days accrued per year increases with the length of employment as follows:  

|Length of Service|PTO Days in the Calendar Year|
|---|---|
|1 - 4 years|17 PTO days/136 hours|
|5 - 9 years|22 PTO days/176 hours|
|10 + years|27 PTO days/216 hours|  

---
**Accruals**  
: Are based upon hours up to 2080 hours per year, excluding overtime. The accruals will be awarded on the pay date for each pay period. *No PTO hours will accrue beyond the maximum accruals listed.  
---  

|# of PTO days per year|Avg \# of hours worked per day|Semi-monthly accrual in HOURS|  
|---|---|---|
|17|8|5.67 hrs (max 136 per yr)|
|22|8|7.33 hrs (max 176 per yr)|
|27|8|9.00 hrs (max 216 per yr)|  


PTO may be taken in no less than 1 hour increments.  


**Carry-over PTO**  
: Unused PTO days may be carried over year to year up to a maximum of 40 hours or as otherwise required by applicable state law.  Carry-over PTO must be used by June 30 of the following year or will be forfeited, except where prohibited by applicable state law.


**Borrowing PTO**    
Until September 15th of each calendar year a team member may borrow up to 5 days of PTO. Beginning September 16th team members may only borrow what they will accrue for the remainder of the calendar year.  

Unpaid time off will only be granted in conjunction with an approved leave.  Absences not covered under an approved leave following the exhaustion of available PTO time will result in disciplinary action.

PTO time must be scheduled in advance and approved by your manager. Your manager can deny a PTO request at his or her discretion, including when business necessitates that you be available. Team members who are sick must notify their manager before the scheduled start of the workday.

New hires are not eligible to take PTO until the pay period following completion of 90 days of employment.

Temps or contract positions converting to full-time positions do not have to meet the 90 day waiting period to begin accruing and using PTO hours, provided they have already completed 90 days as a temp or contractor.

Rehired team members must meet the 90-day wait period if the break in service is more than 90 days.

PTO is paid at the team member’s straight time rate.  It does not include overtime or any special forms of compensation such as incentives, commissions and bonuses.  

### PTO Entitlement for New Hires

In the first calendar year of employment, you will accrue PTO time based on hire date. The accrual is based on whether you are hired between the first and the fifteenth of the month or between the sixteenth and the end of the month, as follows:  

|Month of Hire|Hired Between 1<sup>st</sup> and 15<sup>th</sup>|Hired Between 16<sup>th</sup> and end of Month|
|--|--|--|
|January|136 hours|130.34 hours|
|February|124.67 hours|119.01 hours|
|March|113.33 hours|107.67 hours|
|April|102 hours|96.34 hours|
|May|90.67 hours|85.01 hours|
|June|79.33 hours|73.67 hours|
|July|68 hours|62.34 hours|
|August|56.67 hours|51.01 hours| 
|September|45.33 hours|39.67 hours|
|October|34 hours|28.34 hours|
|November|22.67 hours|17.01 hours|
|December|11.33 hours|5.67 hours|  

### PTO Entitlement During a Leave  

During a leave of absence you do not accrue PTO time.  Accrued PTO time will be applied to any leaves and will run con-currently.  After the exhaustion of such PTO, the remaining leave will be unpaid.  

### PTO Termination of Employment

Upon voluntary termination of employment, a team member must give a 2 week notice in order to receive payment for any accrued, unused PTO time through the last day of employment where permitted by applicable law. No PTO may be used during the team member’s resignation period. PTO payouts will be paid on the next regularly scheduled pay period.  

If a team member is involuntarily terminated because violation of the Company policies or of non-compliance with any local, state or federal law, PTO time will not be paid out.  

Where required by applicable law, PTO will always be paid upon termination.    

Any PTO time taken that was not earned is treated as a salary advance owed to the Company by you and will be deducted from your last paycheck, as permitted under applicable law.  

## PTO Donation 

The Company recognizes team members may have a family emergency or a personal crisis that causes a severe impact to them resulting in a need for additional time off in excess of their available PTO time.   To address this need all eligible team members will be allowed to donate PTO time from their unused accrued balance to co-workers in need in accordance with the policy.  The donation made will go to a general pool and cannot be specified for a particular team member.  Federal and State regulations will govern this process in terms of amount and type that can be donated.  This policy is strictly voluntary.

Contact People & Culture (HR) for more details on the policy and donation approval process.  

## Holidays 

ConstructConnect recognizes 10 paid holidays throughout the year:

|CORE HOLIDAYS|DAY RECOGNIZED|  
|--|--|  
|New Year's Day|January 1<sup>st</sup>|
|Memorial Day|Last Monday in May|
|Independence Day|July 4<sup>th</sup>|
|Labor Day|First Monday in September|
|Thanksgiving|Fourth Thursday in November|
|Day After Thanksgiving| Fourth Friday in November|
|Christmas Eve|December 24<sup>th</sup>|
|Christmas Day|December 25<sup>th</sup>|
|Floating Holidays|2 days per year|  

*Team Members hired between October 1st and November 15th will receive one (1) Floating Holiday to use in the first calendar year.   Team Members hired between November 16th and December 31st will not receive Floating Holidays their first calendar year.* 

For team members who are scheduled to work at least 30 but fewer than 40 hours per week, holiday pay will be calculated by multiplying the team member’s base hourly rate by the number of hours per day the team member is normally scheduled to work.  

Dependent upon the calendar, some holidays are observed on their actual day, and others may be assigned to a different date to take advantage of long weekends.  A recognized holiday that falls on a Saturday will be observed on the preceding Friday. A recognized holiday that falls on a Sunday will be observed on the following Monday. 

If a recognized holiday falls during an eligible team member’s paid absence such as vacation, holiday pay will be provided instead of the PTO benefit that would otherwise have applied. 

The day before a holiday is considered a normal work day with standard work hours.  PTO must be utilized for any hours not worked.

If a recognized holiday falls during an eligible team member’s leave or suspension such as FMLA, military, or personal leave, holiday pay will not be provided.

Salary non-exempt and hourly team members are required to work a complete day on the last scheduled day before the holiday, and on the first scheduled day of work after the holiday to be paid for that holiday.  Scheduled PTO or excused absences are exceptions to this requirement. 

Paid-time off for holidays will not be counted as hours worked for the purposes of determining whether overtime pay is owed.  

Floating holidays can only be used in full one (1) day increments and are available beginning on your first day of employment.  Except where prohibited by applicable state law, unused floating holidays will be forfeited December 31st of each year.  


## Community Outreach Volunteer Benefit

For team members who are scheduled to work at least 30 but fewer than 40 hours per week, holiday pay will be calculated by multiplying the team member’s base hourly rate by the number of hours per day the team member is normally scheduled to work.  

Dependent upon the calendar, some holidays are observed on their actual day, and others may be assigned to a different date to take advantage of long weekends. A recognized holiday that falls on a Saturday will be observed on the preceding Friday. A recognized holiday that falls on a Sunday will be observed on the following Monday. 

If a recognized holiday falls during an eligible team member’s paid absence such as vacation, holiday pay will be provided instead of the PTO benefit that would otherwise have applied. 

The day before a holiday is considered a normal work day with standard work hours. PTO must be utilized for any hours not worked.

If a recognized holiday falls during an eligible team member’s leave or suspension such as FMLA, military, or personal leave, holiday pay will not be provided.

Salary non-exempt and hourly team members are required to work a complete day on the last scheduled day before the holiday, and on the first scheduled day of work after the holiday to be paid for that holiday.  Scheduled PTO or excused absences are exceptions to this requirement. 

Paid-time off for holidays will not be counted as hours worked for the purposes of determining whether overtime pay is owed.  

Floating holidays can only be used in full one (1) day increments and are available beginning on your first day of employment. Except where prohibited by applicable state law, unused floating holidays will be forfeited December 31st of each year.    

## EAP Work/Life Assistance Program

Today’s world is challenging as we strive to balance the demands of work, family and our personal needs.  We recognize the impact of daily pressures on an individual’s performance -both personally and professionally.  Our  LifeWorks Program (EAP) is a confidential program that allows team members to access EAP free of charge.  

Contact People & Culture (HR) for additional information on our EAP program.


## Local Activity Discounts

Local activity discounts offer team members the opportunity to participate in a broad range of leisure activities.  Team members can enjoy discounts and reduced admissions at major amusement centers such as major Theme Parks.  Other discounts may include admissions to selected movie theaters, zoos and museums.  

These programs vary from location to location.  Visit the ConstructConnect Intranet for a list of discounted activities available at your location.  


## Worker's Compensation 

ConstructConnect provides a worker’s compensation program for team members. This program covers most injuries or illnesses sustained in the course and zone of employment that require medical, surgical or hospital treatment. Subject to applicable legal requirements, worker’s compensation insurance provides benefits after a short waiting period or, if the team member is hospitalized, immediately.

Injuries that occur on the job should be immediately reported to the team member’s manager and a member of People & Culture (HR). Off-site team members should report an accident to a member of People & Culture (HR) immediately.  If an accident occurs, team members who witness the accident should contact the appropriate medical personnel and should not attempt to transport or make medical decisions on their own.  An Accident Report then must be completed within 24 hours of the accident.

A team member who requires medical attention at a medical facility because of a worker’s compensation injury may be required to take an alcohol/drug screen test at the facility.  A team member that tests positive or refuses to submit to chemical testing may be disqualified for compensation and benefits under the Worker’s Compensation Act.

Team members who need to take time off from work due to a workers’ compensation illness or injury may also be eligible for a leave of absence under the Company’s leaves of absence or reasonable accommodation policies. Team members should consult with People & Culture (HR) for additional information.

Team members who are ready to return to work following a workers’ compensation-related leave of absence must supply a certification from a health care provider confirming the team member’s ability to return to work.  


## Time Off to Vote 

ConstructConnect encourages its team members to participate in the election of government leaders. Therefore, if a team member does not have sufficient time outside their regular work hours to vote, a team member may schedule PTO to take time off at the beginning or end of the workday to exercise this right as per the Company’s PTO policy.  Where state law provides for time off to vote, ConstructConnect will comply with such state law.  

# COMPENSATION

## Timekeeping

All salaried non-exempt and hourly team members must accurately and consistently record their time worked through the use of the Company’s electronic timekeeping system.  Exempt team members are not required to record hours worked and are expected to work the number of hours required to complete their assigned tasks. 

The electronic timekeeping system is used to compute earnings and is kept as a permanent record.  Each non-exempt team member is responsible for accurately reporting the time they began and ended their work, as well as the beginning and ending time of each meal period. They should also record the beginning and ending time of any split shift or departure from work for personal reasons. 

It is mandatory that all clock-in and clock-out activity of a team member’s time be performed at an authorized computer ONLY.  Logging in from any other access point may result in the termination of employment.

Altering, falsifying, tampering with time records, working off the clock, or recording time on another team member’s time record are one of the most serious violations of the Company’s rules and as a result may lead to immediate termination. 
 
When a team member clocks in they are working on the job performing assigned duties.  This may be different from the time they arrived or left the workplace.  All overtime must be scheduled and approved in advance by their manager.  (See Overtime below.)

It is a violation of the Company’s policy for anyone to instruct or encourage another team member to work “off the clock,” to incorrectly report hours worked, or to alter another team member’s time records. If any team member is directed or encouraged to incorrectly report hours worked, or to alter another team member’s time records, they should report the incident immediately to a supervisor or People & Culture (HR).  

## Overtime

When business requirements or other needs cannot be met during regular working hours, team members may be scheduled to work overtime hours.  When possible, advance notification of these mandatory assignments will be provided. Overtime assignments will be distributed as equitably as practical to all team members qualified to perform the required work.
All overtime must be scheduled and approved via e-mail by the team member’s manager in advance of working the overtime hours when possible.  When not possible, notification of overtime worked must be provided to the team member’s manager via e-mail within 24 hours.   
All non-exempt team members will be paid one and one half times their regular rate for all hours worked in excess of 40 in one work week or as otherwise required by applicable state law. Hours for which a team member is paid for but which they do not actually work (such as holidays, PTO, jury duty, etc.) are not counted as hours worked for computing overtime payments. Exempt team members are not eligible to be paid overtime.  


## Non-Exempt Travel  

Some team members in non-exempt positions may be required to travel in the course of their responsibilities. Team members in positions classified as non-exempt under the Fair Labor Standards Act are eligible for compensation for the time they spend traveling. The compensation a team member receives depends upon the kind of travel and whether the travel time takes place within normal work hours or outside of normal work hours.  

“Normal work hours,” for the purposes of this policy, are based on your regular hours of work. Travel from home to work at the beginning and end of the regular workday day is not considered work time.    

**Home to Work on a Special One-Day Assignment in another City or Location:** In this situation, a team member who regularly works at a fixed location in one city is given a special one-day assignment in another city and returns home the same day. The time spent in traveling to and returning from the other city is considered work time.  Deducted from the total work time will be time that the team member would normally spend commuting to their regular work site.  

**Travel that is all in a Day’s Work:** Time spent by a team member in travel as part of their principal activity, such as travel from work site to a customer location during the workday, is work time and must be counted as hours worked.  

**Travel Away from the Home Community:** Travel that keeps a team member away from home overnight is travel away from home. Travel away from home is clearly work time when it cuts across the team member’s workday. The time is not only hours worked on regular working days outside of regular working hours but also during corresponding hours on nonworking days. Time spent in travel away from home outside of regular working hours as a passenger on an airplane, train, boat, bus, or automobile is not considered working time and therefore is not compensable.  

For more information regarding this policy please contact the People & Culture (HR) Team.


# WORK ENVIRONMENT

## Hours of Work

Work schedules for team members vary throughout our Company. Managers will advise team members of their individual work schedules. Staffing needs and operational demands may necessitate variations in starting and ending times, as well as variations in the total hours that may be scheduled each day and week. 

The standard work week is forty (40) hours. The standard workday is eight (8) hours for non-exempt workers. Workday lengths for exempt team members are determined primarily by the hours required to accomplish their current workloads. General business hours are from 8:00 a.m. to 5:00 p.m. daily.

Unless otherwise provided by applicable law, an unpaid meal period is provided to any team member who works a minimum of six (6) hours per day (minors five (5) hours). Non-exempt team members are required to clock out for their lunch periods.  

Unless otherwise provided by applicable law, non-exempt team members receive two fifteen-minute paid break periods for each full workday. If deemed necessary the Company can modify a team member’s starting and quitting time as well as the number of hours worked to accommodate business needs.  


## Attendance 

Team members are expected to arrive at work on time each scheduled work day.  Punctuality and regular attendance are basic and essential requirements for satisfactory performance.

We recognize the need for team members to be absent from work due to illness or the need to take care of personal business during the normal workday.  The Company instituted Personal Time Off (PTO) to provide for these needs as they arise.  Having provided for these situations, it is important to remember that excessive absenteeism or breaks, arriving late, and/or leaving early causes disruption to the business and our customers.   

You are expected to have a consistent start and end time to your work schedule that has been established by your manager. Team members who are going to be absent, late or leaving early from work are responsible for notifying their managers as soon as possible.  Notification should be done prior to the start of the shift and at least 24 hours’ notice should be given for planned PTO. Failure to notify your manager is seen as a violation of the policy. 

Team members that are absent for three (3) or more consecutive days due to illness, injury or disability, may be required to provide written documentation from an attending physician.

Team members that have been absent for three (3) consecutive days without direct notification to their manager or People & Culture (HR) will be considered to have voluntarily terminated their employment. If absences or attendance related issues become problematic or disruptive to the business, they will be addressed. 

Issues related to attendance may be handled through the use of an Improvement Agreement, which is a tool designed for the manager and team member to identify the cause of the issue and create an action plan to improve/mitigate the issue.  If a satisfactory improvement cannot be obtained and sustained, further action, up to and including immediate termination can occur when the Company believes, in its sole discretion, such action is warranted.  

**Note: Some Teams have Team specific attendance policies and performance metrics to which each team member is expected to adhere.**  

The following types of time off will not be considered grounds for disciplinary action under this policy:  

- Time off that was previously approved, including PTO;  
- Paid sick and safe time provided under a mandatory sick and safe time leave law;  
- Approved state and federal leaves of absence, including but not limited to jury duty leave, military leave, leave protected under the Family and Medical Leave Act or similar state laws, and time off or leave specifically approved by the Company as an accommodation under the Americans with Disabilities Act or similar state laws; and/or  
- Time off due to a work-related injury that is covered by workers’ compensation.  

## Flexible Work Schedule

The company is committed to a work environment that accommodates our People First/Family First values. Each team offers a flexible schedule based on business sustainability and individual performance.  

The following basic requirements must be met for all teams:  

- Team members must have satisfactory attendance and cannot be on a Performance Improvement Plan.  
- The workweek for all full-time regular team members is 40 hours per week and 8 hours per day.  
- There will be times when team members are required to depart from the flexible work schedule to accommodate changing situations and staffing needs.  Reasonable notice will be provided when possible.  
- The day before a holiday falling on a workday is treated as a normal 8 hour business day.  

## Friday Early Release

In keeping with our People First/Family First philosophy, we offer our team members the ability to leave 1.5 hours early on Friday.  

- Team members may leave 1.5 hours earlier than their normal schedule as long as individual performance goals are being met and 40 hours have been worked in the week.  
- Team members on a Performance Improvement Plan are excluded from this benefit.  
- Managers reserve the right to ask team members to stay for their entire schedule due to business needs.  

## Personal Appearance 

ConstructConnect provides a casual yet professional work environment for its team members.   Even though the dress code is casual, it is important to project a professional image to our customers, visitors, and co-workers.   All team members are expected to dress in a manner consistent with good hygiene, safety, and good taste.  Please use common sense.  

Provocative or potentially offensive dress or clothing that is ill-fitting, or has inappropriate slogans, sayings or pictures is not considered to be in good taste.  Inappropriate tattoos are included in this category.

The Company reserves the right to determine whether or not dress is inappropriate and may ask team members change under these circumstances.  

Nothing in this policy is intended to prevent team members from wearing a hair or facial hair style that is consistent with their cultural, ethnic or racial heritage or identity.  This policy will be interpreted to comply with applicable local, state or federal law.

The Company will reasonably accommodate exceptions to this policy if required due to an team member's religious beliefs, medical condition or disability. Team members who need such an accommodation should contact their supervisor or Human Resources.  

## Drug Free Workplace 

The use of illegal drugs and alcohol, and the abuse of legal prescription pharmaceuticals, account for tremendous losses in efficiency, attendance and costs of Company-provided health care.  These abuses also diminish the safety of all team members and visitors, impair the reputation of the Company, and violate state and federal laws.  For these reasons, the Company has adopted a no tolerance drug and alcohol policy.  With this policy, it is the intention of the Company to use every lawful means to establish and maintain a drug and alcohol-free workplace.  

Illegal drugs are substances that are controlled or outlawed, are not obtainable by lawful methods, or are legally obtainable but were not obtained in a lawful manner. 

This policy prohibits the use, sale, manufacture, distribution or possession of alcohol or illegal drugs, drug paraphernalia or any combination thereof, on any Company premises or at any location where the team member is performing their job duties and includes Company vehicles on or off Company premises.  Included within this prohibition is medical or recreational marijuana, which remains illegal under federal law.  Under appropriate circumstances and in the absolute discretion of the Company, any illegal substance may be turned over to the appropriate law enforcement agency and may result in criminal prosecution.  Violation of this policy will subject the team member to disciplinary action up to and including immediate termination and may have legal consequences.  

To the extent permitted by applicable state law, the Company shall have the right to require any team member to submit to drug and/or alcohol testing under the following circumstances:

1. Post-Accident – Where the team member was involved in an accident that resulted in property damage or physical injury, requiring professional medical treatment beyond first aid.  
1. Reasonable Suspicion – Upon the belief of management that the team member may have alcohol or illegal drugs in their system while at work or while performing their job duties away from the workplace.  

This list is not meant to limit the circumstances under which a drug or alcohol test may be required.  The Company can test for the presence of alcohol or illegal drugs for other lawful purposes.

For purposes of this policy, a positive (or failure) of a drug or alcohol test is defined as having the presence of any detectable amount of an illegal drug or alcohol in the team member’s system when tested.  Similarly, refusal to submit to a drug or alcohol test when requested by the Company, or any attempt to interfere with the test or alter the sample, also constitutes failure of the test and will result in a team member subject to discipline up to and including termination.

Independent Contractors and temporary team members for the Company will be required to adhere to this policy. 

If a team member is using medication prescribed by a licensed physician, he is responsible for obtaining assurances from that physician that the medication will not impair their judgment or ability to safely and efficiently perform their job duties.  If impairment is possible the team member should report the use to their manager prior to reporting to work.

Any team member who knows or believes that there is unlawful involvement by other team members, vendors or guests with illegal drugs or alcohol contrary to this policy should immediately notify their manager or People & Culture (HR).  The Company will utilize all lawful investigative techniques in response to this information.  Evidence obtained by the Company of the unlawful use, manufacture, trafficking, distribution or possession of controlled substances will be provided to the appropriate law enforcement authorities.

Team members who are convicted of off-the-job drug or alcohol related crimes may be considered to be in violation of this policy.  In deciding what action to take, management will take into consideration the type of criminal conviction involved and all other factors relating to the circumstances of the criminal conviction and the impact on the company.

The Company recognizes that drug or alcohol abuse may, in some cases, be an illness or mental health problem.  Team members who need help in dealing with these problems are encouraged to seek assistance voluntarily before the situation requires management intervention.  An team member’s decision to seek help voluntarily will not be used as a basis for disciplinary action, although the individual may be transferred, given work restrictions or placed on leave, as appropriate. A request for help is considered voluntary only if it is made before the team member is asked to submit to any drug or alcohol test or is discovered to have otherwise violated this policy.  After satisfactorily completing the program and furnishing proof to that effect, the team member will be returned to work.  Further involvement in the use of illegal drugs or alcohol in violation of this policy will result in discipline up to and including termination.

Information related to substance test results and/or requests for team member assistance related to a substance problem is confidential.  Access will be limited to designated Company personnel.  

This policy is not meant to prohibit the consumption of alcohol when and where it is specifically authorized by management as part of a Company function.

This policy does not limit the right of the Company to invoke disciplinary action for any unauthorized activity not enumerated above.  


## Business Conduct

As a team member of the Company, you have an obligation to conduct business according to the ConstructConnect Code of Business Conduct described below:  

- You will not use the Company's name in connection with any outside venture without the prior written approval of the CEO.  
- You will not intentionally misrepresent the Company in any way in an effort to obtain information from or about our competition.  
- Company team members may not enter into contracts on behalf of the Company unless their position is granted the authority to do so by Corporate resolution.  
- You will not make any illegal payments, directly or indirectly, in order to obtain or retain business for the Company.  
- All accounting entries, invoices and other documentation shall correctly describe the transactions to which they relate. There will be no omissions from the books and records, nor any hidden accounts or funds.  
- You must not disclose or misuse Confidential Information, as defined elsewhere in this Handbook.  
- You may not use the Company's name, property, services or resources for your personal benefit without prior approval from the CEO.  
- You will not falsify employment or other company records.  


## Ethics and Conflicts of Interest 

Team members are expected to use good judgment, adhere to high ethical standards and avoid situations that create an actual or perceived conflict between their personal interests and those of the Company. ConstructConnect requires that the transactions team members participate in are ethical and within the law, both in letter and in spirit.

The Company recognizes that different companies have different codes of ethics. However, just because a certain action may be acceptable by others outside of ConstructConnect as “standard practice”, that is by no means sufficient reason to assume that such practice is acceptable at our Company. There is no way to develop a comprehensive, detailed set of rules to cover every business situation. The tenets of this policy outline some basic guidelines for ethical behavior at the Company. Whenever team members are in doubt, they should consult their manager or People & Culture (HR).

Conflicts of interests or unethical behavior may take many forms including, but not limited to, the acceptance of gifts from competitors, vendors, potential vendors or customers of the Company. Gifts may only be accepted if they have a nominal value and only on appropriate occasions (for example, a holiday gift). Team members are cautioned not to accept any form of remuneration or non-business related entertainment, nor may team members sell to third parties any information, products or materials acquired from the Company. Team members may engage in outside business activities, provided such activities do not adversely affect the Company or the team member’s job performance and the team member does not work for a competitor, vendor or customer. Team members are prohibited from engaging in financial participation, outside employment or any other business undertaking that is competitive with, or prejudicial to, the best interests of Company. Team members may not use proprietary and/or Confidential Information, as defined elsewhere in this Handbook, for personal gain or to the Company’s detriment, nor may they use assets or labor for personal use.  
 
If a team member or someone with whom the team member has a close personal or familial relationship has a financial or employment relationship with a competitor, vendor, potential vendor or customer of the Company, the team member must disclose this fact in writing to their manager and the Chief People Officer.  The Company will determine what course of action must be taken to resolve any conflict it believes may exist. If the conflict is severe enough, the Company may be forced to ask the team member to tender their resignation. The Company has sole discretion to determine whether such a conflict of interest exists.

Should any team member become aware of what they believe to be unethical or illegal action with any connection to the Company or its business associates, that team member has the duty and responsibility to immediately report that information to their manager and/or People & Culture (HR) for investigation. Failure to do so will subject the team member to discipline up to and including termination. All team members should realize that any wrongdoing is counter to our vision, mission and values and will therefore, not be tolerated. Team members are encouraged to seek assistance from their managers with any legal or ethical concerns. However, the Company realizes this may not always be possible. As a result, team members may contact People & Culture (HR) to report anything that they cannot discuss with their manager.  


## Alcohol at Company Functions

The Company is committed to limiting the consumption of alcohol by team members at company functions.  Excessive alcohol consumption can endanger the health and safety of team members and result in inappropriate behavior that harms both the reputation of the individuals involved, as well as the Company.  

Company functions to which this policy applies include but are not limited to company parties, outings, and team events.  Team members who choose to drink alcoholic beverages at company functions will be limited to two alcoholic beverages purchased by the company.  All other beverages consumed will be at the cost of the team member. Alcoholic beverages may not be served in offices or work areas. Alcoholic beverages may not be consumed during office hours or at lunch or breaks.  

Team members who choose to drink alcoholic beverages are expected to behave in a professional manner with usual business standards and all company policies and codes of conduct. Failure to do so may result in disciplinary action up to and including termination.  


## Solicitation & Distribution

Solicitation by a ConstructConnect team member of another team member is prohibited during the working time of either person. Working time is defined as time when team member’s duties require that they be engaged in work tasks. Distribution of printed or digital  material or literature of any nature shall be limited to  non-work times. No literature shall be posted anywhere without the authorization of People & Culture (HR). Solicitation and/or distribution of material on Company  sites by persons not employed by the Company are prohibited at all times.  

## Team Member Work Space

Good work habits and a neat place to work are essential for job safety and efficiency.  Offensive materials and pictures should not be displayed in a team member’s work space at any time.  An example of such prohibited offensive material is abusive or profane language, racial, ethnic, sexual, religious, disability or age related slurs, or other forms of verbal assault, horseplay, bullying, offensive teasing, threats, coercion, demeaning gossip, and unwanted proselytizing.  

Team members are expected to use proper care when using the Company’s property and equipment.  No property may be removed from the premises without proper authorization of management.  Unsafe, broken, lost or damaged property or equipment should be reported to the team member’s manager immediately.

Personal belongings brought onto Company premises are the team member’s responsibility.  While the Company may take reasonable measures to protect a team member’s property, it is not responsible for the loss or theft of personal belongings.  If a team member finds property missing or damaged, they should immediately report it to their manager and the manager will report it to People & Culture (HR).  The Company, as stated elsewhere in this Handbook, can and will perform routine inspections or searches as it deems necessary without prior notice or prior consent by a team member.  Because even a routine inspection or search might result in the viewing of a team member’s personal possessions, team members are encouraged not to bring any item of personal property to the workplace that they do not want revealed to others in the Company.  


## Tobacco-Free 

The use of tobacco products including, but not limited to cigarettes, smokeless cigarettes, cigars, tobacco pipes and all forms of smokeless tobacco, is prohibited inside any of the Company’s facilities or vehicles except for outside designated areas ONLY.  For purposes of this policy, the prohibition of tobacco use is extended to include electronic cigarettes and/or any item, device or method of administering nicotine or any other drug by inhalation, whether a drug is included in the formulation or not.  

This policy relates to all work areas at all times, including before and after normal working hours.  Team members may use tobacco products at scheduled break periods in the designated area ONLY.  All cigarette butts must be placed in the ashtray located in the designated smoking area.  


## Use of Telephones & Recording Devices 

Office telephones are a vital part of our business operation.  Because of the large volume of business transacted by telephone, personal use of the telephone or cellular phones should be limited and personal calls should be brief.  

While at work team members are expected to exercise the same discretion in using personal cellular phones and personal digital assistants as is expected for the use of Company phones.  Excessive personal calls during the workday, regardless of the phone used, can interfere with team member productivity and can be distracting to others and is therefore strictly prohibited as a violation of the Company’s rules and regulations.  Team members are expected to make personal calls on non-work time when possible and to ensure that friends and family members are aware of the Company’s policy.  This policy includes the usage of text messaging. Team members that have excessive phone usage, including text messaging, for personal calls on Company phones or personal cell phones will be subject to disciplinary action, up to and including termination.  In addition, use of any company or personal phones in the restrooms is strictly prohibited.

Team members are not prohibited from photographing, taping or recording, or attempting to record any person, document, conversation, communication, or activity that occurs at, or is related to the business activities of the Company.  However, the following are examples of restrictions that do apply:

- Working time is for work.  Unless photographing, taping or recording is required as part of a job function you are performing, it should not be done during your working time.  Photographing, taping or recording during non-working time is permitted, subject to the other qualifications below.  
- If photographing, taping or recording would violate other Company policies, such as the Company’s policy prohibiting discrimination based on any protected characteristics, it is not permitted at any time.  
- Photographing, taping or recording Company trade secrets or other confidential information, such as customer lists, internal financial information and account numbers, proprietary technical formulas and processes, and protected medical records, is not allowed at any time.  
- Photographing, taping or recording that infringes on reasonable expectations of privacy, such as the privacy of co-workers using restrooms and locker rooms, is not permitted.  
- Recording coworkers without their knowledge or consent may infringe on their privacy rights.  Nonconsensual recordings may be viewed as particularly intrusive and unreasonable, and result in claims of illicit harassment; be aware that some religions may prohibit the taking of photographs or videos of members of that religion.  
- Some jurisdictions have laws that prohibit recordings of any person without their knowledge or consent.  Recording in any manner that violates state or federal law is prohibited.  

Photographing and videotaping is not restricted when team members are acting in concert for their mutual aid and protection and no overriding contrary interest is present.  Consistent with the foregoing, recordings may include team members documenting workplace conditions, or other terms or conditions of employment.  

## Travel & Entertainment Policy 

All travel arrangements should be made through Concur or by contacting Teplis Travel.  It is the traveler’s responsibility to report his or her actual travel expenses in a responsible ethical manner, in accordance with the regulations set forth in the complete Travel & Entertainment policy.  Submission of travel expenses should be completed within 60 days of the trip and approved by the team member’s immediate supervisor.  Please refer to the complete Travel & Entertainment policy for details of the Company’s travel and entertainment policies.  

## Driving 

Team Members required to drive for company business must at all times meet the following criteria:  

- Team members must have a current, valid driver’s license for the state in which the team member lives.  
- Team members who have occasion to drive on company business must report the loss of driving privileges to their immediate Manager and People & Culture (HR) within 24 hours of when it occurred.  
- Team members must maintain a clean driving record, i.e., must remain insurable under the company’s liability insurance policy.  
- Team member must ensure that they have the state minimum insurance on their own vehicle at all times.  
- Team member must provide a copy of their driver’s license and proof of insurance upon request.  

### Alcohol and Drug Abuse

Driving a vehicle for Company business while under the influence of alcohol or any drugs or narcotics is strictly prohibited and subject to disciplinary action including termination of employment.  

### Cellular Use While Driving

Team members whose job responsibilities include regular or occasional driving and who use their personal cellular telephone for business-related work are expected to put safety first.  Operators driving vehicles that are not equipped with a “hands-free” application must pull over safely and park before engaging in any cellular phone conversations or reading or responding to text messages.  Use of other electronic devices (laptops, iPad, iPod, etc.) is prohibited while driving.  Texting or reviewing texts while driving is likewise strictly prohibited.  This policy is in effect for your safety and the safety of others, as well as the safety of the Company’s property.  Where applicable state law prohibits the use of cell phones of any kind while driving, Company policy will adhere to the state law.  

Team members who are charged with traffic violations, or cause accidents or injuries resulting from their use of personal or Company-issued cellular telephones or other devices while driving, will be solely responsible for all liabilities, fines, etc. that result, to the extent permissible under the law.  

Team members whose job responsibilities do not specifically include driving as an essential function, but who are issued a Company-provided cellular telephone for business use or who use their personal cellular telephone for business use, are also expected to abide by the provisions of this policy.  

## Relationships in the Workplace 

ConstructConnect strongly believes that an environment where team members maintain clear boundaries between team member personal and business interactions is most effective for conducting business.  Since dating relationships that include a manager and/or supervisor with another team member may lead to apparent favoritism or actual conflicts of interest, they are strictly prohibited in the same Team at any level (higher or lower) or in the same line of authority that may affect employment decisions.  

Also, having two non-management or management team members trying to keep the proper balance at work between objective business professionalism and subjective romantic interest is clearly not in the best interest of the company or its team members so any romantic link between two non-management team members is discouraged and may lead to job action if causing any problems.

If any team member relationship becomes disruptive or, in the discretionary view of management, interferes with the proper and efficient conduct of business, the team members involved will be counseled one time about the issue and given the opportunity to jointly correct the problem.  If this correction does not occur, the team members involved will be given the opportunity to decide which team member will voluntarily leave employment.  If this is not accomplished, Company management will make a decision whether one or both team members will leave employment.

In keeping with policies on Unlawful Harassment and Workplace Violence described elsewhere in this Handbook, team members are strictly prohibited from engaging in physical contact that would in any way be deemed inappropriate by a reasonable person while on Company premises, whether during working hours or not.


## Call Monitoring 

Team members must treat customers in a courteous and respectful manner at all times.  To measure and evaluate customer service, ConstructConnect may observe, listen, and record team members’ telephone conversations made in the normal course of business.  The monitoring of telephone conversations will be random and periodic and will be used for training purposes. Advanced notice will not be given prior to monitoring telephone conversations.  Any team member whose telephone conversation may be observed, listened to and recorded will be asked to sign a consent form authorizing the monitoring.  This form will be completed during the team member’s orientation or at the time the team member assumes a position subject to monitoring.

Monitoring will be limited to business-related objectives.  Monitoring will be discontinued if it becomes apparent that the team member is engaged in discussing personal matters


## Outside Employment 

While ConstructConnect does not prohibit team members from having a second job, secondary employment must be reported if it presents a conflict of interest and/or will have an adverse impact on the Company.  Conflict of interest includes, but is not limited to, working directly for a competitor within the same industry or job function.  


## Media Communications 

Do not respond to media inquiries on the Company’s behalf without authorization.  To ensure that the Company communicates with the media in a consistent, timely and professional manner about matters related to the Company, you should notify your manager and/or VP of Marketing if you have been contacted by the media if asked to speak on behalf of the Company.  This rule does not prevent you from speaking with the media, but you should not attempt to speak on behalf of the Company unless you have specifically been authorized to do so by an officer of the Company.  


## Verification of Employment 

All reference check inquiries must be directed in writing to People & Culture (HR).  Responses to such inquiries will confirm only dates of employment and position(s) held.  No employment data will be released without a written authorization and release signed by the individual subject of the inquiry.  All calls, contacts and written inquiries concerning current or former team members should be referred to People & Culture (HR).  


# LEAVES OF ABSENCE

## Bereavement Leave

ConstructConnect recognizes the need for time away from work for funeral preparation and funeral attendance.   A team member who wishes to take time off due to a death of a family member or friend should notify their manager immediately.  

Paid bereavement leave is granted up to five (5) days for the death of a close relative.  For purposes of this policy, a close relative is defined as spouse, domestic partner, child, step-child, father, mother, step-parent, parents-in-law, brother, sister, grandchild, daughter/son-in-law, grandparent and brother/sister-in-law.  All exceptions to this policy need to be approved by People & Culture.

In the event of the death of other family members or friends, the team member may request to take Paid Time-off to pay their respects.  

The amount of Bereavement Leave that may be granted is determined by the team member’s involvement in the funeral preparation and one day of Bereavement Leave must be used to attend the funeral.  Bereavement Leave is paid based on the team member’s base pay rate at the time of absence and is paid for their normal work schedule eight (8) hours.  Bereavement leave does not count toward hours worked for purposes of overtime entitlement and is not paid for scheduled hours of overtime missed due to use of bereavement leave.  

Bereavement leave will normally be granted absent unusual business needs or staffing requirements. A team member may, with their manager’s approval, use any available Paid Time-Off and/or Personal Leave for additional time off as necessary.  Bereavement pay will not be granted for team members on FMLA leave, personal leave, military leave, and maternity leave, or suspension, or if the bereavement leave falls on a paid holiday.     

The Company may request, at any time, pertinent information including the deceased relative’s name, the name and address of the funeral home, and the date of the funeral.   


## Jury Duty and Witness Service Leave 

The Company supports team members called to fulfill their civic duty to serve on jury duty.  Full-time and part-time team Members are eligible to request up to 2 weeks of paid jury duty leave over any 1-year period.

Should extraordinary circumstances exist, at the time of your call to jury duty, which would make your absence severely detrimental to the operation of our Company, we can and will contact the court to request that your service be postponed.

Team members may keep any compensation they are paid for jury duty or witness service. They will be paid their straight time base rate of pay for all hours missed up to eighty (80) hours due to jury duty or witness service on behalf of the Company in addition to any compensation received from the court.  If team members are required to serve jury duty beyond the period of paid jury duty leave, they may use any available PTO or may request an unpaid jury duty leave of absence.

Team members are required to give a copy of the subpoena, or notice or summons from the court, to People & Culture (HR) prior to serving.  If the team member is released from jury duty with 4 or more hours left in their shift they must report to work for the remainder of their shift, except where prohibited by law.  Documentation of daily hours served from the court must also be given to People & Culture (HR) upon a team member’s return.  

Team members called to testify as a voluntary witness at the request of the Company, by subpoena or otherwise, will be paid for the day or days in which the court requires attendance or during which the Company otherwise requires the team members to prepare for testimony. If team members are subpoenaed to appear in court as witnesses, but not at the request of the Company, they will be excused from work in order to comply with the subpoena and will be paid jury/witness duty pay.  

Where applicable state law provides additional benefits beyond those described in this policy, the state law will apply.


## Family and Medical Leaves of Absence (FMLA)

The Company will grant family and medical leave in accordance with the requirements of applicable state and federal law in effect at the time the leave is granted.  Although the federal and state laws sometimes have different names, the Company refers to these types of leaves collectively as “FMLA Leave.”  In any case, team members will be eligible for the most generous benefits available under applicable law.  


### Team Member Eligibility

To be eligible for FMLA Leave benefits, you must:  
 
1. Have worked for the Company for a total of at least 12 months;  
1. Have worked at least 1,250 hours over the previous 12 months as of the start of the leave; and  
1. Work at a location where at least 50 team members are employed by the Company within 75 miles, as of the date the leave is requested.  

### Reasons for Leave  

State and federal laws allow FMLA Leave for various reasons. Because a team member’s rights and obligations may vary depending upon the reason for the FMLA Leave, it is important to identify the purpose or reason for the leave.  FMLA Leave may be used for one of the following reasons, in addition to any reason covered by an applicable state family/medical leave law:  

1. The birth, adoption, or foster care of a team member’s child within 12 months following birth or placement of the child (“Bonding Leave”);   
1. To care for an immediate family member (spouse, child, or parent with a serious health condition (“Family Care Leave”);   
1. A team member’s inability to work because of a serious health condition (“Serious Health Condition Leave”);  
1. A “qualifying exigency,” as defined under the FMLA, arising from a spouse’s, child’s, or parent’s “covered active duty” (as defined below) as a member of the military reserves, National Guard or Armed Forces (“Military Emergency Leave”); or  
1. To care for a spouse, child, parent or next of kin (nearest blood relative) who is a “Covered Servicemember,” as defined below (“Military Caregiver Leave”).  

#### Definitions  

- **“Child,”** for purposes of Bonding Leave and Family Care Leave, means a biological, adopted, or foster child, a stepchild, a legal ward, or a child of a person standing in loco parentis, who is either under age 18, or age 18 or older and incapable of self-care because of a mental or physical disability at the time that Family and Medical Leave is to commence.  “Child,” for purposes of Military Emergency Leave and Military Caregiver Leave, means a biological, adopted, or foster child, stepchild, legal ward, or a child for whom the person stood in loco parentis, and who is of any age.  
- **“Parent,”** for purposes of this policy, means a biological, adoptive, step or foster father or mother, or any other individual who stood in loco parentis to the person.  This term does not include parents “in law.” For Military Emergency leave taken to provide care to a parent of a military member, the parent must be incapable of self-care, as defined by the FMLA.  
- **“Covered Active Duty”** means (1) in the case of a member of a regular component of the Armed Forces, duty during the deployment of the member with the Armed Forces to a foreign country; and (2) in the case of a member of a reserve component of the Armed Forces, duty during the deployment of the member with the Armed Forces to a foreign country under a call or order to active duty (or notification of an impending call or order to active duty) in support of a contingency operation as defined by applicable law.  
- **“Covered Servicemember”** means (1) a member of the Armed Forces, including a member of a reserve component of the Armed Forces, who is undergoing medical treatment, recuperation, or therapy, is otherwise in outpatient status, or is otherwise on the temporary disability retired list, for a serious injury or illness incurred or aggravated in the line of duty while on active duty that may render the individual medically unfit to perform his or her military duties, or (2) a person who, during the five (5) years prior to the treatment necessitating the leave, served in the active military, Naval, or Air Service, and who was discharged or released therefrom under conditions other than dishonorable (a “veteran” as defined by the Department of Veteran Affairs), and who has a qualifying injury or illness incurred or aggravated in the line of duty while on active duty that manifested itself before or after the member became a veteran.  For purposes of determining the five-year period for covered veteran status, the period between October 28, 2009 and March 8, 2013 is excluded.  


### Length of Leave

The maximum amount of FMLA Leave will be twelve (12) workweeks in any 12-month period when the leave is taken for:  

1. Bonding Leave;  
1. Family Care Leave;  
1. Serious Health Condition Leave; and/or  
1. Military Emergency Leave.  

However, if both spouses work for the Company and are eligible for leave under this policy, the spouses will be limited to a total of 12 workweeks off between the two of them when the leave is for Bonding Leave or to care for a parent using Family Care Leave. In determining eligibility for leave, a "rolling" twelve-month period is used, measuring forward from the date the team member uses any FMLA leave.  

The maximum amount of FMLA Leave for a team member wishing to take Military Caregiver Leave will be a combined leave total of twenty-six (26) workweeks in a single 12-month period.  A "single 12-month period" begins on the date of your first use of such leave and ends 12 months after that date.  

If both spouses work for the Company and are eligible for leave under this policy, the spouses will be limited to a total of 26 workweeks off between the two when the leave is for Military Caregiver Leave only or is for a combination of Military Caregiver Leave, Military Emergency Leave, Bonding Leave and/or Family Care Leave taken to care for a parent.  

Under some circumstances, you may take FMLA Leave intermittently—which means taking leave in blocks of time, or by reducing your normal weekly or daily work schedule.  Leave taken intermittently may be taken in increments of no less than one hour. Team members who take leave intermittently or on a reduced work schedule basis for planned medical treatment must make a reasonable effort to schedule the leave so as not to unduly disrupt the Company’s operations. Please contact the People & Culture (HR) team prior to scheduling planned medical treatment. If Family and Medical Leave is taken intermittently or on a reduced schedule basis due to foreseeable planned medical treatment, the Company may require you to transfer temporarily to an available alternative position with an equivalent pay rate and benefits, including a part-time position, to better accommodate recurring periods of leave.  

When a team member who has been approved for intermittent leave seeks leave time that is unforeseeable, the team member must specifically reference either the qualifying reason for leave or the need for FMLA leave at the time the team member calls off.  

As discussed more generally below, if your request for intermittent leave is approved, the Company may later require you to obtain recertifications of your need for leave.  For example, the Company may request recertification if it receives information that casts doubt on your report that an absence qualifies for Family and Medical Leave.  

### Notice and Certification

#### Bonding, Family Care, Serious Health Condition, and Military Caregiver Leave Requirements

Team members are required to provide:  

- when the need for the leave is foreseeable, 30 days advance notice or such notice as is both possible and practical if the leave must begin in less than 30 days (normally this would be the same day the team member becomes aware of the need for leave or the next business day);  
- when the need for leave is not foreseeable, notice within the time prescribed by the Company’s normal absence reporting policy, unless unusual circumstances prevent compliance, in which case notice is required as soon as is otherwise possible and practical;  
- when the leave relates to medical issues, a completed Certification of Health-Care Provider form within 15 calendar days (for Military Caregiver Leave, an invitational travel order or invitational travel authorization may be submitted in lieu of a Certification of Health-Care Provider form);  
- periodic recertification (upon request); and  
- periodic reports during the leave.  

Certification forms are available from People & Culture (HR).  At the Company’s expense, the Company may also require a second or third medical opinion regarding your own serious health condition or the serious health condition of your family member. In some cases, the Company may require a second or third opinion regarding the injury or illness of a “Covered Servicemember.” Team members are expected to cooperate with the Company in obtaining additional medical opinions that the Company may require.  

When leave is for planned medical treatment, you must try to schedule treatment so as not to unduly disrupt the Company’s operation.  Please contact People & Culture (HR) prior to scheduling planned medical treatment.


#### Recertifications After Grant of Leave

In addition to the requirements listed above, if your Family and Medical Leave is certified, the Company may later require medical recertification in connection with an absence that you report as qualifying for Family and Medical Leave. For example, the Company may request recertification if:  

1. the team member requests an extension of leave;  
1. the circumstances of the team member’s condition as described by the previous certification change significantly (e.g., your absences deviate from the duration or frequency set forth in the previous certification; your condition becomes more severe than indicated in the original certification; you encounter complications); or  
1. the Company receives information that casts doubt upon your stated reason for the absence. In addition, the Company may request recertification in connection with an absence after six months have passed since your original certification, regardless of the estimated duration of the serious health condition necessitating the need for leave.  Any recertification requested by the Company shall be at the team member’s expense.  


#### Military Emergency Leave Requirements

Team members are required to provide:  

- as much advance notice as is reasonable and practicable under the circumstances;  
- a copy of the covered military member's active duty orders when the team member requests leave and/or documentation (such as Rest and Recuperation leave orders) issued by the military setting forth the dates of the military member’s leave; and  
- a completed Certification of Qualifying Exigency form within 15 calendar days, unless unusual circumstances exist to justify providing the form at a later date.  

Certification forms are available from People & Culture (HR).  


#### Failure to Provide Certification and to Return from Leave

Absent unusual circumstances, failure to comply with these notice and certification requirements may result in a delay or denial of the leave.  If you fail to return to work at your leave’s expiration and have not obtained an extension of the leave, the Company may presume that you do not plan to return to work and have voluntarily terminated your employment.  

#### Pay and Benefits During Leave

People & Culture (HR) will provide team members with a written explanation of the status of their pay and benefits at the start of the leave.

Generally, FMLA Leave is unpaid.  However, you may be eligible to receive benefits through state-sponsored or Company-sponsored wage-replacement benefit programs.  If you are eligible to receive these benefits, you may also choose to supplement these benefits with the use of accrued PTO, to the extent permitted by law and Company policy.  All such payments will be integrated so that you will receive no more than your regular compensation during this period.  If you are not eligible to receive any of these wage-replacement benefits, you will be required to apply accrued PTO.  The use of paid benefits will not extend the length of a FMLA Leave.  

1. Available PTO will be applied toward any FMLA-qualified leave and will run concurrently.  After exhaustion of such PTO, remaining FMLA will be unpaid.  
1. While on FMLA leave, team members will be required to continue paying their portion of insurance premiums.  If the FMLA leave is covered by paid leave, premiums will be deducted as usual.   If the FMLA leave is unpaid, the team member must remit payment at the first of the month.  If the team member fails to make the required payments, the insurance may be cancelled and the team member will remain responsible for the amount of the team member’s share paid by the Company.  The Company will take action to recover such monies.  
1. PTO time will not accrue during a FMLA leave and a team member will not be eligible for holiday pay or paid bereavement leave during a FMLA leave.   
1. Holidays occurring during a full week of FMLA leave count as FMLA leave; if a team member works any part of a work week during which a holiday falls, the holiday does not count as FMLA.  
1. Deductions for any hours of unpaid intermittent or reduced schedule FMLA leave may be made from the salaries of exempt or nonexempt team members after accrued PTO is exhausted. 
1. Team members who qualify for short term disability (STD) will receive pay in accordance with the terms of the plans.  
1. Bonuses or other payments based on achievement of a specified goal or based on days or hours actually worked or total earnings may take into account the absences or pro-rated based on FMLA absences if an equivalent non-FMLA leave is treated the same.  
1. Team members who qualify for worker’s compensation benefits will receive pay continuation according to the requirements of state law and the Company’s insurance plan in each state.  


#### Benefits During Leave

The Company will continue making contributions for your group health benefits during your leave on the same terms as if you had continued to work.  This means that if you want your benefits coverage to continue during your leave, you must also continue to make any premium payments that you are now required to make for yourself or your dependents.  Team members taking Bonding Leave, Family Care Leave, Serious Health Condition Leave, and Military Emergency Leave will generally be provided with group health benefits for a 12 workweek period.  Team members taking Military Caregiver Leave may be eligible to receive group health benefits coverage for up to a maximum of 26 workweeks.  In some instances, the Company may recover premiums it paid to maintain health coverage if you fail to return to work following a FMLA Leave.  


#### Job Reinstatement

Under most circumstances, you will be reinstated to the same position held at the time of the leave or to an equivalent position with equivalent pay, benefits, and other employment terms and conditions.  However, you have no greater right to reinstatement than if you had been continuously employed rather than on leave.  For example, if you would have been laid off had you not gone on leave, or if your position has been eliminated during the leave, then you will not be entitled to reinstatement.  

Prior to being allowed to return to work, a team member wishing to return from a Serious Health Condition Leave must submit an acceptable release from a health care provider that certifies the team member can perform the essential functions of the job as those essential functions relate to the team member's serious health condition.  For a team member on intermittent FMLA leave, such a release may be required if reasonable safety concerns exist regarding the team member’s ability to perform his or her duties, based on the serious health condition for which the team member took the intermittent leave.  

“Key team members,” as defined by law, may be subject to reinstatement limitations in some circumstances.  If you are a “key team member,” you will be notified of the possible limitations on reinstatement at the time you request a leave.  


#### Confidentiality  

Documents relating to medical certifications, recertifications or medical histories of team members or team members' family members will be maintained separately and treated by the Company as confidential medical records, except that in some legally recognized circumstances, the records (or information in them) may be disclosed to supervisors and managers, first aid and safety personnel or government officials.  


#### Fraudulent Use of FMLA Prohibited

A team member who fraudulently obtains Family and Medical Leave from the Company is not protected by FMLA’s job restoration or maintenance of health benefits provisions.  In addition, the Company will take all available appropriate disciplinary action against such team member due to such fraud.  


#### Nondiscrimination  

The Company takes its FMLA obligations very seriously and will not interfere, restrain or deny the exercise of any rights provided by the FMLA. We will not terminate or discriminate against any individual for opposing any practice, or because of involvement in any proceeding related to the FMLA. If an team member believes their FMLA rights have been violated in any way, they should immediately report the matter to People & Culture (HR).  


#### Additional Information Regarding FMLA

A Notice to Team Members Of Rights Under FMLA (WHD Publication 1420) is located at Appendix One.   

In addition, a number of states have family leave laws that provide leave benefits which exceed those available to team members under the FMLA. Team members should contact People & Culture (HR) for additional information.  


## Leave of Absence

There may be a rare occasion when a team member is faced with an emergency or special circumstance and needs to take an unpaid personal leave of absence.  The team member’s manager, in conjunction with the Chief People Officer, may grant a personal leave of absence without pay.   


### Medical Leave (Non-FMLA)

Team members who do not qualify under FMLA but have a personal medical condition that prohibits them from being at work may qualify for unpaid medical leave.  The Company will comply with all its obligations under the Americans With Disabilities Act (“ADA”) and comparable state laws.

Guidelines:
- Each request for a leave of absence will be evaluated on an individual basis, taking into consideration staffing and team needs, reason for the leave, the team member’s length of service and work record.  
- Medical documentation must be provided. Requests for leaves of absences should be in writing and unless there is an emergency situation, all requests for leave should be made thirty (30) days prior to the start of the leave. 
- This leave can be taken only after all available PTO has been exhausted.    
- If the condition qualifies for short term disability, the leave will follow the duration of the approved STD leave with no additional leave being provided.  
- If a team member is on a personal leave of absence and becomes eligible for FMLA, the team member will no longer be eligible for a personal leave of absence if the leave falls under the provisions of FMLA.  
- While on leave, team members must continue to pay their portion of insurance premiums.  If the leave is unpaid, premium payments are due the first (1) day of the month to the People & Culture (HR) team.  
- PTO time will not accrue during a continuous leave and the team member will not be eligible for holiday pay or bereavement pay during the leave.  
- Failure to return from leave will constitute a resignation by the team member.  


### Personal Leave

Personal leave of up to thirty (30) days may be extended for circumstances outside of the team member’s own personal medical condition such as an immediate family member’s illness or injury, other catastrophic personal matters that need immediate attention or other special circumstances.  

Guidelines:  

- Each request for a leave of absence will be evaluated on an individual basis, taking into consideration staffing and team needs, reason for the leave, the team member’s length of service and work record.    
- Requests for leaves of absences should be in writing and unless there is an emergency situation, all requests for leave should be made thirty (30) days prior to the start of the leave.    
- To qualify for a personal leave of absence, a team member must be classified as full-time and must have completed at least six (6) months of full-time service at the time of the request.    
- This leave can be taken only after all available PTO has been exhausted.    
- Personal leaves of absence will only be granted one (1) time in a “rolling” backwards twelve (12) month period.   
- If a team member becomes eligible for FMLA while on a personal leave of absence, the leave will be converted to and becomes eligible for FMLA, the team member will no longer be eligible for a personal leave of absence if the leave falls under the provisions of FMLA.    
- While on leave, team members must continue to pay their portion of insurance premiums.  If the leave is unpaid, premium payments are due the first (1) day of the month to the P&C (HR) team.  
- PTO time will not accrue during the leave and the team member will not be eligible for holiday pay or bereavement pay during the leave.  
- The Company cannot guarantee team members that their original position or equivalent position will be available when they return.  
- Failure to return from leave will constitute a resignation by the team member.  


## Maternity Leave

Active, regular, full-time team members are eligible for six (6) weeks of paid maternity leave immediately after the birth of their own baby.  Maternity leave benefits will be concurrent with all leave policies, and not in addition to, any FMLA, Personal Leave or other leave or disability insurance that may be available.  Maternity leave will be paid at 100% of base salary and the average of last twelve (12) months commissions.     

**Note to Team members: Where a team member’s applicable state law provides greater benefits than those described above, the state law will apply.**  


## Parental Leave

All active, regular, full-time team members are eligible for up to 2 weeks of paid parental leave during the first three (3) months of the birth or adoption of their own child. Team members will be paid at 100% of their base salary and the average of last twelve (12) months commissions for the duration of the parental leave.

Parental leave benefits will be concurrent with all leave policies. If a team member is eligible for Parental Leave benefits, it will be offered concurrently with, not in addition to, any FMLA, Personal Leave or other leave that may be available. Parental Leave may be used either as 2 weeks of continuous leave or in 1 week increments.

To be eligible for Parental Leave benefits, a team member must notify his or her manager and the People & Culture (HR) Team for approval.  Requests for Parental Leave should be at least 30 days of the requested leave.

**Note to Team members: Where a team member's applicable state law provides greater benefits than those described above, the state law will apply.**

## Military Leave 

The company is committed to protecting the job rights of a team member absent on military leave. In accordance with the [Uniformed Services Employment and Reemployment Rights Act (USERRA)](https://www.dol.gov/agencies/vets/programs/userra){:target="_blank"}, it is the Company's policy that no team member or prospective team member will be subjected to any form of discrimination on the basis of that person's membership in or obligation to perform service for any of the Uniformed Services of the United States. Specifically, no person will be denied employment, reemployment, promotion, or other benefit of employment on the basis of such membership. Furthermore, no person will be subjected to retaliation or adverse employment action because such person has exercised his or her rights under this policy.  

A military leave of absence will be granted by the Company to team members who are absent from work because of service in the United States uniformed services. Advance notice of military service, as far in advance as reasonable, is required unless military necessity prevents such notice or it is otherwise impossible or unreasonable. Subject to certain exceptions under the applicable laws, these benefits are generally limited to five (5) years of leave of absence. Contact the People & Culture (HR) Team to request a temporary or extended military leave of absence.

### Benefits

1. Team members on temporary or extended military leave may, at their option, use any or all accrued paid time off during their absence.    
1. Per USERRA, the company will maintain health benefits and other benefits for the first 31 days of military leave as if the team member is actively employed.  After 31 days, the team member and dependents can continue group health coverage through COBRA at 102% of the overall premium rate.  
1. Team members do not accrue PTO (paid time off), receive holiday or bereavement pay while on military leave of absence status.  
1. Company paid benefits will terminate the day the team member becomes active military for more than 31 days.  


### Reemployment

Team members returning from military leave will be placed in the position they would have attained had they remained continually employed or a comparable one depending on the length of military service in accordance with USERRA. They will be treated as though they were continuously employed for purposes of determining benefits based on length of service.  

Team members will be required to provide to the Company with military discharge documentation to establish the timeliness of the request for reemployment, the duration of the military service, and the honorable discharge from the military service.  

Upon a team member’s prompt application for reemployment (as defined below), a team member will be reinstated to employment in the following manner depending upon the team member’s period of military service:  

1. Less than 91 days of military service – (i) in a position that the team member would have attained if employment had not been interrupted by military service; or (ii) if found not qualified for such position after reasonable efforts by the Company to qualify the team member, in the position in which the team member has been employed prior to military service.  If the team member is not qualified to perform either of these positions, the team member shall be reemployed in any position for which the team member is qualified in nearest approximation to first to the position the team member would have obtained, then the pre-service position, with full seniority.  
1. More than 90 days and less than 5 years of military service – (i) in a position that the team member would have attained if employment had not been interrupted by military service or a position of like seniority, status and pay, the duties of which the team member is qualified to perform; or (ii) if proved not qualified after reasonable efforts by the Company to qualify the team member, in the position of like seniority, status and pay, the duties of which the team member is qualified to perform. If the team member is not qualified to perform either of these positions, the team member shall be reemployed in any position for which the team member is qualified in nearest approximation to first to the position the team member would have obtained, then the pre-service position, with full seniority.
1. Team member with a service-connected disability -  if after reasonable accommodation efforts by the employer, a team member with a service connected disability is not qualified for employment in the position he or she would have attained or in the position that he or she left, the team member will be employed in (i) any other position of similar seniority, status and pay for which the team member is qualified or could become qualified with reasonable efforts by the Company; or (ii) if no such position exists, in the nearest approximation consistent with the circumstances of the team member’s situation.  

A team member who has engaged in military service must, in order to receive the reemployment rights set forth above, submit a request for reemployment according to the following schedule:  

1. If service is less than 31 days (or for the purpose of taking an examination to determine fitness for service) - the team member must report for reemployment at the beginning of the first full regularly scheduled working period on the first calendar day following the completion of service and the expiration of eight hours to allow for safe transportation back to the team member’s residence.  
1. If service is 31 days or more but less than 181 days – the team member must submit a request for reemployment with People & Culture (HR) no later than 14 days following the completion of service.    
1. If service is over 180 days – the team member must submit a request for reemployment with People & Culture (HR) no later than 90 days following completion of service.  


# EMPLOYMENT PRACTICES

## Equal Employment Opportunity

In order to provide equal employment and advancement opportunities to all individuals, employment decisions at ConstructConnect will be based on qualifications, abilities and merit. Equal employment opportunity is not only good practice - it’s the law and applies to all areas of employment, including recruitment, selection, hiring, training, transfer, promotion and demotion, layoff and recall, termination, compensation and benefits.  

As an equal opportunity employer, the Company does not discriminate in its employment decisions on the basis of race, religion, color, national origin or ancestry, sex (including pregnancy, lactation, childbirth, or related medical conditions), sexual orientation, gender identity, age, disability, citizenship status, genetic information, military or veteran status or any other basis that would be in violation of any applicable federal, state or local law. Furthermore, the Company will make reasonable accommodations for qualified individuals with known disabilities unless doing so would result in an undue hardship.  

Any individuals with questions or concerns about any type of discrimination in the workplace should bring these issues to the attention of their immediate manager or the Chief People Officer. After a report is received, a thorough and objective investigation by management will be undertaken. The investigation will be completed and a determination made and communicated to the team member as soon as practical. The Company expects all team members to cooperate fully with any investigation conducted by the Company into a complaint of proscribed harassment, discrimination or retaliation, or regarding the alleged violation of any other Company policies, and during the investigation, to keep matters related to the investigation confidential.  

Anyone found to be engaging in any type of unlawful discrimination will be subject to disciplinary action, up to and including termination of employment.   

Individuals can raise concerns and make reports without fear of reprisal.  Retaliation is prohibited.  Prohibited retaliation includes, but is not limited to, termination, demotion, suspension, failure to hire or consider for hire, failure to give equal consideration in making employment decisions, failure to make employment recommendations impartially, adversely affecting working conditions or otherwise denying any employment benefit.  


## Disability and Accommodation  

The Company will reasonably accommodate qualified individuals with disabilities consistent with all Federal and state laws concerning the employment of persons with disabilities.  

It is Company policy not to discriminate against qualified individuals with disabilities in regard to application procedures, hiring, advancement, discharge, compensation, training, or other terms, conditions, and privileges of employment.  

The Company will reasonably accommodate qualified individuals with a temporary or long-term disability so that they can perform the essential functions of a job.  

An individual who can be reasonably accommodated for a job, without undue hardship and without creating a direct threat to the team member or others in the workplace, will be given the same consideration for that position as any other applicant.  

People & Culture (HR) is responsible for implementing this policy, including resolution of reasonable accommodation, safety, and undue hardship issues.  Disabled team members who want reasonable accommodation should contact People & Culture (HR).  


## Religious Accommodation  

The Company will provide reasonable accommodation for team members’ religious beliefs, observances, and practices when a need for such accommodation is identified and reasonable accommodation is possible.   A reasonable accommodation is one that eliminates the conflict between an team member’s religious beliefs, observances, or practices and the team member’s job requirements, without causing undue hardship to the Company.  

The Company has developed an accommodation process to assist team members, management, and People & Culture (HR). Through this process, the Company establishes a system of open communication between team members and the Company to discuss conflicts between religion and work and to take action to provide reasonable accommodation for team members’ needs. The intent of this process is to ensure a consistent approach when addressing religious accommodation requests. Any team member who perceives a conflict between job requirements and religious belief, observance, or practice should bring the conflict and request for accommodation to the attention of People & Culture (HR) to initiate the accommodation process.  The Company requests that accommodation requests be made in writing, and in the case of schedule adjustments, as far in advance as possible.  


## Lactation Accommodation  

The Company will provide a reasonable amount of break time to accommodate a team member desiring to express breast milk for the team member’s infant child. Team members needing breaks for lactation purposes may use ordinary paid rest breaks or may take other reasonable break time when needed. If possible, the lactation break time should run concurrently with scheduled meal and rest breaks already provided to the team member. If the lactation break time cannot run concurrently with meal and rest breaks already provided or additional time is needed for the team member, the lactation break time will be unpaid for non-exempt team members.  

Team members will be relieved of all work-related duties during any unpaid break. Where unpaid breaks or additional time are required, team members should work with their supervisor or People & Culture (HR) regarding scheduling and reporting the extra break time. Where state law imposes more specific requirements regarding the break time or lactation accommodation, the Company will comply with those requirements.  

Because exempt team members receive their full salary during weeks in which they work, all exempt team members who need lactation accommodation breaks do not need to report any extra break time as “unpaid.”  

The Company will provide team members with the use of a room or a private area, other than a bathroom or toilet stall, that is shielded from view and free from intrusion from coworkers and the public. The Company will make a reasonable effort to identify a location within close proximity to the work area for the team member to express milk. This location may be the team member’s private office, if applicable.  

The Company will otherwise treat lactation as a pregnancy-related medical condition and address lactation-related needs in the same manner that it addresses other non-incapacitating medical conditions, including requested time off for medical appointments, requested changes in schedules and other requested accommodations.  

Team members should discuss with People & Culture (HR) the location for storage of expressed milk. In addition, team members should contact People & Culture (HR) during their pregnancy or before their return to work to identify the need for a lactation area.  

For team members working in a jurisdiction that has a mandatory lactation accommodation law, the Company will comply with all legal requirements, including providing greater or different benefits than those indicated here.  


## Workplace Harassment

ConstructConnect strives to maintain a workplace that fosters mutual team member respect and promotes harmonious, productive working relationships. Our Company believes that discrimination or harassment in any form constitutes misconduct that undermines the integrity of the employment relationship. Therefore, the Company prohibits discrimination and harassment on the basis of race, color, religion, sex, pregnancy (including lactation, childbirth or related medical conditions), sexual orientation, gender identity, age (40 and over), national origin or ancestry, physical or mental disability, genetic information (including testing and characteristics), veteran status, uniformed servicemember status or any other status protected by federal, state or local law. This policy applies to all applicants and team members throughout the Company and all individuals who may have contact with any team member of this Company.   

The term “harassment” means any unwelcome verbal, visual, or physical conduct, comments, communications or treatment of a discriminatory nature about, relating to or because of a person’s legally-protected characteristic or activity, which has the purpose or effect of unduly interfering with an individual’s work performance; creates an intimidating, hostile, or offensive work environment; or otherwise adversely affects an individual’s employment opportunities.  

Examples of harassment include, but are not limited to, making inappropriate or offensive jokes or remarks relating to sex, race, color, religion, national origin, age or disability; using e-mail, voice mail, playing music or other methods of communication to disseminate such jokes or remarks; accessing such offensive material using Company equipment; distributing such jokes or remarks received from others outside the Company.  

The term “sexual harassment” means unwelcome sexual advances, requests for sexual favors, and/or verbal or physical conduct of a sexual nature when:  

- Submission to such conduct is made a term or condition of employment; or   
- Submission to, or rejection of, such conduct is used as a basis for employment decisions affecting the individual; or  
- Such conduct has the purpose or effect of unreasonably interfering with an team member's work performance or creating an intimidating, hostile or offensive working environment.  

Examples of sexual harassment include but are not limited to drawings, pictures, accessing offensive material, sexually suggestive humor or behavior including staring or ogling, unacceptable voicemails/e-mails, uninvited and unnecessary physical contact, and other sexually related comments and behavior.    

If an applicant or team member feels they have been subjected to any form of harassment and/or discrimination, they should, if they are comfortable doing so, firmly and clearly tell the person engaging in the harassing and/or discriminating conduct that it is unwelcome, offensive and should stop at once. The team member also should report any discrimination and/or harassment to the Chief People Officer. At that point they will take the necessary steps to initiate an investigation of the discrimination and/or harassment claim.  

People & Culture (HR) will conduct an investigation in as confidential a manner as possible and as permitted by law. A timely resolution of each complaint will be reached and communicated to the team member and the other parties involved. Appropriate disciplinary action, up to and including termination, will be taken promptly against any team member engaging in discrimination and/or harassment. Retaliation against any team member for filing a complaint or participating in an investigation is strictly prohibited. However, any team member that knowingly makes a false claim of harassment and/or discrimination will be subject to disciplinary action up to and including termination.  

Occasionally, talking with a manager or someone in People & Culture (HR) about this conduct is not an option. If a team member feels that his/her complaint has not been or cannot be properly handled, they may forward the complaint to Chief Executive Officer (CEO).  


### Manager’s Responsibility

All supervisors and managers are responsible for:  

- Implementing this policy, which includes, but is not limited to, taking steps to prevent harassment and retaliation;  
- Ensuring that all team members under their supervision have knowledge of and understand this policy;  
- Promptly reporting any complaints to the designated Human Resources Representative so they may be investigated and resolved in timely manner;  
- Taking and/or assisting in prompt and appropriate corrective action when necessary to ensure compliance with this policy; and   
- Conducting themselves, at all times, in a manner consistent with this policy.  

Failure to meet these responsibilities may lead to disciplinary action, up to and including termination.  


### Protection against Retaliation  

Retaliation is prohibited against any person by another team member or by ConstructConnect for using this complaint procedure, reporting proscribed harassment, objecting to such conduct or filing, testifying, assisting or participating in any manner in any investigation, proceeding or hearing conducted by a governmental enforcement agency. Prohibited retaliation includes, but is not limited to, termination, demotion, suspension, failure to hire or consider for hire, failure to give equal consideration in making employment decisions, failure to make employment recommendations impartially, adversely affecting working conditions or otherwise denying any employment benefit.  

Individuals who believe they have been subjected to retaliation, or believe that another individual has been subjected to retaliation, should report this concern to the highest ranking on-site supervisor or manager or to any People & Culture (HR) Representative. Any report of retaliatory conduct will be investigated in a thorough and objective manner. If a report of retaliation prohibited by this policy is substantiated, appropriate disciplinary action, up to and including termination of employment, will be taken.  If a complaint cannot be substantiated, the Company may take appropriate action to reinforce its commitment to providing a work environment free from retaliation. 


# WORKPLACE SAFETY AND SECURITY

## Visitors in the Workplace 

 In order to assure the safety and security of Company team members, security of sensitive/proprietary/confidential information, its visitors, and to ensure that only authorized personnel have access to the Company facilities, all visitors on Company property must enter the building through the lobby.   All visitors are required to sign the Visitors log when they enter and when they leave and must wear and clearly display a visitor’s badge while on the premises at all times where applicable. Visitors shall not be permitted outside the lobby without being escorted at all times by an appropriate team member.  Team members are responsible for the conduct, locality and well-being of their visitors at all times.  It is the affirmative obligation of each team member to report to management any strangers in the workplace that look like they don’t belong there and/or are apparently unattended.  Team members must have their visitors surrender their visitor’s badge when they are leaving the building and sign out on the Visitors log.  

## Computer and Internet Usage 

The term “Company Computer Systems” in context of this policy includes Company-provided internet, instant messaging, common drives, e-mail, phone systems, and all related hardware and software.  This policy applies to all types of communication activities utilizing Company owned computers, communication equipment, systems and networks.   

### Team Member Usage Expectations  

Access to the Company’s IT resources is a privilege granted to all users to support their job duties and to support the goals and objectives of the Company. More details can be found in the [Acceptable Use Policy](/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Acceptable%20Use%20Policy){:target="_blank"} and the related [Acceptable Use Guidelines](/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Acceptable%20Use%20Guideline){:target="_blank"}.  

The Company expects that team members will:  

- Know and comply with the Acceptable Use Policy and Acceptable Use Guidelines.  
- Operate computer equipment for the performance of their official duties. Conduct themselves honestly and appropriately while using computer equipment, and respect the copyrights, software licensing rules, property rights and privacy of others.  
- Take proper care of all company equipment that the team member is entrusted with.  Upon leaving the Company a team member will return all company equipment in proper working order. Failure to return equipment will be considered theft and will lead to criminal prosecution by the Company.  

The Company requires that team members will not:  

- Attempt to disable, defeat or circumvent any Company security feature or mechanism.  
- Access, archive, store, distribute, edit, print, display, or record any kind of sexually explicit, threatening, demeaning, harassing, offensive or otherwise inappropriate software or electronic files (images, documents, videos, etc.).  
- Attach personal devices to Company equipment (e.g. USB drives, personal music players, personal computers, laptops and/or games).   
- Knowingly violate the laws and regulations of the United States or any other nation, or the laws and regulations of any state, city, or other local jurisdiction.  

**Use of any of these resources for illegal activity may be grounds for discipline up to and including immediate termination.**


## Social Media  

At the Company, we understand that social media can be a fun and rewarding way to share your life and opinions with family, friends and co-workers around the world.  However, use of social media also presents certain risks and carries with it certain responsibilities.  To assist you in making responsible decisions about your use of social media, the Company has established  guidelines for appropriate use of social media.  Those guidelines can be found in the [Acceptable Use Guidelines](/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Acceptable%20Use%20Guideline){:target="_blank"}.

In short, the Company expects that team members will:  

- Know and follow the rules,  
- Be respectful,  
- Avoid posting information you know to be false,  
- Maintain confidentiality,  
- Express only your personal opinions,  
- Only use social media at work as appropriate, and   
- Never retaliate.  


## Confidential & Proprietary Information  

Team members of ConstructConnect will receive and have access to information that is confidential in nature to the Company, its customers and vendors. It is the responsibility and the legal obligation of all team members to safeguard confidential Company information. The economic well-being of our Company depends upon protecting and maintaining proprietary Company information. Additionally, we are responsible for the security of confidential client information. Confidential information includes but is not limited to trade secrets or confidential information relating to products, processes, know-how, debtors, clients, customers, designs, drawings, test data, marketing data, business plans and strategies, and negotiations and contracts.  More information about safeguarding information can be found in the [Data Management Policy](/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Data%20Management%20Policy){:target="_blank"}.  

Notwithstanding the foregoing, “Confidential Information” does not include information lawfully acquired by non-management team members about wages, hours or other terms and conditions of employment if used by them for purposes protected under §7 of the National Labor Relations Act such as joining or forming a union, engaging in collective bargaining, or engaging in other concerted activity for their mutual aid or protection.  Nothing in this policy prohibits team members from filing a charge or complaint with, or from participating in, an investigation or proceeding conducted by the NLRB, or from exercising rights under Section 7 of the NLRA to engage in joint activity with other team members.  

To ensure that we hold ourselves accountable and conduct ourselves in a professional manner, ConstructConnect and its affiliates will adhere to the spirit, as well as the intent, of all state, federal and local laws and regulations that apply to our industry. Team members will receive training on those laws and regulations that are applicable to their job.   

The Company has developed certain proprietary products and processes that are unique to the Company. Keeping such information from competitors plays an important part in our success. The Company protects proprietary information by restricting team members and visitor’s access to certain designated areas and access to documents to only those who have business reasons to view them.  

Team members who improperly use or disclose trade secrets or confidential business information will be subject to disciplinary action, up to and including termination of employment and legal action, even if they do not actually benefit from the disclosed information.   

Nothing in this policy prohibits you from reporting an event that you reasonably and in good faith believe is a violation of law to the relevant law-enforcement agency, or from cooperating in an investigation conducted by such a government agency.  This may include disclosure of trade secret or confidential information within the limitations permitted by the Defend Trade Secrets Act (DTSA).  You are notified that under the DTSA, no individual will be held criminally or civilly liable under Federal or State trade secret law for disclosure of a trade secret (as defined in the Economic Espionage Act) that is: (A) made in confidence to a Federal, State, or local government official, either directly or indirectly, or to an attorney, and made solely for the purpose of reporting or investigating a suspected violation of law; or, (B) made in a complaint or other document filed in a lawsuit or other proceeding, if such filing is made under seal so that it is not made public.  And, an individual who pursues a lawsuit for retaliation by an employer for reporting a suspected violation of the law may disclose the trade secret to the attorney of the individual and use the trade secret information in the court proceeding, if the individual files any document containing the trade secret under seal, and does not disclose the trade secret, except as permitted by court order.  


## Confidential Information Data Protection  

The protection of confidential business information and trade secrets is vital to the interests and success of the Company. Such confidential information includes, but is not limited to, the following examples:  

- Financial account numbers, such as credit card and bank numbers  
- Debtor, client and/or customer information of any kind  
- Pending projects and proposals   

All team members are required to sign a confidentiality agreement as a condition of employment.   

Access to systems will be restricted to only include those individuals that have a job need.  

Team members who improperly use or disclose confidential business information will be subject to disciplinary action, including termination of employment and legal action, even if they do not actually benefit from the disclosed information.  


## Inclement Weather & Natural Disasters  

There may be times when weather or other natural disasters such as winter storms, hurricanes, tornados, wildfires or earthquakes affect a team member’s ability to work.     

If instances such as this occur, the team member should notify their immediate manager prior to the start of their shift.  If the team member is unable to work for a period greater in length than 1 (one), 8-hour workday they should work with their manager to determine next steps including using PTO for time off or finding a different place to work if needed.  


## Safety in the Workplace  

It is the policy of ConstructConnect that every team member is entitled to work under the safest possible conditions.  To this end, every reasonable effort will be made in the interest of accident prevention, fire protection, and health preservation.  

Each team member and manager must practice safety awareness by thinking defensively, anticipating unsafe situations and reporting unsafe conditions immediately.   

Please observe the following precautions:  

- Notify a manager of any unsafe condition or emergency situation.  If an illness or injury occurs at work inform your manager or People & Culture (HR) immediately.  Call 911, do not attempt to move the individual or transport the individual to the hospital on your own.  
- The use of alcoholic or controlled substances, or the abuse of legal prescription drugs during work hours will not be tolerated.  
- Use, adjust, and repair machines and equipment only when trained and qualified to do so.  
- Know the locations, contents and use of first aid and firefighting equipment.  

A violation of safety standards, which cause hazardous or dangerous situations, or team members who fail to report or, where appropriate, remedy such situations, may be subject to disciplinary action, up to and including termination of employment.  


## Workplace Violence  

The safety and security of all team members is of primary importance at ConstructConnect. Threats, threatening and abusive behavior, or acts of violence against team members, visitors, customers, vendors or other individuals by anyone will not be tolerated.   Violations of this policy will lead to accelerated disciplinary action, not corrective action, up to and including termination and/or referral to appropriate law enforcement agencies for arrest and prosecution.  The Company can and will take any necessary legal action to protect its team members, customers and property.  

Any person who makes threats, exhibits threatening behavior or engages in violent acts on Company premises shall be removed from the premises as quickly as safety permits and shall remain off Company premises pending the outcome of the investigation. Following investigation, the Company will initiate an immediate and appropriate response. This response may include, but is not limited to, suspension and/or termination of any business relationship, reassignment of job duties, suspension or termination of employment and/or criminal prosecution of the person or persons involved.  

All team members are responsible for notifying management of any threats that they witness or receive or that they are told another person witnessed or received. Even without a specific threat, all team members should report any behavior that they have witnessed that they regard as potentially threatening or violent or which could endanger the health or safety of a team member when the behavior has been carried out on a Company-controlled site or is connected to Company employment or Company business. Team members are responsible for making this report regardless of the relationship between the individual who initiated the threatening behavior and the person or persons being threatened. The Company understands the sensitivity of the information requested and has developed confidentiality procedures in accordance with applicable law that recognize and respect the privacy of the reporting team member.  


## Weapons-Free Workplace  

To ensure that ConstructConnect maintains a workplace safe and free of violence for all team members, the Company prohibits the possession or use of weapons of any kind on Company property.  A license to carry the weapon does not supersede Company policy.  Any team member in violation of this policy will be subject to prompt disciplinary action, up to and including termination.  All Company team members are subject to this provision, including contract and temporary team members, visitors, vendors and customers on Company property.  

“Dangerous weapons” include, but are not limited to, firearms, explosives, chemical irritants, stun guns, clubs, illegal knives and other weapon or device that might be considered dangerous or that could cause harm.  Team members are responsible for making sure that any item possessed by the team member is not prohibited by this policy.    

“Company property” is defined as all Company-owned or leased buildings and surrounding areas such as sidewalks, walkways, driveways and parking lots under the Company’s ownership or control. This policy applies to all vehicles that come onto Company property where permitted by applicable law.  

This policy is administered and enforced by People & Culture (HR).  Anyone with questions or concerns specific to this policy should contact the Chief People Officer.  


## Right to Search/Privacy Expectations  

Access to ConstructConnect premises is conditioned upon its right to inspect or search the person, vehicle or personal effects of any team member or visitor. This may include any team member’s office, desk, computer & related equipment, file cabinet, closet, locker, lunchbox, clothing or similar place. Team members should have no expectation of privacy in connection with any of these listed places. Because even a routine inspection or search might result in the viewing of a team member’s personal possessions, team members are encouraged not to bring any item of personal property to the workplace that they do not want revealed to others in the Company.  

Any prohibited materials (or materials that may be found to be prohibited) that are found in a team members possession during an inspection or search will be collected and retained by management and placed in a sealed container or envelope. The team member’s name, date, circumstances under which the materials were collected, and by whom they were collected will be recorded and attached to the container or written upon the envelope. If after further investigation, the collected materials prove not to be prohibited, they will be returned to the team member, and the team member must sign a receipt for the contents. If the prohibited materials prove to be illegal and/or dangerous, they will not be returned to the team member but may be turned over to the appropriate authorities.  

From time to time, and without prior announcement, inspections or searches may be made of anyone entering, leaving, or on the premises or property of the Company (including alcohol and/or drug screens or other testing). Refusal to cooperate in such an inspection or search (including alcohol and/or drug screens) shall be grounds for disciplinary action, up to and including termination.  


# SEPARATION OF EMPLOYMENT

Resignation is a voluntary act initiated by the team member to terminate employment with the Company.  A team member resigning is requested to give at least two (2) weeks’ notice before voluntarily terminating employment so that a smooth transition may occur in filling their vacated position.  After a team member tenders their resignation, they are no longer able to take PTO hours and floating holidays.  

Team members that work out their two (2) week notice will be paid out unused, accrued PTO time.  Team members that do not work out their two (2) week notice will not be paid out unused, accrued PTO time where permitted by applicable law.    

Team members that have their employment involuntarily terminated because of non-compliance with any local, state or federal law or violation of the Company’s policies will not be paid out unused, accrued PTO time where permitted by applicable law.    

Management reserves the right to provide a team member with two weeks’ pay in lieu of notice in situations where job or business needs warrant.    

Team members who fail to report to work for three consecutive days without properly communicating to their manager the reasons for their absences will be viewed as voluntarily resigning their employment as of the third day.  

Team members are required to turn in all Company property in proper working order no later than two (2) weeks from their last day of worked.  A team member not returning Company property will not be considered eligible for re-hire. Failure to return company equipment will be considered theft and the Company may take legal action against the team member to recover monies representative of the property not returned.  

When team members leave the Company, they may be asked to participate in an exit meeting. The primary purpose of the exit meeting is to ask for valuable feedback about team members’ work experiences at the Company. Participation in such exit interviews is strictly voluntary.   

Any team member that has had their employment terminated from the Company is not permitted to be on Company property without the advance permission of the Chief People Officer.  Contact with team members during work hours is also prohibited.  Any violations to this policy may result in legal action.  


# APPENDIX ONE

## A Notice to Team Members of Rights Under the FMLA

[FMLA Poster (pdf format)](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/fmlaen.pdf){:target="_blank"}    



# State Supplements  

## California  

### GENERAL INFORMATION  

#### About This California Supplement  

ConstructConnect is committed to workplace policies and practices that comply with federal, state and local laws. For this reason, California employees will receive the Company's national handbook ("National Handbook") and the California Supplement to the National Handbook (“California Supplement”) (together, the "Employee Handbook").  

The California Supplement applies only to California employees. It is intended as a resource containing specific provisions derived under California law that apply to the employee's employment. It should be read together with the National Handbook and, to the extent that the policies in the California are different from or more generous than those in the National Handbook, the policies in the California Supplement will apply.  

The California Supplement is not intended to create a contract of continued employment or alter the at-will employment relationship. **Only the President/Owner of the Company or that person’s authorized representative has the authority to enter into an agreement that alters the at-will employment relationship and any such agreement must be in writing signed by the President/Owner of the Company or an authorized representative.**  

If employees have any questions about these policies, they should contact their Human Resources representative.  

### COMMITMENT TO DIVERSITY  

#### Discrimination, Harassment and Retaliation Prevention Policy  

##### Equal Employment Opportunity  

ConstructConnect is an equal opportunity employer. In accordance with applicable law, we prohibit discrimination and harassment against employees, applicants for employment, individuals providing services in the workplace pursuant to a contract, unpaid interns and volunteers based on their actual or perceived: race (including traits historically associated with race, such as hair texture and protective hairstyles), religious creed, color, national origin, citizenship status, ancestry, physical or mental disability, medical condition, genetic information, marital status (including registered domestic partnership status), sex and gender (including pregnancy, childbirth, lactation and related medical conditions), gender identity and gender expression (including transgender individuals who are transitioning, have transitioned, or are perceived to be transitioning to the gender with which they identify), age (40 and over), sexual orientation, Civil Air Patrol status, military and veteran status and any other consideration protected by federal, state or local law (collectively referred to as "protected characteristics").  

For purposes of this policy, discrimination on the basis of "national origin" also includes discrimination against an individual because that person holds or presents the California driver's license issued to those who cannot document their lawful presence in the United States, as well as discrimination based upon any of the following: an individual’s or individual’s ancestors’ actual or perceived physical, cultural or linguistic characteristics associated with a national origin group; marriage to or association with individuals of a national origin group; tribal affiliation; membership in or association with an organization identified with or seeking to promote the interests of a national origin group; attendance or participation in schools, churches, temples, mosques or other religious institutions generally used by persons of a national origin group; or a name that is associated with a national origin group. An employee's or applicant for employment's immigration status will not be considered for any employment purpose except as necessary to comply with federal, state or local law.  

The Company allows employees to self-identify their gender, name and/or pronoun, including gender-neutral pronouns.  The Company will use an employee’s gender or legal name as indicated on a government-issued identification document, only as necessary to meet an obligation mandated by law.  Otherwise, the Company will identify the employee in accordance with the employee’s current gender identity and preferred name.  

The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law. The Company also does not retaliate or otherwise discriminate against applicants or employees who request a reasonable accommodation for reasons related to disability or religion.   Our commitment to equal opportunity employment applies to all persons involved in our operations and prohibits unlawful discrimination and harassment by any employee, including supervisors and co-workers.  

##### Prohibited Harassment  

ConstructConnect is committed to providing a work environment that is free of illicit harassment based on any protected characteristics. As a result, the Company maintains a strict policy prohibiting sexual harassment and harassment against employees, applicants for employment, individuals providing services in the workplace pursuant to a contract, unpaid interns or volunteers based on any legally-recognized basis, including, but not limited to, their actual or perceived race (including traits historically associated with race, such as hair texture and protective hairstyles), religious creed, color, national origin, ancestry, physical or mental disability, medical condition, genetic information, marital status (including registered domestic partnership status), sex and gender (including pregnancy, childbirth, lactation and related medical conditions), gender identity and gender expression (including transgender individuals who are transitioning, have transitioned, or are perceived to be transitioning to the gender with which they identify), age (40 or over), sexual orientation, Civil Air Patrol status, military and veteran status, immigration status or any other consideration protected by federal, state or local law. For purposes of this policy, discrimination on the basis of "national origin" also includes harassment against an individual because that person holds or presents the California driver's license issued to those who cannot document their lawful presence in the United States and based on any of the following: an individual’s or individual’s ancestors’ actual or perceived physical, cultural or linguistic characteristics associated with a national origin group; marriage to or association with individuals of a national origin group; tribal affiliation; membership in or association with an organization identified with or seeking to promote the interests of a national origin group; attendance or participation in schools, churches, temples, mosques or other religious institutions generally used by persons of a national origin group; or a name that is associated with a national origin group.  All such harassment is prohibited.  
This policy applies to all persons involved in our operations, including coworkers, supervisors, managers, temporary or seasonal workers, agents, clients, vendors, customers, or any other third party interacting with the Company (“third parties”) and prohibits proscribed harassing conduct by any employee or third party of ConstructConnect, including nonsupervisory employees, supervisors and managers. If such harassment occurs on the Company’s premises or is directed toward an employee or a third party interacting with the Company, the procedures in this policy should be followed.  

##### Sexual Harassment Defined  

Sexual harassment includes unwanted sexual advances, requests for sexual favors or visual, verbal or physical conduct of a sexual nature when:  

- Submission to such conduct is made a term or condition of employment; or  
- Submission to, or rejection of, such conduct is used as a basis for employment decisions affecting the individual; or  
- Such conduct has the purpose or effect of unreasonably interfering with an employee's work performance or creating an intimidating, hostile or offensive working environment.  

Sexual harassment also includes various forms of offensive behavior based on sex and includes gender-based harassment of a person of the same sex as the harasser. The following is a partial list:  

- Unwanted sexual advances.  
- Offering employment benefits in exchange for sexual favors.  
- Making or threatening reprisals after a negative response to sexual advances.  
- Visual conduct: leering; making sexual gestures; displaying sexually suggestive objects or pictures, cartoons, posters, websites, emails or text messages.  
- Verbal conduct: making or using derogatory comments, epithets, slurs, sexually explicit jokes, or comments about an employee's body or dress.  
- Verbal sexual advances or propositions.  
- Verbal abuse of a sexual nature; graphic verbal commentary about an individual's body; sexually degrading words to describe an individual; suggestive or obscene letters, notes or invitations.  
- Physical conduct: touching, assault, impeding or blocking movements.  
- Retaliation for reporting harassment or threatening to report sexual harassment.  

An employee may be liable for harassment based on sex even if the alleged harassing conduct was not motivated by sexual desire. An employee who engages in unlawful harassment may be personally liable for harassment even if the Company had no knowledge of such conduct.  

##### Other Types of Harassment  

Harassment on the basis of any legally protected classification is prohibited, including harassment based on: race (including traits historically associated with race, such as hair texture and protective hairstyles), color, national origin, ancestry, physical or mental disability, medical condition, genetic information, marital status (including domestic partnership status), age (40 or over), sexual orientation, Civil Air Patrol status, military and veteran status, immigration status or any other consideration protected by federal, state or local law.  Prohibited harassment may include behavior similar to the illustrations above pertaining to sexual harassment. This includes conduct such as:  

- Verbal conduct including threats, epithets, derogatory comments or slurs based on an individual’s protected classification;  
- Visual conduct, including derogatory posters, photographs, cartoons, drawings or gestures based on protected classification; and  
- Physical conduct, including assault, unwanted touching or blocking normal movement because of an individual’s protected status.  

##### Abusive Conduct Prevention  


It is expected that the Company and persons in the workplace perform their jobs productively as assigned, and in a manner that meets all of management’s expectations, during working times, and that they refrain from any malicious, patently offensive or abusive conduct including but not limited to conduct that a reasonable person would find offensive based on any of the protected characteristics described above.  Examples of abusive conduct include repeated infliction of verbal abuse, such as the use of malicious, derogatory remarks, insults, and epithets, verbal or physical conduct that a reasonable person would find threatening, intimidating, or humiliating, or the intentional sabotage or undermining of a person's work performance.  

##### Protection Against Retaliation  

Retaliation is prohibited against any person by another employee or by ConstructConnect for using the Company’s complaint procedure, reporting proscribed discrimination or harassment or filing, testifying, assisting or participating in any manner in any investigation, proceeding or hearing conducted by a governmental enforcement agency. Prohibited retaliation includes, but is not limited to, termination, demotion, suspension, failure to hire or consider for hire, failure to give equal consideration in making employment decisions, failure to make employment recommendations impartially, adversely affecting working conditions or otherwise denying any employment benefit.  

##### Discrimination, Harassment, Retaliation and Abusive Conduct Complaint Procedure  

Any employee who believes they have been harassed, discriminated against, or  subjected to retaliation or abusive conduct by a co-worker, supervisor, agent, client, vendor, customer, or any other third party interacting with ConstructConnect in violation of the foregoing policies, or who is aware of such behavior against others, should immediately provide a written or verbal report to their supervisor, any other member of management or Human Resources.  

Employees are not required to make a complaint directly to their immediate supervisor. Supervisors and managers who receive complaints of misconduct must immediately report such complaints to Human Resources, who will attempt to resolve issues internally.  When a report is received, the Company will conduct a fair, timely, thorough and objective investigation that provides all parties appropriate due process and reaches reasonable conclusions based on the evidence collected. The Company expects all employees to fully cooperate with any investigation conducted by the Company into a complaint of proscribed harassment, discrimination or retaliation, or regarding the alleged violation of any other Company policies. The Company will maintain confidentiality surrounding the investigation to the extent possible and to the extent permitted under applicable federal and state law.  

Upon completion of the investigation, the Company will communicate its conclusion as soon as practical.  If the Company determines that this policy has been violated, remedial action will be taken, commensurate with the severity of the offense, up to and including termination of employment. Appropriate action will also be taken to deter any such conduct in the future.  

The federal Equal Employment Opportunity Commission (EEOC) and the California Department of Fair Employment and Housing (DFEH) will accept and investigate charges of unlawful discrimination or harassment at no charge to the complaining party. Information may be located by visiting the agency website at [EEOC](www.eeoc.gov) or [DFEH](www.dfeh.ca.gov).  

##### Accommodation for Adult Literacy Programs  

ConstructConnect provides reasonable accommodation and assistance to an employee who reveals a literacy problem and requests assistance to enroll in an adult literacy education program unless doing so will result in an undue hardship to the company's business operations. Examples of assistance include providing employees with the location of local literacy programs and arranging for jobsite visits by literacy education providers.  

Employees who wish to self-identify as an individual with a literacy problem and request an accommodation should contact Human Resources. The Company will take reasonable steps to safeguard the privacy of any employee who self-identifies. In addition, employees who are performing satisfactorily will not be subject to termination of employment because they have disclosed literacy problems.  

While ConstructConnect encourages employees to improve their literacy skills, the Company will not reimburse employees for the costs incurred in attending a literacy program. Time off to attend literacy programs may be provided as a reasonable accommodation unless doing so will result in an undue hardship. However, if time off is provided, the time off may be unpaid. If time off is unpaid, employees wishing to take such leave may utilize their existing vacation time or other accrued paid time off.  

##### Accommodation for Victims of Domestic Violence, Sexual Assault or Stalking  

ConstructConnect will make reasonable accommodations for employees who report that they are the victim of domestic violence, sexual assault or stalking and request that the Company accommodate their safety while at work, unless providing the accommodation will impose an undue hardship on the company's business operations or violates the company's duty to provide a safe and healthy working environment for all employees.  

Reasonable accommodations may include, but are not limited to: a transfer; reassignment; modified work schedule; change in work telephone number; change in work station; installed lock; assistance in documenting domestic violence, sexual assault, stalking or other crime that occurs at the workplace; implemented safety procedures; or other adjustment to a job structure, workplace facility or work requirement in response to a domestic violence, sexual assault,  stalking, or other crime, or referral to a victim assistance organization. The Company will engage in a timely, good faith and interactive process with the employee to identify effective reasonable accommodations.  

Employees may also be entitled to a leave of absence under the company's Victim Leave policy, Leave to Attend Court Proceedings Related to Certain Felonies policy and/or Leave to Attend Court Proceedings for Serious Crimes policy.  Employees should consult those policies and/or Human Resources for additional information.  

The Company may request that an employee provide a written statement signed by the employee (or an individual acting on behalf of the employee) certifying that the requested accommodation is for the employee's safety while at work. The Company may also require an employee to provide a certification that the employee is the victim of domestic violence, sexual assault or stalking and may request recertification every six months. Any of the following will be considered sufficient certification: a police report indicating the employee was a victim; a court order protecting or separating the employee from the perpetrator of the crime or abuse, or other evidence from the court or prosecuting attorney that the employee has appeared in court; documentation from a licensed medical professional, domestic violence counselor, sexual assault counselor, victim advocate, licensed health care provider or counselor that the employee was undergoing treatment or receiving services for physical or mental injuries resulting from the crime or abuse; or any other form of documentation that reasonably verifies that the crime or abuse occurred, including but not limited to, a written statement signed by the employee, or an individual acting on the employee’s behalf, certifying that the absence is for an authorized purpose.  

Employees must notify the Company if their needs change or if they no longer need an accommodation.  

The Company will keep all information submitted in connection with an employee's request for an accommodation confidential to the extent permissible by law. If the law requires disclosure of information, the Company will notify the employee before any information is released.  

The Company will not discriminate or retaliate against any employee because of the individual’s status as a victim of crime or abuse, if the employee provides the Company notice of such status, the Company has actual knowledge of such status, or the employee requests a reasonable accommodation in accordance with this policy.  

Employees who have questions about this policy or who wish to request a reasonable accommodation under this policy should contact their Human Resources representative.  

##### Accommodation for Drug or Alcohol Treatment or Rehabilitation  

ConstructConnect will attempt to reasonably accommodate employees with chemical dependencies (drugs or alcohol), if they voluntarily wish to seek treatment and/or rehabilitation, unless the accommodation imposes an undue hardship on the Company's business operations. The Company's support for treatment and rehabilitation does not obligate the Company to hire or employ any person who violates the Company's drug and alcohol abuse policy or who, because of current use of drugs or alcohol, is unable to perform their duties or cannot perform the duties in a manner that would not endanger the employee’s own health or safety or the health or safety of others.  

The Company will keep all information submitted in connection with an employee's enrollment in a drug or alcohol rehabilitation program confidential to the extent permissible by law. Time off for these purposes is unpaid. However, employees wishing to take such leave may utilize their sick leave or accrued paid time off, if applicable.  

Employees who have questions about this policy or who wish to request a reasonable accommodation under this policy should contact their Human Resources representative.  

### GENERAL EMPLOYMENT PRACTICES  

#### Access to Personnel Files and Payroll Records  

Upon written request, a current or former employee or a designated representative may inspect and receive a copy of the employee's personnel file and records that relate to the employee's performance or to any grievance concerning the employee in the presence of an ConstructConnect representative at a mutually convenient time, at the employee's expense. Employees may add their version of any disputed item to the file. The Company will comply with a written personnel file request at reasonable intervals and reasonable times within 30 calendar days of the written request. The parties may agree to a date beyond 30 calendar days provided it is not longer than 35 calendar days from the employer's receipt of the written request.  

For a current employee, personnel records will be available for inspection where the employee reports to work or at another location that is mutually agreeable. For a former employee, personnel records will be available for inspection where the records are stored or at another location that is mutually agreeable.  

Current and former employees also may inspect their payroll records upon written or oral request, and may request a copy of these records. The Company will comply with written payroll records requests as soon as practicable, but no later than 21 calendar days following the request.  Current and former employees who request a copy of their payroll records may be charged a reasonable fee related to the cost of copying the requested documents.  

Only authorized members of management and Human Resources have access to an employee's personnel file. Only Human Resources is authorized to release information about current or former employees on behalf of the Company. However, the Company will cooperate with—and provide access to an employee's personnel file to—law enforcement officials or local, state or federal agencies in accordance with applicable law, or in response to a subpoena, in accordance with applicable law.  

### TIMES OFF AND LEAVES OF ABSENCE  

#### Vacation/PTO  

The vacation/PTO policy set forth in the Company’s Employee Handbook applies to employees in California except as follows.  

In California, vacation/PTO accrues on a pro-rata basis. Additionally, an employee may accrue up to two times their annual vacation/PTO accrual, at which point accrual ceases. Accrual resumes only when, and to the extent, that the employee’s vacation/PTO leave balance drops down below the maximum. Unused accrued vacation/PTO time will not be cashed out except upon termination. At that time, employees will be paid for unused vacation in accordance with applicable law.  

#### Paid Sick & Safe Time (California)  

The Company provides paid sick and safe time to eligible employees in compliance with California's Healthy Workplaces Healthy Families Act (HWHFA).  

##### Eligibility  

Employees (including full-time, part-time and temporary employees) become eligible for paid sick and safe time once they have worked in California for the Company for 30 days within a year from the start of employment.  
Employees may begin to use their accrued time beginning on their 90th day of employment. Employees who have been employed by the Company for at least 90 days prior to becoming eligible to accrue paid sick and safe time may use such leave immediately upon accrual.  

##### Annual Accrual of Paid Sick and Safe Time  

Eligible employees begin to accrue paid sick and safe time on July 1, 2015, or upon the first day of employment, whichever is later.  

Paid sick and safe time accrues at a rate of one hour for every 30 hours worked, up to a maximum accrual cap of 48 hours or the equivalent of six work days, (based on the employee's work schedule), whichever is greater. The number of hours a nonexempt employee is deemed to work each week will be based on time records and includes all hours worked, including overtime hours. Exempt employees are assumed to work 40 hours per workweek, unless their normal workweek is fewer than 40 hours per week, in which case accrued paid sick and safe time is based upon that normal workweek. Once the maximum accrual cap is reached, employees will not accrue additional paid sick and safe time until their accrual balance falls below the cap.  

Paid sick and safe time may be used in increments of one hour or greater to cover all or just part of a work day.  

An employee's use of paid sick and safe time is limited to 24 hours, or the equivalent of three work days (based on the employee's work schedule), whichever is greater, per calendar year.  

Employees will not accrue paid sick and safe time during unpaid leaves of absence.  

Employees are not required to find an employee to cover their work when they take paid sick and safe time.  

##### Reasons Sick and Safe Time May be Used  

Employees may use paid sick and safe time for themselves and their family members:  

- For diagnosis, care or treatment of an existing medical condition; or  
- For preventive care;   
- Employees may also use paid sick and safe time if the employee is a victim of domestic violence, sexual assault or stalking and time off is needed to:   
	- Obtain or attempt to obtain any relief (e.g., a temporary restraining order, restraining order or other injunctive relief) to help ensure the health, safety or welfare of the victim or the victim’s child;  
	- Seek medical attention for injuries caused by domestic violence, sexual assault or stalking;  
	- Obtain services from a domestic violence shelter, program or rape crisis center as a result of domestic violence, sexual assault or stalking;  
	- Obtain psychological counseling related to an experience of domestic violence, sexual assault or stalking; or  
	- Participate in safety planning and take other actions to increase safety from future domestic violence, sexual assault or stalking, including temporary or permanent relocation.  
	
For purposes of this policy, "family members" include a:  

- Spouse;   
- Biological, adopted or foster child, stepchild, legal ward or a child to whom the employee stands in loco parentis;   
- Biological, adoptive or foster parent, stepparent, or legal guardian of an employee or the employee’s spouse or registered domestic partner or a person who stood in loco parentis when the employee was a minor child;   
- Sibling;   
- Grandparent or grandchild; and   
- Registered domestic partner (as defined by state or local law), as well as the child or parent of a registered domestic partner.  

The definition of "child" applies irrespective of a child's age or dependency status.  

##### Requesting Paid Sick and Safe Time  

When the need for paid sick and safe time use is foreseeable, employees must provide reasonable advance oral or written notice to their supervisor for any absence from work. If the need for paid sick and safe time is unforeseeable, employees must provide notice to their supervisor of the need to use the time as soon as practicable. In all circumstances, employees must specify that the requested time off is for sick or safe time reasons (as opposed to, for example, vacation time), so that the absence may be designated accordingly. Failure to obtain approval as soon as possible after determining the need to take such time may result in discipline.  

##### Rate of Pay for Sick and Safe Time  

For nonexempt employees, pay for sick and safe time is calculated in the same manner as the employee's regular rate of pay for the workweek in which the employee uses sick and safe time, regardless of whether the employee works overtime in that workweek.   For exempt employees, payment for sick and safe time is calculated in the same manner as wages are calculated for other forms of paid leave time.  

##### Carryover  

Accrued but unused paid sick and safe time will carry over from year to year.  

##### Separation From Employment  

Compensation for accrued and unused sick and safe time is not provided upon separation from employment for any reason. If an employee is rehired by the Company within 12 months of separation from employment, previously accrued but unused sick and safe time will immediately be reinstated (up to the maximum of 48 hours or the equivalent of six days (per the employee's previous work schedule)). Rehired employees will be allowed immediate use of this time and to accrue additional paid sick days upon rehiring, consistent with the use and accrual limitations of this policy.  

##### Confidentiality  

The Company will keep confidential the health information of the employee or employee's covered family member, as well as information related to domestic violence perpetrated against or sexual assault of the employee or employee's covered family member. Such information will not be disclosed except to the affected employee or as required by law.  

##### Effect on Other Rights and Policies  

The Company may provide other forms of leave for employees to care for medical conditions or for issues related to domestic violence under certain federal, state and municipal laws. In certain situations, leave under this policy may run at the same time as leave available under another federal, state or municipal law, provided eligibility requirements for that law are met. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other federal, state and municipal domestic violence, medical or family leave rights.  

##### No Discrimination or Retaliation  

The Company prohibits discrimination and/or retaliation against employees who request or use paid sick and safe time for authorized circumstances or for making a complaint or informing a person about a suspected violation of this policy. Likewise, the Company prohibits discrimination and/or retaliation for cooperating with city or state officials in investigating claimed violations of any paid sick leave law (including the HWHFA), cooperating or participating in any investigation, administrative hearing or judicial action regarding an alleged violation, opposing any policy or practice that is prohibited by any paid sick leave law, or informing any person of their potential rights under the law.  

#### Paid Sick Leave (Long Beach)  

The Company provides eligible Long Beach employees with paid sick time in accordance with Long Beach's paid sick leave ordinance.  

The guidelines set forth in this policy do not supersede applicable federal, state or local law regarding leaves of absence, including leave taken under the Family and Medical Leave Act (FMLA) and/or as a reasonable accommodation under the Americans with Disabilities Act (ADA) or Americans with Disabilities Act Amendments Act of 2008 (ADAAA) or any other applicable federal, state or local law, including those prohibiting discrimination and harassment.  

Where applicable and allowed by law, paid sick leave will be counted toward unpaid leave provided under federal, state or local law. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other leave rights.  

##### Eligibility  

All hotel employees working within the city limits of Long Beach are eligible for leave under this policy.  

##### Accrual of Sick Leave  

Eligible employees will be allowed up to five days of paid sick leave per calendar year. Eligible employees accrue paid sick leave at the rate of 5/12 of a day of paid sick time for each full month that they are employed by the Company.  

Employees begin accruing paid sick leave at the beginning of employment and are entitled to use the leave as soon as the days are accrued.  

##### Cash Out  

If an eligible employee has not used all accrued sick leave by the end of any calendar year, the Company will pay a lump sum equivalent to the value of the unused sick leave.  

##### Nondiscrimination  

The Company will not discriminate, or tolerate discrimination, against any employee who seeks or obtains leave in accordance with this policy, who makes a good-faith complaint about a violation of the Long Beach paid sick leave ordinance, who uses any civil remedies to enforce the ordinance or who otherwise asserts rights under the ordinance.  

#### Paid Sick and Safe Leave (San Francisco)  

The Company provides eligible employees with paid sick and safe leave in accordance with the requirements of the San Francisco Paid Sick Leave Ordinance (PSLO).  

##### Eligibility  

All employees (whether full-time, part-time or temporary and including undocumented and household employees) who perform work in San Francisco are eligible to accrue paid sick and safe leave, except for employees who:  

- Perform work in San Francisco for fewer than 56 hours in a calendar year;  
- Work from their home in San Francisco and work fewer than 56 hours in a calendar year; and  
- Are not based in San Francisco but who stop in San Francisco to work (e.g., for a pick-up or delivery), if they perform fewer than 56 hours of work in the City in a calendar year.  

Employees are eligible to accrue paid sick and safe leave only for the hours worked in San Francisco.  

##### Accrual of Sick and Safe Leave  

Eligible employees hired before January 1, 2017 began accruing paid sick and safe leave 90 days after employment began.  Eligible employees hired on or after January 1, 2017 begin accruing paid sick and safe leave on the first day of employment.  

Employees accrue one hour of paid sick and safe leave for every 30 hours worked in San Francisco (excluding vacation or sick and safe leave).  The number of hours a nonexempt employee is deemed to work each week will be based on time records and includes all hours worked, including overtime hours. Exempt employees are assumed to work 40 hours per workweek, unless their normal workweek is fewer than 40 hours per week, in which case accrued paid sick and safe time is based upon that normal workweek. Paid sick and safe leave accrues to a maximum of 72 hours.  

Eligible employees can begin to use their accrued paid sick and safe leave on their 90th day of employment. After that, paid sick and safe leave may be used as soon as it is accrued.  

Accrued but unused sick and safe leave can be carried over from year to year. However, once the maximum amount has been accrued, no further sick and safe leave will accrue until previously accrued sick leave is used.  

Employees will not accrue sick and safe leave during unpaid leaves of absence.  

##### Reasons Sick and Safe Leave May be Used  

Paid sick and safe leave may be used only for the following reasons:  

- When the employee is ill, injured or receiving medical care, treatment or diagnosis;   
- To care for an eligible family member who is ill, injured or receiving medical care, treatment or diagnosis;  
- For the employee’s or a family member’s preventive care;  
- For the employee to donate bone marrow or an organ or to assist a family member in doing so;  
- If the employee is a victim of domestic violence, sexual assault or stalking and time off is needed to:  
	- Obtain or attempt to obtain any relief (e.g., a temporary restraining order, restraining order or other injunctive relief) to help ensure the health, safety or welfare of the victim or their child;  
	- Seek medical attention for injuries caused by domestic violence, sexual assault or stalking;  
	- Obtain services from a domestic violence shelter, program or rape crisis center as a result of domestic violence, sexual assault or stalking;  
	- Obtain psychological counseling related to an experience of domestic violence, sexual assault or stalking; or  
	- Participate in safety planning and take other actions to increase safety from future domestic violence, sexual assault or stalking, including temporary or permanent relocation.  
	
Eligible family members include an employee's child (including child of a domestic partner and child of a person standing in loco parentis), parent (including a person who stood in loco parentis when an employee was a minor child and a person who is biological, adoptive, or foster parent, stepparent, or guardian of the employee’s spouse or registered domestic partner), legal guardian or ward, sibling, grandparent, or grandchild (the relationships may be biological, legal, foster, adoptive or a step-relationship), as well as a spouse or registered domestic partner. If an employee does not have a spouse or registered domestic partner, the employee may designate one person as to whom the employee wishes to use sick leave to aid or care for that person. Designation of this person must be done within 10 workdays of the Company providing the opportunity to make a designation. The Company will provide an opportunity to re-designate a person on an annual basis thereafter.  

Sick and safe leave may not be used for personal reasons and may not be used during holidays, vacations or for hours of work outside an employee's regular schedule. If there is reason to believe that sick and safe leave has been misused, sick pay may not be awarded.  

Sick and safe leave may be used in increments of one hour or greater to cover all or just part of a work day.  

Employees are not required to find an employee to cover their work when they take paid sick and safe leave.  

##### Requesting Sick and Safe Leave/Documentation

Except in the case of an emergency, employees must give reasonable advance notice of any absence from work for which they intend to use paid sick and safe leave. To provide notice of the need to use sick and safe leave, employees should contact their Human Resources representative.  

Upon return, employees must immediately complete a time card (nonexempt employees) or absence report (exempt employees) documenting the use of sick and safe leave.  

##### Rate of Pay for Sick and Safe Leave  

For nonexempt employees, pay for sick and safe leave is calculated in the same manner as the employee's regular rate of pay for the workweek in which the employee uses sick and safe time, regardless of whether the employee works overtime in that workweek.   For exempt employees, payment for sick and safe leave is calculated in the same manner as wages are calculated for other forms of paid leave time.  

##### Integration With Other Benefits  

It is an employee's responsibility to apply for any applicable benefits for which the employee may be eligible as a result of the illness or disability, including California State Disability Insurance, workers' compensation insurance, paid family leave benefits and/or any other disability insurance benefits. If an employee elects to integrate paid sick and safe leave with other paid benefits, the Company will integrate all paid benefits such that an employee will not be paid more than their regular compensation at any time.  

##### Separation From Employment  

Compensation for accrued and unused paid sick and safe leave is not provided upon separation from employment for any reason. If an employee is rehired by the Company within one year from the date of separation, previously accrued but unused sick and safe leave will immediately be reinstated (up to the maximum of 72 hours). Rehired employees will be allowed immediate use of this time and to accrue additional paid sick and safe leave upon rehiring, consistent with the use and accrual limitations of this policy.  

##### Retaliation  

The Company will not retaliate, or tolerate retaliation, against any employee who seeks or obtains sick leave in accordance with this policy, who makes a good-faith complaint about a PSLO violation or who communicates with any person about such a violation. In addition, the Company will not retaliate against any employee who informs another person about the rights under the PSLO.  

#### Paid Sick and Safe Time (Los Angeles)  

The Company provides paid sick and safe time to eligible employees in compliance with the Los Angeles Minimum Wage Ordinance (LAMWO).  

##### Eligibility  

Employees (including full-time, part-time, temporary or seasonal employees) are eligible for paid sick and safe time if they perform at least two hours of work in any particular week within the geographic boundaries of the City of Los Angeles (“Los Angeles”), qualify as an employee entitled to the state minimum wage and have worked for the Company for 30 days or more during any 12-month period which occurs after the start of employment. The 12-month period begins on the first day the employee works in the City of Los Angeles.  

Employees may begin to use their accrued time beginning on July 1, 2016, or their 90th day of employment, whichever is later. Employees who have been employed by the Company for at least 90 days prior to becoming eligible to accrue paid sick and safe time may use such leave immediately upon accrual.  

Employees who are based outside of Los Angeles but travel to and perform work in Los Angeles are required to track the time spent working within Los Angeles city limits.  

##### Annual Accrual of Paid Sick and Safe Time  

Eligible employees begin to accrue paid sick and safe time on July 1, 2016, or upon the first day of employment, whichever is later.  

Paid sick and safe time accrues at a rate of one hour for every 30 hours worked in Los Angeles, up to a maximum accrual cap of 72 hours. Once the maximum accrual cap is reached, employees will not accrue additional paid sick and safe time until their accrual balance falls below the cap.  

An employee’s use of paid sick and safe time is limited to 48 hours per calendar year. Employees based outside of Los Angeles may only use sick and safe time under this policy during times when they are scheduled to perform work in Los Angeles.  

Paid sick and safe time may be used in increments of two hours or greater to cover all or just part of a workday.  

Employees will not accrue paid sick and safe time during unpaid leaves of absence.  

##### Reasons Sick and Safe Time May be Used  

Eligible employees may use paid sick and safe time for themselves and their family members:  

- For diagnosis, care or treatment of an existing medical condition; and   
- For preventive care.  

Eligible employees may also use paid sick and safe time if the employee is a victim of domestic violence, sexual assault or stalking and time off is needed to:  

- Obtain or attempt to obtain any relief (e.g., a temporary restraining order, restraining order or other injunctive relief) to help ensure the health, safety or welfare of the victim or the victim’s child;  
- Seek medical attention for injuries caused by domestic violence, sexual assault or stalking;  
- Obtain services from a domestic violence shelter, program or rape crisis center as a result of domestic violence, sexual assault or stalking;  
- Obtain psychological counseling related to an experience of domestic violence, sexual assault or stalking; or  
- Participate in safety planning and take other actions to increase safety from future domestic violence, sexual assault or stalking, including temporary or permanent relocation.  

For purposes of this policy, “family members” include a:  

- Spouse;   
- Biological, adopted or foster child, stepchild, legal ward or a child to whom the employee stands in loco parentis;   
- Biological, adoptive or foster parent, stepparent, a legal guardian of an employee or the employee’s spouse or registered domestic partner, or a person who stood in loco parentis when the employee was a minor child;   
- Sibling;   
- Grandparent or grandchild;   
- Registered domestic partner (as defined by state or local law); and   
- An individual related to the employee by blood or affinity whose close association with the employee is equivalent to a family relationship.  

The definition of “child” applies regardless of a child’s age or dependency status.  

##### Requesting Paid Sick and Safe Time  

Employees should contact their supervisor to request time off. For foreseeable absences, employees should contact their supervisor at least one hour before the beginning of the employee’s start time. For unforeseeable absences, employees should provide notice of their intention to use sick and safe time as soon as practicable. Employees should specify that the requested time off is for sick and safe time reasons (as opposed to, for example, vacation time), so that the absence may be designated as a paid sick and safe time absence.  

##### Rate of Pay for Sick and Safe Time  

The Company will calculate the regular rate of pay owed to an employee for used sick and safe time based upon one of the following methods:  

- In the same manner as the regular rate of pay for the workweek in which sick time is used (regardless of whether overtime is worked that workweek); or  
- By dividing total wages – excluding overtime premium pay – by total hours worked in the full pay periods of the prior 90 days of employment.  

##### Carryover  

Accrued but unused paid sick and safe time will carry over from year to year but with an overall cap of 72 hours.  Therefore, once an employee has a bank of 72 hours of sick and safe time, no further time will be carried over or accrued until previously accrued sick and safe time is used.  

##### Separation from Employment  

Compensation for accrued and unused sick and safe time is not provided upon separation from employment for any reason. If an employee is rehired by the Company within 12 months of separation from employment, previously accrued but unused sick and safe time will immediately be reinstated (up to the maximum of 72 hours). Rehired employees will be allowed immediate use of this time and to accrue additional paid sick days upon rehiring, consistent with the use and accrual limitations of this policy.  

##### Confidentiality  

The Company will keep confidential the health information of the employee or employee’s covered family member, as well as information related to domestic violence, sexual assault, or stalking perpetrated against the employee. Such information will not be disclosed except to the affected employee or as required by law.  

##### Effect on Other Rights and Policies  

The Company may provide other forms of leave for employees to care for medical conditions or for issues related to domestic violence, sexual assault, or stalking under certain federal, state and municipal laws. In certain situations, leave under this policy may run at the same time as leave available under another federal, state or municipal law, provided eligibility requirements for that law are met. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other federal, state and municipal domestic violence, sexual assault, stalking, medical or family leave rights.  

##### No Discrimination or Retaliation  

The Company prohibits discrimination and/or retaliation against employees for requesting or using paid sick and safe time for authorized circumstances, participating in proceedings related to the LAMWO, opposing any practice prohibited by the LAMWO or seeking to enforce or otherwise assert rights under the LAMWO by lawful means.  In addition, the Company prohibits discrimination and/or retaliation against any employee for making a good-faith complaint or report about alleged noncompliance with the LAMWO, informing any person about their rights under the law or assisting a person in asserting those rights.  

#### Paid Sick and Safe Time (San Diego)  

The Company provides paid sick and safe time to eligible employees in compliance with the City of San Diego Earned Sick Leave and Minimum Wage Ordinance (ESLO).  

##### Eligibility  

Employees (including full-time, part-time and temporary employees) are eligible for paid sick and safe time if they perform at least two hours of work in a calendar week within the geographic boundaries of the City of San Diego (San Diego) and qualify as an employee entitled to the state minimum wage or as a participant in a State of California Welfare-to-Work Program.  

Employees may begin to use their accrued time beginning on July 11, 2016 or their 90th day of employment, whichever is later.  

##### Annual Accrual of Paid Sick and Safe Time  

Eligible employees begin to accrue paid sick and safe time on July 11, 2016, or upon the first day of employment, whichever is later.  

Paid sick and safe time accrues at a rate of one hour for every 30 hours worked in San Diego, up to a maximum accrual cap of 80 hours. One hour of paid sick and safe time will accrue upon completion of the entire 30 hours worked and will not accrue in increments of less than one hour or for fractions of the 30-hour work period.  The number of hours a nonexempt employee is deemed to work each week will be based on time records and include all hours worked, including overtime hours.   Exempt employees are assumed to work 40 hours per workweek, unless their normal workweek is fewer than 40 hours per week, in which case accrued paid sick and safe time is based upon that normal workweek.  

Paid sick and safe time may be used in increments of one hour or greater to cover all or just part of a workday.  

An employee's use of paid sick and safe time is limited to 40 hours per calendar year.  

Employees will not accrue paid sick and safe time during unpaid leaves of absence.  

Employees are not required to find an employee to cover their work when they take paid sick and safe time.  

##### Reasons Sick and Safe Time May be Used  

Employees may use paid sick and safe time for the following reasons:  

- When the employee is ill, injured or receiving medical care, treatment or diagnosis;   
- When the employee requires leave for other medical reasons, such as pregnancy or obtaining a physical examination;   
- To care for or assist an eligible family member who is ill, injured or receiving medical care, treatment or diagnosis of a medical condition; or   
- If the employee’s place of business is closed or the employee is providing care or assistance to a child whose school or childcare provider is closed by order of a public official due to a public health emergency.  

Employees may also use paid sick and safe time if needed because of domestic violence, sexual assault or stalking, so long as the time is used to obtain one or more of the following for the employee or the employee’s family member:  

- Medical attention needed to recover from injury or disability caused by domestic violence, sexual assault or stalking;  
- Services from a victim services organization;   
- Psychological or other counseling;  
- Relocation due to domestic violence, sexual assault or stalking; or  
- Legal services, including preparing for or participating in any civil or criminal legal proceeding related to or resulting from domestic violence, sexual assault or stalking.  

For purposes of this policy, "family members" include a:  

- Spouse;   
- Biological, adopted or foster child, stepchild, legal ward or a child to whom the employee stands in loco parentis;   
- Biological, adoptive or foster parent, stepparent, a legal guardian of an employee or the employee's spouse or registered domestic partner, or a person who stood in loco parentis when the employee was a minor child;   
- Sibling;   
- Grandparent or grandchild; and   
- Registered domestic partner (as defined by state or local law), as well as the child or parent of a registered domestic partner.  

##### Requesting Paid Sick and Safe Time  

When the need for paid sick and safe time use is foreseeable, employees must provide reasonable advance notice to their supervisor at least seven days before the date sick leave will begin. If the need for paid sick and safe time is unforeseeable, employees must provide notice to their supervisor of the need to use the time as soon as practicable. In all circumstances, employees must specify that the requested time off is for sick or safe time reasons (as opposed to, for example, vacation time), so that the absence may be designated accordingly.  

##### Rate of Pay for Sick and Safe Time  

For nonexempt employees, sick and safe time will be paid at the regular rate of pay for the work week in which the employee uses the leave.  For exempt employees, sick and safe time will be paid at the same rate or in the same manner used to calculate compensation for paid working time.  

##### Carryover  

Accrued but unused paid sick and safe time will carry over from year to year.  

##### Separation From Employment  

Compensation for accrued and unused sick and safe time is not provided upon separation from employment for any reason. If an employee is rehired by the Company within six months of separation from employment, previously accrued but unused sick and safe time will immediately be reinstated.  Rehired employees will be allowed immediate use of this time and to accrue additional paid sick days upon rehiring, consistent with the use limitations of this policy.  

##### Confidentiality  

The Company will keep confidential the medical or other personal information about an employee or employee's covered family member. Such information will not be disclosed except with the permission of the affected employee or as required by law.  

##### Effect on Other Rights and Policies  

The Company may provide other forms of leave for employees to care for medical conditions or for issues related to domestic violence under certain federal, state and municipal laws. In certain situations, leave under this policy may run at the same time as leave available under another federal, state or municipal law, provided eligibility requirements for that law are met. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other federal, state and municipal domestic violence, medical or family leave rights.  

##### No Discrimination or Retaliation  

The Company will not retaliate, or tolerate retaliation, against any employee who seeks or obtains sick and safe time in accordance with this policy, who makes a good-faith complaint about a possible ESLO violation or who communicates with any person about such a violation. In addition, the Company will not retaliate against any employee who informs another person about their potential rights under the ESLO.  

#### Paid Sick Leave (Oakland)  

The Company provides eligible employees with paid sick time in accordance with the requirements of the Oakland Minimum Wage Ordinance (OMWO).  

The guidelines set forth in this policy do not supersede applicable federal, state or local law regarding leaves of absence, including leave taken under the Family and Medical Leave Act (FMLA) or the California Family Rights Act (CFRA) and/or as a reasonable accommodation under the Americans with Disabilities Act (ADA), Americans with Disabilities Act Amendments Act of 2008 (ADAAA) or California Fair Employment and Housing Act (FEHA), or any other applicable federal, state or local law, including those prohibiting discrimination and harassment.  

##### Eligibility  

All employees (whether full-time, part-time or temporary and including household employees) who perform at least two hours of work in Oakland in a workweek are eligible to accrue paid sick leave.  

Employees are eligible to accrue paid sick leave only for the hours worked in Oakland.  

##### Accrual and Use of Sick Leave  

Eligible employees begin accruing paid sick leave upon the first day of employment, but may not use it until after 90 calendar days of employment with the Company. Paid sick time accrues at a rate of one hour for every 30 hours worked in Oakland, up to a maximum of 72 hours. The number of hours a nonexempt employee is deemed to work each week will be based on time records and includes all hours worked, including overtime hours. Exempt employees are assumed to work 40 hours per workweek, unless evidence exists that their normal workweek is fewer than 40 hours per week, in which case accrued paid sick and safe time is based upon that particular workweek.  Once the maximum accrual cap is reached, employees will not accrue additional paid sick time until their accrual balance falls below the cap.  

Accrued but unused sick leave can be carried over from year to year. However, once the maximum amount has been accrued, no further sick leave will accrue until previously accrued sick leave is used. Employees will not accrue sick leave during unpaid leaves of absence.  

##### Reasons Sick Leave May be Used  

Paid sick leave can be used only for the following reasons:  

- When an employee is physically or mentally unable to perform their duties due to illness, injury, pregnancy or medical condition;  
- To obtain a professional diagnosis or treatment of a medical condition or undergo a physical examination; and  
- To aid or care for a child, parent, legal guardian or ward, sibling, grandparent, grandchild, spouse, registered domestic person or a “Designated Person” who is ill, injured, or receiving medical care, treatment or diagnosis.  

A family member includes an employee's child, parent, legal guardian or ward, sibling, grandparent, grandchild, spouse or registered domestic partner (the relationship may be biological, foster, adoptive or a step-relationship.) “Child” includes a child of a domestic partner and a child of a person standing in loco parentis.  If an employee does not have a spouse or registered domestic partner, the employee may designate one person for whom the employee wishes to use sick leave to aid or provide care for that person. Designation of this person must be done within 10 workdays of the Company providing the opportunity to make a designation. The Company will provide an opportunity to re-designate a person on an annual basis thereafter.  

Sick leave can be used only for the reasons identified above and only for hours when an employee is scheduled to work in Oakland. If there is reason to believe that sick pay has been misused, sick pay may not be awarded.  

Sick leave may be used in increments of one hour or greater to cover all or just part of a work day.  

Employees are not required to find an employee to cover their work when they take paid sick leave.  

##### Requesting Sick Leave/Documentation  

Except in the case of an emergency, employees must give reasonable advance notice of any absence from work for which they intend to use paid sick leave. To provide notice of the need to use sick time, employees should contact their Human Resources representative.  

Upon return, employees must immediately complete a time card (nonexempt employees) or absence report (exempt employees) documenting the use of sick time.  

For absences of three or more consecutive days, the Company reserves the right to request documentation of the qualifying use of paid sick leave, as permitted under applicable local, state, or federal law. Additionally, if the Company reasonably suspects an employee is abusing this policy, it may require a doctor’s note for subsequent use of paid sick leave, even if the employee’s use of paid sick leave was for fewer than three consecutive workdays.  

##### Rate of Pay for Sick and Safe Leave  

For nonexempt, hourly employees, sick leave will be paid at the employee's regular hourly rate of pay at the time the employee uses the leave. For exempt employees, payment will be calculated by dividing the employee’s weekly salary by 40 hours, unless evidence suggests that the employee’s regular workweek is less than 40 hours.  In that case, pay will be calculated by dividing the employee’s weekly salary by the number of hours during their regular workweek.  

##### Integration with Other Benefits  

It is an employee's responsibility to apply for any applicable benefits for which the employee may be eligible as a result of the illness or disability, including California State Disability Insurance, workers' compensation insurance, paid family leave benefits and/or any other disability insurance benefits. If an employee elects to integrate paid sick leave with other paid benefits, the Company will integrate all paid benefits such that an employee will not be paid more than their regular compensation at any time.  

##### Separation from Employment  

Compensation for accrued and unused paid sick time is not provided upon separation from employment for any reason.  

##### Confidentiality  

The Company will keep confidential any medical documentation regarding leave use, in accordance with federal, state and local law.  

##### Effect on Other Rights and Policies  

The Company may provide other forms of leave for employees to care for medical conditions or for issues related to domestic violence under certain federal, state and municipal laws. In certain situations, leave under this policy may run at the same time as leave available under another federal, state, or municipal law, provided eligibility requirements for that law are met. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other federal, state, and municipal domestic violence, medical, or family leave rights.  

##### No Discrimination or Retaliation  

The Company will not retaliate, or tolerate retaliation, against any employee who seeks or obtains sick leave in accordance with this policy and/or the law, who makes a good-faith complaint about an OMWO violation, participates in any proceedings, uses any civil remedies to enforce their rights or otherwise asserts rights under the OMWO.  

#### Paid Sick Leave (Berkeley)  

The Company provides eligible employees with paid sick leave in accordance with the requirements of the Berkeley Paid Sick Leave Ordinance (BPSLO).  

##### Eligibility  

Employees, (including full-time, part-time and temporary employees) are eligible for paid sick leave if they: (1) perform at least two hours of work for the Company per week within the geographic boundaries of the City of Berkeley; and (2) qualify as an employee entitled to the state minimum wage or are a participant in a Welfare-to-Work program.  

Employees are eligible to accrue paid sick leave only for the hours worked in Berkeley.  

##### Accrual of Sick Leave  

Employees hired before October 1, 2017 who are covered by California's Healthy Workplaces, Healthy Families Act (HWHFA) will continue to accrue and be permitted to use paid sick leave in accordance with the HWHFA and the Paid Sick and Safe Time policy that appears in this California Handbook Supplement.  Employees who are not covered by California’s HWHFA begin to accrue paid sick leave on October 1, 2017 or the first day of employment, whichever occurs later.  

Employees accrue one hour of paid sick leave for every 30 hours worked in Berkeley, up to a maximum of 72 hours. Leave accrues in one-hour increments.  

Eligible employees can begin to use their accrued paid sick leave 90 days after the first day of employment. After that, paid sick leave may be used as soon as it is accrued.  

Employees will not accrue paid sick leave during unpaid leaves of absence.  

##### Reasons Sick Leave May be Used  

Paid sick leave can be used only for the following reasons:  

- When the employee is ill, injured or receiving medical care, treatment or diagnosis;  and  
- To care for an eligible family member who is ill, injured or receiving medical care, treatment or diagnosis.  

A family member includes an employee's child, parent, legal guardian or ward, sibling, grandparent, grandchild, spouse or registered domestic partner or designated person (the relationship may be biological, foster, adoptive or a step-relationship.) "Child" includes a child of a domestic partner and a child of a person standing in loco parentis. If an employee does not have a spouse or registered domestic partner, the employee may designate one person for whom the employee wishes to use sick leave to aid or provide care for that person. Designation of this person must be done within 10 workdays of the Company providing the opportunity to make a designation. Thereafter, the Company will provide an opportunity to re-designate a person on an annual basis with a window of 10 workdays.  

Employees are not required to find an employee to cover their work when they take paid sick leave.  

##### Requesting Paid Sick Leave  

When the need for paid sick leave is foreseeable, employees must give reasonable advance notice of their intention to use paid sick leave. For unforeseeable absences, employees should provide notice of their intention to use paid sick leave as soon as practicable.  To provide notice of the need to use paid sick leave, employees should contact their Human Resources representative.  

##### Rate of Pay  

Sick leave will be paid at the employee’s hourly wage. For salaried non-exempt employees, the hourly rate will be calculated by dividing the employee's total wages (not including overtime premium pay), by the employee's total hours worked in the full pay periods of the prior 90 days of employment.  

##### Carryover  

Accrued but unused sick leave will carry over from year to year but with an overall cap of 72 hours.  Therefore, once an employee has a bank of 72 hours of paid sick leave, no further time will be carried over or accrued until previously accrued paid sick leave is used.  

##### Effect on Other Rights and Policies  

The Company may provide other forms of leave for employees to care for medical conditions or for family members under certain federal, state and municipal laws. In certain situations, leave under this policy may run at the same time as leave available under another federal, state, or municipal law, provided eligibility requirements for that law are met. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other federal, state, and municipal medical or family leave rights.  

##### Integration With Other Benefits  

It is an employee's responsibility to apply for any applicable benefits for which the employee may be eligible as a result of the illness or disability, including California State Disability Insurance, workers' compensation insurance, paid family leave benefits and/or any other disability insurance benefits. If an employee elects to integrate paid sick leave with other paid benefits, the Company will integrate all paid benefits such that an employee will not be paid more than their regular compensation at any time.  

##### Separation From Employment  

Compensation for accrued and unused paid sick leave is not provided upon separation from employment for any reason.  

##### Retaliation  

The Company will not retaliate, or tolerate retaliation, against any employee who seeks or obtains sick leave in accordance with this policy, who makes a good-faith complaint about a possible BPSLO violation or who communicates with any person about such a violation. In addition, the Company will not retaliate against any employee who informs another person about their rights under the BPSLO.  

#### Family and Medical Leave (50 or more employees)   

The Company will grant family and medical leave in accordance with the requirements of applicable federal and state law in effect at the time the leave is granted. Although the federal and state laws have different names, the Company refers to the federal Family and Medical Leave Act (Fed-FMLA) and the California Family Rights Act (CFRA) collectively as "FMLA Leave." In any case, employees will be eligible for the most generous benefits available under applicable law.  

##### Employee Eligibility  

To be eligible for FMLA Leave, employees must: (1) have been employed by the Company for a total of at least 12 months (not necessarily consecutive); (2) have worked at least 1,250 hours during the previous 12 months immediately prior  to the start of the leave; and (3) (Fed-FMLA only) have worked at a location where at least 50 employees are employed by the Company within 75 miles of the employee’s worksite, as of the date the leave is requested. Eligibility requirements may differ for employees who have been on a protected military leave of absence. If employees are unsure whether they qualify for FMLA Leave, they should contact Human Resources.  

##### Reasons for Leave  

Federal and state laws allow FMLA Leave for various reasons. Because employees' legal rights and obligations may vary depending upon the reason for the FMLA Leave, it is important to identify the purpose or reason for the leave. Fed-FMLA leave and CFRA leave run concurrently except for the following reasons: to care for a child without regard to age or dependency status, registered domestic partner,  a child of a registered domestic partner, grandparent, grandchild, or sibling (CFRA only), incapacity due to pregnancy or prenatal care as a serious health condition (Fed-FMLA only), qualifying exigency leave as defined under the FMLA (Fed-FMLA only), qualifying exigency leave as defined under the CFRA (CFRA only) and military caregiver leave (Fed-FMLA only). Additionally, CFRA coverage for an employee’s own serious health condition that also constitutes a disability under the California’s Fair Employment and Housing Act (FEHA) is separate and distinct from FEHA protections. If the employee cannot return to work at the expiration of the CFRA leave, the Company will engage the employee in the interactive process to determine whether an extension of the leave would be a reasonable accommodation under the FEHA.
FMLA Leave may be used for one of the following reasons:  

- The birth, adoption or foster care of an employee's child within 12 months following birth or placement of the child (Bonding Leave);  
- To care for an immediate family member (spouse, child, parent and for CFRA Leave: registered domestic partner, child of a registered domestic partner, grandparent, grandchild, or sibling) with a serious health condition (Family Care Leave);  
- An employee's inability to work because of a serious health condition (Serious Health Condition Leave);  
- A "qualifying exigency," as defined under the FMLA, arising from a spouse's, child's or parent's "covered active duty" as a member of the military reserves, National Guard or Armed Forces or as defined under the CFRA, related to the covered active duty or call to covered active duty of an employee's spouse, domestic partner, child, or parent in the Armed Forces of the United States (Qualifying Exigency Leave); or  
- To care for a spouse, child, parent or next of kin (nearest blood relative) who is a "Covered Servicemember" (Military Caregiver Leave).  

##### Definitions  

- **"Child,"** for purposes of Bonding Leave and Family Care Leave, means a biological, adopted or foster child; a stepchild; a legal ward; or a child of a person standing in loco parentis, and for Fed-FMLA only, who is either under age 18, or age 18 or older and incapable of self-care because of a mental or physical disability, at the time that FMLA Leave is to commence. "Child," for purposes of Qualifying Exigency Leave and Military Caregiver Leave, means a biological, adopted or foster child; stepchild; legal ward; or a child for whom the person stood in loco parentis, and who is of any age.  
- **"Parent,"** for purposes of this policy, means a biological, adoptive, step or foster father or mother, or any other individual who stood in loco parentis to the person. This term does not include parents-in-law. For Qualifying Exigency Leave taken to provide care to a parent of a deployed military member, the parent must be incapable of self-care as defined by the FMLA.  
- **"Covered Active Duty"** means (1) in the case of a member of a regular component of the Armed Forces, duty during the deployment of the member with the Armed Forces to a foreign country; and (2) in the case of a member of a reserve component of the Armed Forces, duty during the deployment of the member with the Armed Forces to a foreign country under a call or order to active duty (or notification of an impending call or order to active duty) in support of a contingency operation as defined by applicable law.  
- **"Covered Servicemember"** means (1) a member of the Armed Forces, including a member of a reserve component of the Armed Forces, who is undergoing medical treatment, recuperation or therapy; is otherwise in outpatient status; or is otherwise on the temporary disability retired list, for a serious injury or illness incurred or aggravated in the line of duty while on active duty that may render the individual medically unfit to perform his or her military duties; or (2) a person who, during the five years prior to the treatment necessitating the leave, served in the active military, Naval or Air Service, and who was discharged or released under conditions other than dishonorable (a "veteran" as defined by the Department of Veteran Affairs), and who has a qualifying injury or illness incurred or aggravated in the line of duty while on active duty that manifested itself before or after the member became a veteran. For purposes of determining the five-year period for covered veteran status, the period between October 28, 2009, and March 8, 2013, is excluded.  
- **"Spouse"** means a husband or wife. Husband or wife refers to the other person with whom an individual entered into marriage as defined or recognized under state law in the state in which the marriage was entered into or, in the case of a marriage entered into outside of any state, if the marriage is valid in the place where entered into and could have been entered into in at least one state. This definition includes an individual in a same-sex or common law marriage that either (1) was entered into in a state that recognizes such marriages; or (2) if entered into outside of any state, is valid in the place where entered into and could have been entered into in at least one state. For purposes of CFRA leave, a spouse includes a registered domestic partner or same-sex partners in marriage.  
- **"Key employee"** means a salaried Fed-FMLA Leave eligible employee who is among the highest paid 10 percent of all the employees employed by the employer within 75 miles of the employee's worksite at the time of the Fed-FMLA leave request.  
- **"Serious health condition"** means an illness, injury, impairment or physical or mental condition that involves either:    
	- Inpatient care (including, but not limited to, substance abuse treatment) in a hospital, hospice or residential medical care facility, including any period of incapacity (that is, inability to work, attend school or perform other regular daily activities) or any subsequent treatment in connection with this inpatient care; or  
	- Continuing treatment (including, but not limited to, substance abuse treatment) by a health care provider that includes one or more of the following:  
		- A period of incapacity (that is, inability to work, attend school or perform other regular daily activities due to a serious health condition, its treatment or the recovery that it requires) of more than three consecutive calendar days, and any subsequent treatment or period of incapacity relating to the same condition, that also involves treatment two or more times via an in-person visit to a health care provider, or at least one visit to a health care provider that results in a regimen of continuing treatment under the supervision of the health care provider.  
		- Any period of incapacity due to pregnancy or prenatal care (under the Fed-FMLA, but not the CFRA).  
		- Any period of incapacity or treatment for incapacity due to a chronic serious health condition that requires periodic visits to a health care provider, continues over an extended period of time and may cause episodic incapacity.  
		- A period of incapacity that is permanent or long-term due to a condition for which treatment may not be effective, such as Alzheimer's, a severe stroke and the terminal stages of a disease.  
		- Any period of absence to receive multiple treatments (including any period of recovery) by a health care provider either for (a) restorative surgery after an accident or other injury; or (b) a condition that would likely result in a period of incapacity of more than three consecutive calendar days in the absence of medical intervention or treatment.  
- **"Serious injury or illness"** in the case of a current member of the Armed Forces, National Guard or Reserves is an injury or illness incurred by a covered servicemember in the line of duty on active duty (or that preexisted the member's active duty and was aggravated by service in the line of duty on active duty) in the Armed Forces that may render him or her medically unfit to perform the duties of his or her office, grade, rank or rating. In the case of a covered veteran, "serious injury or illness" means an injury or illness that was incurred in the line of duty on active duty (or existed before the beginning of the member's active duty and was aggravated by service in line of duty on active duty) and that manifested itself before or after the member became a veteran.  
- **"Qualifying exigency"** for Fed-FMLA is defined by the Department of Labor and for CFRA is defined by the California Unemployment Insurance Code and generally includes events related to short-notice deployment, military ceremonies, support and assistance programs, changes in childcare, school activities, financial and legal arrangements, counseling and post-deployment activities. Qualifying Exigency Leave may also be used to spend up to 15 days with military members who are on short-term, temporary, rest and recuperation leave during their period of deployment.  

##### Length of Leave  

If the reason for leave is common to both Fed-FMLA and CFRA and, therefore, running concurrently, the maximum amount of FMLA Leave will be 12 workweeks in any 12-month period.  If the reason for leave is not common to both Fed-FMLA and CFRA and, therefore, not running concurrently, then an eligible employee may be entitled to additional leave under applicable law.  

The applicable "12-month period" utilized by the Company is the rolling 12-month period measured backward from the date an employee uses his/her FMLA leave.  

The maximum amount of Fed-FMLA Leave for an employee wishing to take Military Caregiver Leave will be a combined leave total of 26 workweeks in a single 12-month period. A "single 12-month period" begins on the date of the employee's first use of such leave and ends 12 months after that date.  

If both spouses work for the Company and are eligible for leave under this policy, under the Fed-FMLA, the spouses will be limited to a total of 26 workweeks off between the two when the leave is for Military Caregiver Leave only or is for a combination of Military Caregiver Leave, Bonding Leave and/or Family Care Leave taken to care for a parent.  

When CFRA leave is for the birth or placement of a child and both parents work for the Company, they will each be allowed up to 12 weeks of CFRA leave within 12 months of the child’s birth or placement.  

To the extent required by law, leave beyond an employee's FMLA Leave entitlement will be granted when the leave is necessitated by an employee's work-related injury or illness, a pregnancy-related disability or a "disability" as defined under the Americans with Disabilities Act (ADA) and/or the Fair Employment and Housing Act (FEHA).  When the reason for CFRA leave was the employee’s serious health condition, which also constitutes a “disability” under the FEHA and the employee cannot return to work at the conclusion of the CFRA leave, the Company will engage in an interactive process to determine whether an extension of leave would constitute a reasonable accommodation under the FEHA.  

##### Intermittent or Reduced Schedule Leave  

Under some circumstances, employees may take FMLA Leave intermittently, which means taking leave in blocks of time or reducing the employee's normal weekly or daily work schedule. An employee may take leave intermittently or on a reduced schedule whenever it is medically necessary to care for the employee’s child, parent or spouse with a serious health condition or because the employee has a serious health condition. The medical necessity of the leave must be determined by the health care provider of the person with the serious health condition.  

Intermittent or reduced schedule leave may also be taken for absences where the employee or his or her family member is incapacitated or unable to perform the essential functions of the job because of a chronic serious health condition, even if the person does not receive treatment by a health care provider.  

Leave due to military exigencies may also be taken on an intermittent basis.  

Leave taken intermittently may be taken in increments of no less than one hour. Employees who take leave intermittently or on a reduced work schedule basis for planned medical treatment must make a reasonable effort to schedule the leave so as not to unduly disrupt the Company's operations. Please contact Human Resources prior to scheduling medical treatment. If FMLA Leave is taken intermittently or on a reduced schedule basis due to planned medical treatment, we may require employees to transfer temporarily to an available alternative position with an equivalent pay rate and benefits, including a part-time position, to better accommodate recurring periods of leave.  

If an employee using intermittent leave or working a reduced schedule finds it physically impossible to start or stop work mid-way through a shift in order to take CFRA leave and  is therefore forced to be absent for the entire shift, the entire period will be counted against the employee’s CFRA entitlement.  However, if there are other aspects of work that the employee is able to perform that are not physically impossible, then the employee will be permitted to return to work, thereby reducing the amount of time to be charged to the employee’s CFRA entitlement.  

##### Notice and Certification  

**Bonding, Family Care, Serious Health Condition and Military Caregiver Leave Requirements**

Employees are required to provide:  

- When the need for the leave is foreseeable, 30 days' advance notice or such notice as is both possible and practical if the leave must begin in fewer than 30 days (normally this would be the same day the employee becomes aware of the need for leave or the next business day);  
- When the need for leave is not foreseeable, notice within the time prescribed by the Company's normal absence reporting policy, unless unusual circumstances prevent compliance, in which case notice is required as soon as is otherwise possible and practical;  
- When the leave relates to medical issues, a completed Certification of Health Care Provider form within 15 calendar days (for Military Caregiver Leave, an invitational travel order or invitational travel authorization may be submitted in lieu of a Certification of Health Care Provider form);  
- Periodic recertification (as allowed by law); and  
- Periodic reports during the leave.  

In addition to other notice provisions, employees requesting leave for CFRA qualifying reasons must respond to any questions designed to determine whether an absence is potentially qualifying for leave under this policy. Failure to respond to permissible inquiries regarding the leave request may result in denial of CFRA leave protections. Similarly, an employee or the employee’s spokesperson may be required to provide additional information needed to determine whether a requested leave qualifies for Fed-FMLA protections.  An employee’s failure to adequately explain the reason for the leave may result in the denial of Fed-FMLA protections.  

Certification forms are available from Human Resources. At the Company's expense, we may require a second or third medical opinion regarding the employee's own serious health condition for Fed-FMLA purposes and, for CFRA purposes, the employee's own serious health condition or the serious health condition of an employee's family member. In limited cases, we may require a second or third opinion regarding the injury or illness of a Covered Servicemember. Employees are expected to cooperate with the Company in obtaining additional medical opinions that we may require.  

When leave is for planned medical treatment, employees must try to schedule treatment so as not to unduly disrupt the Company's operation. Please contact Human Resources prior to scheduling planned medical treatment.  

If an employee does not produce the certification as requested, the FMLA leave will not be protected.  

##### Recertification After Grant of Leave  

In addition to the requirements listed above, if an employee's Fed-FMLA leave is certified, the Company may later require medical recertification in connection with an absence that the employee reports as qualifying for Fed-FMLA leave. For example, the Company may request recertification if (1) the employee requests an extension of leave; (2) the circumstances of the employee's condition as described by the previous certification change significantly (e.g., employee absences deviate from the duration or frequency set forth in the previous certification; employee's condition becomes more severe than indicated in the original certification; employee encounters complications); or (3) the Company receives information that casts doubt upon the employee's stated reason for the absence. In addition, the Company may request recertification in connection with an absence after six months have passed since the employee's original certification, regardless of the estimated duration of the serious health condition necessitating the need for leave. Any recertification requested by the Company will be at the employee's expense.  

In addition to the requirement listed above, a recertification under the CFRA may only be requested at the expiration of the time period in the original certification for time off for the employee's own serious health condition.  

If an employee does not produce the recertification as requested, the leave will not be CFRA protected.  

##### Qualifying Exigency Leave Requirements  

Employees are required to provide:  

- As much advance notice as is reasonable and practicable under the circumstances;  
- A copy of the covered servicemember's active duty orders when the employee requests leave and/or documentation (such as Rest and Recuperation leave orders) issued by the military setting forth the dates of the servicemember's leave; and  
- A completed Certification of Qualifying Exigency form within 15 calendar days, unless unusual circumstances exist to justify providing the form at a later date.  

Certification forms are available from Human Resources.  

##### Failure to Provide Notice or Certification and to Return From Leave  

Absent unusual circumstances, failure to comply with these notice and certification requirements may result in a delay or denial of the leave. If an employee fails to return to work at the leave's expiration and has not obtained an extension of the leave, the Company may presume that the employee does not plan to return to work and has voluntarily terminated his or her employment.  

##### Compensation During Leave  

Generally, FMLA Leave is unpaid. However, employees may be eligible to receive benefits through state-sponsored programs or the Company's sponsored wage-replacement benefit programs. Employees may also choose to use accrued vacation and sick leave, to the extent permitted by law and the Company's policy. If employees elect to have wage-replacement benefits and accrued paid leave integrated, the integration will be arranged such that employees will receive no greater compensation than their regular compensation during this period. The Company may require employees to use accrued vacation/PTO to cover some or all of a Fed-FMLA Leave. However, the Company will only require employees to use accrued vacation/PTO if the CFRA leave is otherwise unpaid. The CFRA leave is not unpaid if the employee is receiving state disability insurance, short or long term disability payments pursuant to an employer provided plan, or is receiving Paid Family Leave through the state. The use of paid benefits will not extend the length of FMLA Leave.  

##### Benefits During Leave  

The Company will continue making contributions to employees' group health benefits during their leave on the same terms as if the employees had continued to actively work. This means that if employees want their benefits coverage to continue during their leave, they must also continue to make the same premium payments that they are now required to make for themselves or their dependents. Employees taking leave for a reason that is common to both Fed-FMLA and CFRA and, therefore, leave is running concurrently, will generally be provided with group health benefits for a 12-workweek period. When employees take leave for a reason that is not common to both Fed-FMLA and CFRA and, therefore, leave is running consecutively, the Company will continue the employee’s health insurance benefits for up to a maximum of 12 workweeks in a 12-month period during each applicable leave.  Employees taking Military Caregiver Leave may be eligible to receive group health benefits coverage for up to a maximum of 26 workweeks. In some instances, the Company may recover premiums it paid on an employee's behalf to maintain health coverage if the employee fails to return to work following FMLA Leave.  

An employee's length of service will remain intact, but benefits such as vacation and sick leave may not accrue while on an unpaid FMLA Leave.  

##### Job Reinstatement  

Under most circumstances, employees will be reinstated to the same position they held at the time of the leave or to an equivalent position with equivalent pay, benefits and other terms and conditions of employment. If an employee becomes unqualified during CFRA leave as a result of not attending a necessary course, or renewing a license, the employee will be given a reasonable opportunity to fulfill those conditions upon returning to work. Further, the Company may grant an employee’s request to work a different shift, in a different or better position, or in a different location, that is better suited to the employee’s personal needs upon returning from CFRA leave. The Company will also consider a reasonable accommodation under the FEHA if the employee is returning from CFRA leave for his or her own serious health condition.  However, employees have no greater right to reinstatement than if they had been continuously employed rather than taken leave. For example, if an employee would have been laid off or his or her position would have been eliminated even if he or she had not gone on leave, then the employee will not be entitled to reinstatement. However, if an employee has been replaced or the employee’s position was restructured to accommodate the employee absence, the employee is entitled to reinstatement.  

For Fed-FMLA purposes only, key employees may be subject to reinstatement limitations in some circumstances. If employees are considered a "key employee," those employees will be notified of the possible limitations on reinstatement at the time the employee requests a leave of absence, or when leave begins, if earlier.  

##### Confidentiality  

Documents relating to medical certifications, recertifications or medical histories of employees or employees' family members will be maintained separately and treated as confidential medical records, except that in some legally recognized circumstances, the records (or information in them) may be disclosed to supervisors and managers, first aid and safety personnel or government officials.  

##### Fraudulent Use of FMLA Leave Prohibited   

An employee who fraudulently obtains FMLA Leave from the Company is not protected by the Fed-FMLA's or the CFRA's job restoration or maintenance of health benefits provisions. In addition, the Company will take all available appropriate disciplinary action against an employee due to such fraud.  

##### Nondiscrimination  

The Company takes its FMLA Leave obligations very seriously and will not interfere with, restrain or deny the exercise of any rights provided by the Fed-FMLA or the CFRA. We will not terminate or discriminate against any individual for opposing any practice or because of involvement in any proceeding related to the Fed-FMLA or CFRA. If an employee believes that his or her Fed-FMLA or CFRA rights have been violated in any way, he or she should immediately report the matter to Human Resources.  

##### Additional Documentation  

The Company's "Employee Rights and Responsibilities" notice provides additional details regarding employees' rights and responsibilities under the Fed-FMLA. Employees may obtain a copy of the "Employee Rights and Responsibilities" notice from Human Resources.  

Employees should contact Human Resources as to any Fed-FMLA or CFRA questions they may have.  

#### Pregnancy and Pregnancy-Related Disabilities Leave and Accommodation  

##### Pregnancy Disability Leave  

Any employee who is disabled by pregnancy, childbirth or a related medical condition (including medical conditions relating to lactation) is eligible for up to four months of pregnancy disability leave. If an employee is also eligible for leave under the federal Family and Medical Leave Act (Fed-FMLA), the Fed-FMLA leave and the pregnancy disability leave will run concurrently.  

For purposes of this policy, employees are "disabled by pregnancy" when, in the opinion of their health care provider, they cannot work at all or are unable to perform any one or more of the essential functions of their job or to perform them without undue risk to themselves, the successful completion of their pregnancy or other persons as determined by a health care provider. The term "disabled" also applies to certain pregnancy-related conditions, such as severe morning sickness or the need to take time off for prenatal or postnatal care, bed rest, post-partum depression and the loss or end of pregnancy (among other pregnancy-related conditions that are considered to be disabling).  

##### Reasonable Accommodation for Pregnancy-Related Disabilities  

Any employee who is affected by pregnancy may also be eligible for a temporary transfer or another accommodation. Employees are "affected by pregnancy" if they are pregnant or have a related medical condition and their health care provider has certified that it is medically advisable for the employee to temporarily transfer or to receive some other accommodation.  

The Company will provide a temporary transfer to a less-strenuous or -hazardous position or duties or other accommodation to an employee affected by pregnancy if:  

- The employee requests a transfer or other accommodation;  
- The request is based upon the certification of a health care provider as "medically advisable"; and  
- The transfer or other requested accommodation can be reasonably accommodated pursuant to applicable law.  

No additional position will be created, and the Company will not terminate another employee, transfer another employee with more seniority, or promote or transfer any employee who is not qualified to perform the new job as a part of the accommodation process.  

Examples of reasonable accommodations include:  

1. modifying work schedules to provide earlier or later hours;  
1. modifying work duties, practices or policies;  
1. providing time off;  
1. providing furniture (such as stools) and modifying equipment and devices; and  
1. providing additional break time for lactation or trips to the restroom. If time off or a reduction in hours is granted as a reasonable accommodation, the Company will consider the reduced hours/time off as pregnancy disability leave and deduct those hours from an employee's four-month leave entitlement.  

##### Advance Notice and Medical Certification  

To be approved for a pregnancy disability leave of absence, a temporary transfer or other reasonable accommodation, employees must provide the Company with:  

- 30 days' advance notice before the leave of absence, transfer or reasonable accommodation is to begin, if the need is foreseeable;  
- As much notice as is practicable before the leave, transfer or reasonable accommodation when 30 days' notice is not possible; and  
- A signed medical certification from their health care provider that states that they are disabled due to pregnancy or that it is medically advisable for them to be temporarily transferred or to receive some other requested accommodation.  

The Company may require employees to provide a new certification if they request an extension of time for their leave, transfer or other requested accommodation.  

Failure to provide the Company with reasonable advance notice may result in the delay of leave, transfer or other requested accommodation.  

##### Duration  

The Company will provide employees with pregnancy disability leave for a period not to exceed four months. The four months is defined as the number of days (and hours) the employee would normally work within four calendar months or 17.33 workweeks. This leave may be taken intermittently or on a continuous basis, as certified by the employee's health care provider.  

The Company may require an employee to temporarily transfer to an available alternative position to meet the medical need of the employee to take intermittent leave or work on a reduced schedule as certified by the employee's health care provider. The employee must be qualified for the alternative position, which will have an equivalent rate of pay and benefits, but not necessarily equivalent job duties.  

Any temporary transfer or other reasonable accommodation provided to an employee affected by pregnancy will not reduce the amount of pregnancy disability leave time the employee has available unless the temporary transfer or other reasonable accommodation involves a reduced work schedule or intermittent absences from work.  

The length of the transfer or other accommodation will depend upon the period of time for which it is medically advisable.  

##### Benefits  

The Company will maintain an employee's health insurance benefits during an employee's pregnancy disability leave for a period of up to four months (as defined above) on the same terms as they were provided prior to the leave time. If employees take additional time off following a pregnancy disability leave that qualifies as leave under the California Family Rights Act (CFRA) the Company will continue their health insurance benefits for up to a maximum of 12 workweeks in a 12-month period.  

In some instances, the Company may recover premiums it paid to maintain health insurance benefits if an employee fails to return to work following pregnancy disability leave for reasons other than taking additional leave afforded by law or Company policy or not returning due to circumstances beyond the employee's control.  

##### Integration With Other Benefits  

Pregnancy disability leaves and accommodations that require employees to work a reduced work schedule or to take time off from work intermittently are unpaid. Employees may use their accrued vacation, sick or other paid time off (PTO) benefits during the unpaid leave of absence, if applicable. However, use of sick, vacation or other PTO benefits will not extend the available leave of absence time. Sick, vacation and other PTO leave hours will not accrue during any unpaid portion of the leave of absence, and employees will not receive pay for official holidays that are observed during their leave of absence except during those periods when they are substituting vacation or sick leave for unpaid leave.  

Any State Disability Insurance for which employees are eligible may be integrated with accrued vacation, sick leave or other PTO benefits so that they do not receive more than 100 percent of their regular pay.  

##### Reinstatement  

If the employee and the Company have agreed upon a definite date of return from the leave of absence or transfer, the employee will be reinstated on that date if they notify the Company that they are able to return on that date. If the length of the leave of absence or transfer has not been established, or if it differs from the original agreement, the employee will be returned to work within two business days, where feasible, after notifying the Company of their readiness to return.  

Before employees will be allowed to return to work in their regular job following a leave of absence or transfer, they must provide Human Resources with a certification from their health care provider that they can perform safely all of the essential duties of the position, with or without reasonable accommodation. If employees do not provide such a release prior to or upon reporting for work, they will be sent home until a release is provided. This time before the release is provided will be unpaid.  

Employees will be returned to the same position upon the conclusion of their leave of absence or transfer unless the position ceases to exist. In cases where the employee's position no longer exists, the Company will provide a comparable position on the scheduled return date or within 60 calendar days of that return date. However, employees will not be entitled to any greater right to reinstatement than if they had not taken the leave.  

To the extent required by law, some extensions beyond an employee's pregnancy disability leave entitlement may be granted when the leave is necessitated by an employee's injury, illness or "disability" as defined under the Americans with Disabilities Act and/or applicable state or local law.  

The Company will not discriminate or retaliate against employees because they request or make use of leave, a transfer or other accommodations in accordance with this policy. This policy does not limit a pregnant employee's rights under any other policy or laws protecting gender, pregnancy and childbirth, or health conditions related to pregnancy or childbirth.  

Employees who have questions about this policy or who wish to request leave, transfer or other reasonable accommodation under this policy should contact Human Resources.  

#### Family Military Leave  

Employees may take up to 10 days of unpaid leave if they work an average of 20 or more hours per week and their spouse or registered domestic partner is on leave from deployment as a member of: (1) the Armed Forces of the United States deployed to an area of military conflict designated as a combat theater or combat zone by the President of the United States; or (2) the National Guard or Reserves deployed during a period of military conflict. For purposes of this policy "military conflict" includes "a period of war declared by the United States Congress" or a period of deployment for which a member of the Reserves is ordered to active duty either by the Governor or the President of the United States.  

Employees must provide the Company with notice of their intention to take leave within two business days of receiving official notice that their spouse or registered domestic partner will be on leave from deployment. The Company may also request that employees submit written documentation certifying that their spouse or registered domestic partner will be on military leave from deployment during the time of the requested leave.  

Eligible employees may use all available accrued paid leave, such as vacation and paid time off, during a period of unpaid family military leave. Leave taken under this policy will not affect an employee's right to any other benefits.  

The Company will not discriminate against, or tolerate discrimination against, any employee who requests and/or takes leave under this policy.  

#### School or Child Care Activities Leave  

An employee who is a parent to one or more children who are of the age to attend a licensed child care provider, kindergarten or grades one through 12 may take up to 40 hours of leave per school year to participate in any of the following:  

- Finding, enrolling or reenrolling the child in a school or with a licensed child care provider;  
- Participating in school or child care-related activities; or   
- Addressing a child care provider or school emergency.  
- “Parent” includes parent, guardian, stepparent, foster parent, grandparent, and persons who stand in loco parentis (in place of a parent) to a child.  

Time off for reasons other than a child care provider or school emergency is limited to eight hours per calendar month.  Child care provider or school emergencies occur when the child cannot remain in school or with a child care provider due to one of the following:  

- The school or child care provider has requested that the child be picked up or has an attendance policy (excluding planned holidays) that prohibits the child from attending or requires that the child be picked up from school or child care;   
- Behavioral or discipline problems;  
- Closure or unexpected unavailability of the school or child care provider (excluding planned holidays);  
- A natural disaster (e.g., fire, earthquake or flood).  

Employees wishing to take time off for a planned absence (e.g., to participate in scheduled school or child care provider activities or enroll a child in school or with a child care provider), must provide reasonable advance notice to their supervisor.  Employees needing time off to address a child care provider or school emergency must provide notice to their supervisor as soon as practicable.  

The Company may require employees to provide documentation from the school or child care provider verifying that the employee participated in the school or childcare activity, including the date and time of the activity.  

If both parents of a child work for ConstructConnect, only one parent - the first to provide notice - may take the time off, unless ConstructConnect approves both parents taking time off simultaneously.  

Employees must substitute any existing vacation time or other accrued paid time off (PTO) for any part of this leave. Employees who do not have vacation time or PTO available will be allowed time off without pay.  

#### School Discipline Leave  

Employees who are the parent or custodial guardian of a child in kindergarten or grades one through 12 may take time off to attend a school conference involving the possible suspension of their child.  

To be eligible for leave, the child must be living with the employee, and the employee must provide advance notice that their appearance at the school has been requested.  

The Company may require employees to provide documentation, including a copy of the school's notice or some other certification stating that the employee's presence at the school is mandatory.  

Employees wishing to take such leave may utilize their existing vacation time or other accrued paid time off.  

School visits for other purposes may be covered under the Company's School or Day Care Activities Leave policy.  

#### Bone Marrow Donor Leave  

Eligible employees who undergo a medically necessary procedure to donate bone marrow to another person will be provided with five workdays off in any one-year period, without a loss in pay. For purposes of this policy, a "one-year period" is 12 consecutive months from the date the employee begins their leave. Employees may take leave in one or more periods, as long as the leave does not exceed five days in any one-year period.  

Employees are eligible for leave if they have worked for the Company for at least 90 continuous days prior to the start of their leave.  

Employees who seek leave under this policy must provide verification from a physician detailing the purpose and length of leave, including the medical necessity for the donation.  

Employees may use all available accrued sick, vacation or paid time off (PTO) concurrently with this time off. If an employee does not have enough earned sick, vacation or PTO time to cover the leave period, the remaining days of leave will be paid by the Company. Use of this leave will not be counted against any available leave under the federal Family and Medical Leave Act (FMLA) or the California Family Rights Act (CFRA), if applicable. Leave under this policy is also not considered a break in service for purposes of, salary adjustments, sick leave, vacation, PTO, annual leave or seniority.  

While on bone marrow donor leave, the Company will maintain all group health insurance benefits as if the employee was still at work. In most circumstances, upon return from this leave, an employee will be reinstated to their original job or to an equivalent job with equivalent pay, benefits and other employment terms and conditions. However, an employee has no greater right to reinstatement than if the employee did not take a leave. For example, if an employee on bone marrow donor leave would have been laid off had they not taken a leave, or if the employee's job is eliminated during the leave and no equivalent or comparable job is available, then the employee would not be entitled to reinstatement.  

The Company will not retaliate or tolerate retaliation against any employee for requesting or taking bone marrow donor leave in accordance with this policy.  

#### Organ Donor Leave  

Eligible employees who undergo a medically necessary procedure to donate an organ to another person will be provided with up to 30 workdays off, without a loss in pay, and an additional 30 workdays off without pay, in any one-year period. For purposes of this policy, a "one-year period" is 12 consecutive months from the date the employee begins their leave. Employees may take leave in one or more periods, as long as the leave does not exceed 60 days in any one-year period.  

Employees are eligible for leave if they have worked for the Company for at least 90 continuous days prior to the start of their leave.  

Employees who seek leave under this policy must provide verification from a physician detailing the purpose and length of leave, including the medical necessity for the donation.  

Employees may use all available accrued sick, vacation or paid time off (PTO) concurrently with this time off. Any remaining days of paid leave will be paid by the Company, up to 30 workdays.  Use of this leave will not be counted against any available leave under the federal Family and Medical Leave Act (FMLA) or the California Family Rights Act (CFRA), if applicable. Leave under this policy is also not considered a break in service for purposes of, salary adjustments, sick leave, vacation, PTO, annual leave or seniority.  

While on organ donor leave, the Company will maintain all group health insurance benefits as if the employee was still at work. In most circumstances, upon return from this leave, an employee will be reinstated to their original job or to an equivalent job with equivalent pay, benefits and other employment terms and conditions. However, an employee has no greater right to reinstatement than if the employee did not take a leave. For example, if an employee on organ donor leave would have been laid off had they not taken a leave, or if the employee's job is eliminated during the leave and no equivalent or comparable job is available, then the employee would not be entitled to reinstatement.  

The Company will not retaliate or tolerate retaliation against any employee for requesting or taking organ donor leave in accordance with this policy.  

#### Workers’ Compensation Leave  

ConstructConnect will grant you a workers’ compensation disability leave in accordance with state law if you incur an occupational illness or injury. As an alternative, the Company may offer you modified work. Leave taken under the workers’ compensation disability policy runs concurrently with family and medical leave under both federal and state law.  

You must report all accidents, injuries, and illnesses, no matter how minor, to your immediate supervisor.  

#### Military Leave  

In addition to the federal protections included in the Company’s National Handbook, employees in California who serve in the military are entitled to the rights and protections set forth in the California Military and Veteran’s Code. Employees who are members of the National Guard or United States Reserve will be granted a temporary leave of absence without pay while engaged in military duty ordered for purposes of military training, drills, encampment, naval cruises and special exercises or like activities. This leave is not to exceed 17 calendar days annually, including time involved in going to and returning from such duty. Collateral benefits will not be restricted or terminated because of an employee's temporary incapacity as a result of the employee's duty in the National Guard, Naval Militia, State Military Reserve or federal reserve components of the United States Armed Forces, if the employee is ordered to duty or training for 52 weeks or less. Similarly, employees who are members of the state Military Reserve will be granted a temporary leave of absence without pay while engaged in military duty for purposes of military training, drills, unit training assemblies or similar inactive duty training. This leave is not to exceed 15 calendar days annually, including time involved in going to and returning from that duty.  

Employees who are members of California's National Guard or the National Guards of other states will be entitled to reinstatement upon return from a military leave for active service, so long as certain conditions are met.  Employees returning from leave who were full-time employees will be restored to the same position or to a position of similar seniority, status and pay unless the Company’s circumstances have so changed as to make it impossible or unreasonable to do so, and part-time employees will be restored to the same position or to a position of similar seniority, status and pay, if any exists, so long as:  

- The employee is an officer or enlisted member of the National Guard of any state;  
- The employee was called to active duty by the Governor of the state in which the employee serves in the National Guard or by the President of the United States;  
- The employee received a certificate of satisfactory service in the National Guard;  
- The employee is still qualified to perform the duties of the position;   
- If the employee left a full-time position, they applied for reemployment within 40 days of being released from service; or, if the employee left part-time employment, they applied for reemployment within five days of being released from service; and  
- The employee's position was not temporary.  

For one year following reemployment, the Company will not discharge the employee without cause.  

The Company will not discriminate against members of the military or naval services of California or the federal reserve component of the United States Armed Forces.  If the proper authority calls upon an employee to perform military service or duty or attend a military encampment or place of drill or instruction, the Company will not hinder or prevent the employee from performing that service.  

#### Emergency Responder Leave  

The Company will not terminate or discipline any employee who is a volunteer firefighter, reserve peace officer or emergency rescue personnel because the employee takes time off to perform emergency duty or engages in fire, law enforcement or emergency rescue training. In the event you need to take time off for this type of emergency duty, please alert your supervisor before leaving the company's premises.  

A "volunteer firefighter" includes any person registered as a volunteer member of a regularly organized fire department of a city, county, city and county or district having official recognition of the government of the city, county or district in which the department is located; or a regularly organized fire department of an unincorporated town.  

"Emergency rescue personnel" includes any volunteer or paid officers, employees, or members of a fire department or fire protection or firefighting agency who perform first aid and medical services, rescue procedures and transportation or other related activities necessary to insure the health or safety of a person in immediate danger. Such personnel include those who work for the:  

1. federal or state government;  
1. city, county, city and county, district or other public or municipal corporation or political subdivision of this state;  
1. sheriff's department, police department or private fire department; or  
1. disaster medical response entity sponsored or requested by the state.  

Employees will also be allowed up to 14 calendar days of leave per year to engage in fire, law enforcement or emergency rescue training.  

All time off taken under this policy is unpaid, except that exempt employees will be paid when required under applicable law.  

#### Civil Air Patrol Leave  

The Company will not terminate or discriminate against an employee who is a volunteer member of the Civil Air Patrol or prevent a member from performing service as part of the California Wing of the Civil Air Patrol during an emergency operational mission. Additionally, the Company will not retaliate against an employee for requesting or taking Civil Air Patrol leave in accordance with this policy.  

The Company will provide eligible employees with up to 10 days per year of leave, but no more than three days at a time, unless the emergency is extended by the entity in charge of the operation and the Company approves the extension. To be eligible for leave, employees must have been employed by the Company for at least 90 days immediately preceding the start of the leave, and must be duly directed and authorized by a political entity that has the authority to authorize an emergency operational mission of the California Wing of the Civil Air Patrol.  

Employees must request leave with as much notice as possible. The Company may require certification from the proper Civil Air Patrol authority to verify an employee's eligibility for leave. The Company may deny leave if the employee fails to provide the required certification.  

Leave taken under this policy is unpaid except that exempt employees will be paid when required by applicable law. Employees will not be required to exhaust accrued vacation or sick leave or any other type of accrued leave prior to taking unpaid civil air patrol leave, but may choose to use such benefits during leave to receive pay.  

Following leave, an employee must return to work as soon as practicable and must provide evidence of the satisfactory completion of Civil Air Patrol service. If the employee complies with these requirements, the employee will be restored to their prior position without loss of status, pay or other benefits.  

#### Jury and Witness Duty Leave  

The Company encourages all employees to fulfill their civic responsibilities and to respond to jury service summonses or subpoenas, attend court for prospective jury service or serve as a juror or witness under court order. Under no circumstances will employees be terminated, coerced or penalized because they request or take leave in accordance with this policy.  

Employees must notify their supervisor with notice of any jury summons or subpoena or court order within a reasonable time after receipt and before their appearance is required. Verification from the court clerk of having served or appeared may be required.  

Time spent engaged in attending court for prospective jury service or for serving as a juror or witness is not compensable except that exempt employees will not incur any reduction in pay for partial week's absence due to jury or witness duty. Employees may use vacation, personal leave or compensatory time off that is otherwise available to the employee for time spent responding to a summons and/or subpoena, for participating in the jury selection process or for serving on a jury or as a witness. Employees may retain any mileage allowance or other fees paid for the jury or witness duty.  

Any employee on jury or witness duty is expected to report or return to work for the remainder of the work schedule when dismissed from jury or witness duty.  

#### Crime Victim Leave  

ConstructConnect will provide time off to any employee who is a victim, as that term is defined in this policy, so that the employee may obtain or attempt to obtain relief and to help ensure the health, safety or welfare of the employee or the employee's child. For purposes of this policy, “victim” includes a victim of stalking, domestic violence, or sexual assault; a victim of a crime that caused physical injury or that caused mental injury and a threat of physical injury; or a person whose immediate family member is deceased as the direct result of a crime.  

"Relief" includes, but is not limited to, a temporary restraining order, restraining order or other injunctive relief. “Immediate family member” includes the employee’s:  

- Child, regardless of age (including a biological, adopted, step-, or foster child; legal ward; child of a domestic partner; child to whom the employee stands in loco parentis; or person to whom the employee stood in loco parentis when the person was a minor);  
- Parent (including a biological, adoptive, step-, foster parent or legal guardian of the employee or the employee’s spouse or domestic partner or a person who stood in loco parentis when the employee or employee’s spouse or domestic partner was a minor child);  
- Sibling (including a biological, foster, step-, half- or adoptive sibling);  
- Spouse or registered domestic partner; or  
- Any other individual whose close association with the employee is the equivalent of such family relationships.  

Any employee against whom any crime has been committed will also be permitted time off to appear in court to comply with a subpoena or other court order as a witness in a judicial proceeding.  

Employees should give the Company reasonable notice of the need for leave, unless advance notice is not feasible. When an unscheduled absence occurs, the Company may require the employee to provide written certification of the need for time off.  Any of the following will be considered sufficient certification: a police report indicating the employee was a victim; a court order protecting or separating the employee from the perpetrator of the crime or abuse, or other evidence from the court or prosecuting attorney that the employee has appeared in court; documentation from a licensed medical professional, domestic violence counselor, sexual assault counselor, victim advocate, licensed health care provider or counselor that the employee was undergoing treatment or receiving services for physical or mental injuries or abuse resulting in victimization from the crime or abuse; or any other form of documentation that reasonably verifies that the crime or abuse occurred, including but not limited to, a written statement signed by the employee, or an individual acting on the employee’s behalf, certifying that the absence is for an authorized purpose.  

Additionally, an employee who is a victim may take time off for any of the following reasons:  

1. to seek medical attention for injuries caused by the crime or abuse;  
1. to obtain services from a domestic violence shelter, program, rape crisis center or victim services organization or agency as a result of the crime or abuse;  
1. to obtain psychological counseling or mental health services related to an experience of crime or abuse; and  
1. to participate in safety planning and take other actions to increase safety from future crime or abuse, including temporary or permanent relocation.  

If the reason for the leave is also covered by the federal Family and Medical Leave Act (FMLA) and/or the California Family Rights Act (CFRA), the leave pursuant to this policy and FMLA/CFRA will run concurrently.  Additionally, the length of leave under this policy is limited to that provided under the FMLA. For example, an employee is not entitled to time off due to reasons in this policy if they have already exhausted the maximum 12 weeks of leave under the FMLA.  

Employees may use accrued paid time off, such as vacation time, in order to receive compensation during the leave of absence.  

Employees may also be entitled to a reasonable accommodation under the Company's Accommodation for Victims of Domestic Violence, Sexual Assault or Stalking policy and to additional leave under the Company’s Leave to Attend Court Proceedings Related to Certain Felonies policy and Leave to Attend Court Proceedings for Serious Crimes policy.   Employees should consult those policies and/or Human Resources for additional information. The Company will keep all information submitted in connection with an employee's request for leave confidential to the extent permissible by law. If the law requires disclosure of information, the Company will notify the employee before any information is released.  

The Company will not discriminate or retaliate against any employee because of the employee’s status as a victim of crime or abuse, if the employee provides the Company notice of such status, the Company has actual knowledge of such status, or the employee takes or requests leave in accordance with this policy.  

Employees who have questions about this policy or who wish to request a leave of absence under this policy should contact their Human Resources representative.  

#### Leave to Attend Judicial Proceedings Related to Certain Felonies  

ConstructConnect prohibits discrimination against an employee who wishes to take time off from work to attend judicial proceedings related to certain violent, serious or theft/embezzlement related felonies committed against the employee, the employee's immediate family member, the employee's registered domestic partner or a child of the employee's registered domestic partner.  

"Immediate family member" is defined as an employee's spouse, child, stepchild, brother, stepbrother, sister, stepsister, mother, stepmother, father or stepfather.  

Before an employee may be absent from work to attend a judicial proceeding, the employee must give the employer a copy of the notice of each scheduled proceeding that is provided to the victim by the agency responsible for providing notice, unless advance notice is not feasible. When advance notice is not feasible or an unscheduled absence occurs, the employee must provide within reasonable time documentation evidencing the judicial proceeding from  

1. the court or government agency setting the hearing;  
1. the district attorney or prosecuting attorney's office; or  
1. the victim/witness office that is advocating on behalf of the victim.  

Confidentiality of the situation, including an employee's request for the time off, will be maintained to the greatest extent possible.  

Employees may use accrued benefits, such as vacation time or sick leave, in order to receive compensation during the time taken off from work.  

#### Leave To Attend Court Proceedings for Serious Crimes  

ConstructConnect prohibits discrimination against an employee who is a victim of certain serious criminal offenses and wishes to take time off to appear in court to be heard at any proceeding, including any delinquency proceeding, involving a post-arrest release decision, plea, sentencing, or post-conviction release decision or any proceeding in which a right of the victim is at issue.  

A "victim" means any employee who suffers direct or threatened physical, psychological or financial harm as a result of the commission or attempted commission of a serious criminal offense. The term "victim" also includes the employee's spouse, registered domestic partner, parent, child, sibling or guardian.  

Before employees may take time off under this policy, they must provide the Company with reasonable advance notice of their intention to take time off, unless the advance notice is not feasible. If an employee must take an unscheduled absence due to victimization from a serious criminal offense, the employee must provide the Company with a certification within a reasonable time. The types of certification to account for an unscheduled absence include: a police report indicating the employee was a victim of one of the specified serious criminal offenses; a court order protecting or separating the employee from the perpetrator of one or more of the specified offenses, or other evidence from the court or prosecuting attorney that the employee has appeared in court; or documentation from a medical professional, domestic violence counselor or advocate for victims of sexual assault, health care provider or counselor that the employee was undergoing treatment for physical or mental injuries resulting in victimization from one of the specific serious criminal offenses.  

Confidentiality of the situation, including an employee's request for the time off, will be maintained to the greatest extent possible.  

Employees may use accrued benefits, such as vacation time or sick leave, in order to receive compensation during the time taken off from work.  

#### Time Off to Vote  

The Company encourages all employees to fulfill their civic responsibilities and to vote in official public elections. Most employees' schedules provide sufficient time to vote either before or after working hours.  

Any employees who do not have sufficient time outside of working hours to vote in a statewide public election, while the polls are open, may take up to two hours off from work, without loss of pay. Any additional time off will be without pay. Employees must take the time off at the beginning or end of their regular work schedule, whichever allows the greatest amount of free time for voting and the least amount of time off from work, unless mutually agreed otherwise.  

Employees must provide at least two working days' notice of the need for leave when, on the third working day prior to the election day, the employee knows or has reason to believe they will need time off to vote on election day. Otherwise, employees must give reasonable notice of the need to have time off to vote.  

#### Election Officer Leave  

The Company will not terminate, suspend or otherwise discriminate against employees who miss work to serve as an election officer on Election Day.  

Time off under this policy will be unpaid.  

The Company asks that employees provide reasonable advance notice of the need for time off to serve as an election official, so that the time off can be scheduled to minimize disruption to normal work schedules.  

Proof of having served as an election official may be required.  

### PAY PRACTICES  

#### Overtime  

When operating requirements or other needs cannot be met during regular working hours, employees may be scheduled to work overtime. **All overtime must be approved in advance by the employee's supervisor.** Working overtime without prior authorization may result in disciplinary action up to and including termination of employment.  

All nonexempt employees in California will be paid a premium for overtime hours as follows:  

1. One and one-half times their regular rate of pay for all hours worked in excess of 8 per workday, up to 12, or in excess of 40 in a workweek;  
1. One and one-half times their regular rate of pay for the first 8 hours on the seventh consecutive day of work in a workweek; and  
1. Double the regular rate of pay for all hours worked in excess of 12 in a workday and after 8 hours on the seventh consecutive day of work in a workweek.  

All nonexempt employees are entitled to at least one day of rest every seven days in a workweek unless certain exceptions apply as described in the Company’s Day of Rest Policy.  An employee may independently and voluntarily choose not to take a day of rest and confirm such choice in writing with the Company.  

#### Discussion of Wages  

No employee is prohibited from disclosing the amount of their wages. The Company will not terminate, demote, suspend, or otherwise discriminate or retaliate against an employee who makes such a disclosure or because an employee exercises their rights, or aids or encourages other employees in exercising their rights, under California's Equal Pay Law.  

This policy does not require disclosure of wages.  

#### Meal and Rest Breaks  

The Company complies with federal and state legal requirements concerning meal and rest breaks. The Company recognizes that employees perform at their best when they have the rest and nourishment they need. This policy explains when the Company expects employees to take meal and rest breaks.  

##### Meal Breaks  

The Company provides at least a 30-minute meal period to employees who work more than five hours and a second 30-minute meal period to employees who work more than 10 hours in a workday, unless they have elected to waive a meal period in accordance with the Company’s policy and state law.  

Employees are relieved of all of their duties during meal periods and are allowed to leave the premises.  

The Company provides meal periods as follows:  

|Number of Actual Hours Worked Per Shift|# of Meal Periods|Comments|
|--|--|--|
|0 to </= 5.0|0|An employee who does not work more than five hours in a workday is not provided with a meal period.|
|> 5.0 to </= 10.0|1|An employee who works more than five hours in a workday, but who does not work more than ten hours in a workday, is provided with a 30-minute meal period available before working more than five hours, subject to any meal period waiver in effect.|
|> 10.0|2|An employee who works more than ten hours in a workday is provided with a second 30-minute meal period available before working more than ten hours, subject to any meal period waiver in effect.  The meal period waiver will be invalidated if the employee works more than 12 hours.|  

The Company does not pay non-exempt employees for meal periods, and consequently, non-exempt employees must record the start and stop times of their meal periods.  

##### Rest Breaks  

Employees are authorized and permitted to take a 10-minute paid rest break for every four hours worked, or major fraction thereof.  Employees are relieved of all of their duties during rest periods and are allowed to leave the premises. The Company authorizes and permits rest breaks as follows:  

|Number of Actual Hours Worked Per Shift|# of 10 Minute Rest Breaks|Comments|  
|--|--|--|  
|0 to < 3.5|0|A non-exempt employee who works less than 3.5 hours in a workday is not entitled to a rest break.|  
|3.5 to </= 6|1|A non-exempt employee who works between 3.5 and 6 hours in a workday is entitled to one 10-minute rest break.|  
|> 6.0 to </= 10.0|2|A non-exempt employee who works more than 6 hours in a workday but who does not work more than 10 hours in a workday is entitled to two 10-minute rest breaks.|  
|> 10.0 to </= 14|3|A non-exempt employee who works more than 10 hours in a workday but who does not work more than 14 hours in a workday is entitled to three 10-minute rest breaks.<sup>1</sup>|  

<sup>1</sup>Non-exempt employees who work more than 14 hours in a workday may be entitled to additional rest breaks.

Whenever practicable, rest breaks should be taken near the middle of each four-hour work period.  Employees may not accumulate rest breaks or use rest breaks as a basis for starting work late, leaving work early, or extending a meal period.  

Because rest breaks are paid, non-exempt employees should not clock out for them.  

##### RESPONSIBILITIES  

Supervisors are responsible for administering their department’s meal and rest breaks.  

Any non-exempt employee who is not provided with a meal period or authorized and permitted to take a rest break pursuant to the terms of this Policy is immediately entitled to a meal or rest break premium.   Supervisors will be responsible for authorizing meal or rest break premiums.  Any supervisor who knows or should reasonably know that a meal or rest period was not provided in accordance with this Policy should arrange for a premium to issue to the employee.  Employees are responsible for reporting to their supervisor any meal break that was not provided or any rest break not authorized and permitted where the supervisor would have no reason to otherwise know of this fact.  Employees who feel they are owed a premium as a result of this Policy, but have not received the premium should report the missing premium immediately to their supervisor.  

#### Lactation Accommodation  

Employees have the right to request lactation accommodation. The Company will provide a reasonable amount of break time to accommodate an employee desiring to express breast milk for the employee's infant child each time the employee has need to express milk. If possible, the lactation break time should run concurrently with scheduled meal and rest breaks already provided to the employee. If the lactation break time cannot run concurrently with meal and rest breaks already provided or additional time is needed, the lactation break time will be unpaid for nonexempt employees.  

Employees will be relieved of all work-related duties during any unpaid break. When unpaid breaks or additional time are required, employees should work with their supervisor regarding scheduling and reporting the extra break time.  

Because exempt employees receive their full salary during weeks in which they work, all exempt employees who need lactation accommodation breaks do not need to report any extra break time as "unpaid."  

The Company will provide employees with the use of a room or other location to express milk in private.  The lactation room or other location will not be a bathroom and will be safe, clean, free from hazardous materials, in close proximity to the employee’s work area, shielded from view and free from intrusion by co-workers and/or the public. This location may be the place where the employee normally works, if applicable.  The lactation room or other location will include a surface on which to place a breast pump or other personal items, a place to sit and electricity or alternative devices (e.g., an extension cord or charging station) needed to operate an electric or battery-powered breast pump.  Lactating employees who pump breastmilk will also have access to a sink with running water and a refrigerator or alternative cooling device suitable for storing milk in close proximity to their workspace.  

A room or other location identified for lactation may also be used for other purposes. However, during times when an employee is using the location for lactation purposes, that use will take precedence over all other uses. Employees who have questions or concerns related to lactation room scheduling conflicts should contact their supervisor or a Human Resources representative. Any non-exempt employee who is not provided with a break as requested to express milk, should immediately contact Human Resources.  

Lactation is considered a pregnancy-related condition under California law.  

Employees who wish to request lactation accommodation should contact Human Resources. If the Company cannot provide break time or a location that complies with this Lactation Accommodation policy, the employee requesting the accommodation will be notified in writing.  

The Company will not discriminate or retaliate against an employee who requests or uses a lactation accommodation in accordance with this policy or otherwise exercises their rights under California’s lactation accommodation law.  Employees who feel their lactation accommodation rights have been violated can file a complaint with the California Labor Commissioner’s Office.  

#### Lactation Accommodation (San Francisco)  

The Company complies with the San Francisco Lactation in the Workplace Ordinance (“LWO”) and, in accordance with that law, will provide a reasonable amount of break time to accommodate an employee who performs 56 or more hours of work in San Francisco in a calendar year and wants to express breast milk for their children. Employees needing breaks for lactation purposes may use ordinary paid rest breaks or may take other reasonable break time when needed. If possible, the lactation break time should run concurrently with scheduled meal and rest breaks already provided to the employee. If the lactation break time cannot run concurrently with meal and rest breaks already provided or additional time is needed, the lactation break time will be unpaid for nonexempt employees.  

Employees will be relieved of all work-related duties during any unpaid break.  

When unpaid breaks or additional time are required, employees should work with their supervisor regarding scheduling and reporting the extra break time. The time an employee spends walking to and from the designated lactation location and/or a refrigerator or sink will not be counted as part of the employee’s break time.  

Because exempt employees receive their full salary during weeks in which they work, all exempt employees who need lactation accommodation breaks do not need to report any extra break time as “unpaid.”  

The Company will provide employees with the use of a room or a private area, other than a bathroom or toilet stall, in close proximity to their work area that is shielded from view and free from intrusion from co-workers and the public (the “Lactation Location”). The Lactation Location may be the employee's normal work area, if suitable.  The Lactation Location will: be safe, clean and free from toxic or hazardous materials; contain a surface (e.g., a table or shelf) to place a breast pump and other personal items; contain a place to sit; and have access to electricity. The Company will also provide access to a refrigerator where employees can store breast milk and access to a sink with running water.  

Employees have a right to request lactation accommodation.  To request a lactation accommodation, employees should contact Human Resources orally, by email or in writing.  The Company will respond to a request for accommodation within five business days and will engage in an interactive process with the employee to determine the appropriate break periods and the Lactation Location for the employee. If the Company denies a request for lactation accommodation, it will provide a written statement identifying the reason(s) for doing so.  

The Company prohibits retaliation against employees who request a lactation accommodation, file a complaint or otherwise report an alleged violation of the LWO, cooperate in an investigation of an alleged violation of the LWO or inform another person about their rights under the LWO.  

Lactation is considered a pregnancy-related condition under California law. The Company will otherwise treat lactation as a pregnancy-related medical condition and address lactation-related needs in the same manner that it addresses other non-incapacitating medical conditions, including requested time off for medical appointments, requested changes in schedules and other requested accommodations.  

#### Family Friendly Workplace (San Francisco)  

Employees may request a flexible or predictable working arrangement to assist with caregiving responsibilities when the employee is the primary contributor to the ongoing care for:  

- A child or children under the age of 18 for whom the employee has assumed parental responsibility;  
- A person with a serious health condition in a family relationship with the employee; or  
- Parents of the employee who are age 65 or older.  

For the purposes of this policy, a "child" includes the employee's biological, adopted or foster child, a stepchild, a legal ward or a child of a person standing in loco parentis to that child. A "family relationship" is defined as a relationship in which a caregiver is related by blood, legal custody, marriage or domestic partnership to another person as a spouse, domestic partner, child, parent, sibling, grandchild or grandparent.  

A "flexible working arrangement" is a change in the employee's regular working arrangement that provides an employee with flexibility to help with caregiving responsibilities. Examples of flexible working arrangements include, but are not limited to, a modified work schedule, changes in start and/or end times of work, part-time employment, job sharing arrangements, working from home, telecommuting, changes in work duties or part-year employment. If an employee requests time off as a flexible work arrangement under this policy, such time will also be designated under the federal Family and Medical Leave Act, California Family Rights Act and/or paid sick leave law if the employee has not yet exhausted available leave under the applicable law(s).  

A "predictable working arrangement" is a change in the employee's regular working arrangement that provides an employee with scheduling predictability to help with caregiving responsibilities. If there is insufficient work for the employee during the predictable working arrangement period, the employee will not be paid during this time.  

##### Employee Eligibility  

To be eligible for a flexible or predictable working arrangement, an employee must have worked for the Company for at least six months, be employed within the geographic boundaries of San Francisco and regularly work at least eight hours per week.  

##### Guidelines for Employee Requests  

Employees may request a flexible or predictable working arrangement twice every 12 months. Employees that experience a major life event, such as the birth of a child, the placement with an employee of a child through adoption or foster care or an increase in an employee's caregiving duties for a family member with a serious health condition, may make an additional request in the same 12-month period.  

Requests for a flexible or predictable working arrangement must be submitted in writing to Human Resources. The written request must specify the arrangement being sought, the date on which the employee wishes the arrangement to become effective, the proposed duration of the arrangement and an explanation of how the requested arrangement relates to caregiving.  

The Company may require employees to provide verification of caregiver responsibilities.  

A Human Resources representative will meet with the employee within 21 days of the request and will respond to the request in writing within 21 days of the meeting. These time frames may be extended by written agreement between the Company and the employee.  

Although the Company will consider all requests for flexible or predictable working arrangements made in accordance with this policy, the Company may deny such requests for bona fide business-related reasons.  

If an employee's request for a flexible or predictable working arrangement is denied, the employee may submit a written request for reconsideration within 30 days of the decision. A Human Resources representative will meet with the employee within 21 days of receiving the request for reconsideration and will inform the employee of the final decision in writing no later than 21 days after the meeting.  

If an employee's request for a flexible or predictable working arrangement is approved, the Company will confirm the arrangement to the employee in writing. Either the Company or the employee may revoke a flexible or predicable working arrangement with 14 days' written notice. If either the Company or the employee revokes the arrangement, the employee may submit a new request for a different arrangement. If the Company revokes the arrangement, the employee will be allowed to submit a request in addition to the two requests generally allowed per 12-month period.  

##### Discrimination and Retaliation Prohibited  

The Company prohibits discrimination against employees because of their caregiver status and will not take adverse employment action (e.g., termination, demotion) or otherwise retaliate against employees for exercising their rights under this policy or the San Francisco Family Friendly Workplace Ordinance.  

#### Scheduling for Part-Time Employees (San Jose)  

In accordance with the San Jose Opportunity to Work Ordinance, the Company adopts the following policies and practices:  

##### Additional Hours for Qualified Part-Time Employees  

Before hiring new employees or using subcontractors, temporary services or a staffing agency to do work, the Company will offer additional hours of work to existing part-time employees, provided that:  

- The Company determines in its good faith and reasonable judgment that the part-time employees have the skills and experience to perform the additional work; and  
- The additional hours of work would not cause the Company to have to compensate the employee at time-and-a-half or any other premium rate under any law or collective bargaining agreement.  

For purposes of this policy, part-time employees are defined as non-exempt employees working less than 35 hours per week who

1. performed at least two hours of work for the Company in the last calendar week and within the geographic boundaries of San Jose, and  
1. are entitled to payment of the minimum wage under California law.  

The Company will use a transparent and nondiscriminatory process to distribute hours of work among existing employees.  

##### Retaliation Prohibited  

The Company will not terminate, threaten to terminate, demote, suspend, harass, discriminate or otherwise take adverse action against an employee in retaliation for exercising rights protected under the San Jose Opportunity to Work Ordinance, nor will the Company tolerate such retaliation.  

#### Day of Rest  

In each workweek, ConstructConnect will provide employees with at least one day of rest for every seven days within the workweek unless their total hours worked are 30 hours or less  in the workweek and six hours or fewer every day of the workweek.  If the nature of the employee’s work reasonably requires that the employee work seven or more consecutive days, the day of rest requirement may be met by providing an average of one day’s rest for every seven days on a monthly basis (e.g., four days of rest per calendar month). An employee may also independently and voluntarily choose and confirm in writing not to take a day of rest. Day of Rest Confirmation Forms are available from Human Resources.  

This policy does not apply in cases of emergency or to work performed in the protection of life or property from loss or destruction.  

The Company will reasonably accommodate the observance of a Sabbath or other religious holy day by employees, unless doing so would result in undue hardship to the conduct of Company business.  

Employees will be paid for all hours worked in compliance with federal, state and local law.  

### EMPLOYEE BENEFITS  

#### Family Leave Insurance  

Employees may be eligible for up to eight weeks of state-provided paid family leave (PFL) insurance benefits when they take time off for one of the following purposes:  

- To bond with a child during the first 12 months after the child's birth or after the placement of a child for adoption or foster care with the employee;   
- To care for an immediate family member (spouse, registered domestic partner, child, parent, grandparent, grandchild, sibling and parent-in-law defined by the PFL law) who is seriously ill and requires care; or  
- To participate in a qualifying exigency related to the covered active duty or call to covered active duty of the employee’s spouse, domestic partner, child or parent in the U.S. Armed Forces.  

The PFL benefits described in this policy are a state-provided partial wage replacement benefit, not a protected leave of absence. To obtain approval for a leave of absence for the reasons set forth above, employees must contact their supervisor and comply with applicable eligibility, notice, and certification requirements when required by state or federal law  

##### Amount and Duration of Benefits  

The weekly benefit amount is generally 60 or 70 percent of the employee's earnings (depending upon the employee’s income), with benefits capped according to a state-imposed maximum weekly benefits amount.  Employees may receive up to eight weeks of PFL benefits during a 12-month period, but may not receive more benefits than earned in wages during the base period for calculating benefits (generally, the 12 months prior to the quarter in which the claim is made).  

When applicable, PFL benefits will run concurrently with leave time available under the California Family Rights Act and the federal Family and Medical Leave Act. Employees may use any accrued but unused sick leave prior to receiving PFL benefits.  

#### Supplemental Compensation for New Child Bonding (San Francisco)  

Pursuant to the San Francisco Paid Parental Leave Ordinance ("SFPPLO"), the Company will provide Supplemental Compensation to an eligible employee during employment when the employee receives California Paid Family Leave ("California PFL") benefits from the State of California ("the State") to bond with a minor child during the first year after the child’s birth or placement through foster care or adoption.  

##### Eligibility  

All employees who perform work within San Francisco are eligible if they satisfy all of the following requirements:  

- The employee began employment with the Company at least 180 calendar days prior to the first day of leave for which California PFL benefits for New Child Bonding are payable;  
- The employee performs at least eight hours of work per week for the Company in San Francisco;  
- At least 40% of the employee’s total weekly hours worked for the Company are in San Francisco; and  
- The employee is eligible to receive paid family leave compensation under the California Paid Family Leave law for the purpose of New Child Bonding.  

Employees can elect to receive California PFL benefits intermittently, receiving the eight weeks of California PFL benefits in separate increments while taking leave during the 12-month period following the birth or placement of a child.  For employees using California PFL intermittently, eligibility for Supplemental Compensation will be assessed at the beginning of each increment of intermittent leave.  Accordingly, an employee who does not meet the 180-day eligibility requirement during the first increment of intermittent leave could still satisfy the requirement for subsequent increments.  In addition, an employee may become ineligible for Supplemental Compensation if, between one intermittent receipt of California PFL benefits and the next, the employee’s hours or work location change such that the employee no longer meets the eligibility requirements.  

##### Definitions  

- **New Child Bonding:** Bonding with the employee’s minor child during the 12-month period immediately following the birth of the child or placement of the child, through adoption or foster care, with the employee, for the period covered by the California PFL benefits law.  
- **Maximum Weekly Benefit Amount:** The Maximum Weekly Benefit Amount is determined by the State by using the employee’s highest-earning calendar quarter during an approximate 12- month base period.  
- **Supplemental Compensation:** Supplemental Compensation is a partial wage replacement that is provided by the Company to an eligible employee during the period when the employee receives California PFL benefits from the State for New Child Bonding. Supplemental Compensation and California PFL benefits together will not to exceed 100% of an employee’s weekly salary and are subject to the Maximum Weekly Benefit Amount.  

##### Duration and Timing of Supplemental Compensation  

An employee may receive Supplemental Compensation for a period of up to eight weeks so long as the employee meets the eligibility, accrued vacation/PTO use, and documentation requirements set forth in this policy. The timing of an employee’s receipt of Supplemental Compensation will depend on when the Company receives information directly from the State or, from the employee, a copy of the State’s Notice Computation and confirmation that the employee has received the first California PFL benefits payment. Upon receipt of information from the employee and/or the State that is necessary to process payment, the Company will make a good faith effort to process the initial Supplemental Compensation payment in the next full pay period.  To the extent possible, any additional Supplemental Compensation payment(s) will be processed in accordance with the Company’s established pay schedule. There may be some situations where Supplemental Compensation is not paid to the employee until after the employee has returned from new child bonding.  In those cases, the Company will pay the total Supplemental Compensation within thirty days of receiving the information required to process payment.  

##### Calculation of Supplemental Compensation  

Under California’s PFL benefit program, an employee may receive income replacement from the State equal to approximately 60% or 70% of the employee’s weekly wages, subject to the Maximum Weekly Benefit Amount. Supplemental Compensation, which includes up to two weeks of accrued vacation/PTO as outlined in this policy, is provided to an eligible employee so that, in combination with the California PFL benefit, the eligible employee may receive approximately 100% of the employee’s weekly wages, subject to the Maximum Weekly Benefit Amount. All payments will be integrated so that an eligible employee will receive no greater compensation than their regular compensation during this period.  

The State sets a ceiling on the amount an employee receiving California PFL benefits can be assumed to earn. This ceiling is also applied to Supplemental Compensation. In the case of an eligible employee whose weekly wages exceed the ceiling, Supplemental Compensation will not be calculated to reach 100% of the employee’s normal gross weekly wage. Rather, the amount of Supplemental Compensation will be subject to the ceiling and will be calculated based on the gross wage obtained by dividing the State’s Maximum Weekly Benefit Amount by the percentage rate of wage replacement provided under the California PFL benefit law.  

The Company will determine the amount of weekly Supplemental Compensation to be paid to an eligible employee once the necessary information regarding California PFL benefits is obtained from the employee or the State. Any increases in an employee’s regular compensation will not necessarily result in an increase in Supplemental Compensation. However, the Company may recalculate the amount of Supplemental Compensation provided to an employee in situations where the employee’s leave is intermittent and the employee’s weekly wages decrease between the time the employee receives the first increment of PFL benefits and any subsequent period where the benefits are received for the same leave. This will be done to ensure the employee does not exceed 100% of the employee’s weekly wage and is not subject to an overpayment charge from the State.  

##### Integration with Vacation  

During the period when eligible employees are receiving California PFL benefits, the Company requires them to agree to use up to two weeks of accrued, but unused, vacation benefits that will be applied toward the Company’s Supplemental Compensation obligation under the law. If the employee does not agree to use vacation benefits during the California PFL period, the Company will not be obligated to provide Supplemental Compensation. An employee’s decision not to allow use of vacation benefits will not prevent the employee from being eligible for leave or from receiving California PFL benefits.  

##### Involuntary Separation from Employment  

If an employee is involuntarily separated from employment, during the New Child Bonding period, the Company will continue to provide Supplemental Compensation for that period during which the employee continues to receive California Paid Family Leave benefits.  

##### Voluntary Separation from Employment  

If an employee voluntarily separates from employment with the Company within 90 days of the end of the California PFL period for New Child Bonding, the employee will be required to reimburse the Company for the full amount of Supplemental Compensation paid to them, upon receiving a written request for reimbursement from the Company.  

##### Required Documentation for Supplemental Compensation  

An employee must provide (or agree to provide) certain documentation and information to the Company before the employee will be able to receive Supplemental Compensation. Prior to receiving any Supplemental Compensation, an employee must either:  

1. provide the Company with a copy of the Notice of Computation of PFL Benefits the employee receives from the State; or  
1. authorize the State to disclose the employee’s California PFL weekly benefit amount to the Company, at the time when the employee applies for California PFL benefits.  

An employee may choose to do both 1 and 2 in order to help avoid potential delays in calculating Supplemental Compensation.  

If an employee chooses option 1, the employee must, upon receipt, provide the Company with the Notice of Computation and also upon receipt of the first California PFL benefits payment, submit a copy of the Notice of Payment.  If an employee chooses option 2, the employee must notify the Company upon receipt of the first California PFL payment, so that the Company can contact the State to determine the employee’s weekly California PFL benefit amount.  

Employees must also complete a San Francisco Paid Parental Leave form (the “PPLO Form”).   In Section 3 of the PPLO Form, employees must execute an agreement to reimburse the full amount of Supplemental Compensation received from the Company in the event that they voluntarily separate from employment under the circumstances described in the Voluntary Separation from Employment section above.  

Employees with more than one employer must also complete section 4 of the PPLO Form by providing information pertaining to wages received from all employers during the 90 days prior to the California PFL period.  

Employees who are receiving California PFL benefits for intermittent new child bonding leave must provide the Company with the schedule of intermittent leave they have submitted to the State and notify the Company of any changes in that schedule.  

Employees who fail to provide any or all of the required documentation will be disqualified from receiving Supplemental Compensation.  

##### Protected Rights  

The Company will not interfere with, restrain, or deny the exercise of or the attempt to exercise, any right protected under the SFPPLO. Such rights include but are not limited to the right to Supplemental Compensation pursuant to the SFPPLO; the right to file a complaint or inform any person about any employer's alleged violation of the SFPPLO; the right to cooperate with the San Francisco Office of Labor Standards in its investigations of alleged violations of the SFPPLO; and the right to inform any person of their possible rights under the SFPPLO.  

### SAFETY AND SECURITY  

#### Smoke-Free Workplace  

The Company provides a work environment that is smoke-free. Smoking is strictly prohibited inside the building. For purposes of this policy, smoking includes the use of electronic smoking devices, such as electronic cigarettes, cigars, pipes or hookahs,that create an aerosol or vapor.  Employees that observe other individuals smoking in the workplace have a right to object and should report the violation to their supervisor or to another member of management. Employees will not be disciplined or retaliated against for reporting smoking that violates this policy.  

Employees that violate this policy or who tamper with No Smoking signs may be subject to disciplinary action up to and including termination.  

#### Recovery/Cool-Down Periods  

ConstructConnect permits employees who work outside to spend not fewer than five minutes in the shade to cool down when necessary to avoid heat illness, during which they are relieved of all duties. There is no set schedule for recovery/cool-down periods and there is no limit on how many recovery/cool-down periods employees may take when performing work outside. Any employee experiencing any signs or symptoms of heat illness must immediately contact their supervisor.  

Time spent taking a recovery/cool-down period in compliance with this policy is considered "hours worked" and will be paid. Any nonexempt employee who is required to work through some or all of a cool-down period should complete a "California Cool-Down Premium Request Form" and submit it to their supervisor no later than the end of the pay period (Premium Request Forms are provided upon request). The Company will assume that any nonexempt employee who fails to record a missed cool-down period missed the cool-down period voluntarily.  

#### Injury and Illness Prevention Program   

The health and safety of employees and others on Company property are of critical concern to the Company. We strive to attain the highest possible level of safety in all activities and operations. The Company also intends to comply with all health and safety laws applicable to our business.  

To this end, the Company must rely upon employees to help keep work areas safe and free of hazardous conditions. Employees should be conscientious about workplace safety, including proper operating methods and known dangerous conditions or hazards. You should report any unsafe conditions or potential hazards to your supervisor immediately; even if you believe you have corrected the problem. If you suspect a concealed danger is present on the Company's premises, or in a product, facility, piece of equipment, process, or business practice for which the Company is responsible, bring it to the attention of your supervisor immediately.  

Additionally, the Company has developed a written Injury and Illness Prevention Program as required by law. A copy of the Program is available for your review from Human Resources. In addition to attending any training required by the Company, it is your responsibility to read, understand and observe the Injury and Illness Prevention Program provisions applicable to your job.  

Any workplace injury, accident, or illness must be reported to your supervisor as soon as possible (within 24 hours), regardless of the severity of the injury or accident. If medical attention is required immediately, supervisors will assist employees in obtaining medical care, after which the details of the injury or accident must be reported.  

#### Cell Phone Use / Texting While Driving  

As is set forth in the National Handbook, the Company prohibits employees from using cellular phones for business reasons while driving or for any reason while driving for work-related purposes or driving a company-owned vehicle.  Employees should also be aware that driving while holding and operating a handheld wireless telephone or electronic wireless communications device is a violation of California law unless the device is specifically designed and configured to allow hands-free operation and is used in that manner while driving.  Under California law, such handheld devices can only be operated while driving in a manner requiring use of the driver’s hand if:  the device is mounted on the vehicle’s windshield or affixed to the dashboard or center console in a manner that does not hinder the driver’s view of the road; and the driver uses their hand to activate or deactivate a feature of the device with a single swipe or tap of the driver’s finger.  


## Colorado  

### GENERAL INFORMATION  

#### About This Colorado Supplement  

ConstructConnect is committed to workplace policies and practices that comply with federal, state and local laws. For this reason, Colorado employees will receive the Company’s national handbook (“National Handbook”) and the Colorado Supplement to the National Handbook (“Colorado Supplement”) (together, the “Employee Handbook”).  

The Colorado Supplement applies only to Colorado employees. It is intended as a resource containing specific provisions derived under Colorado law that apply to the employee’s employment. It should be read together with the National Handbook and, to the extent that the policies in the Colorado Supplement are different from, or more generous than those in the National Handbook, the policies in the Colorado Supplement will apply.  

The Colorado Supplement is not intended to create a contract of continued employment or alter the at-will employment relationship. **Only the President/Owner of the Company or that person’s authorized representative has the authority to enter into an agreement that alters the at-will employment relationship and any such agreement must be in writing signed by the President/Owner of the Company or an authorized representative.**  

If employees have any questions about these policies, they should contact their Human Resources representative.  

### COMMITMENT TO DIVERSITY

#### Equal Employment Opportunity  

As set forth in the National Handbook, the Company is committed to equal employment opportunity and to compliance with federal antidiscrimination laws. We also comply with Colorado law, which prohibits discrimination and harassment against any employees or applicants for employment based on race (including hair texture, hair type or protective hairstyles commonly or historically associated with race (e.g., braids, locs, twists, tight coils or curls, cornrows, bantu knots, afros and headwraps)), color, creed, sex (including pregnancy, married women and unmarried mothers), religion, disability, age (over 40), national origin, sexual orientation (including actual or perceived orientation and transgender status), ancestry, religion, civil air patrol status and lawful activities during nonworking hours. The Company also does not discriminate against qualified applicants because they did not apply through a private employment agency and does not discriminate against employees who inquire about, disclose, compare or otherwise discuss their wages. The Company generally does not discriminate against employees or applicants for employment solely because they are married to a co-worker, though exceptions exist where, for example, one employee exercises supervisory authority over, audits or has access to the employer’s confidential information about the other. The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law.  

#### Pregnancy Accommodation  

Employees and applicants for employment may request a reasonable accommodation for pregnancy, physical recovery from childbirth or related health conditions.  A reasonable accommodation that would enable the employee or applicant to perform the essential functions of their job will be provided unless the accommodation would impose an undue hardship on the company's business operations.  

Reasonable accommodations may include but are not limited to: more frequent or longer breaks; more frequent restroom, food or water breaks; acquisition or modification of equipment or seating; limitations on lifting; temporary transfer to a less strenuous or hazardous position, if available, with return to the current position following pregnancy; job restructuring; light duty, if available; assistance with manual labor; or a modified work schedule.  

The Company may require that employees provide a certification from a licensed health care provider regarding the medical necessity of a reasonable accommodation.  

Employees who have questions about this policy or who wish to request a reasonable accommodation under this policy should contact their Human Resources representative.  

### GENERAL EMPLOYMENT PRACTICES  

#### Access to Personnel Files  

Upon request, employees will be allowed to inspect their personnel files at least one time per year. Employees who wish to review their personnel files should contact Human Resources. The review will take place in the presence of a Company representative at a time that is convenient for both the employee and the Company. Employees are permitted to obtain a copy of their personnel files, but may be required to pay reasonable costs for the duplication of the documents.  

Following separation from employment, former employees may inspect and/or obtain a copy of their personnel files one time. The former employee may be required to pay reasonable costs for duplication of the documents.  

For purposes of this policy, a personnel file does not include documents required by law to be placed in a separate file or records relating to: confidential reports from previous employers of the employee; an active criminal investigation; an active disciplinary investigation by the Company; an active investigation by a regulatory agency or and information that identifies an individual who made a confidential accusation against the employee.  

### PAY PRACTICES  

#### Meal and Rest Breaks  

Nonexempt employees who work five or more consecutive hours will be provided at least one 30-minute meal break. During the break employees will be relieved of all duties. An uninterrupted 30-minute meal break will be unpaid. If the nature of an employee’s job or circumstances makes an uninterrupted meal break impracticable, the employee will be allowed an on-duty meal break without any loss of time or compensation.  

Nonexempt employees will also be permitted a 10-minute rest break for every four hours of work, in accordance with the schedule below:  

|Duration of Shift In Hours|# of 10 Minute Rest Breaks|Comments|  
|--|--|--|  
|0 to <2|0|Employees who work less than two hours in a workday are not required or permitted to take a rest break.|  
|2 to <6|1|Employees who work at least two hours in a workday but less than six hours in a workday are allowed one 10-minute rest break.|  
|6.0 to 10.0|2|Employees who work at least six hours in a workday but less than 10 hours in a workday are allowed two 10-minute rest breaks.|  
|10.0 to < 14.0|3|Employees who work at least 10 hours in a workday but less than 14 hours in a workday are allowed three 10-minute rest breaks.|  

To the extent practical, rest periods will be provided in the middle of each four-hour work period. Employees who are unable to take all of the meal or rest breaks to which they are entitled in accordance with this policy, or who have been prevented or discouraged from taking a break to which they are entitled under this policy, should immediately notify a Human Resources representative.  The Company will not threaten, coerce, discriminate or otherwise retaliate against any employee who reports a violation of this policy or files a claim or participates in an investigation, hearing or other process or proceeding related to an alleged violation of federal or state wage and hour laws.  

##### Additional Information  

A Colorado Overtime and Minimum Pay Standards Order (COMPS Order #37) Poster is attached to this handbook.  

#### Overtime  

Employees will be paid one and one-half times their regular rate of pay for any work in excess of:  

1. 40 hours per week;    
1. 12 hours per workday; or    
1. 12 consecutive hours without regard to the starting and ending time of the workday (excluding duty-free meal breaks), whichever calculation results in the greatest payment of wages.  

For additional information, a Colorado Overtime and Minimum Pay Standards Order (COMPS Order #37) Poster is attached to this handbook.  

#### Lactation Accommodation  

The Company will provide a reasonable amount of break time to accommodate an employee desiring to express breast milk for the employee’s child. The Company will provide this break time for up to two years following the birth of a child.  

Nursing mothers can elect to take time to express breast milk during their regularly scheduled meal and rest breaks. If the break time cannot run concurrently with the meal and/or rest breaks already provided to the employee, the break time will be unpaid for nonexempt employees. Where additional breaks are required, employees should work with their supervisor regarding scheduling.  

The Company will make reasonable efforts to provide employees with the use of a private location, other than a toilet stall, in close proximity to the employee’s work area, for the employee to express milk.  

Employees should provide reasonable notice to the Company that they intend to take breaks for expressing breast milk upon returning to work. Employees should discuss with their supervisor, a Human Resources representative the location to express their breast milk and for storage of expressed milk and to make any other arrangements under this policy.  

The Company reserves the right to not provide additional break time or a private location for expressing breast milk if doing so would substantially disrupt the Company's operations.  

The Company will not demote, terminate or otherwise take adverse action against an employee who requests or makes use of the accommodations and break time described in this policy.  

#### Discussion of Wages  

No employee is prohibited from inquiring about, disclosing, comparing or otherwise discussing their wages. The Company will not terminate, discipline, coerce or otherwise discriminate against employees because they make such inquiries, disclosures, comparisons or otherwise engage in such discussions of their wages.  

### TIME OFF AND LEAVES OF ABSENCE  

#### Paid Sick and Safe Leave and Public Health Emergency Leave  

The Company provides eligible employees with paid sick and safe leave (“PSSL”) and public health emergency leave (“PHEL”) in accordance with the requirements of Colorado’s Healthy Families and Workplaces Act (“HFWA”).  

##### Eligibility  

Colorado employees are eligible to accrue PSSL and may receive additional leave for use during a public health emergency (as defined further below).  

##### Accrual and Use of Paid Sick and Safe Leave (“PSSL”)  

Eligible employees will begin to accrue PSSL on January 1, 2021, or their date of hire, whichever occurs later. PSSL accrues at a rate of one hour for every 30 hours worked, up to a maximum accrual of 48 hours in a single calendar year (the “Benefit Year”). Exempt employees accrue PSSL based on their normal hours worked, up to a maximum of 40 hours per week.  

PSSL can be used as it is accrued.  However, the Company may verify employee hours within the month after work is performed and adjust PSSL accrual amounts to correct any inaccuracy.  The Company will notify employees in writing of any such change in accrued PSSL amounts.  

PSSL may be used in one-hour increments. Eligible employees may use up to 48 hours of PSSL in any Benefit Year.  

Failure to use PSSL in good faith and for the reasons specified in this policy can result in discipline.  

##### Reasons PSSL May be Used  

Eligible employees may use PSSL for the following reasons:  

- When a mental or physical illness, injury or health condition prevents the employee from working;  
- To care for a family member who has a mental or physical illness, injury or health condition;  
- To obtain a medical diagnosis, care or treatment of a mental or physical illness, injury or health condition of the employee or employee’s family member;   
- To obtain preventive medical care for the employee or employee’s family member;    
- If the employee or a family member is the victim of domestic abuse, sexual assault or harassment and needs leave to:  
	- Seek medical attention to recover from a mental or physical illness, injury or health condition caused by the domestic abuse, sexual assault or harassment;  
	- Obtain services from a victim services organization;    
	- Obtain mental health or other counseling;  
	- Seek relocation due to the domestic abuse, sexual assault or harassment; or  
	- Seek legal services, including preparing for or participating in a civil or criminal proceeding relating to or resulting from the domestic abuse, sexual assault or harassment.  
- When, due to a public health emergency (as defined below), a public official has ordered the closure of:   
	- The employee’s place of business; or  
	- The school or place of care of the employee’s child and the employee needs to be absent from work to care for their child.  

For purposes of this policy, a “family member” means:  

- An employee's immediate family member (i.e., a person related by blood, marriage, civil union or adoption);  
- A child to whom the employee stands in loco parentis;  
- A person who stood in loco parentis to the employee when the employee was a minor; or  
- A person for whom the employee is responsible for providing or arranging health-or safety-related care.  

##### Requesting PSSL  

The Company will allow use of PSSL for a covered use upon request.  Requests can be made orally or in writing (including electronically).  When possible, employees should include the expected duration of the absence in their request for leave.  

When the need for PSSL is foreseeable, employees must make a good faith effort to provide advance notice of the need for leave and a reasonable effort to schedule the leave in a manner that does not unduly disrupt the Company’s operations. To provide this advance notice of the foreseeable need to use PSSL, employees should contact their Human Resources representative.  

Employees are not required to search for or find a replacement worker to cover the hours during which they are using PSSL. The Company will not count employees' use of PSSL in compliance with this policy as an absence when evaluating absenteeism. Therefore, any such use of PSSL will not lead to or result in discipline, demotion, suspension or termination.  

##### Documentation of PSSL  

If PSSL is for four or more consecutive work days (meaning at least four consecutive days that the employee would ordinarily have worked), the Company may request that employees provide reasonable documentation that the PSSL is being used for a permissible purpose. If the documentation submitted by the employee is not sufficient, the Company will notify the employee of the deficiency.  The employee will then have seven days to provide adequate documentation.  

In accordance with the HWFA, the Company does not require the disclosure of details regarding an employee’s or employee’s family member’s health information or the domestic violence, sexual assault, or stalking that is the basis for the request for leave.  

##### Carryover of PSSL  

Employees can carry over up to 48 hours of accrued but unused PSSL from one Benefit Year to the next. However, employees may not use more than 48 hours of PSSL in a Benefit Year.  

The Company does not offer pay in lieu of actual PSSL.  

##### Public Health Emergency Leave  

In addition to the PSSL described above, the Company will provide employees with PHEL in accordance with the terms below.  

For purposes of this policy, a “public health emergency” is:  

- An act of bioterrorism, a pandemic influenza or an epidemic caused by a novel and highly fatal infectious agent, for which:    
	- An emergency is declared by a federal, state or local public health emergency; or   
	- A disaster emergency is declared by the governor; or  
- A highly infectious illness or agent with epidemic or pandemic potential for which a disaster emergency is declared by the Governor.  

On the day a public health emergency is declared, employees will immediately be able to access a one-time supplement of PHEL in addition to whatever amount of PSSL employees have accrued prior to the declaration of the public health emergency. Employees who normally work forty or more hours in a week will have access to up to 80 hours of total paid leave. Employees who normally work fewer than 40 hours per week will have access to paid leave equaling the greater of:  

1. the amount of time the employee is scheduled for work or paid leave in the 14-day period after the leave request; or    
1. the amount of time the employee actually worked in the 14-day period prior to the declaration of the public health emergency or the leave request, whichever is later.  

From the declaration of a public health emergency until four weeks after the official termination or suspension of the emergency declaration, PHEL can be used for any of the following reasons:  

- To self-isolate and care for oneself or a family member who is self-isolating because the employee or family member is diagnosed with, or experiencing symptoms of, a communicable illness that is the cause of a of a public health emergency;  
- To seek or obtain for oneself or care for family member who needs a medical diagnosis, care, or treatment if experiencing symptoms of a communicable illness that is the cause of a public health emergency;  
- To seek for oneself or a family member preventive care concerning a communicable illness that is the cause of a public health emergency;   
- An employee is unable to work because the employee has a health condition that may increase susceptibility to or risk of communicable illness that is the cause of the public health emergency;   
- Either the Company or a public health authority with appropriate jurisdiction determines that an employee’s presence on the job or in the community would jeopardize the health of others because of the individual's exposure to a communicable illness that is the cause of a public health emergency or because the individual is exhibiting symptoms of such a communicable illness, regardless of whether the individual has been diagnosed with the illness;   
- To care for a family member after either the family member’s employer or a public health authority with appropriate authority determines that the family member’s presence on the job or in the community would jeopardize the health of others because of the family member's exposure to a communicable illness that is the cause of a public health emergency or because the family member is exhibiting symptoms of such a communicable illness, regardless of whether the family member has been diagnosed with the illness;  
- To care for a child or other family member when their child care provider is unavailable due to a public health emergency or their school or place of care has been closed due to a public health emergency (including when the school or place of care is physically closed but providing instruction remotely).  

PHEL will become available on the date a public health emergency is declared and will remain available until four weeks after the official termination or suspension of the public health emergency.  Employees are only eligible for these amounts of PHEL one time during the entirety of a public health emergency (even if the public health emergency is extended, amended, restated or prolonged).  

During a public health emergency, employees will continue to accrue PSSL in accordance with this policy. Any accrued, unused PSSL will be counted in determining the amount of PHEL available.  

When the need for PHEL is foreseeable and the workplace has not been closed, employees must notify the Company of the need for PHEL as soon as practicable.  To provide notice of the need to use PHEL, employees should contact their Human Resources representative.  

##### Rate of Pay  

PSSL and PHEL are paid at the same hourly rate or salary (not including overtime, bonuses or holiday pay) and with the same benefits, including health care benefits, as the employee normally earns during hours worked. Leave will be paid on the same schedule as regular wages.  

##### Employee Records Requests  

Upon an employee’s request, the Company will provide (in writing or electronically) documentation indicating the current amount of PSSL and/or PHEL available for use and the amount of such leave already used during the current calendar year. Employees will be allowed to make one such request per month, except they may make an additional request when any need for PSSL or PHEL arises.  

##### Effect on Other Rights and Policies  

The Company may provide other forms of leave for employees to care for medical conditions or for issues related to public health emergencies or domestic abuse, sexual assault or harassment under certain federal, state and local laws. In certain situations leave under this policy may run at the same time as leave available under another federal, state or local law, provided eligibility requirements for that law are met. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other federal, state and local medical, victim, public health emergency or family leave rights.  

##### Confidentiality  

The Company will keep confidential the health or safety information of an employee or employee's family member. Such information will not be disclosed except to the affected employee, with the written permission of the affected employee or as otherwise required by law.  

##### Separation from Employment  

Compensation for accrued and unused PSSL or available PHEL is not provided upon separation from employment for any reason.  If an employee is rehired by the Company within six months of separation from employment, previously accrued but unused PSSL will be immediately reinstated.  

##### Retaliation  

Employees have the right to request and use PSSL and PHEL in a manner consistent with the HWFA. The Company will not discriminate or retaliate, or tolerate discrimination or retaliation, against any employee who: seeks or obtains leave in accordance with this policy; files a complaint regarding an alleged violation of the HWFA; participates in an investigation, hearing or proceeding or cooperates in or assists with an investigation related to an alleged violation of the HWFA; informs any person of their potential rights under the HWFA; or otherwise exercises their rights under the HWFA.  

#### Vacation  

The Company will pay employees for any earned or accrued but unused vacation at termination of employment.  

#### Adoption Leave  

Employees who are adoptive parents will be permitted to take leave under the same terms as leave provided to biological parents for the adoption of a child. Requests for additional leave due to the adoption of an ill child or child with a disability will be considered on the same basis as comparable cases of complications accompanying the birth of a child.  

This policy does not apply to adoption by the spouse of a custodial parent or to second-parent adoption.  

For further information or to request leave under this policy, contact a Human Resources representative.  

#### Family Care Act Leave  

Employees who are eligible for leave under the federal Family and Medical Leave Act (FMLA) and who are in registered domestic partnerships or civil unions may take leave in accordance with the FMLA to care for their domestic or civil union partners with a serious health condition. A serious health condition has the same meaning as reflected in the Company’s FMLA policy.  

Employees seeking leave under this policy must comply with the eligibility, notice, certification and other requirements set forth in the FMLA policy contained in the National Handbook and will be required to provide reasonable documentation of a family relationship.  

Where applicable, Family Care Act Leave and FMLA leave will run concurrently.  

For further information or to request leave under this policy, contact a Human Resources representative.  

#### Jury Duty Leave  

The Company encourages all employees to fulfill their civic responsibilities and to respond to jury service summons or subpoenas, attend court for prospective jury service or serve as a juror. Under no circumstances will employees be deprived of any benefits of employment, terminated, threatened, harassed, or coerced because they request or take leave in accordance with this policy.  

Employees will receive their regular compensation up to $50 per day, unless otherwise agreed to by the Company, during the first three days of jury service. This includes part-time, casual and temporary employees, so long as their employment hours can be determined by a schedule, custom or practice established during the three-month period preceding jury service. Any additional time off under this policy will be without pay, except that exempt employees will not incur any reduction in pay for a partial week absence due to jury duty.  

Employees seeking compensation for jury duty leave must provide a juror service certificate from the court as soon as practical. The Company will compensate the employee in accordance with this policy within 30 days of receiving the service certificate.  

Employees should provide their supervisor with notice of any jury summons or subpoena within a reasonable amount of time after receipt and before their appearance is required.  

#### Crime Victim Leave  

Employees may take time off from work for the purpose of responding to a subpoena to testify in a criminal proceeding or to participate in the preparation of a criminal proceeding, if:  

- The employee is a victim of the crime at issue in the proceeding;  
- The employee is the crime victim’s spouse, child by birth or adoption, stepchild, parent, stepparent, sibling, legal guardian or significant other (i.e., someone in a family-type living arrangement, who would constitute the spouse or partner of the victim if they were married); or  
- The victim is deceased or incapacitated and the employee is the victim’s spouse, partner, parent, child, sibling, grandparent, significant other or other lawful representative.  

Employees, who are in custody for the crime, accused of the crime or otherwise accountable for the crime, are not eligible for time off under this policy.  

Leave under this policy will be unpaid except that exempt employees will not incur any reduction in pay for a partial week absence due to witness duty.  

#### Domestic Violence Victim Leave  

Employees who are victims of domestic violence, including sexual abuse, stalking, sexual assault or any other crime including an act found by a court to be domestic violence, may take up to three working days of unpaid leave time within a 12-month period. Only employees employed with the Company for 12 or more months are eligible for this leave.  

Before taking unpaid leave under this policy, employees must exhaust any available annual vacation leave, PTO, personal leave and sick leave, if applicable.  

Employees may use leave available under this policy to:  

- Seek a civil protection order to prevent domestic abuse;  
- Obtain medical care and/or medical health counseling for the employee or the employee’s children to address physical or psychological injuries resulting from the act of domestic abuse, stalking, sexual assault or other crime involving domestic violence;   
- Make the employee’s home secure from the perpetrator of the crime or seek new housing to escape the perpetrator; or  
- Seek legal assistance to address issues arising from the crime and attend and prepare for court-related proceedings arising from the act or crime.  

Except in a case of imminent danger, an employee seeking leave from work under this policy must provide the Company with advance notice of the leave. In addition, the Company may require the employee to provide documentation verifying the need for the leave.  

Confidentiality of the situation will be maintained to the extent possible.  

The Company will not retaliate or tolerate retaliation against any employee who seeks or obtains leave under this policy.  

#### Time Off to Vote  

The Company encourages all employees to fulfill their civic responsibilities and to vote in all public elections. Most employees’ schedules provide sufficient time to vote either before or after working hours.  

Employees who have less than three consecutive hours outside of work during which the polls are open will be allowed up to two hours of time off to vote, without loss of pay. Upon request, the Company will schedule the leave at the beginning or end of the employee’s shift. The Company will otherwise specify when the leave may be taken.  

Employees must provide notice of the need for time off prior to Election Day.  

#### Military Leave  

In addition to the military leave rights set forth in the National Handbook, regular full-time and part-time employees who are members of the Colorado National Guard are entitled to an unpaid leave of absence to perform active state service.  

Additionally, regular full-time and part-time employees who are members of the Colorado National Guard or United States armed forces reserves may take up to 15 days of unpaid leave per calendar year for military training with the United States armed forces.  

Upon return from active state service or military training, employees will be reinstated to their former position or to a position of like seniority, status and pay, so long as they:  

- Had a nontemporary job before taking leave;  
- Provide evidence that training or service was satisfactorily completed; and  
- Are still qualified to do the job.  

Absence for military service or training will not affect an employee’s rights to receive normal vacation, sick leave, bonuses, advancement or other advantages of employment that would otherwise be expected for the employee’s particular job.  

#### Civil Air Patrol Leave  

Regular full-time and part-time employees who are members of the Civil Air Patrol are entitled to an unpaid leave of absence, not to exceed 15 work days in any calendar year, when called to serve on a Civil Air Patrol mission.  

Upon return from the leave, employees will be reinstated to their former position or to a similar position, so long as they:  

- Had a nontemporary job before taking leave;  
- Return as soon as practicable to their position after being relieved from service for the mission;  
- Provide evidence that the service was satisfactorily completed; and  
- Are still qualified to do the job.  

Absence for Civil Air Patrol service will not affect employee rights to receive normal vacation, sick leave, bonuses, advancement or other advantages of employment that would otherwise be expected for the employee’s particular job.  

#### Volunteer Firefighters Leave  

Employees who serve as volunteer firefighters may take time off to respond to an emergency summons that occurred prior to the time the employee is scheduled to report to work.  

Employees who serve as volunteer firefighters will also be allowed time off to respond to an emergency summons after the employee has begun work, if:  

- The Company does not consider the employee to be essential to the daily operations of the employer’s daily enterprise;  
- The employee previously provided written verification of volunteer status from the fire chief; and  
- The emergency is within the response area of the employee’s fire department and is of such magnitude that all firefighters must respond.  

Employees must provide written verification from the fire chief of the time, date and duration of the employee’s response to the emergency.  

Time off under this policy will be unpaid except that exempt employees may be paid, as required by law.  

#### Qualified Volunteers Leave  

Regular full-time and part-time (i.e., nontemporary) employees who are qualified volunteers will be allowed time off if called into service by a volunteer organization during a disaster, so long as they provide proof of their status as a qualified volunteer. For purposes of this policy, employees will be considered a qualified volunteer if the:  

- Employee is a member of a volunteer organization that enters into a memorandum of understanding with a county sheriff, local government, local emergency planning committee or state agency;  
- Volunteer organization is included on the qualified volunteer organization list created and maintained by the Department of Local Affairs;  
- Employee is called to service through the volunteer organization under the authority of the county sheriff, local government, local emergency planning committee, or state agency to volunteer in a disaster; and   
- Employee receives the appropriate verification from the Colorado Department of Local Affairs that: (a) indicates the volunteer was called to service by a volunteer organization for the purpose of assisting in a disaster; (b) verifies the volunteer reported for service and performed the activities required by the volunteer organization; and (c) includes the number of days of service that the volunteer provided.  

Leave under this policy will not exceed 15 work days in any calendar year and will be unpaid.  

Employees will, upon completion of the volunteer emergency service and return to work, be restored to the same or similar position as they held prior to the leave. Taking leave under this policy will not affect an employee’s rights to vacation, sick leave, bonus, advancement or other employment benefits or advantages relating to and normally to be expected for the employee’s particular employment.  

Employees must return to their employment position as soon as practicable after being relieved from service.  

Leave may be denied if more than 20 percent of the Company’s employees on any work day request such leave. Leave may also not be available for essential employees, defined as those employees the Company deems essential to the operation of the Company’s daily enterprise, whose absence would likely cause the Company to suffer economic injury, or whose duties including assisting in disaster recovery for the Company.  

### SAFETY AND SECURITY  

#### Smoke-Free Workplace  

The Company prohibits smoking marijuana or any other substance that is illegal under federal law or Colorado law anywhere on its premises.  

The Company prohibits smoking in the workplace and within 15 feet of any window, ventilation intake or entrance to the workplace. For purposes of this policy, smoking includes the use of electronic smoking devices (e.g., e-cigarettes or vaping).  Employees wishing to smoke must do so outside of company’s facilities, in locations where smoke does not migrate back into the workplace, during scheduled work breaks.  

Employees that observe other individuals smoking in the workplace have a right to object and should report the violation to their supervisor or to another member of management. Employees will not be disciplined or retaliated against for reporting smoking that violates Colorado law or this policy.  

Employees that violate this policy may be subject to disciplinary action up to and including termination.  

#### Cell Phone Use / Texting While Driving  

As set forth in the National Handbook, the Company prohibits employees from using cellular phones for business reasons while driving or for any reason while driving for work-related purposes or driving a company-owned vehicle. Employees should also be aware that texting while driving is a violation of Colorado law, in addition to being a violation of company policy. It is also a violation of Colorado law to use earphones while driving, unless the earphone is built into protective headgear or is a device that only covers one ear and is connected to a wireless, handheld telephone.  

### ADDITIONAL RESOURCES

[COLORADO OVERTIME & MINIMUM PAY STANDARDS Effective January 1, 2021 ORDER (“COMPS Order”) #37 POSTER](https://cdle.colorado.gov/sites/cdle/files/COMPS%20Order%20%2337%20%282021%29%20Poster%20CLEAN.pdf){:target="_blank"}  


---  
**ACKNOWLEDGMENT OF RECEIPT OF COLORADO OVERTIME AND MINIMUM PAY STANDARDS ORDER (COMPS ORDER #37) POSTER**  

I acknowledge that I have been provided with a copy of the Colorado Overtime and Minimum Pay Standards Order (COMPS ORDER #37) poster.  

TEAM MEMBER'S NAME (printed):

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  

TEAM MEMBER'S SIGNATURE:

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

DATE:  

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  

---  

## Florida

### GENERAL INFORMATION  

#### About This Florida Supplement  

ConstructConnect is committed to workplace policies and practices that comply with federal, state and local laws. For this reason, Florida employees will receive the Company’s national handbook (“National Handbook”) and the Florida Supplement to the National Handbook (“Florida Supplement”) (together, the “Employee Handbook”).  

The Florida Supplement applies only to Florida employees. It is intended as a resource containing specific provisions derived under Florida law that apply to the employee’s employment. It should be read together with the National Handbook and, to the extent that the policies in the Florida Supplement are different from, or more generous than; those in the National Handbook, the policies in the Florida Supplement will apply.  

The Florida Supplement is not intended to create a contract of continued employment or alter the at-will employment relationship. **Only the President/Owner of the Company or that person’s authorized representative has the authority to enter into an agreement that alters the at-will employment relationship and any such agreement must be in writing signed by the President/Owner of the Company or an authorized representative.**  

If employees have any questions about these policies, they should contact their Human Resources representative.  

### COMMITMENT TO DIVERSITY  

#### Equal Employment Opportunity  

As set forth in the National Handbook, ConstructConnect is committed to equal employment opportunity and to compliance with federal antidiscrimination laws. We also comply with Florida law, which prohibits discrimination and harassment against any employees or applicants for employment based on race, color, religion, sex (including pregnancy, childbirth, or related medical conditions), pregnancy, national origin or ancestry, age, handicap, marital status, genetic testing, HIV or AIDS (actual or perceived) or based on sickle cell trait or testing. The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law.  


### TIME OFF & LEAVES OF ABSENCE  

#### Military Leave  

An employee will not be discriminated against because the employee either: (1) belongs to the National Guard and is required to report for active duty; or (2) has an obligation as a member of a reserve component of the United States armed forces. Florida employees who are members of the National Guard may take unpaid military leave when ordered to state active duty.  

Members of the National Guard who take leave under this policy must notify the Company of their intent to return to work promptly upon the completion of active duty. Members of the National Guard may, but are not required to, use any accrued vacation or similar paid leave during military leave.  

Employees who are members of the Florida National Guard or the United States reserves who are called to active national or state duty are eligible to continue health insurance coverage, for themselves or their dependents, at the premium in effect prior to the military leave. If the employee elects not to continue coverage while on active duty, the Company will, at the employee’s request, reinstate coverage upon return from active duty. Such coverage will be reinstated without the employee having to satisfy a waiting period and without a disqualification for any condition that existed at the time the employee was called to active duty.  

#### Civil Air Patrol Leave  

Eligible employees who are members of the Civil Air Patrol will be allowed up to 15 days of unpaid leave annually for the purpose of participating in Civil Air Patrol training or missions. Employees will not be required to use accrued vacation, sick leave or any other type of accrued leave prior to taking unpaid Civil Air Patrol leave, but may choose to use such benefits.  

Employees are eligible for leave if they are senior members of the Florida Wing of the Civil Air Patrol with at least an emergency services qualification and have worked for the Company for at least 90 days prior to the start of leave.  

The Company will not terminate, reprimand or otherwise penalize a Civil Air Patrol member because of his or her absence due to the use of Civil Air Patrol leave.  

Employees must promptly notify the Company of his or her intent to return to work following the completion of Civil Air Patrol leave.   

Employees returning from Civil Air Patrol leave will receive the seniority they had as of the date their leave began, and any other rights and benefits they would receive as a result of such seniority. Employees returning from Civil Air Patrol leave will also receive any additional seniority they would have attained if they had remained continuously employed, and any other rights and benefits they would receive as a result of such seniority.    

Civil Air Patrol members returning to work from leave will not be terminated for a year after the date the employee returns to work, except for cause.  

#### Domestic or Sexual Violence Victim Leave  

Employees who are victims of domestic or sexual violence or that have a family or household member who are a victim of domestic violence may take up to three working days of unpaid leave time within a 12-month period. Employees employed with the Company for three or more months are eligible for this leave. Employees must exhaust any available annual vacation leave, PTO, personal leave, and sick leave, if applicable.  

“Family or household member” is defined as “spouses, former spouses, persons related by blood or marriage, persons who are presently residing together as if a family or who have resided together in the past as if a family, and persons who are parents of a child in common regardless of whether they have been married.”  

Employees may use leave available under this policy to:  

- Seek an injunction for protection against domestic violence or an injunction for the protection in cases of repeat violence, dating violence or sexual violence;  
- Obtain medical care and/or medical health counseling for the employee, a family member, or household member to address physical or psychological injuries resulting from domestic or sexual violence;  
- Obtain services from a victim-services organization, including, but not limited to, a domestic violence shelter or program or a rape crisis center as a result of the act of domestic or sexual violence;  
- Make the employee’s home secure from the perpetrator or seek new housing to escape the perpetrator; and/or  
- Seek legal assistance in addressing issues arising from the act of domestic or sexual violence or to attend and prepare for a related court proceeding.  

Except in cases of imminent danger to the health and safety of the employee or a family member, employees needing domestic or sexual violence leave must provide the Company with at least 48 hours advance notice. The Company may require employees to provide certification of the purpose of the leave in the form of a letter from a volunteer services organization, police report or court record or other corroborating evidence.  

#### Jury Duty Leave  

The Company encourages all employees to fulfill their civic responsibilities and to respond to jury service summons or subpoenas, attend court for prospective jury service or serve as a juror. Under no circumstances will employees be terminated, threatened, coerced, or penalized because they respond to a jury service summons or subpoenas, attend court for prospective jury service or serve as a juror.  

Employees must provide their supervisor with notice of any jury summons or subpoena on the next day they return to work after the receipt of the summons or subpoena. Verification from the court clerk of having served may be required.    

Time spent engaged in attending court for prospective jury service or for serving as a juror is not compensable except that exempt employees will not incur any reduction in pay for a partial week's absence due to jury duty.  

#### Jury Duty Leave (Miami-Dade County)  

The Company encourages all employees to fulfill their civic responsibilities and to respond to jury service summons or subpoenas, attend court for prospective jury service or serve as a juror. Under no circumstances will employees be terminated, threatened, coerced or penalized because they request or take leave in accordance with this policy.  

Employees must provide their immediate supervisor with notice of any jury summons or subpoena at least five workdays before jury duty services. Verification from the court clerk of having served may be required.  

Full-time employees (those who are regularly scheduled to work at least 35 hours per week) will receive their regular compensation during absences for jury service.  Compensation for absences due to jury service does not include commissions, overtime pay or compensation for more than eight working hours per day.  For part-time employees, time spent engaged in attending court for prospective jury service or for serving as a juror is not compensable except that exempt employees will not incur any reduction in pay for a partial week's absence due to jury duty.  

#### Family and Medical Leave—Miami-Dade County Employees  

Eligible employees may take family and medical leave in accordance with the family and medical leave policy (“FMLA policy”) set forth in the National Handbook. In addition to the group of family members for which an employee may take FMLA leave, eligible employees working in Miami-Dade County may also take leave to care for a grandparent with a serious health condition. Such leave will be under the same terms and conditions as leave is permitted under the FMLA policy to care for a parent with a serious health condition. For purposes of this policy, “grandparent” means a grandparent of the employee for whom the employee has assumed primary financial responsibility.  

Eligible employees are those that work in Miami-Dade County for at least 12 months and have at least 1,250 hours during the previous 12-month period.  

The length of leave permitted is 12 workweeks in a 12-month period to run concurrently with any leave provided under the federal FMLA.  

#### Domestic Violence Leave—Miami-Dade County Employees  

Eligible employees located in Miami-Dade County may take up to 30 working days of unpaid leave in any 12 month period if the employee or his or her dependent child is the victim of domestic violence. Employees may take up to three working days of unpaid leave within any 12-month period when the employee’s family or household member, other than a dependent child, is the victim of domestic violence.  

Eligible employees are those who:  

- Have been continually employed by the Company for at least 90 days;  
- Worked at least 308 hours or more during the 90-day period; and  
- Have exhausted all accrued paid vacation, sick and personal leave.  

Employees may use leave under this policy to:  

- Obtain medical and dental treatment for conditions resulting from domestic violence, including treatment for the employee's dependent children;  
- Obtain legal assistance relating to domestic violence, including criminal prosecution and protective orders as well as divorce, child custody and child support;  
- Attend court appearances relating to domestic violence;  
- Receive counseling or support services for the employee or the employee's dependent children; or  
- Permit other arrangements necessary to provide for the safety and well-being of the employee.  

Employees may take leave under this policy intermittently or on a reduced schedule basis. However, if an employee requests intermittent or reduced schedule leave that is foreseeable, the Company may require the employee to transfer temporarily to an available alternative position for which the employee is qualified and better accommodates recurring periods of leave.  

Employees requesting leave, except in cases of imminent danger to the health or safety of the employee, dependent child or other family or household member, must provide seven days’ notice prior to the proposed first day of leave. The notice must include the reason for requesting leave and sufficient documentation to show that the employee qualifies for leave. This documentation may include, but is not limited to, copies of restraining orders, police reports, orders to appear in court and/or certifications issued by an authorized person from a health care provider, attorney of record, counselor, law enforcement agency, clergy, domestic violence advocacy agency, domestic violence center or domestic violence shelter.  

Eligible employees who return from domestic violence leave will be restored to the position held prior to their leave or placed in an equivalent position, with equivalent employment benefits, pay, and other terms and conditions of employment. However, restored employees will not accrue seniority or employment benefits during any period of leave nor will they receive any right, benefit or position of employment other than any right, benefit or position to which the employee would have been entitled had the employee not taken the leave.  

To the extent that there is overlap between leave taken under this policy and leave taken under the Domestic or Sexual Violence Victim Leave policy in the Florida Supplement, the leaves will run concurrently.  

Employees will not be terminated, demoted, suspended, retaliated against, or in any other manner discriminated against for exercising their rights under this policy.  

### WORKPLACE SAFETY AND SECURITY  

#### Weapons in the Workplace  

In the interest of maintaining a workplace that is safe and free of violence, and in accordance with the policy set forth in the National Handbook, the Company generally prohibits the presence or use of firearms and other weapons on the Company’s property, regardless of whether or not the person is licensed to carry the weapon  

In compliance with Florida law, the Company permits employees who lawfully possess firearms or ammunition to store their firearms or ammunition inside their locked, privately-owned vehicles in the Company’s parking lots or other parking areas provided by the Company. Such lawfully possessed firearms and ammunition may not be removed from the employees' personal vehicle or displayed to others.  

#### Smoke-Free Workplace  

The Company prohibits smoking, including vaping, in the workplace. Employees wishing to smoke or vape must do so outside company facilities during scheduled work breaks.  

Employees who observe other individuals smoking in the workplace in violation of this policy have a right to object and should report the violation to their supervisor or another member of management. Employees will not be disciplined or retaliated against for reporting smoking that violates this policy.  

Employees who violate this policy may be subject to disciplinary action up to and including termination of employment.  


## Georgia  

### GENERAL INFORMATION  

#### About This Georgia Supplement  

ConstructConnect is committed to workplace policies and practices that comply with federal, state and local laws. For this reason, Georgia employees will receive the Company’s national handbook (“National Handbook”) and the Georgia Supplement to the National Handbook (“Georgia Supplement”) (together, the “Employee Handbook”).  

The Georgia Supplement applies only to Georgia employees. It is intended as a resource containing specific provisions derived under Georgia law that apply to the employee’s employment. It should be read together with the National Handbook and, to the extent that the policies in the Georgia Supplement are different from, or more generous than those in the National Handbook, the policies in the Georgia Supplement will apply.  

The Georgia Supplement is not intended to create a contract of continued employment or alter the at-will employment relationship. **Only the President/Owner of the Company or that person’s authorized representative has the authority to enter into an agreement that alters the at-will employment relationship and any such agreement must be in writing signed by the President/Owner of the Company or an authorized representative.**  

If employees have any questions about these policies, they should contact their Human Resources representative.  

### COMMITMENT TO DIVERSITY  

#### Equal Employment Opportunity  

As set forth in the National Handbook, ConstructConnect is committed to equal employment opportunity and compliance with federal antidiscrimination laws. We also comply with Georgia law, which prohibits discrimination and harassment against employees and applicants for employment based on age (40-70) and disability and prohibits wage differentials based on sex. The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law.  

### GENERAL EMPLOYMENT PRACTICES  

#### Employment Eligibility and Work Authorization  

ConstructConnect participates in the Electronic Verification System (E-Verify) to electronically verify the work authorization of newly-hired employees. E-Verify is an internet-based program that compares information from an employee's Form I-9 to data contained in the federal records of the Social Security Administration and the Department of Homeland Security to confirm employment eligibility.  

The Company is committed to honoring all terms and conditions of E-Verify. Employees who do not contest a Tentative Nonconfirmation, or who receive a Final Nonconfirmation or No Show, are subject to immediate termination of employment.  

The Company will not tolerate any form of discrimination or harassment prohibited by federal, state or local law, including discriminatory treatment based on an individual's national origin or citizenship status. Employees who believe they have been subject to discrimination or harassment, including during the Form I-9 and E-Verify process, should immediately report the matter as further discussed in the policies regarding discrimination and harassment set forth in the Company’s National Handbook. The Company prohibits retaliation against employees for making such complaints.  

### PAY PRACTICES  

#### Lactation Accommodation  

The Company will provide reasonable breaks to accommodate an employee desiring to express breast milk at the work site, during work hours. Break time for lactation will be paid at the employee’s regular rate of compensation.  

The Company will provide employees with the use of a private location, other than a restroom, to express breast milk in privacy at the work site.  Employees should discuss with their supervisor, a Human Resources representative the location to express breast milk and to make any other arrangements under this policy.  

### TIME OFF AND LEAVES OF ABSENCE  

#### Mandatory Time Off/Day of Rest  

ConstructConnect will reasonably accommodate employees whose habitual day of worship falls on Saturday or Sunday, if the employee is scheduled to work on that day.  

#### Military Leave  

In addition to the military leave rights set forth in the National Handbook, Georgia regular full-time or part-time employees who leave their position with the Company to perform military service will be eligible for reinstatement upon completion of that service if they:  

- Provide a certificate of military service completion;  
- Are still qualified to perform the duties of the job; and  
- Apply for reinstatement within 90 days after being relieved from military service.  

Eligible employees will be restored to the same employment position or to a position of like seniority, status and pay. An exception may arise if the Company’s circumstances change such that it is impossible or unreasonable to provide reinstatement following the leave of absence.  

Non-temporary employees who must leave the Company for up to six months in a four year period to participate in assemblies or annual training, or to attend service schools conducted by the United States armed forces, are also entitled to reinstatement to their previous position, provided they are still qualified for the position and they apply for reemployment within 10 days after completion of the temporary period of service.  

Members of the Georgia National Guard or the National Guard of another state who are called into active state service by the Governor or other commander-in-chief and who, because of that service, are discharged or suspended from employment, will be eligible for reinstatement if:  they are still qualified to perform the duties of the position, and they apply for reinstatement within 10 days of the discharge or suspension or, if serving in active state service, within 10 days following the termination of that state service.  

Eligible employees who are reinstated following military service will be reinstated without loss of seniority and entitled to participate in insurance or other benefits in accordance with the Company’s established rules and practices relating to employees on furlough or a leave of absence.  Eligible employees also will not be discharged without cause for one year following reinstatement.  

Please contact a Human Resources representative for more information.  

#### Time Off to Vote  

The Company encourages all employees to fulfill their civic responsibilities and to vote in public elections. Most employees’ schedules provide sufficient time to vote either before or after working hours.  

Any employee who has less than two hours outside of working hours to vote while the polls are open may take up to two hours off from work, without loss of pay to vote. Any additional time off will be without pay for nonexempt employees.  

Employees must provide reasonable advance notice of the need for time off to vote so that the time off can be scheduled to minimize disruption to normal work schedules.  

Proof of having voted may be required.  

#### Jury and Witness Duty Leave  

The Company encourages all employees to fulfill their civic responsibilities and to respond to jury service or witness summons and subpoenas, attend court for prospective jury service or serve as a juror or witness. Under no circumstances will employees be terminated, threatened, coerced, or penalized because they request or take leave in accordance with this policy.  

Employees will receive their regular compensation for time spent engaged in attending court for prospective jury service or for serving as a juror or a witness under a subpoena or court order. Any mileage allowance or other fee paid for jury or witness duty will be credited against payments made by the Company. Employees who are absent from work because of a court order or judicial process due to being charged with a crime are not entitled to leave.  

An employee must provide the Company with reasonable advance notice that they received a jury duty summons or a subpoena. In addition, verification from the court clerk of having served or appeared may be required.  


#### Sick Leave to Care for Relatives  

Employees who work at least 30 hours per week for the Company may use paid, earned sick leave benefits provided by the Company (not including short-term or long-term disability) to care for an immediate family member, on the same terms that the employee is able to use personal sick leave benefits for the employee’s own incapacity, illness, or injury. Immediate family members include an employee’s child, spouse, grandchild, grandparent, or parent or any dependents as shown in the employee’s most recent tax return.  

The Company may, at its sole discretion, limit the use of an eligible employee’s sick time to care for an immediate family member under this policy to no more than five days of sick leave per calendar year.  

Employees with questions or concerns regarding this policy or who would like to request a leave of absence under this policy should contact their Human Resources representative.  


### SAFETY AND SECURITY  

#### Weapons in the Workplace  

In the interest of maintaining a workplace that is safe and free of violence, and in accordance with the policy set forth in the National Handbook, the Company generally prohibits the presence or use of firearms and other weapons on the Company’s property, regardless of whether or not the person is licensed to carry the weapon. In compliance with Georgia law, the Company permits employees who lawfully possess a concealed weapon to leave their weapon in their locked car in the Company’s parking lot. In the event that an employee has been subject to a disciplinary action or who has a disciplinary action pending, the Company reserves the right to prohibit the employee from bringing a concealed weapon onto company property.  

The Company will not be liable for criminal or civil actions resulting from the theft a firearm from an employee’s vehicle, and the Company will not provide additional security for employees who wish to store a firearm in their personal vehicle in compliance with this policy.  

The Company will generally not search an employee’s private vehicle or condition employment on an agreement by an employee not to store a licensed weapon in a locked car. However, employees should note that vehicles may be searched by law enforcement personnel, by the Company if the vehicle is employer-owned or in any situation in which a reasonable person would believe that accessing an employee’s locked vehicle is necessary to prevent an immediate threat to human health, life or safety.  

#### Smoke-Free Workplace  

The Company prohibits smoking in the workplace. Employees wishing to smoke must do so outside company facilities during scheduled work breaks.  

Employees that observe other individuals smoking in the workplace in violation of this policy have a right to object and should report the violation to their supervisor or another member of management. Employees will not be disciplined or retaliated against for reporting smoking that violates Georgia law or this policy.  

Employees that violate this policy will be subject to disciplinary action up to and including termination of employment.  

#### Cell Phone Use/Texting While Driving  

As is set forth in the National Handbook, the Company prohibits employees from using handheld cellular phones for business reasons while driving or for any reason while driving for work-related purposes or driving a company-owned vehicle. Employees should also be aware that physically holding or supporting a wireless communication or standalone electronic device, texting, and watching, recording or broadcasting a video on a wireless communication or standalone electronic device (other than an electronic device used solely for recording or broadcasting video) while driving is a violation of Georgia law, in addition to being a violation of company policy.  


## Illinois  


### GENERAL INFORMATION  

#### About This Illinois Supplement  

ConstructConnect is committed to workplace policies and practices that comply with federal, state and local laws. For this reason, Illinois employees will receive the Company’s national handbook (“National Handbook”) and the Illinois Supplement to the National Handbook (“Illinois Supplement”) (together, the “Employee Handbook”).  

The Illinois Supplement, however, applies only to Illinois employees. It is intended as a resource containing specific provisions derived under Illinois law that apply to the employee’s employment. It should be read together with the National Handbook and, to the extent that the policies in the Illinois Supplement are different from or more generous than those in the National Handbook, the policies in the Illinois Supplement will apply.  

The Illinois Supplement is not intended to create a contract of continued employment or alter the at-will employment relationship. **Only the President/Owner of the Company or that person’s authorized representative has the authority to enter into an agreement that alters the at-will employment relationship, and any such agreement must be in writing and signed by the President/Owner of the Company or an authorized representative.**  

If employees have any questions about these policies, they should contact their Human Resources representative.  

### COMMITMENT TO DIVERSITY  

#### Equal Employment Opportunity  

As set forth in the National Handbook, ConstructConnect is committed to equal employment opportunity and to compliance with federal antidiscrimination laws.  We also comply with Illinois law, which prohibits discrimination and harassment against any employees or applicants for employment based on their actual or perceived race, color, sex (including married women and unmarried mothers), religion, age (40 or older), national origin, ancestry, marital status, protective order status, military status, unfavorable discharge from military service, sexual orientation (including actual or perceived orientation and gender identity), citizenship status, genetic information, ancestry, pregnancy (including childbirth or medical or common conditions related to pregnancy or childbirth, past pregnancy condition and the potential or intention to become pregnant), certain arrest or criminal history records, homelessness (i.e., lack of a permanent mailing address or a mailing address that is a shelter or social services provider) and use of lawful products outside of work during nonworking hours. The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law.  

The Company also complies with the Illinois law that restricts the circumstances under which employers may base employment-related decisions on an individual’s credit report or credit history and with the Illinois law prohibiting sexual harassment of unpaid interns.  

#### Sexual and Other Unlawful Harassment  

ConstructConnect is committed to providing a work environment free of harassment. The Company complies with Illinois law and maintains a strict policy prohibiting sexual harassment and unlawful discrimination against employees or applicants for employment based on their actual or perceived race, color, religion, sex (including pregnancy, childbirth and related medical conditions), national origin, ancestry, age (40 or over), marital status, physical or mental disability, military status, sexual orientation (actual or perceived), gender identity, unfavorable discharge from military service, or citizenship status. The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law. The Company’s anti-harassment policy applies to all persons involved in its operations, including contractors or consultants, and prohibits harassing conduct by any employee of ConstructConnect, including supervisors, managers and nonsupervisory employees. This policy also protects employees from prohibited harassment by third parties, such as customers, vendors, clients, visitors, or temporary or seasonal workers.  

All employees are expected to comply with the Company’s Sexual and Other Unlawful Harassment policy as set forth in the National Handbook. While the Sexual and Other Unlawful Harassment policy sets forth the Company’s goals of promoting a workplace that is free of harassment, the policy is not designed or intended to limit the Company’s authority to discipline or take remedial action for workplace conduct that we deem unacceptable, regardless of whether that conduct satisfies the definition of unlawful harassment.  

Any employee who is found to have engaged in discriminatory or harassing conduct will be subject to appropriate disciplinary action, up to and including termination. Retaliation against anyone reporting acts of harassment or discrimination, participating in an investigation, or helping others exercise their right to complain about discrimination is unlawful and will not be tolerated.  

In addition to the complaint procedures set forth in the National Handbook, any employee who believes they have been harassed or discriminated against may file a complaint with the Illinois Department of Human Rights (IDHR).  

The IDHR may be reached at the following locations:  

- Chicago Office: James R. Thompson Center, 100 West Randolph Street, Suite 10-100, Chicago, Illinois 60601, telephone number (312) 814-6200, (866) 740-3953 (TTY), fax number (312) 814-6251.  
- Springfield Office: 535 W. Jefferson Street, 1st Floor, Springfield, Illinois 62702, telephone number (217) 785-5100, (866) 740-3953 (TTY), fax number (217) 785-5106.  
- Website: [IDHR](www.illinois.gov/dhr). Email: IDHR.Intake@illinois.gov.  

The employee may also report his or her concerns to the IDHR’s Illinois Sexual Harassment and Discrimination Helpline at (877) 236-7703.  

#### Pregnancy Accommodation  

Employees and applicants for employment may request a reasonable accommodation for pregnancy, childbirth or related medical or common conditions to enable them to perform the essential functions of their job.  In accordance with the Illinois Human Rights Act, a reasonable accommodation will be provided unless the accommodation would impose an undue hardship to the Company’s ordinary business operations.  

Reasonable accommodations may include but are not limited to: more frequent or longer bathroom, water or rest breaks; assistance with manual labor; light duty; temporary transfer to a less-strenuous or -hazardous position; acquisition or modification of equipment; reassignment to a vacant position; private, non-restroom space for expressing breast milk and breastfeeding; job restructuring; a part-time or modified work schedule; appropriate adjustment to or modification of examinations, training materials or policies; seating; an accessible worksite; and time off to recover from conditions related to childbirth or a leave of absence necessitated by pregnancy, childbirth or medical or common conditions resulting from pregnancy or childbirth.  

Employees who take leave as an accommodation under this policy will be reinstated to their original job or to an equivalent position with equivalent pay, seniority, benefits and other terms and conditions of employment upon their notification to the Company of their intent to return to work or when the employee’s need for a reasonable accommodation ends. Reinstatement is not required, however, if an undue hardship would result to the company’s business operations.  

The Company may request certain documents from the individual’s health care provider regarding the need for an accommodation. It is the employee’s or applicant’s duty to provide requested documentation to the Company.  

The Company will not deny employment opportunities or take adverse employment actions against employees or otherwise qualified applicants for employment based on the need to make such reasonable accommodations, nor will the Company retaliate against applicants or employees who request accommodations or otherwise exercise their rights under the Illinois Human Rights Act.  

Employees who have questions about this policy or who wish to request a reasonable accommodation under this policy should contact their Human Resources representative.  

#### Accommodation for Victims of Domestic Violence, Sexual Violence or Gender Violence  

The Company will provide reasonable accommodations for qualified employees or applicants for employment who are the victim of domestic violence, sexual violence (including sexual assault and stalking) or gender violence, or who are the family or household member (i.e., spouse, civil union partner, parent, son, daughter, or other person related by blood or by present or prior marriage, other person who shares a relationship through a son or daughter or a person jointly residing in the same household with the employee)  of such a victim, unless providing the accommodation will impose an undue hardship on the Company’s business operations.  

Reasonable accommodations may include, but are not limited to, the following adjustments to job structure, the workplace or a work requirement in response to actual or threatened domestic, sexual or gender violence:  

- Transfer;  
- Reassignment;  
- Modified schedule;  
- Leave of absence;  
- Changed telephone number;  
- Changed seating assignment;  
- Installation of a lock;  
- Implementation of a safety procedure; and  
- Assistance in documenting domestic, sexual or gender violence that occurs in the workplace or related settings.  

Employees may also be entitled to a leave of absence under the Domestic Violence, Sexual Violence or Gender Violence Victims Leave policy set forth in this Illinois Supplement and should consult that policy and or Human Resources for additional information.  

The Company will not discriminate, harass or retaliate against any employee or applicant for employment:  

1. because the individual is, or is perceived to be, a victim of domestic, sexual or gender violence or requests a reasonable accommodation in accordance with this policy; or  
1. when the workplace is disrupted or threatened by the action of a person that the individual states has committed or threatened to commit domestic, sexual or gender violence against the individual or the individual's family or household member.  

Employees who have questions about this policy or who wish to request a reasonable accommodation under this policy should contact their Human Resources representative.  

#### Religious Accommodation  

Employees and applicants for employment may request a reasonable accommodation for their sincerely held religious beliefs, practices, and/or observances, including but not limited to the wearing of any attire, clothing or facial hair in accordance with the requirements of their religion. In line with the Illinois Human Rights Act, a reasonable accommodation will be provided unless such accommodation would impose an undue hardship on the conduct of the Company’s business.  

The Company will not deny employment opportunities or take adverse employment actions against employees or otherwise qualified applicants for employment based on the need to make such reasonable accommodations, nor will the Company retaliate against applicants or employees who request accommodations or otherwise exercise their rights under the Illinois Human Rights Act. Employees who have questions about this policy or who wish to request a reasonable accommodation under this policy should contact their Human Resources representative.  

### GENERAL EMPLOYMENT PRACTICES  

#### Access to Personnel Files  

Employees in Illinois can access their own personnel file at least two times each calendar year at reasonable intervals. An employee’s request to access his or her personnel file must be in writing. Current employees will be permitted to inspect, and if requested, copy their personnel files within seven business days after the Company receives their written request. If the Company is unable to provide access to the personnel file within seven working days, the Company will do so within the following seven working days.  

Employees subject to recall after layoff or on a leave of absence with a right to return to work and former employees whose employment ended during the previous year may also request to inspect their personnel file.  

Inspection will take place during regular business hours at a location at, or reasonably near, the employee’s place of employment. If an employee demonstrates that they are unable to review the file at the place of employment and submits a written request, the Company will provide a copy of the file.  Employees who request and receive a copy or partial copy of their personnel file may be required to pay the cost of duplication.  

An employee who is involved in a current grievance against the employer, may designate in writing a representative to inspect their personnel file.  

Personnel file documents do not include letters of reference, materials that are used by the Company to plan for future operations, information contained in separately maintained security files, test information, the disclosure of which would invalidate the test, certain personal information about people other than the employee, or documents which are being developed or prepared for use in civil, criminal or grievance procedures.  

If an employee disagrees with any of the information contained in his or her personnel file, the employee may request that the Company remove or correct such information. If the employee and the Company cannot agree upon such removal or correction, the employee may submit a written statement explaining his or her position. The employee’s written statement will be maintained as part of his or her personnel file or medical records and will accompany any transmittal or disclosure from such file or records made to a third party.  

### PAY PRACTICES  

#### Meal Breaks  

Employees who work seven and one-half or more consecutive hours will be provided at least one 20-minute meal break, no later than five hours after the start of work.  During the break, employees will be relieved of all duties.  

An uninterrupted meal break lasting 30 or more minutes will be unpaid.  

Employees who are unable to take all of the meal breaks to which they are entitled in accordance with this policy, or who have been prevented or discouraged from taking a break to which they are entitled under this policy, should immediately notify a Human Resources representative.  

#### Lactation Accommodation  

The Company will provide reasonable breaks to accommodate an employee desiring to express breast milk for the employee’s infant child, for one year after the child’s birth. If possible, nursing mothers should take time to express breast milk during their regular meal and/or rest breaks.  If the break time cannot run concurrently with the meal and/or rest breaks already provided to the employee, employees should work with their supervisor regarding scheduling.  

The Company will make reasonable efforts to provide employees with the use of a private room in close proximity to the work area, other than a toilet stall, for employees to express milk. Employees should discuss with their supervisor, a Human Resources representative the location to express and store their breast milk and to make any other arrangements under this policy.  

The Company strictly prohibits discrimination against or harassment of employees because they are breastfeeding mothers and request or take breaks in accordance with this policy.  

#### Discussion of Wages  

No employee is prohibited from inquiring about, disclosing, comparing or otherwise discussing his or her wages or the wages of another employee. The Company will not terminate or otherwise discriminate against employees because they make such inquiries, disclosures, comparisons or discussions about their wages or the wages of another employee.  

The Company also will not terminate or otherwise discriminate against any employee who files a charge, institutes a proceeding, provides information in connection with an inquiry or proceeding, or testifies in any proceeding related to the Illinois Equal Pay Act or encourages another employee to exercise his or her rights under the Illinois Equal Pay Act.  

This policy does not apply to disclosure of other employees’ wage information by representatives who have access to such information solely as part of their essential job functions and who, while acting on behalf of the Company, make unauthorized disclosure of that information.  

#### Schedules and Hours Under the Fair Workweek Ordinance (Chicago)  

ConstructConnect complies with Chicago’s Fair Workweek Ordinance. In accordance with that law, the Company adopts the policies and practices described below.  

These policies and practices apply to employees who (a) spend the majority of their time at work while physically present within the City of Chicago; (b) earn less than or equal to $50,000 per year as a salaried employee, or less than or equal to $26.00 per hour as an hourly employee; and (c) perform the majority of their work in building services, healthcare, hotels, manufacturing, restaurants, retail, or warehouse services.  

##### Good Faith Initial Estimate of Work Schedule  

Prior to or upon the commencement of employment, the Company will provide covered employees, in writing, with a good faith estimate of the employee’s projected days and hours of work for the first 90 days of employment, including:  

- The location(s) at which the employee will work;  
- The average number of weekly work hours the employee can expect to work each week;  
- Whether the employee can expect to work any on-call shifts; and  
- A subset of days and times or shifts that the employee can expect to work, or days of the week and times or shifts on which the employee will not be scheduled to work.  

This good faith estimate does not constitute a contractual offer, and the Company is not bound by the estimate.  



Prior to or upon the commencement of employment, employees may request that the Company modify the initial estimate of work schedule. The Company will consider any such request, and in its sole discretion may accept or reject the request. The Company will notify the employee of its determination, in writing, within three days of the employee’s request.  

##### Advance Notice of Work Schedule  

The Company will provide covered employees with written notice of their work hours by posting their work schedules no later than 10 days before the first day of any new schedule (“the deadline”). The written work schedule will span at least a calendar week and will generally include the shifts and on-call status of all current covered employees at that worksite. Upon request, the Company will refrain from posting or transmitting to other employees the work schedule of an employee who is a victim of domestic violence or sexual violence or the family member of a victim of domestic or sexual violence. The Company will transmit work schedules electronically upon an employee’s request.  

The Company reserves the right to change a covered employee's work schedule after it is posted and/or transmitted. The Company may also add an employee to a posted work schedule after the deadline when that employee is returning to work from a leave of absence.  

##### Declining Schedule Changes  

Employees can decline any previously unscheduled hours that the Company adds to the employee's schedule if the employee has been provided less than 10 days advance notice before the first day of any new schedule. Beginning July 1, 2022, employees can decline any previously unscheduled hours that the Company adds to the employee's schedule if the employee has been provided less than 14 days advance notice before the first day of any new schedule.  

##### Alterations to Work Schedules  

If the Company alters a covered employee's work schedule by more than 15 minutes after the deadline, in addition to the regular rate of pay, the employee will receive:  

- One hour of predictability pay for each shift in which the Company:  
	- adds hours of work;  
	- changes the date or time of a work shift with no loss of hours; or  
	- with more than 24 hours’ notice, cancels or subtracts hours from a regular or on-call shift.  
- No less than 50% of the employee's regular rate of pay for any scheduled hours the employee does not work because the Company, with less than 24 hours' notice, subtracts hours from a regular or on-call shift, or cancels a regular or on-call shift, including while the employee is working on a shift.  

The Company will amend the posted work schedule and transmit it to the employee in writing within 24 hours of a schedule change.  

##### Exceptions to Predictability Pay  

The predictability pay requirements outlined above do not apply in the following situations:  

- The work schedule changes because of events outside the Company’s control, such as: threats to the Company, Company property or employees; a recommendation by civil authorities that work not begin or continue; war; civil unrest; strikes; threats to public safety; pandemics; acts of nature (e.g., floods, earthquakes, tornadoes or blizzards); or civil unrest;  
- The work schedule change is a mutually agreed upon shift trade or coverage arrangement between employees, subject to any existing Company policy regarding required conditions for employees to exchange shifts;    
- A work schedule change that is mutually agreed to by the employee and the Company and is confirmed in writing;  
- The posted work schedule is changed by 15 minutes or less;   
- The employee requests a shift change, that is confirmed in writing, including but not limited to use of sick leave, vacation leave, or other policies offered by the Company;  
- The Company subtracts hours from a work schedule for disciplinary reasons for just cause; or  
- When employees self-schedule.  

##### Additional Work Hours to Existing Employees  

When the Company needs to fill additional shifts of work, it will first offer such shifts to existing employees who are covered by this policy, if the employees are qualified to do the additional work, as determined by the Company.  

When distributing additional work hours among qualified and interested existing employees, the Company will first distribute work hours to employees whose regular workplace is the location where the work will be performed.  Whenever practicable, the Company will first offer the hours to part-time employees.  

The Company may choose to not schedule employees to work hours required to be paid at a premium rate.  

In distributing hours, the Company will not discriminate or tolerate discrimination on the basis of race, color, creed, religion, ancestry, national origin, sex, sexual orientation, gender identity or expression, disability, age or marital or familial status.  

##### Right to Rest and Additional Pay  

Employees have the right to decline work schedule hours that are less than 10 hours after the end of the previous day's shift. Employees may voluntarily consent in writing to work a shift that begins sooner than 10 hours after the end of the previous day’s shift.  When an employee works a shift that begins less than 10 hours after the end of the previous day's shift, the Company will pay the employee at a rate of 1.25 times the employee's regular rate of pay for that shift, except that hours in such a shift that exceed a workweek of 40 hours will be paid at the usual overtime rate of 1.5 the employee’s regular rate of pay.  

##### Right to Request a Flexible Working Arrangement  

Employees also have the right to request a modified work schedule, including, but not limited to: additional shifts or hours; changes in days of work; changes in shift start and end times; permission to exchange shifts with other employees; limitations on availability; part-time employment; job sharing arrangements; reduction or change in work duties; or part-year employment. The Company will respond to such requests in writing.  

##### Retaliation Prohibited  

The Company will not retaliate against an employee for exercising rights protected under Chicago’s Fair Workweek Ordinance, including reporting or testifying about any violation, or requesting changes to their working arrangement.  

### TIME OFF AND LEAVES OF ABSENCE  

#### Vacation/Paid Time Off (PTO)  

The Company will pay employees for any accrued but unused vacation/PTO upon termination of employment.  

#### Paid Sick Leave (Cook County)  

The Company provides eligible employees with sick leave pursuant to the Cook County Earned Sick Leave Ordinance (the “Ordinance”). The guidelines in this policy do not supersede federal, state or local laws regarding leaves of absence, including but not limited to leave taken under the Family and Medical Leave Act (FMLA), leave taken under the Illinois Victims’ Economic Security and Safety Act, leave taken as a reasonable accommodation under the Americans with Disabilities Act (ADA) or the Illinois Human Rights Act (IHRA), or any other applicable federal, state or local law, including those prohibiting discrimination and harassment.  

##### Eligible Employees  

All employees, whether full-time, part-time or temporary, who, in any particular two-week period, perform at least two hours of work for the Company while physically present within the geographic boundaries of Cook County are entitled to paid sick leave under this policy.  

Employees are not eligible to use accrued paid sick leave until they work at least 80 hours for the Company, regardless of location, in any 120-day period after the employee’s start of employment.  

##### Accrual and Use of Sick Leave  

Eligible employees begin accruing paid sick leave on July 1, 2017, or the first calendar day after the start of their employment, whichever is later. Employees who were already working for the Company outside of Cook County on July 1, 2017, begin accruing paid sick leave on the date on which the employee works for the Company for two hours in Cook County in any particular two-week period.  

Employees accrue one hour of paid sick leave for every 40 hours worked within the geographic boundaries of Cook County, up to a maximum of 40 hours in a 12-month accrual period. This 12-month accrual period begins on the date the employee starts accruing paid sick leave. Employees will not accrue paid sick leave for work performed outside of the geographic boundaries of Cook County or within the geographic boundaries of a municipality that has lawfully opted out of the Ordinance.  

Employees accrue paid sick leave in one-hour increments and may not accrue in any fraction of an hour.  

For accrual purposes, exempt employees are assumed to work 40 hours per workweek, unless their normal workweek is fewer than 40 hours per week, in which case paid sick leave accrues based upon the employee’s normal workweek hours. Nonexempt employees accrue paid sick leave based on all hours worked, including overtime.  

Employees may use accrued paid sick leave on the date the employee has worked 80 hours within a 120-day period or on the employee’s 180th calendar day after employment begins, whichever is later. Thereafter, employees may use the leave as it accrues. Except as otherwise provided in this policy, employees may use no more than 40 hours of accrued paid sick leave during any 12-month accrual period, without regard to whether the hours were earned in the current 12-month accrual period or carried over from the prior accrual period.  

Employees may use paid sick leave in four-hour increments only. Employees may use accrued paid sick leave in any location where the employee works for the Company.  

Employees are not required to search for or find a replacement worker as a condition of using paid sick leave. Employees are also not required to use paid sick leave if the employee has been suspended or otherwise placed on leave for disciplinary reasons.  

Additionally, employees are not required to work an alternate shift to make up for any paid sick leave use.  

##### Reasons Paid Sick Leave May Be Used  

Paid sick leave may be used for the following reasons:  

- The employee or the employee’s family member is ill or injured; is receiving medical care, medical treatment, medical diagnosis or preventative medical care; or needs to recuperate;  
- The employee or the employee’s family member is the victim of domestic violence, sexual violence or stalking; or  
- A public official closes the employee’s place of business because of a public health emergency, or the employee needs to care for a child after a public official has closed the child’s school or place of care because of a public health emergency.  

For purposes of this policy, “family member” includes the employee’s:  

- Child (including a biological child, adopted child, stepchild, foster child and child to whom the employee stands in loco parentis);  
- Legal guardian or ward;  
- Spouse;  
- Domestic partner (including a party to a civil union);  
- Parent (including a biological parent, adoptive parent, foster parent, stepparent,  person who stood in loco parentis when the employee was a minor child and spouse or domestic partner’s parent);  
- Sibling;  
- Grandparent;  
- Grandchild; and  
- Any other individual related by blood or whose close association with the employee equates to a family relationship.  

Additionally, if an employee is eligible for leave under the federal Family and Medical Leave Act (“FMLA”), paid sick leave may be used for any reason that an employee can take job-protected leave pursuant to the FMLA. An employee’s use of paid sick leave for FMLA purposes runs concurrently with the employee’s use of leave under the FMLA.  

##### Rate of Pay for Paid Sick Leave  

Paid sick leave is paid based on the employee’s normal hourly rate. If the employee uses paid sick leave during hours that would have been designated as overtime, the Company will not pay the employee the overtime rate of pay.  

To the extent the employee is eligible for other Company-provided benefits, the Company will compensate the employee with these additional benefits in the same manner and to the same extent as if the employee had performed regular work instead.  

Paid sick leave will be paid no later than the next regular payroll period beginning after the paid sick leave was used by the employee.  

##### Requesting Sick Leave/Documentation  

When the need for paid sick leave is reasonably foreseeable, employees must provide seven days’ notice of the need for sick leave. A reasonably foreseeable absence includes, but is not limited to, any non-emergency, prescheduled appointment with a health care provider for the employee or the employee’s family member and any non-emergency, prescheduled court date in a case related to domestic violence, sexual violence or stalking of an employee or the employee’s family member. To provide notice of the need to use sick leave, employees should contact their Human Resources representative.  

If the need for paid sick leave is unforeseeable, employees must provide notice as soon as practicable on the day the employee intends to take the paid sick leave. Employees may provide notification of their need for unforeseeable leave via phone, e-mail or text message, and the Company will accept notice from any other person providing such notice on the employee’s behalf.  

These notice requirements are waived in the event an employee is unable to provide the required notice because they are unconscious or otherwise incapacitated.  

If paid sick leave is used for more than three consecutive workdays, the Company may require that the employee provide certification that the leave was used for a covered purpose. An employee can satisfy this requirement by providing documentation signed by a licensed health care provider. However, if the need for leave is due to domestic violence or a sex offense, the employee can meet the certification requirement by providing a copy of any of the following documents:  

- Police report;  
- Court document;  
- Signed statement from an attorney, a clergy member or a victim services advocate;  
- The employee’s own sworn declaration;  
- The sworn declaration of any person with knowledge of the circumstances; or  
- Any other evidence that supports the employee’s reason for taking paid sick leave.  

The Company does not require any documentation to reveal the nature of any illness or injury or to describe the details of any domestic violence, sexual assault or stalking.  

The Company will not delay the use of paid sick leave or payment of wages due during a paid sick leave absence on the basis that the Company has not yet received the required documentation outlined above. However, the employee may be subject to discipline, including termination, for failure to provide the necessary documentation where the Company has given the employee a reasonable period of time to produce the requested documentation.  

##### Carryover  

Accrued but unused paid sick leave may be carried over from year to year. At the end of the employee’s 12-month accrual period, the employee may carry over half of his or her accrued but unused paid sick leave, up to a maximum of 20 hours, into the following 12-month accrual period to be used for paid sick leave purposes only (“Ordinance-Restricted Paid Sick Leave”).  

Additionally, if an employee is eligible for FMLA leave, the employee may carry over up to 40 hours of any remaining accrued but unused paid sick leave to be used for FMLA-eligible purposes only (“FMLA-Restricted Paid Sick Leave”). These hours will not be divided in half. However, under no circumstances will the employee be permitted to carry over more than 40 hours of FMLA-Restricted Paid Sick Leave or more than 20 hours of Ordinance-Restricted Paid Sick Leave into the following 12-month accrual period. If the employee carries over the maximum allowable 40 hours of FMLA-Restricted Paid Sick Leave from the previous 12-month accrual period and then uses all 40 of these hours during the current 12-month accrual period, the Company will allow the employee to use up to an additional 20 hours of Ordinance-Restricted Paid Sick Leave during the current 12-month accrual period (for a total maximum of 60 hours of paid sick leave used during the accrual period).  

In addition, if it is clear that the employee is ineligible to take FMLA leave at any time during the accrual period to which accrued but unused paid sick leave is being carried over (e.g., the employee works too few hours to qualify for FMLA leave), the employee will not be allowed to carry over any FMLA-Restricted Paid Sick Leave from the employee’s current 12-month accrual period to the next 12-month accrual period.  

Employees are allowed to carry over unused paid sick leave only in hourly increments (not fractional). If the employee’s amount of carryover hours results in a fraction, it will be rounded up to the next whole number (e.g., 4.5 hours of carryover will round up to five hours).  

Paid sick leave will not be paid out in lieu of use or carryover.  

##### Separation from Employment and Rehire  

Compensation for accrued and unused paid sick leave is not provided upon separation from employment for any reason.  

If an employee is rehired more than 120 days after separation, the Company will not restore any unused sick leave the employee may have had at separation. The employee must re-establish coverage and eligibility.  

An employee rehired within 120 days after separation is considered to have continued their employment with the Company for purposes of determining whether they are a covered employee, whether they are eligible to use paid sick leave and whether they have met the Company’s 180-day waiting period.  

##### Confidentiality  

The Company will keep confidential any medical documentation regarding leave use, in accordance with federal, state and local law.  

##### Effect on Other Rights and Policies  

The Company may provide other forms of leave for employees to care for medical conditions or for issues related to domestic violence under certain federal, state and municipal laws. In certain situations, leave under this policy may run at the same time as leave available under another federal, state or municipal law, provided eligibility requirements for that law are met. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other federal, state and municipal medical, domestic violence or family leave rights.  

While the Company will not forbid or require an employee to take paid sick leave, the Company may require an employee to use accrued paid sick leave when the employee can do so instead of taking an unpaid absence from work.  

##### No Discrimination or Retaliation  

The Company will not retaliate or discriminate against any employee for exercising his or her rights under the Ordinance or for participating as a party or witness in a case alleging a violation of the Ordinance that is or was pending before the Cook County Commission on Human Rights.  

#### Paid Sick Leave (Chicago)  

The Company provides eligible employees with sick leave pursuant to the Chicago Minimum Wage and Paid Sick Leave Ordinance (the Ordinance). The guidelines in this policy do not supersede federal, state or local laws regarding leaves of absence, including but not limited to leave taken under the Family and Medical Leave Act (FMLA), Illinois Employee Sick Leave Act, leave taken as a reasonable accommodation under the Americans with Disabilities Act (ADA) or the Illinois Human Rights Act (IHRA), or any other applicable federal, state or local law, including those prohibiting discrimination and harassment.  

##### Eligible Employees  

All employees, whether full-time, part-time or temporary, who work at least two hours within any two week period in the City of Chicago are covered under this policy.  Employees who work 80 hours in any 120-day period are eligible to use paid sick leave.  

##### Accrual and Use of Sick Leave  

Covered employees begin accruing paid sick leave on July 1, 2017, or their first calendar day after the start of their employment, whichever is later. Employees accrue one hour of paid sick leave for every 40 hours worked within the City of Chicago, up to a maximum of 40 hours in a 12-month accrual period. This 12-month accrual period begins on the date the employee starts accruing paid sick leave.  

Employees accrue paid sick leave in one-hour increments and may not accrue in any fraction of an hour. Employees will not accrue paid sick leave during any paid or unpaid leave of absence. Employees may use paid sick leave in four-hour increments.  

Exempt employees who receive a salary accrue one hour of paid sick leave for each week of employment unless the employee’s salaried position is for an amount different from 40 hours per week, in which case the employee will accrue one hour of paid sick leave for every 40 hours worked.  

Eligible employees may not use accrued paid sick leave until the 180th calendar day following the start of employment. Thereafter, employees may use the leave as it accrues. Employees may use a maximum of 40 hours of accrued paid sick leave during each 12-month accrual period.  FMLA-eligible employees may use a maximum of 60 hours of accrued paid sick leave during each 12-month period. This includes 40 hours of accrued but unused paid sick leave for FMLA-eligible purposes only (“FMLA-Restricted Paid Sick Leave”) and 20 hours of regular paid sick leave.  

Employees are not required to seek or find an employee to cover their work when they take paid sick leave.  

Employees will not accrue paid sick leave during an unpaid leave of absence or while using paid leave.  

##### Reasons Sick Leave May Be Used  

Paid sick leave may be used for the following reasons:  

- The employee or the employee’s family member is ill, injured or receiving medical care, treatment, diagnosis or preventative medical care;  
- The employee or the employee’s family member is the victim of domestic violence or a sex offense; and  
- A public official closes the employee’s place of business because of a public health emergency, or the employee needs to care for a child after a public official has closed the child’s school or place of care because of a public health emergency.  

Eligible family members include the employee’s:  

- Child (including a biological child, adopted child, stepchild, foster child and child to whom the employee stands in loco parentis);  
- Legal guardian or ward;  
- Spouse;  
- Domestic partner;  
- Parent (including a biological parent, adoptive parent, foster parent, stepparent, legal guardian of the employee, a person who stood in loco parentis when the employee was a minor child, and spouse or domestic partner’s parent);  
- Co-parent  
- Sibling;  
- Grandparent;  
- Grandchild;    
- Godparent;  
- Godchild; and  
- Any other individual related by blood or whose close association with the employee equates to a family relationship.  

##### Payment of Paid Sick Leave

Paid sick leave will be paid no later than the next regular payroll period beginning after the paid sick leave was used by the employee.  

##### Requesting Sick Leave/Documentation  

When the need for paid sick leave is reasonably foreseeable, employees must provide seven days’ notice of the need for sick leave. Reasonably foreseeable absences include but are not limited to prescheduled appointments with health care providers for the employee or a family member and court dates in domestic violence cases. To provide notice of the need to use sick leave, employees should contact their Human Resources representative.  

If the need for paid sick leave is unforeseeable, employees must provide notice as soon as practicable on the day the employee intends to take the paid sick leave.  Employees may provide notification of their need for unforeseeable leave via phone, email or text message. These notice requirements are waived in the event an employee is unable to provide the required notice because they are unconscious or otherwise incapacitated.  

If paid sick leave is used for more than three consecutive workdays, the Company may require that the employee provide certification that the paid sick leave was used for a covered purpose. An employee can satisfy this requirement by providing documentation signed by a licensed health care provider. However, for leave related to domestic violence or a sex offense, the employee may provide a copy of any of the following documents:  

- Police report;  
- Court document;  
- Signed statement from an attorney, clergy or victim services advocate;  
- The employee's own written statement;  
- The written statement of any person with knowledge of the circumstances; or  
- Any other evidence that supports the employee’s reason for taking paid sick leave.  

##### Carryover  

Accrued but unused paid sick leave may be carried over from year to year. At the end of the employee’s 12-month accrual period, the employee may carry over half of his or her accrued but unused paid sick leave, up to a maximum of 20 hours. If an employee has an odd number of accrued but unused paid sick leave hours, the number will be rounded up to the next even number for purposes of calculating the number of hours for carryover (e.g., nine hours will be rounded up to ten hours, so the employee may carry over five hours of accrued but unused paid sick leave in into the next 12-month accrual period).  Additionally, FMLA-eligible employees may carry over up to 40 hours of accrued but unused paid sick leave for FMLA-Restricted Paid Sick Leave. These hours will not be divided in half. However, under no circumstances will the employee be permitted to carry over more than 40 hours of FMLA-Restricted Paid Sick Leave or more than 20 hours of regular paid sick leave into the following 12-month accrual period. Employees may use no more than 60 total hours of combined regular paid sick leave and FMLA-Restricted Paid Sick Leave per 12-month period.  

##### Separation From Employment and Rehire  

Compensation for accrued and unused paid sick time is not provided upon separation from employment for any reason.  

If an employee is separated from employment with the Company but rehired at a later date, the Company has sole discretion in determining whether previously accrued but unused paid sick leave will be available for the employee’s use.  

##### Confidentiality  

The Company will keep confidential any medical documentation regarding leave use, in accordance with federal, state and local law.  

##### Effect on Other Rights and Policies  

The Company may provide other forms of leave for employees to care for medical conditions or for issues related to domestic violence under certain federal, state and municipal laws. In certain situations, leave under this policy may run at the same time as leave available under another federal, state or municipal law, provided eligibility requirements for that law are met. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other federal, state and municipal medical, domestic violence or family leave rights.  

##### No Discrimination or Retaliation  

The Company will not retaliate or discriminate against any employee for exercising, or attempting in good faith to exercise, his or her rights under this Ordinance, including but not limited to, disclosing, reporting or testifying about any violation of this Ordinance or its regulations.  

#### Sick Leave to Care for Relatives  

Employees may use paid or unpaid personal sick leave benefits provided by the Company (not including short or long-term disability, an insurance policy, or other comparable benefit plans or policies) for absences due to the illness, injury, or medical appointment of a covered relative, on the same terms that the employee is able to use personal sick leave benefits for the employee's own illness or injury. Covered relatives include the employee’s child, stepchild, spouse, domestic partner, sibling, parent, mother-in-law, father-in-law, grandchild, grandparent, or stepparent.  

The Company may, at its sole discretion, limit the use of an eligible employee’s sick time to care for a relative under this policy to what the employee would earn or accrue in six months, or half of the employee’s maximum annual grant of personal sick leave benefits.  

The Company may request written verification of the employee's absence from a health care professional. The Company will not discharge, threaten to discharge, demote, suspend, or in any manner discriminate against employees for requesting or using personal sick leave benefits in accordance with this policy, or who attempt in good faith to exercise their rights under the law. Employees with questions or concerns regarding this policy or who would like to request a leave of absence under this policy should contact Human Resources.  

#### Mandatory Time Off/Day of Rest  

ConstructConnect will provide nonexempt, nonsupervisory employees working more than 20 hours per week with at least one day (24 consecutive hours) of rest during every calendar week.  Certain exceptions may apply, including for watchmen or security guards, employees working in agriculture or coal mining; employees engaged in canning and processing of perishable agricultural products on a part-time or seasonal basis; and employees whose services are needed to prevent injury or damage in case of machinery breakdown or other emergency.  

#### Family Military Leave  

Eligible employees who are the spouse, parent, child or grandparent of a person called to military service are entitled to up to 30 days of unpaid leave during the time federal or state deployment orders are in effect.  To be eligible for leave, employees must:  

- Have been employed by the Company for at least 12 months;  
- Have worked for the Company for at least 1,250 hours during the 12-month period immediately preceding the leave; and  
- Be the spouse, parent, child or grandparent of a person called to military service lasting longer than 30 days with the state or the United States pursuant to orders of the Governor or the President.  

The Company may require verification of an employee’s eligibility for leave from the proper military authority.  

Employees may not take family military leave until they have exhausted all accrued vacation, personal, compensatory and any other leave granted to the employee, with the exception of sick and disability leave.  

Employees taking family military leave for five or more consecutive workdays must notify their supervisor of the intended date of the leave at least 14 days in advance.  

If possible, employees must consult with their supervisor regarding the scheduling of the leave to minimize disruption to the Company’s operations.  Employees taking family military leave for fewer than five consecutive days must give their supervisor as much advance notice as is practicable.  

During family military leave, employees may continue any benefits, if applicable, at their own expense.  No loss of seniority status will occur as a result of leave taken under this policy, nor will leave result in the loss of any benefits accrued prior to the leave.  Where applicable, time off under this policy will run concurrently with time off under the federal Family and Medical Leave Act.  

Upon return from leave, employees will be restored to their prior position or to a position with equivalent seniority status, benefits, pay and other terms and conditions of employment.  

The Company will not discriminate against, or tolerate discrimination against, any employee who seeks or obtains leave under this policy.  

#### Military Leave  

In addition to the military leave rights set forth in the National Handbook and subject to the additional provisions set forth in the Illinois Service  Member Employment and Reemployment Rights Act (ISERRA) and described in this policy, members of the Armed Forces of the United States, the National Guard of any state or territory and the Illinois State Guard are entitled to the rights, protections, privileges and immunities provided under the federal Uniformed Services Employment and Reemployment Rights Act (USERRA) and described more fully in the Military Leave policy set forth in the National Handbook.  

Employees are entitled to a military leave of absence for active service in accordance with this policy so long as they provide advance notice of pending military service.  There may be an exception to this advance notice requirement based on military necessity, as determined by the appropriate state military authority. For purposes of this policy, “military service” includes:  

- Service (active or reserve) in the U.S. Armed Forces, the National Guard of any state or territory or the Illinois State Guard;  
- Service in a federally recognized auxiliary of the U.S. Armed Forces when performing official duties in support of military or civilian authorities as the result of an emergency; and  
- A period during which an employee is absent from employment for the purpose of medical or dental treatment for a condition, illness or injury sustained or aggravated during a period of active service and the treatment is paid for by the United States Department of Defense Military Health System.  

Also for purposes of this policy, “active service” means all forms of active and inactive duty (without regard to voluntariness), including, but not limited to:  annual training, active duty for training, initial active duty training, overseas training duty, full-time National Guard duty, active duty other than training, state active duty, mobilizations and muster duty.  

The Company may require additional documentation from an appropriate military authority for leave due to performance of official duties in support of military or civilian authorities as the result of an emergency or for the employee’s medical or dental treatment.  

Employees requesting leave are not required to find an employee to cover their work when they take leave under this policy.  Employees taking military leave are also not required to accommodate work-related needs pertaining to the timing, frequency or duration of their leave.  The Company may bring concerns over the timing, frequency or duration of military leave to the attention of the appropriate military authority, but understands that accommodation of these concerns is subject to military law and the discretion of that military authority.  

Accrued, unused vacation or PTO will be paid during military leave at the employee’s request.  

##### Reinstatement  

In order to be eligible for reinstatement, an employee must have completed his or her service on a basis that is not dishonorable or otherwise prohibited under federal or state law. A retroactive upgrade of a disqualifying discharge or release will restore reemployment rights, provided the service member employee otherwise meets the eligibility criteria under ISERRA.   

An employee who is absent on military leave will, for the period of leave, be credited with the average of the efficiency or performance ratings or evaluations received for the three years immediately prior to the absence for military leave. The rating will not be less than the rating that the employee received for the rated period immediately prior to his or her absence on military leave. Except for during probationary periods, the period of military leave will also be counted in computing seniority and time-in-service requirements for promotion eligibility or any other benefit of employment.  

#### School Activities Leave  

Eligible employees who are the parent or legal guardian of a child (including a biological, adopted, foster or stepchild) enrolled in a public or private primary or secondary school located in Illinois or a state that shares a common border with Illinois may take time off to attend certain academic activities related to their child.  Eligible employees are those who have worked for the Company for at least six consecutive months immediately preceding the leave request and who have worked, on average, a number of hours equal to or greater than one-half of a full-time position during the six-month period.  

Employees will not be permitted to take leave under this policy unless they have first exhausted all accrued vacation and other appropriate leave (not including sick or disability leave).  Employees are also required to submit a written request for leave at least seven days in advance in nonemergency situations, and, in emergency situations, 24 hours in advance.  

Eligible employees will be allowed up to eight hours of leave during any school year to attend school conferences, behavioral meetings or academic meetings related to the employee’s child, if those conferences or meetings cannot be scheduled outside of work hours.  No more than four hours of leave may be taken on any single day. Employees must consult with their supervisor to schedule the leave so as not to unduly disrupt the Company’s business operations. Time off under this policy will be unpaid except that exempt employees will be paid when required by applicable law.  

Employees must provide verification of the academic activity from the school within two working days of the school visit.  The verification should include the time and date of the employee’s visit. For employees who fail to timely submit the verification, the absence may be treated as unexcused.  

The Company will allow, but not require, nonexempt employees to make up the amount of hours taken for the leave, as long as there is a reasonable opportunity to make up the hours in a manner that does not require payment of overtime.  Exempt employees may be required to make up the leave hours within the same pay period.  

The Company will not terminate or otherwise discriminate against employees who take leave in accordance with this policy.  

#### Emergency Responder Leave  

Employees who are volunteer emergency workers will be allowed time off when needed to respond to an emergency call.  For purposes of this policy, “volunteer emergency workers” include volunteer firefighters, emergency medical technicians, ambulance drivers and attendants, first responders, volunteers under the Illinois Emergency Management Agency Act and auxiliary public safety officials.  Employees will not be terminated for being late to or absent from work for this purpose. The Company also will not discipline employees who are volunteer emergency workers because they respond to an emergency call or emergency text message requesting their volunteer emergency medical services or firefighter services during work hours, so long as the employee does not violate the Company’s policies concerning personal devices, standards of conduct, and cell phone/texting while driving.  

Employees must make a reasonable effort to notify the Company of an emergency call.  Upon return, the Company may require that employees provide a written statement certifying that they were responding to an emergency.  
Time off under this policy will be without pay, except that exempt employees may receive pay as required by applicable law.  

#### Blood Donor Leave  

Upon request, eligible employees will be allowed up to one hour of paid leave to donate, or attempt to donate, blood every 56 days.  

Employees who attempt to donate blood but are unsuccessful (as determined by the blood bank) will still be charged the blood donor leave.  

“Eligible employees” are full-time employees who have been employed by the Company for six months or longer and have obtained company approval for the time off.  

Employees will not be required to use accrued or future vacation or sick leave while taking time off to donate blood.  

When requesting time off for this purpose, employees must submit documentation of the appointment to donate blood in advance of the appointment.  The Company may require that employees provide a written statement from the blood bank confirming the employee’s attendance at the appointment.  

Employees who have questions regarding this policy or who feel they have been wrongfully charged leave, denied leave or denied pay for leave under this policy should promptly notify their supervisor, the Human Resources department.  

#### Civil Air Patrol Leave  

Eligible employees who are members of the civil air patrol may be entitled to up to 30 days of unpaid leave for the purpose of serving on a civil air patrol mission.   “Eligible employees” are those who have worked for the Company for 12 months and have worked 1,250 hours during the 12-month period immediately preceding the leave request.  

If the leave will last five or more consecutive workdays, employees must provide at least 14 days’ notice of the intended date upon which the leave will begin.  If the leave will last fewer than five consecutive days, employees must provide as much notice as is practical.  When possible, employees must consult with their supervisors about scheduling time off under this policy, in order to minimize the disruption to business operations.  

The Company may require certification from the proper civil air patrol authority to verify an employee’s eligibility for leave.  

Employees returning from leave will be reinstated to the same position or one with equivalent seniority status and the same pay and benefits as they had prior to the leave, unless factors other than the exercise of leave under this policy prevent reinstatement.  

Employees on civil air patrol leave are entitled to continue benefits at their own expense.  Taking such a leave will not result in employees losing any benefits earned prior to the leave.  

#### Election Judge Leave  

Employees who have been appointed as an election judge will be allowed time off without pay to serve in that capacity.  Employees must provide at least 20 days’ written notice of the need for leave under this policy.  

Leave under this policy will be unpaid, except that exempt employees will receive pay when required under applicable federal or state law.  

#### Jury Duty Leave  

The Company encourages all employees to fulfill their civic responsibilities and to respond to jury service summons or subpoenas, attend court for prospective jury service or serve as a juror. Under no circumstances will employees be terminated, threatened, coerced or penalized because they request or take leave in accordance with this policy.  Additionally, employees who work for the Company at night will not be required to work while serving on a jury during the day.  

Employees must provide their supervisor with notice of any jury summons or subpoena within 10 days after receipt.  Verification from the court clerk of having served may also be required.  

Time spent engaged in attending court for prospective jury service or for serving as a juror is not compensable except that exempt employees will not incur any reduction in pay for a partial week's absence due to jury duty.  Employees serving on a jury will be entitled to participate in insurance and other benefits under the same terms as other employees on a leave of absence. Upon return, employees will be reinstated to their former position without loss of seniority.  

#### Witness Leave  

Employees who witness a crime will be allowed time off from work for the purpose of responding to a subpoena to attend a criminal proceeding relating to that crime.  Employees will also be allowed time off to provide information in connection with a domestic violence proceeding or to testify in such a proceeding.  

Leave under this policy will be unpaid except that exempt employees will not incur any reduction in pay for a partial-week absence.  

#### Domestic Violence, Sexual Violence or Gender Violence Victim Leave  

Eligible employees will be allowed up to 12 weeks of unpaid leave in any 12-month period to address domestic violence, sexual violence or gender violence.  An employee is eligible for leave under this policy if:  

- The employee is the victim of domestic, sexual or gender violence; or  
- The employee’s family or household member (i.e., spouse, civil union partner, parent, son, daughter, other person related by blood or by present or prior marriage, other person who shares a relationship through a son or daughter or a person jointly residing in the same household with the employee) is a victim of domestic, sexual or gender violence and does not have interests adverse to the employee as it relates to the domestic, sexual or gender violence.  

Eligible employees may use leave available under this policy to do any of the following for themselves or for a family or household member identified above:  

- Seek medical attention for or recover from physical or psychological injuries caused by domestic, sexual or gender violence;  
- Obtain services from a victim services organization;  
- Obtain psychological or other counseling;  
- Participate in safety planning, relocate temporarily or permanently or take other actions to increase safety from future domestic, sexual or gender violence or to ensure economic security; or  
- Seek legal assistance or remedies to ensure health and safety, including preparing for or participating in any civil or criminal legal proceeding relating to or derived from domestic, sexual or gender violence.  

Leave may be taken intermittently or on a reduced-schedule basis.  

If applicable, time off under this policy will run concurrently with time off under the federal Family and Medical Leave Act.  

Employees seeking leave under this policy must provide at least 48 hours’ advance notice, unless such notice is impractical. Employees may also be required to periodically report on the status of their circumstances and intent to return to work.  The Company may require certification that the leave was taken for one of the purposes identified above and/or that the employee or employee’s family or household member is a victim of domestic, sexual or gender violence.  Employees must respond to the request for certification within a reasonable period of time and can do so by providing a sworn statement.  Upon obtaining them, the employee must also provide:  

- Documentation from a victim services organization, attorney, member of the clergy or medical or other professional from whom the employee or the employee's family or household member has sought assistance;  
- A police or court record; or  
- Other corroborating evidence.  

The Company will keep all information pertaining to an employee’s request for leave and/or certification of the need for leave confidential, except in cases where an employee requests or consents in writing to disclosure or disclosure is required by federal or state law.  

Time off under this policy is unpaid, except that employees will be allowed, but not required, to substitute any available paid leave, including accrued sick leave.  

Upon return from leave, employees will be restored to the same position or to an equivalent position with equivalent benefits, pay and other terms and conditions of employment.  

The Company will not retaliate or tolerate retaliation against employees who request or take leave in accordance with this policy.  

#### Child Bereavement Leave  

Upon request, eligible employees will be allowed a maximum of two weeks (10 work days) of bereavement leave in connection with the death of an employee’s child. For purposes of this policy, “child” is defined as an employee's son or daughter who is a biological, adopted, or foster child, a stepchild, a legal ward, or a child of a person standing in loco parentis.  

Eligible employees may take leave under this policy for any of the following reasons:  

- To attend the funeral (or funeral alternative) of the child;  
- To make arrangements necessitated by the death of the child; or  
- To grieve the death of the child.  

“Eligible employees” have the same definition as that under the federal Family and Medical Leave Act (“FMLA”). Thus, in order to be eligible for leave under this policy, an employee must:  
1. have worked for the Company for a total of at least 12 months;  
1. have worked at least 1,250 hours over the previous 12 months as of the start of the leave; and  
1. work at a location where at least 50 employees are employed by the Company within 75 miles, as of the date the leave is requested. If employees are unsure whether they qualify, they should contact Human Resources.  

Child bereavement leave under this policy must be completed within 60 days after the date on which the employee receives notice of the death of his or her child. In the event of the death of more than one child within a 12-month period, an employee may take two weeks of leave per child, up to a total of six weeks of bereavement leave during a 12-month period.  

Employees may elect to substitute other types of leave for child bereavement leave, including but not limited to any available paid leave, such as accrued vacation, PTO or sick leave. While child bereavement leave does not run concurrently with the FMLA, employees who have already exhausted their FMLA leave are ineligible for leave under this policy.  

An employee must provide the Company with at least 48 hours of advance notice of the employee’s intention to take bereavement leave, unless providing such notice is not reasonable or practicable. The Company may request reasonable documentation from the employee to verify the employee’s eligibility for leave under this policy.  

The Company will not retaliate or tolerate retaliation against employees who request or take leave in accordance with this policy.  

#### Time Off to Vote  

The Company encourages all employees to fulfill their civic responsibilities and to vote in all public elections.  Most employees’ schedules provide sufficient time to vote either before or after working hours.  

Employees who have fewer than two consecutive hours outside of work during which the polls are open will be allowed up to two hours of time off to vote, without loss of pay. The Company may specify when the leave must be taken.  

Employees must provide notice of the need for time off prior to Election Day.  

Proof of having voted may be required.  

### SAFETY AND SECURITY  

#### Smoke-Free Workplace  

The Company prohibits smoking in the workplace and within 15 feet of any entrance to the workplace.  Employees wishing to smoke must do so outside company facilities, in locations where smoke does not migrate back into the workplace, during scheduled work breaks.  

Employees who observe other individuals smoking in the workplace have a right to object and should report the violation to their supervisor or to another member of management. Employees will not be disciplined or retaliated against for reporting smoking that violates Illinois law or this policy.  

Employees who violate this policy may be subject to disciplinary action up to and including termination.  

#### Cell Phone Use / Texting While Driving  

As set forth in the National Handbook, the Company prohibits employees from using cellular phones for business reasons while driving or for any reason while driving for work-related purposes or driving a Company-owned vehicle.  Employees should also be aware that using a handheld electronic communication device for any reason (including to watch or stream video) while driving is a violation of Illinois law, in addition to being a violation of Company policy.  

#### Weapons in the Workplace  

In the interest of maintaining a workplace that is safe and free of violence, and in accordance with the policy set forth in the National Handbook, the Company generally prohibits the presence or use of firearms and other weapons on the Company’s property, regardless of whether or not the person is licensed to carry the weapon. In compliance with Illinois law, the Company permits those employees who are concealed weapons license holders and who lawfully possess a firearm or ammunition to transport and store their firearm or ammunition in their personal vehicle inside company parking lots. However, if the employee’s vehicle is not attended, the vehicle must be locked or the firearm and ammunition must be in a locked container outside of plain view.  

Employees who are concealed weapons license holders may also carry a concealed firearm in the immediate area surrounding their vehicle in a Company parking lot only for the limited purpose of storing or retrieving the firearm within the vehicle’s trunk and only after ensuring the firearm is unloaded. Such lawfully possessed firearms and ammunition may not otherwise be removed from an employee’s personal vehicle or displayed to others for any reason.  


## Kentucky

### GENERAL INFORMATION  

#### About This Kentucky Supplement  

ConstructConnect is committed to workplace policies and practices that comply with federal, state and local laws. For this reason, Kentucky employees will receive the Company’s national handbook (“National Handbook”) and the Kentucky Supplement to the National Handbook (“Kentucky Supplement”) (together, the “Employee Handbook”).  

The Kentucky Supplement applies only to Kentucky employees. It is intended as a resource containing specific provisions derived under Kentucky law that apply to the employee’s employment. It should be read together with the National Handbook and, to the extent that the policies in the Kentucky Supplement are different from, or more generous than those in the National Handbook, the policies in the Kentucky Supplement will apply.  

The Kentucky Supplement is not intended to create a contract of continued employment or alter the at-will employment relationship. **Only the President/Owner of the Company or that person’s authorized representative has the authority to enter into an agreement that alters the at-will employment relationship and any such agreement must be in writing signed by the President/Owner of the Company or an authorized representative.**  

If employees have any questions about these policies, they should contact their Human Resources representative.  

### COMMITMENT TO DIVERSITY  

#### Equal Employment Opportunity  

As set forth in the National Handbook, ConstructConnect is committed to equal employment opportunity and to compliance with federal antidiscrimination laws. We also comply with Kentucky law, which prohibits discrimination and harassment against any employees or applicants for employment based on race, color, religion, national origin, sex (including pregnancy, childbirth or related medical conditions), age (40 and over), disability, HIV or AIDS, status as a smoker, nonsmoker or tobacco user, Kentucky National Guard or militia status. The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law.  

#### Pregnancy and Lactation Accommodation  

The Company will provide a reasonable accommodation for limitations related to pregnancy, childbirth or a related medical condition, including lactation or the need to express breast milk, unless doing so would impose an undue hardship on the Company’s business.  Reasonable accommodations may include, but are not limited to: more frequent or longer breaks; time off to recover from childbirth; acquisition or modification of equipment; appropriate seating; temporary transfer to a less strenuous or less hazardous position; job restructuring; light duty; modified work schedule; or private space (other than a bathroom) for the purpose of expressing breast milk.  

When an employee requests an accommodation under this policy, the employee and the Company will engage in a timely, good faith interactive process to determine what accommodation, if any, may be effective.  

Employees with questions or concerns regarding this policy or who would like to request an accommodation should contact Human Resources.  

### PAY PRACTICES  

#### Overtime  

Employees who are not exempt from overtime requirements will be paid overtime in compliance with federal and state law. In addition to the overtime pay provisions set forth in the National Handbook, Kentucky employees who are nonexempt, work more than 40 hours in a week and work every day during a seven-day workweek will be paid one and one-half times their regular rate of pay for all time worked on the seventh day.
This policy does not apply to any officer, foreman or supervisor whose duties are principally limited to directing or supervising other employees.  

#### Meal and Rest Breaks  

Employees are entitled to a reasonable meal break, scheduled as close as possible to the middle of their scheduled shift. Employees will not be required to take their meal break sooner than three hours or later than five hours after the start of their shift.  

During the meal break, employees will be relieved of all duties. An uninterrupted 30-minute meal break will be unpaid for nonexempt employees.  

Employees are also entitled to a 10 minute paid rest break for every four hours of work.  

Any employee who is unable to take all of the meal or rest breaks to which they are entitled in accordance with this policy, or who has been prevented or discouraged from taking a break to which they are entitled under this policy, should immediately notify Human Resources.  

### TIME OFF AND LEAVES OF ABSENCE  

#### Adoption Leave  

Employees who adopt a child under the age of ten may take a reasonable amount of time off, not to exceed six weeks, to receive the adopted child. Employees wishing to take leave under this policy must submit a written request to their Human Resources representative as far in advance as possible. Leave under this policy will be unpaid.  

Where they overlap, leave taken under this policy will run concurrently with leave taken under the federal Family and Medical Leave Act (FMLA).  

This policy does not apply to adoption by fictive kin (i.e., kinship based upon a social agreement), stepparents, stepsiblings or blood relatives (including relatives of half-blood, first cousins, aunts, uncles, nephews, nieces, grandparents and great-grandparents), or when a foster parent adopts a foster child already in their care.  

#### Jury Duty Leave  

The Company encourages all employees to fulfill their civic responsibilities and to respond to jury service summons or subpoenas, attend court for prospective jury service or serve as a juror. Under no circumstances will employees be terminated, threatened, coerced, or penalized because they request or take leave in accordance with this policy.  

Employees must provide their supervisor with notice of any jury summons or subpoena and a copy of the court or administrative certificate within a reasonable time after receipt and before their appearance is required.  

Time spent engaged in attending court for prospective jury service or for serving as a juror is not compensable except that exempt employees will not incur any reduction in pay for a partial week's absence due to jury duty.  

#### Court Attendance Leave  

Employees will be allowed time off from work for a required appearance in court or an administrative tribunal or hearing, so long as they provide advance notice of the need for leave. Employees seeking leave under this policy must notify their Human Resources representative and provide a copy of the court or administrative certificate regarding the employee’s required appearance.  

Time off under this policy will be without pay except that exempt employees will not incur any reduction in pay for a partial week's absence for leave to appear as a witness.  

#### Time Off to Vote  

The Company encourages all employees to fulfill their civic responsibilities and to vote in public elections. Most employees’ schedules provide sufficient time to vote either before or after working hours.  

Employees that do not have sufficient time to vote outside of working hours will be provided at least four hours of time off for the purpose of voting on Election Day, or to request an application for, or execute, an absentee ballot during the office of the clerk’s normal business hours. Time off will be without pay for nonexempt employees. The Company may specify the hours during which the employee can be absent.  

Employees must provide notice of the need for time off under this policy at least one day before leave will be taken. Employees who take time off under this policy but do not vote or request an application for an absentee ballot may be subject to disciplinary action.  

#### Election Official Leave  

Employees will be granted a full-day leave of absence when serving or training to serve as an election official. Employees wishing to take leave under this policy should notify their Human Resources representative in advance. Employees will not be terminated or otherwise penalized or discriminated against for taking leave under this policy. Leave under this policy will be unpaid, except that exempt employees may be compensated for partial day absences, as required by applicable law.  

The Company may specify the hours during which the employee can be absent from work.  

#### Military Leave  

In addition to the military leave rights set forth in the National Handbook, Kentucky employees who are members of the National Guard (including the Army National Guard and Air National Guard) of Kentucky or of any other state are entitled to an unpaid leave of absence for the period required to perform active duty or training. Upon release from a period of active duty or training, returning employees will be reinstated to their former position of employment with the seniority, status, pay and other rights and benefits they would have had if they had not been absent.  

#### Emergency Responder Leave  

The Company will not terminate employees who are absent or tardy form work due to their service as a volunteer emergency responder when responding to an emergency prior to the time they were supposed to report to work. The Company also will not terminate an employee who serves as a volunteer emergency responder and is absent for a period of no more than 12 months because of injuries incurred in the line of duty.  

For purposes of this policy, “volunteer emergency responder” means an individual who is a volunteer firefighter, rescue squad member, emergency medical technician, peace officer or a member of an emergency management agency.  

The Company may require employees who are absent or late to work under this policy to provide a written statement from the supervisor or acting supervisor of the volunteer fire department, rescue squad, emergency medical services agency, law enforcement agency or the director of the emergency management agency stating that the employee responded to an emergency and listing the time and date of the emergency. For employees who are absent due to an injury suffered while serving as a volunteer emergency responder, the Company may require a written statement: (1) from the supervisor, acting supervisor or director of the volunteer fire department, rescue squad or agency under whose command the employee was serving setting forth when the employee’s injury occurred; and (2) from at least one licensed and practicing physician stating that the employee is injured and a date that the employee will to return to work.  

Leave under this policy will be unpaid, except that exempt employees may receive pay, as required by law.  

### SAFETY AND SECURITY  

#### Cell Phone Use / Texting While Driving  

As is set forth in the National Handbook, the Company prohibits employees from using cellular phones for business reasons while driving or for any reason while driving for work-related purposes or driving a company-owned vehicle. Employees should also be aware that texting while driving on a highway is a violation of Kentucky law, in addition to being a violation of Company policy.  

#### Weapons in the Workplace  

In the interest of maintaining a workplace that is safe and free of violence, and in accordance with the policy set forth in the National Handbook, the Company generally prohibits the presence or use of firearms and other weapons on the Company’s property, regardless of whether or not the person is licensed to carry the weapon. In compliance with Kentucky law, the Company permits those who lawfully possess a firearm to store a firearm, part of a firearm, ammunition or ammunition component inside a personal vehicle on Company property. Such lawfully possessed firearms and ammunition may not be removed from the employees' personal vehicle or displayed to others for any reason other than lawful defensive purposes.


## North Carolina  

### GENERAL INFORMATION  

#### About This North Carolina Supplement  

ConstructConnect is committed to workplace policies and practices that comply with federal, state and local laws. For this reason, North Carolina employees will receive the Company’s national handbook (“National Handbook”) and the North Carolina Supplement to the National Handbook (“North Carolina Supplement”) (together, the “Employee Handbook”).  

The North Carolina Supplement applies only to North Carolina employees. It is intended as a resource containing specific provisions derived under North Carolina law that apply to the employee’s employment. It should be read together with the National Handbook and, to the extent that the policies in the North Carolina Supplement are different from, or more generous than those in the National Handbook, the policies in the North Carolina Supplement will apply.  

The North Carolina Supplement is not intended to create a contract of continued employment or alter the at-will employment relationship. **Only the President/Owner of the Company or that person’s authorized representative has the authority to enter into an agreement that alters the at-will employment relationship and any such agreement must be in writing signed by the President/Owner of the Company or an authorized representative.**  

If employees have any questions about these policies, they should contact their Human Resources representative.  

### COMMITMENT TO DIVERSITY  

#### Equal Employment Opportunity  

As set forth in the National Handbook, ConstructConnect is committed to equal employment opportunity and to compliance with federal antidiscrimination laws. We also comply with North Carolina law, which prohibits discrimination and harassment against employees or applicants for employment based on race, color, religion, national origin sex (including pregnancy), disability, age, sickle cell or hemoglobin C trait, genetic information or testing and military service. The Company also prohibits discrimination against employees because they engage in the lawful use of lawful products (e.g., tobacco products) outside of work, as long as those activities do not adversely affect job performance or the safety of other employees. Additionally, the Company prohibits discrimination against employees with regard to continued employment on the basis of HIV or AIDS status.  

The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law.  

### GENERAL EMPLOYMENT PRACTICES   

#### Employment Eligibility and Work Authorization

ConstructConnect participates in the Electronic Verification system (E-Verify) to electronically verify the work authorization of newly-hired employees. E-Verify is an internet-based program that compares information from an employee's Form I-9 to data contained in the federal records of the Social Security Administration and the Department of Homeland Security to confirm employment eligibility. The Company does not use E-Verify to pre-screen job applicants.  

The Company is committed to honoring all terms and conditions of E-Verify. Employees who do not contest a Tentative Nonconfirmation or who receive a Final Nonconfirmation or No Show are subject to immediate termination of employment.  

The Company will not tolerate any form of discrimination or harassment prohibited by federal, state or local law, including discriminatory treatment based on an individual's national origin or citizenship status. Employees who believe they have been subject to prohibited discrimination or harassment, including during the Form I-9 and E-Verify process, should immediately report the matter as further discussed in the policies regarding discrimination and harassment set forth in the Company’s National Handbook. The Company prohibits retaliation against employees for making such complaints.  

### TIME OFF AND LEAVES OF ABSENCE  

#### Jury Duty Leave  

The Company encourages all employees to fulfill their civic responsibilities and to respond to jury service summons or subpoenas, attend court for prospective jury service or serve as a juror. Under no circumstances will employees be terminated, demoted or otherwise penalized because they request or take leave in accordance with this policy.  

Employees must provide their supervisor with notice of any jury summons or subpoena within a reasonable amount of time after receipt and before their appearance is required. Verification from the court clerk of having served may also be required.  

Time spent engaged in attending court for prospective jury service or for serving as a juror will not be paid, except that exempt employees will not incur any reduction in pay for a partial week's absence due to jury duty.  

#### Precinct Official Leave  

Employees who are appointed as precinct officials will be allowed time off on Election Day to serve in that capacity. Employees wishing to take leave under this policy should notify their Human Resources representative in writing at least 30 days in advance.  

Employees will not be terminated or otherwise penalized or discriminated against for taking leave under this policy. Time off under this policy will be unpaid, except that exempt employees will be paid when required under applicable law.  

#### Domestic Violence Victims Leave  

Employees who are victims of domestic violence make take reasonable time off from work to seek relief under the domestic violence or civil no-contact laws, including, but not limited to, filing a civil action, obtaining a protective order or obtaining emergency assistance.  

Employees seeking time off under this policy must provide the Company with reasonable advance notice of the leave, unless an emergency prevents them from doing so. The Company may require documentation verifying that an emergency prevented an employee from providing proper notice, as well as other information verifying the reason for the absence.  

Time off under this policy will be unpaid, except that exempt employees may receive compensation, as required by applicable law.  

The Company will not retaliate or tolerate retaliation against any employee who seeks or obtains leave under this policy.  

#### Parental School Involvement Leave  

Employees who are the parent, guardian or person standing in loco parentis of a school-aged child will be allowed up to four hours of time off per year to attend or otherwise be involved at their child’s school (including a public school, private and religious school, preschool or childcare facility).  

Time off must be scheduled at a time mutually agreed upon between the employee and their supervisor.  

The Company may require that employees submit a written request for the leave at least 48 hours before the requested absence. The Company may also require that employees provide written verification from the child’s school, indicating that they attended or were otherwise involved at the school during the time of the leave.  

Leave under this policy will be unpaid except that exempt employees may be paid, as required by law.  

The Company will not terminate, demote or otherwise discriminate against employees who request or take leave in accordance with this policy.  

#### Military Leave  

In addition to the military leave rights set forth in the National Handbook, employees who are members of the North Carolina National Guard are entitled to take an unpaid leave of absence when called into state active duty. Employees will not be required to use accrued vacation or other paid leave during the period of active service.  

In addition, upon honorable release from state active duty, employees who are members of the North Carolina National Guard or the National Guards of any other state will be entitled to reinstatement to their previous position or a position of like seniority, status and salary, as long as:  

- The employee provides proper notice of the intent to return to employment with the Company;  
- The employee is still qualified for employment; and  
- The Company’s circumstances have not changed such that reinstatement is unreasonable.  

Employees must make written application for reemployment in accordance with the following timing requirements:  

- For individuals whose period of service was less than 31 days, not later than the beginning of the first regularly scheduled work period  following the completion of the period of service, safe transport to the individual's residence and an additional eight hour period;   
- For individuals whose period of service was more than 30 days, not later than 14 days after the individual’s release from state duty;  
- For individuals hospitalized for, or convalescing from, an illness or injury incurred in or aggravated during the performance of state duty, not later than the end of the period of recovery (generally not to exceed two years).  

Employees who are no longer qualified for their previous employment will be placed in another position for which they are qualified and that will provide appropriate seniority, status and salary, unless the Company’s circumstances make such a placement unreasonable.  

#### Emergency Responder Leave  

The Company will provide a leave of absence for employees who are called to respond to a declared state of emergency in their capacity as a volunteer firefighter, member of a rescue squad or member of an emergency medical services agency.  

The Company reserves the right to have an employee certified as essential to the employer’s own on-going emergency or disaster relief activities. Employees who are so certified will not be eligible for leave under this policy.  

Leave under this policy will be unpaid except that exempt employees may be paid, as required by law. Additionally, employees may elect to use accrued vacation or other accrued leave during time off under this policy, but are not required to do so.  

### SAFETY AND SECURITY  

#### Smoke-Free Workplace  

The Company prohibits smoking in the workplace. Employees wishing to smoke must do so outside company facilities during scheduled work breaks.  

Employees that observe other individuals smoking in the workplace in violation of this policy have a right to object and should report the violation to their supervisor or another member of management. Employees will not be disciplined or retaliated against for reporting smoking that violates North Carolina law or this policy.  

Employees that violate this policy will be subject to disciplinary action up to and including termination of employment.  

#### Cell Phone Use / Texting While Driving  

As set forth in the National Handbook, the Company prohibits employees from using cellular phones for business reasons while driving or for any reason while driving for work-related purposes or driving a company-owned vehicle. Employees should also be aware that using a mobile telephone to write or read e-mail or text messages while driving is a violation of North Carolina law, in addition to being a violation of company policy.  


## New Jersey  


### GENERAL INFORMATION  

#### About This New Jersey Supplement  

ConstructConnect is committed to workplace policies and practices that comply with federal, state and local laws. For this reason, New Jersey employees will receive the Company's national handbook ("National Handbook") and the New Jersey Supplement to the National Handbook ("New Jersey Supplement") (together, the "Employee Handbook").  

The New Jersey Supplement applies only to New Jersey employees. It is intended as a resource containing specific provisions derived under New Jersey law that apply to the employee's employment. It should be read together with the National Handbook and, to the extent that the policies in the New Jersey Supplement are different from or more generous than those in the National Handbook, the policies in the New Jersey Supplement will apply.  

The New Jersey Supplement is not intended to create a contract of continued employment or alter the at-will employment relationship. **Only the President/Owner of the Company or his or her authorized representative has the authority to enter into an agreement that alters the at-will employment relationship, and any such agreement must be in writing and signed by the President/Owner of the Company or his or her authorized representative.**  

If employees have any questions about these policies, they should contact their Human Resources representative.  

### COMMITMENT TO DIVERSITY  

#### Equal Employment Opportunity  

As set forth in the National Handbook, ConstructConnect is committed to equal employment opportunity and to compliance with federal antidiscrimination laws. We also comply with New Jersey law, which prohibits discrimination and harassment against any employees or applicants for employment based on race (including traits historically associated with race, such as hair texture, hair type and protective hairstyles), creed, color, national origin, ancestry, age, sex, pregnancy or breastfeeding (including childbirth, breastfeeding or expressing milk for breastfeeding or medical conditions related to pregnancy, childbirth or breastfeeding), marital status, civil union or domestic partnership status, affectional or sexual orientation, gender identity or expression, atypical hereditary cellular or blood trait, genetic information, disability (including AIDS and HIV-related illnesses), liability for service in the U.S. Armed Forces and use or non-use of tobacco products outside the workplace.  The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law.  

Additionally, the Company prohibits retaliation against any employee who requests from, discusses with or discloses to a current or former employee, a lawyer from whom the employee seeks legal advice or a government agency information regarding the job title, occupational category, rate of compensation (including benefits), gender, race, ethnicity, military status, or national origin of the employee or any other employee.  Employees are not required to disclose their wage information.  

#### Political Opinions  

The Company will not take adverse action employment action or threaten to take such action in an attempt to induce or compel an employee to vote or refrain from voting for a particular candidate in an election.  

#### Pregnancy and Lactation Accommodation  

Employees may, based on the advice of their physician, request a reasonable workplace accommodation in connection with their own pregnancy, childbirth or related medical conditions, including recovery from childbirth. Employees who are breastfeeding an infant child can also request a workplace accommodation. The request must be based on their physician’s advice. An accommodation will be provided unless it is unreasonable and would impose an undue hardship on the Company’s ordinary operations.  

Reasonable accommodations for pregnancy may include job modifications such as additional bathroom breaks, water breaks, periodic rest breaks, assistance with manual labor, job restructuring, modified work schedules and temporary transfers to less strenuous or less hazardous work. Reasonable accommodations for employees who are breastfeeding their infant children include reasonable daily break time  and a suitable room or other location with privacy, other than a toilet stall, in close proximity to the work area, for the employee to express breast milk for her infant child. The Company will consider accommodation requests on a case-by-case basis and may require accommodation requests to be supported by appropriate medical documentation.  

The Company will not treat a pregnant or breastfeeding employee less favorably than it would treat non-pregnant/non-breastfeeding individuals who are similar in their ability or inability to work.  

Employees who have questions about this policy or who wish to request an accommodation under this policy should contact their Human Resources representative.  

The Company will not tolerate any retaliation against any employee who makes a good-faith request for or uses an accommodation in accordance with this policy.  

### GENERAL EMPLOYMENT PRACTICES  

#### Whistleblower Protections (Conscientious Employee Protection Act)  

Employees have the right to complain of workplace practices or policies they believe to be in violation of the law, against public policy and/or fraudulent or unethical.  

The Company will not terminate, demote, take any other adverse employment action or otherwise retaliate against an employee because the employee:  

1. Discloses or threatens to disclose to a supervisor or to a public body an activity, policy or practice of the employer or another employer with whom there is a business relationship, which the employee reasonably believes is in violation of a law or a rule or regulation issued under the law; is fraudulent or criminal; or, in the case of an employee who is a licensed or certified health care professional, constitutes improper quality of patient care;  
1. Provides information to or testifies before any public body conducting an investigation, hearing or inquiry into any violation of a law or a rule or regulation issued under the law by the employer or another employer with whom there is a business relationship, or, in the case of an employee who is a licensed or certified health care professional, provides information to or testifies before any public body conducting an investigation, hearing or inquiry into quality of patient care;  
1. Provides information to or testifies before any public body conducting an investigation, hearing or inquiry into any violation involving deception of or misrepresentation to any shareholder, investor, client, patient, customer, employee, former employee, retiree or pensioner of the employer or any governmental entity;  
1. Provides information regarding any perceived criminal or fraudulent activity, or policy or practice of deception or misrepresentation, which the employee reasonably believes may defraud any shareholder, investor, client, patient, customer, employee, former employee, retiree or pensioner of the employer or any governmental entity; or  
1. Objects to or refuses to participate in any activity, policy or practice that the employee reasonably believes:    
	- a. Is in violation of a law or a rule or regulation issued under the law or, if the employee is a licensed or certified health care professional, constitutes improper quality of patient care;  
	- b. Is fraudulent or criminal; or  
	- c. Is incompatible with a clear mandate of public policy concerning the public health, safety or welfare or protection of the environment.  
	
When a disclosure is made to a public body, the protection against retaliation does not apply unless the employee has brought the activity, policy or practice to the attention of his or her supervisor in writing and has given the Company a reasonable opportunity to correct the activity, policy or practice. There is an exception, however, to this internal disclosure requirement if the situation is an emergency in nature and the employee reasonably believes that the activity, policy or practice is known to one or more company supervisors or reasonably fears physical harm as a result of the disclosure.  

The Company has designated the following official to receive complaints and answer employee questions regarding this policy:  

Julie Storm, Chief People Officer    
ConstructConnect  
3825 Edwards Road. Suite 800  
Cincinnati, Ohio 45209  
513-645-8004  

### PAY PRACTICES  

#### Meal Breaks for Minors  

Employees under the age of 18 who work five or more consecutive hours will be provided a 30-minute uninterrupted meal break. During the meal break, employees will be relieved of all duties. An uninterrupted 30-minute meal break will be unpaid for nonexempt employees. Breaks of less than 30 minutes will be counted as paid work time.  

Any employee who is unable to take all of the meal breaks to which he or she is entitled in accordance with this policy, or who has been prevented or discouraged from taking a break to which he or she is entitled under this policy, should immediately notify Human Resources.  

### TIME OFF AND LEAVES OF ABSENCE  

#### New Jersey Family Leave  

We recognize that employees may need to be absent from work for an extended period of time for family-related reasons. Accordingly, the Company will grant time off to employees in accordance with the requirements of the federal Family and Medical Leave Act (FMLA) and the New Jersey Family Leave Act (NJFLA). When both the FMLA and NJFLA apply, the leave provided by each will count against the employee’s entitlement under both laws and must be taken concurrently. An employee who is eligible for leave under only one of these laws will receive benefits in accordance with that law only.  

The following policy addresses employee rights under the NJFLA. Employees should refer to the National Handbook for additional details regarding the FMLA. Questions concerning this policy should be directed to Human Resources.  

##### Leave Entitlement and Eligibility  

Employees who work in New Jersey, or who perform some work in New Jersey and have their work directed and controlled from New Jersey, may be eligible for leave under the NJFLA. To be eligible for leave, employees must have been employed by the Company for at least 12 months and have worked at least 1,000 base hours (including regular time, overtime, workers’ compensation leave and military leave) during the 12-month period immediately preceding the leave. The Company may deny leave for certain highly compensated employees.  

Eligible employees are entitled to 12 weeks of unpaid leave in a 24-month period. A 24-month period is determined by a rolling 24-month period measured backward from the date an employee uses NJFLA leave. When two employees from the same family (e.g., spouses or siblings) request leave at the same time, the Company will allow each employee up to 12 weeks of unpaid leave, so long as the employee is otherwise eligible for leave.  

##### Permissible Uses of NJFLA Leave  

Eligible employees may take family leave to provide care for the following reasons:  

- **Bonding Leave:** The birth of a child (including a child born pursuant to a valid written agreement between the employee and a gestational carrier), and to care for a newborn or a child newly placed with the employee for foster care or adoption;  
- **Family Care Leave:** Serious health condition of a family member; or  
- **Public Health Emergency Leave:** During a state of emergency declared by the Governor (or when indicated as necessary by the Commissioner of Health or other public health authority during an epidemic of a communicable disease, a known or suspected exposure to the communicable disease, or efforts to prevent spread of the communicable disease):  
	- To provide in-home care or treatment of the employee’s child because the child’s school or place of care has been closed by order of a public official due to the epidemic or other public health emergency;  
	- To care for a covered family member because a public health authority has issued a determination, including a mandatory quarantine order, requiring or imposing responsive or prophylactic measures as a result of illness caused by the communicable disease (or known or suspected exposure to the communicable disease) because the presence in the community of the covered family member in need of care by the employee would jeopardize the health of others; or  
	- To care for a covered family member who, under the recommendation of a health care provider or public health authority, voluntarily self-quarantines as a result of suspected exposure to the communicable disease because the presence in the community of that family member would jeopardize the health of others.  

For purposes of this policy, a “Parent” includes the employee’s biological, adoptive, resource family parent, foster parent, stepparent, parent-in-law or legal guardian and includes individuals who become the parent of a child pursuant to a valid written agreement with a gestational carrier. A “child” includes, but is not limited to the employee’s biological, adopted, foster child, resource family child, stepchild or legal ward and includes a child who becomes the child of a parent pursuant to a valid written agreement between the parent and a gestational carrier. A “serious health condition” means an illness, injury, impairment or physical or mental condition that requires inpatient care in a hospital, hospice or residential medical care facility or continuing medical treatment or continuing supervision by a health care provider. A “family member” means a child, parent, parent-in-law, sibling, grandparent, grandchild, spouse, domestic partner, partner in a civil union couple or any other individual related by blood to the employee or with whom the employee shows a close association that is the equivalent of a family relationship.  

Leave for the birth of a child or for the placement of an adopted or foster child must begin within one year after the child’s birth or placement for adoption or foster care.  

When a leave is covered by both the FMLA and the NJFLA, the leave will simultaneously count as part of the employee’s entitlement under both laws. However, a leave granted due to the employee’s own serious health condition under the FMLA is not covered by the NJFLA. As a result, a leave of 12 weeks to care for the employee’s own serious health condition under the FMLA may be followed by an additional 12-week leave to care for a family member under the NJFLA. This may result in a combined leave period under both laws of up to 24 weeks.  

##### Requesting Leave  

Employees must provide at least 30 days’ advance notice to the Company before beginning a single, continuous period of NJFLA leave, unless emergent circumstances warrant shorter notice.  Employees must provide prior notice of the leave in a reasonable and practicable manner, unless an emergency or other unforeseen circumstance precludes prior notice.  Notice must be in writing, except that employees may provide verbal notice in emergency situations when written notice is impracticable, as long as they subsequently provide written notice. When taking intermittent leave, employees must provide the Company with at least 15 days’ notice prior to the first day of leave, unless not otherwise practicable.  Employees must make a reasonable effort to schedule NJFLA leave in a manner that does not unduly disrupt Company operations.  

##### Certification for Leave  

A request for NJFLA leave for Bonding Leave or Family Care Leave must be supported by certification issued by a duly licensed or other acceptable health care provider. If a completed certification is not returned in a timely manner, the leave may be denied. If the Company has reason to doubt the validity of the certification, we may require a second (and in some cases a third) medical opinion at the Company’s expense.  

A request for NJFLA Public Health Emergency Leave must be supported by a sufficient certification issued by a school, place of care for children, public health authority, public official, or health care provider (depending upon the reason for which leave is being requested).  

##### Intermittent or Reduced Schedule Leave  

Employees can elect to take NJFLA leave on a reduced leave schedule basis.  However, a reduced schedule may not last longer than 12 months for any one period of leave. In addition, Family Care Leave may be taken on a reduced schedule or intermittent leave may not last longer than 12 months for any period of leave.  

Employees wanting to take leave intermittently or on a reduced schedule basis must:  

- Provide notice of the leave at least 15 days prior to the first day of leave (unless an emergency or other unforeseen circumstance precludes prior notice), and  
- Make a reasonable effort to schedule leave so as not to unduly disrupt the Company’s operations.  

If possible, prior to the commencement of intermittent leave, an employee should provide a regular schedule of the day or days of the week on which intermittent leave will be taken.  

Family Care Leave may be taken on a reduced schedule or an intermittent basis only when medically necessary.  

The Company may require employees on reduced schedule or intermittent leave to temporarily transfer to an available alternative position for which the employee is qualified and that better accommodates a recurring period of leave than does the employee’s regular position. The alternative position will have pay and benefits equivalent to the employee’s regular position.  

Upon returning from a reduced schedule or intermittent leave, the employee will be placed in the same or an equivalent job as the one he or she left when the leave began.  

##### Compensation and Benefits During Leave  

Leaves of absence under this policy are generally without pay. However, some employees may be eligible for temporary disability benefits or paid leave benefits and should consult the Company’s temporary disability benefits and paid family leave insurance policies. In addition, employees who have accrued paid leave (e.g., sick, vacation or personal time) may use that time during their approved NJFLA leave. Use of paid time off will not serve to extend the length of any leave.  

Employees will be permitted to continue employment benefits during the leave at the same level and under the same conditions that coverage would have been provided had the employee continued in employment and not taken leave.  

##### Outside Employment  

Employees may not take a new full-time position while on leave. Employees can take a new part-time job as long as it does not exceed half of the employee's regularly scheduled hours worked for the Company. Employees may also continue full-time or part-time employment they had prior to the leave.  

##### Return From Leave  

Employees generally will be restored to their original position or to a position with equivalent pay, benefits and other terms and conditions of employment. However, employees have no greater right to continued employment than if they had not taken the leave.  

Reinstatement may be denied if:  

1. During the leave, the employee’s job would have been terminated or the employee would have been laid off for reasons unrelated to the leave; or  
1. The employee performed unique services and hiring a permanent replacement during the leave, after giving reasonable notice to the employee of the intent to do so, was the only way for the Company to prevent substantial and grievous economic injury to its operations. Certain highly compensated employees (those earning pay in the top 5% or whose salary is one of the 7 highest, whichever is greater) may be denied leave or reinstatement if necessary to prevent substantial and grievous economic injury to the Company’s business. If an employee falls within this category, he or she will be advised by the Company of any decision to deny leave. This exception does not apply to employees seeking Public Health Emergency Leave.  

##### Retaliation  

The Company will not interfere, restrain or deny the exercise of any rights provided under this policy. If an employee believes that his or her NJFLA rights have been violated in any way, he or she should immediately report the matter to Human Resources.  

#### Paid Sick and Safe Leave  

The Company provides eligible employees with paid sick and safe leave in accordance with the requirements of New Jersey’s earned sick and safe leave law (“ESSLL”).  

##### Eligibility  

Employees (including those working on a full-time, part-time or temporary basis) are generally eligible to accrue paid sick and safe leave.  

##### Reasons Sick and Safe leave May be Used  

Employees may use paid sick and safe leave for the following reasons:  

- The employee's or the employee's family member's mental or physical illness, injury or health condition;  
- For the diagnosis, care or treatment of the employee's or the employee's family member's mental or physical illness, injury or health condition;  
- For preventive medical care for the employee or the employee's family member;  
- The employee or his or her family member is a victim of domestic or sexual violence (including stalking, sexual assault or any sexually violent offense) and needs to obtain:   
	- Medical attention;  
	- Services from a designated domestic violence agency or other victim services organization;  
	- Psychological or other counseling;    
	- Relocation; or  
	- Legal services, including obtaining a restraining order or preparing for or participating in a civil or criminal legal proceeding related to the domestic or sexual violence.   
- To attend a child's school-related conference, meeting, function or other event requested or required by a school administrator, teacher or other professional staff member responsible for the child's education;   
- To attend a meeting regarding a child's care in connection with the child's health or disability;  
- The employee is unable to work because:   
	- The employee's workplace or the employee's child's school or place of care is closed by order of a public official or because of a state of emergency declared by the Governor due to an epidemic or other public health emergency;  
	- The Governor has declared a state of emergency or a health care provider, the Commissioner of Health or another public health authority has issued a determination that the presence in the community  of the employee or the employee’s family member in need of care by the employee would jeopardize the health of others; or  
	- During a state of emergency declared by the Governor, or upon the recommendation, direction, or order of a healthcare provider or the Commissioner of Health or other authorized public official, the employee undergoes isolation or quarantine, or cares for a family member in quarantine, as a result of suspected exposure to a communicable disease and has a finding by the provider or authority that the presence in the community of the employee or family member would jeopardize the health of others.  

For purposes of this policy, "family member" includes a:  

- Child (including a biological, adopted, foster or stepchild, a legal ward, and the child of a domestic partner or civil union partner);  
- Parent (including a biological, adoptive, foster or stepparent; legal guardian; parent of a spouse, domestic partner or civil union partner; a person who stood in loco parentis when the employee was a minor; or a parent’s spouse, domestic partner or civil union partner);  
- Spouse (including a civil union partner or domestic partner);  
- Sibling (including a biological, adopted or foster sibling and a sibling of a spouse, domestic partner or civil union partner);  
- Grandparent (including a grandparent’s spouse, domestic partner or civil union partner);  
- Grandchild; and  
- Any other individual related by blood to the employee or whose close association with the employee is the equivalent of a family relationship, including any person with whom the employee has a significant personal bond that is, or is like, a family relationship, regardless of biological or legal relationship.  

The Company will not count employees' use of sick and safe leave in compliance with this policy as an absence when evaluating absenteeism. Therefore, any such use of sick and safe leave will not count as an “occurrence” under any Company policy. An employee who uses paid sick and safe leave for an unauthorized purpose may be subject to discipline, up to and including termination.  

##### Accrual and Use of Sick and Safe Leave  

Eligible employees begin to accrue paid sick and safe leave under the ESSLL on October 29, 2018, or the employee's first day of work, whichever is later. Sick and safe leave accrues at a rate of one hour of paid sick and safe leave for every 30 hours worked. For exempt employees who are not required to record their hours worked, the Company will presume for the purpose of calculating paid sick and safe time accrual that the employee works 40 hours per week.  

Eligible employees may accrue up to a maximum of 40 hours of paid sick and safe leave in a given year.  

Eligible employees may not use paid sick and leave accrued under this policy and the ESSLL until the 120th calendar day after their employment with the Company began.  

Paid sick and safe leave may be used in increments of four hours. Eligible employees may use up to 40 hours of paid sick time in a year.  

Employees are not required to search for or find an employee to cover their work in order to take paid sick and safe leave.  

##### Requesting Sick and Safe Leave and Documentation  

When the need for paid sick and safe leave is foreseeable, employees must provide notice of the need for leave and its expected duration at least seven days prior to the start of the leave. Employees must make reasonable efforts to schedule the use of paid sick and safe leave in a manner that does not unduly disrupt Company operations.  

If the need for paid sick and safe leave is unforeseeable, employees should provide notice of the need for leave and its expected duration as soon as practical. To provide notice of the need to use paid sick and safe leave, employees should contact their supervisor.  

If paid sick and safe leave is used for three or more consecutive workdays, the Company may require that the employee provide reasonable documentation that the paid sick and safe leave was used for a qualifying reason. For a medical-related absence, an employee can satisfy this requirement by providing documentation signed by a health care professional that indicates the need for leave and, if possible, the amount of leave required. For leave related to domestic or sexual violence, the employee can provide any of the following documents:  

- Medical documentation;   
- A law enforcement agency record or report;   
- A court order;   
- Documentation that the perpetrator of the domestic or sexual violence has been convicted of a domestic or sexual violence offense;   
- Certification from a certified domestic violence specialist or a representative of a designated domestic violence agency or other victim services organization; or  
- Other documentation or certification provided by a social worker, counselor, member of the clergy, shelter worker, health care professional, attorney, or other professional who has assisted the employee or covered relation in dealing with the domestic or sexual violence.  

For leave related to an epidemic or other public health emergency, the employee can provide a copy of the order of the public official or the determination by the health authority.  

##### Confidentiality  

Health information and information pertaining to domestic or sexual violence related to an employee or the employee's family member will be treated as confidential and not disclosed except to the affected employee or with that employee's permission, unless otherwise required by applicable law.  

##### Paid Sick and Safe Leave Carryover  

Accrued, unused paid sick and safe leave can be carried over from year to year, up to a maximum carryover amount of 40 hours.  

##### Rate of Pay and Overtime  

Paid sick and safe leave is compensated at the same rate of pay and with the same benefits an employee normally earns, or at the state minimum wage (whichever is greater).  

##### Integration with Other Benefits  

It is an employee's responsibility to apply for any applicable benefits for which the employee may be eligible as a result of illness or disability, including temporary disability insurance, family leave insurance, workers' compensation insurance, and any other disability insurance benefits. If an employee elects to integrate paid sick and safe leave with other paid benefits, the Company will integrate all paid benefits such that an employee will not be paid more than his or her regular compensation at any time.  

##### Separation from Employment  

Compensation for accrued and unused paid sick and safe leave is not provided upon separation from employment for any reason. If an employee is rehired by the Company within six months of separation from employment, previously accrued but unused sick and safe leave will immediately be reinstated. The previous period of employment will be counted for purposes of determining the employee's eligibility to use paid sick and safe leave.  

##### Retaliation Prohibited  

The Company will not discriminate or retaliate against employees, or tolerate discrimination or retaliation against employees, because they request or use paid sick and safe leave in accordance with this policy and/or the ESSLL, file a complaint alleging a violation of the ESSLL or inform any other person of his or her rights under the ESSLL.  

##### Effect on Other Rights and Policies  

The Company may provide other forms of leave for employees to care for medical conditions or for reasons related to domestic or sexual violence or family leave under certain federal and state laws. In certain situations, sick and safe leave under this policy may run at the same time as leave available under another federal or state law, provided eligibility requirements for that law are met. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other federal and state medical, domestic or sexual violence or family leave rights.  

#### Family Leave Insurance  

Eligible employees may be eligible for up to 12 weeks of state-provided family leave insurance (FLI) benefits through the Division of Temporary Disability Insurance (the Division) when they take time off for one of the following purposes:  

- To bond with a child during the first 12 months after a child’s birth if the employee or employee’s domestic partner or civil union partner is a biological parent of the child or the parent of the child pursuant to a valid gestational carrier agreement, or after the placement of the child for adoption or as a foster child with the employee;   
- To care for a family member with a serious health condition;   
- When the employee or employee’s family member is a victim of an incident of domestic violence or a sexually violent offense, to engage in activities for which an employee can take unpaid leave under the New Jersey Security and Financial Empowerment Act (NJ SAFE Act), as described in the Company’s Domestic or Sexual Violence Victim Leave policy; or  
- During a state of emergency declared by the Governor(or when indicated as necessary by the Commissioner of Health or other public health authority during an epidemic of a communicable disease, a known or suspected exposure to the communicable disease, or efforts to prevent spread of the communicable disease) the employee is required to provide in-home care or treatment of the employee’s covered family member because:  
	- A healthcare provider or the commissioner or other public health authority has issued a determination that the family member’s presence in the community may jeopardize the health of others; and  
	- The provider or authority recommends, directs or orders that the employee’s family member be isolated or quarantined as a result of suspected exposure to a communicable disease.  

If an employee receives benefits for a disability caused by domestic violence or a sexually violent offense, that time off will be considered a period of disability for the employee and not a period of family leave.  

For purposes of FLI, a “family member” is defined to include an employee’s child, spouse, domestic partner, civil union partner, sibling, grandparent, grandchild, parent, parent-in-law or any other individual related to the employee by blood or who has a close association with the employee that is the equivalent of a family relationship. A “child” includes a biological, adopted, foster or stepchild; legal ward; or a child of an employee’s domestic partner or civil union partner or a child who becomes the employee’s child pursuant to a valid written agreement with a gestational carrier. A “parent” includes a biological parent, foster parent, adoptive parent or stepparent of the employee or a person who was a legal guardian of the employee when the employee was a child or who became the parent of the child pursuant to a valid written agreement with a gestational carrier.  

##### Eligibility  

Employees who have worked at least 20 base weeks in the year preceding the leave or earned in total at least 1,000 times the applicable minimum wage during the prior year are eligible to apply for and receive FLI benefits. The Division determines whether an employee is eligible for benefits.  

##### Amount and Duration of Benefits  

The weekly FLI benefit is generally 85% of the employee’s average weekly wage and is subject to a state-imposed cap. The maximum benefit is generally 12 weeks (or 56 intermittent days) during the 12-month period or one-third of the employee’s base year earnings, whichever is less.  

When applicable and allowed under applicable law, FLI benefits will run concurrently with leave time available under the New Jersey Family Leave Act or federal Family and Medical Leave Act. Employees are permitted to use any accrued but unused paid time, including paid sick and safe leave, during a period of family temporary disability leave.  FLI benefits will not be paid for any period during which the employee receives paid sick leave, vacation time or other leave from the Company at full pay.  

##### Benefits on an Intermittent Basis  

Employees may file claims for intermittent periods of time when medically necessary to care for a seriously ill family member. Benefits can be taken on an intermittent basis for this purpose, if:  

- The total leave time the employee takes does not go over 12 months;  
- The employee provides a copy of the appropriate medical certification;  
- The employee provides at least 15 days' notice, unless an emergency or unplanned event prevents the employee from doing so; and  
- The employee makes a reasonable effort to schedule leave in a manner that does not unduly burden the Company and, if possible, provides a regular schedule of the days or days of the week on which the intermittent leave will be taken.  

##### Notice and Certification  

Employees intending to take a single, continuous leave to bond with a newborn or newly adopted child or a child newly placed with the employee for foster care must provide the Company with a minimum of 30 days’ notice prior to beginning the family leave.  

Unless an emergency or other unforeseen circumstance prevents prior notice, employees intending to take continuous leave to care for a family member must provide the Company with advance notice in a reasonable and practicable manner. Employees intending to take leave for reasons related to the employee or employee’s family member being a victim of an incident of domestic violence or a sexually violent offense or for the reasons set forth above that are the result of a state of emergency, must provide written notice as far in advance as is reasonable and practical under the circumstances, unless an emergency or other unforeseen circumstance precludes prior notice.  

Employees intending to take intermittent leave to care for a family member or for the birth, adoption or placement of a child in foster care must provide the Company with a minimum of 15 days’ notice prior to the first day on which benefits will be paid for intermittent leave, absent emergency or unforeseen circumstances. Before intermittent leave related to the birth, adoption or placement of a child for foster care begins, the employee must, if possible, provide the Company with a regular schedule of the days or days of the week on which intermittent leave will be taken.  

Unless the leave is unforeseeable, employees who fail to provide this notice may have the amount of benefits they receive reduced. For continuous leave taken for the birth, adoption or placement of a child for foster care, failure to provide advance notice will result in the reduction of benefits by two weeks’ worth of benefits unless the time for leave is unforeseeable or changes for unforeseeable reasons.  

Employees requesting FLI benefits for leave to care for a family member will be required to provide certification from a health care provider to the Division. Employees requesting FLI benefits for leave related to the employee or employee’s family member being a victim of an incident of domestic violence or a sexually violent offense or for leave resulting from a state of emergency, as listed above, must, if requested by the Division, provide certification in support of their claim.  

The Company will not discharge, threaten or otherwise discriminate or retaliate against an employee or refuse to restore the employee following a period of leave because the employee requested or took FLI benefits.  However, nothing in this policy affords employees any greater right to reinstatement than is available under the New Jersey Family Leave Act, as described in the Company’s New Jersey Family Leave policy.  

#### Temporary Disability Benefits  

New Jersey employees who are temporarily disabled by a non-work-related injury or illness may receive benefits through the New Jersey Temporary Disability Benefits Plan (State Plan). Employees are eligible for temporary disability (TD) benefits only when they suffer an accident or illness that is not covered by New Jersey’s workers’ compensation law, including a disability that is the result of organ or bone marrow donation by a covered employee  

During a state of emergency declared by the Governor or when indicated to be needed by the Commissioner of Health or other public health authority, TD benefits are also available for an illness caused by an epidemic of a communicable disease, a known or suspected exposure to a communicable disease or efforts to prevent the spread of a communicable disease, which requires in-home care or treatment of an employee due to:  

- The determination by a health care provider, the commissioner or other public health authority that the employee's presence in the community may jeopardize the health of others; and  
- The recommendation, direction or order of the provider or authority that the employee be isolated or quarantined because of suspected exposure to a communicable disease.  

To be eligible for TD benefits, employees must: (1) have worked at least 20 “base weeks” in covered New Jersey employment within the base year preceding the week in which the disability began or the week in which the employee submits a claim for benefits; or (2) earned at least 1,000 times the minimum wage in effect on October 1 of the calendar year preceding the calendar year in which the disability began. A “base week” is any calendar week in which the employee earned at least 20 times the state minimum wage. To qualify for TD benefits, the employee’s illness or injury must have started while the employee was eligible for benefits.  

Benefits begin on the eighth consecutive day of a disability and may continue up to a maximum of 26 weeks or one-third of an employee’s total wages in a year. Eligible employees will receive an amount equal to 85% of their weekly wage, up to a maximum of70 percent of the statewide average weekly wage. No benefits are paid for the first week of disability, unless the disability: is related to an illness caused by an epidemic of a communicable disease, a known or suspected exposure to a communicable disease or efforts to prevent the spread of a communicable disease, as described above; is the result of a covered employee donating an organ or bone marrow; or continues for a period lasting longer than three weeks.  

Employees who collect TD benefits from the state may be ineligible to simultaneously receive benefits under other state or federal unemployment, disability or workers’ compensation laws.  

When filing a claim for TD benefits under the State Plan, employees will be required to provide written notice to the New Jersey Department of Labor and Workforce Development’s Temporary Disability Insurance Division within 30 days after the beginning of a period of disability. Employees will also be required to provide certification of the disability from a health care provider.  

#### Reinstatement Following Bone Marrow and Organ Donation  

Employees who experience a period of disability that is the result of donating any organ or bone marrow, and that is a compensable disability under New Jersey's Temporary Disability Benefits Law, will be restored at the end of the period of disability to their original job or to an equivalent job with equivalent pay, benefits and other terms and conditions of employment. However, an employee has no greater right to reinstatement than if they had not been absent for the purpose of organ or bone marrow donation.  

For example, if an employee who was absent from work for a disability resulting from organ or bone marrow donation would have been laid off had they not been absent, or if the employee's job is eliminated during the period of disability and no equivalent or comparable job is available, then the employee would not be entitled to reinstatement.  

#### Jury Duty Leave  

The Company encourages all employees to fulfill their civic responsibilities and to respond to jury service summonses or subpoenas, attend court for prospective jury service or serve as a juror. Under no circumstances will employees be terminated, threatened, penalized or coerced because they request or take leave in accordance with this policy.  

Employees should provide their supervisor with notice of any jury summons or subpoena within a reasonable amount of time after receipt and before their appearance is required. Verification from the court clerk of having served may also be required.  

Time spent engaged in attending court for prospective jury service or for serving as a juror is not compensable except that exempt employees will not incur any reduction in pay for a partial week's absence due to jury duty.  

#### Domestic or Sexual Violence Victim Leave  

Eligible employees who are victims of domestic violence or a sexually violent offense or who have a qualifying family member who is a victim of domestic or sexual violence may take up to 20 days of unpaid leave in the 12-month period following an incident of domestic or sexual violence to:  

- Seek medical attention for or recover from physical or psychological injuries caused by domestic or sexual violence to the employee or the employee's family member;  
- Obtain services from a victim services organization for the employee or the employee's family member;  
- Obtain psychological or other counseling for the employee or the employee's family member;  
- Participate in safety planning, temporarily or permanently relocate or take other actions to increase the safety of the employee or the employee's family member from future domestic or sexual violence or to ensure economic security;  
- Seek legal assistance or remedies to ensure the health and safety of the employee or the employee's family member, including preparing for or participating in any civil or criminal legal proceeding related to or derived from domestic or sexual violence; or  
- Attend, participate in or prepare for a criminal or civil court proceeding relating to domestic or sexual violence.  

For purposes of this policy, a “family member” is an employee’s child, parent, parent-in-law, sibling, grandparent, grandchild, spouse, domestic partner or civil union partner, any other individual related by blood to the employee or any other individual who has a close association with the employee that is the equivalent of a family relationship.  

Employees are eligible for leave under this policy if they have been employed with the Company for at least 12 months and for at least 1,000 base hours during the 12-months immediately preceding the leave.  

When the need for leave is foreseeable, employees must provide written notice of the need as far in advance as is reasonable and practical under the circumstances, unless an emergency or other unforeseen circumstance precludes prior notice. Advance notice is not required for emergency situations.  

Leave may be taken intermittently in intervals of no less than one day.  

Employees will be required to submit documentation verifying the need for leave, such as:  

- A domestic violence restraining order or other documentation of equitable relief issued by a court;  
- A letter or other written documentation from the county or municipal prosecutor documenting the domestic violence or sexually violent offense;  
- Documentation of the conviction of a person for the domestic violence or sexually violent offense;  
- Medical documentation of the domestic violence or sexually violent offense;  
- Certification from a certified domestic violence specialist or the director of a designated domestic violence agency or rape crisis center confirming that the employee or employee's family member is a victim of domestic violence or a sexually violent offense; or  
- Other documentation or certification of the domestic violence or sexually violent offense provided by a social worker, clergy member, shelter worker or other professional who has assisted the employee or employee's family member in dealing with the domestic violence or sexually violent offense.  

All information provided to the Company concerning a domestic violence or sexually violent incident and leave under this policy will be kept confidential, unless disclosure of this information is authorized in writing by the employee or is required by law.  

Employees can choose to use any accrued paid time off or any available family temporary disability leave benefits during their leave. Any paid time off or family temporary disability leave benefits will run concurrently with the unpaid leave. When applicable, time off under this policy will run concurrently with a leave of absence covered by the federal Family and Medical Leave Act or New Jersey Family Leave Act.  

The Company prohibits harassment, discrimination or retaliation against employees because they take or request leave in accordance with this policy or refuse to authorize the release of confidential information.  

#### Military Leave  

In addition to the military leave rights set forth in the National Handbook, New Jersey employees who leave full- or part-time employment to perform military service will be reinstated to their previous position, or one of like seniority status and pay, upon return. For purposes of this policy, “military service” means duty by any person in the active military service of the United States or National Guard active duty ordered by a Governor of a state. Employees (other than temporary employees) who leave their job to perform military service generally are eligible for reinstatement if they:  

- Receive a duly executed certificate of completion of military service;  
- Are still qualified to perform the duties of the position; and  
- Apply for reemployment within 90 days after being relieved from service.  

If the Company’s circumstances have changed and make it impossible or unreasonable to reinstate an employee who left to enter active military service in the Armed Forces of the United States or the Army or Air National Guard of New Jersey or any other state in time of war or emergency, the employee may request to be restored to another available position for which the he or she is able or qualified to perform the duties.  

Qualified employees (not in a temporary position) who take a temporary leave of up to three months to participate in assemblies or annual training or to attend any service schooling conducted by the Armed Forces of the United States are eligible for reemployment if they apply for employment within 10 days after completing service. The leave may not exceed three months in any four-year period.  

Time off under this policy is without pay. Employees will be considered as having been on furlough or a leave of absence during the leave and will be entitled to participate in insurance or other benefits offered by the Company in accordance with the established rules and practices regarding leaves of absence in effect at the time the employee is ordered to military service or training.  

Employees returning from leave under this policy will not be terminated without cause within one year following the date of reemployment.  

The Company will not discriminate or retaliate against an employee because he or she takes a leave of absence in accordance with this policy.  

#### Emergency Responder Leave  

Employees who serve as volunteer emergency responders will be permitted to take time off from work in order to respond to a fire or emergency call or to serve as a volunteer emergency responder during a declared state of emergency, provided they have complied with the Company’s notice requirements set forth below.  

For purposes of this policy, "volunteer emergency responder" means an active member in good standing of a volunteer fire company; a volunteer member of a first aid, rescue or ambulance squad; or a member of a county or municipal volunteer Office of Emergency Management (as long as the member’s official duties include responding to a fire or emergency call).  

Employees are required to provide notice at least one hour before they are scheduled to report to work, and upon returning to work must provide a copy of the incident report and a certification by the incident commander or other official or officer in charge.  

Time off under this policy will be without pay, except that exempt employees will receive pay when required by applicable law. Additionally, employees will be allowed to use any accrued available paid time off.  

The Company may deny requests for leave under this policy for certain employees that are essential to Company operations.  

### SAFETY AND SECURITY  

#### Smoke-Free Workplace  

The Company prohibits smoking, including the use of electronic smoking devices, in the workplace and within a reasonable distance from outside entrances where smoke could enter the building. Employees wishing to smoke must do so outside company facilities, away from entrances, during scheduled work breaks.  

Employees who observe other individuals smoking in the workplace have a right to object and should report the violation to their supervisor or another member of management. Employees will not be disciplined or retaliated against for reporting smoking that violates New Jersey law or this policy.  

Employees who violate this policy will be subject to disciplinary action up to and including termination of employment.  

#### Cell Phone Use/Texting While Driving  

As set forth in the National Handbook, the Company prohibits employees from using cellular phones for business reasons while driving, for any reason while driving for work-related purposes and while driving a company-owned vehicle. Employees should also be aware that talking, text messaging or sending an electronic message on a wireless telephone or electronic communication device without a hands-free function while driving is a violation of New Jersey law, in addition to being a violation of Company policy.  


## New York  


### GENERAL INFORMATION  

#### About This New York Supplement  

ConstructConnect is committed to workplace policies and practices that comply with federal, state and local laws. For this reason, New York employees will receive the Company’s national handbook (“National Handbook”) and the New York Supplement to the National Handbook (“New York Supplement”) (together, the “Employee Handbook”).  

The New York Supplement applies only to New York employees. It is intended as a resource containing specific provisions derived under New York law that apply to the employee’s employment. It should be read together with the National Handbook and, to the extent that the policies in the New York Supplement are different from or more generous than those in the National Handbook, the policies in the New York Supplement will apply.  

The New York Supplement is not intended to create a contract of continued employment or alter the at-will employment relationship. **Only the President/Owner of the Company or that person’s authorized representative has the authority to enter into an agreement that alters the at-will employment relationship and any such agreement must be in writing signed by the President/Owner of the Company or an authorized representative.**  

If employees have any questions about these policies, they should contact their Human Resources representative.  

### COMMITMENT TO DIVERSITY  

#### Equal Employment Opportunity  

As set forth in the National Handbook, ConstructConnect is committed to equal employment opportunity and to compliance with federal antidiscrimination laws. We also comply with New York law which prohibits discrimination and harassment against any employees, applicants for employment or interns, as well as contractors, subcontractors, vendors, consultants, or other individuals providing services in the workplace and their employees, based on race (including traits historically associated with race, such as hair texture and protective hair styles), color, religion, sex (including pregnancy, childbirth or related medical conditions and transgender status), gender identity, familial status, national origin or ancestry, citizenship, physical or mental disability (including gender dysphoria) genetic information (including predisposing genetic characteristics), age (18 and over), veteran status, military status, sexual orientation, marital status, familial status, certain arrest or conviction records and domestic violence victim status. The Company will not tolerate discrimination or harassment based upon an individual’s membership in one or more of these protected categories, known relationship or association with a member of one or more of these protected categories, or any other characteristic protected by applicable federal, state or local law.  

#### Reproductive Health Decision Making  

The Company will not discriminate or retaliate against an employee because of the employee’s or a dependent of the employee’s reproductive health decision making, including the use of particular drugs, devices or medical services.  The Company also will not, without prior informed written consent, access personal information regarding the reproductive health decision making of employees or their dependents, and will not require an employee to sign any document or waiver that purports to deny that employee the right to make their own reproductive health decisions.  

Employees subjected to unlawful discrimination or retaliation on the basis of reproductive health decision making can bring an action in court and may be entitled to certain remedies, including monetary and injunctive relief.  

Employees who feel they have been subjected to discrimination or retaliation on the basis of their reproductive health decision making or that of a dependent, or to any other violation of this policy, should contact their Human Resources representative.  

#### Discrimination on the Basis of Gender, Gender Identity or Transgender Status (New York City)  

The Company prohibits discrimination against and/or harassment of applicants, employees, and interns on the basis of their actual or perceived gender or actual or perceived status as an individual who is transgender, gender non-conforming or intersex.  For purposes of this policy, gender includes gender identity, self-image, appearance, behavior or expression. Harassment includes, but is not limited to, violence, threats of violence and similar conduct.  

The Company evaluates all requests for reasonable accommodation (including requests for medical leaves or schedule changes), changes to the terms and conditions of employment, program participation or use of a public accommodation in a non-discriminatory manner.  This includes, but is not limited to, treating leave requests for medical or heath care needs related to an individual’s gender identity in the same manner as requests related to other medical conditions.  

Employees who engage with the public as part of their job duties are required to do so in a respectful, non-discriminatory manner by respecting gender diversity and ensuring that members of the public are not subject to discrimination (including discrimination with respect to single-gender programs and facilities).  

##### Preferred Names, Titles and Pronouns  

The Company allows employees to self-identify their names and genders and will use an individual’s preferred name, gendered title (e.g., Mr./ Ms.) and pronoun (e.g., he/him/his; she/her/hers; they/them/theirs; or ze/hir).  Requests to be addressed by a certain name and/or pronoun do not require supporting documentation.  

If an employee is unsure what name, title or pronoun another individual prefers, that employee can ask the person how the person would like to be addressed.  

##### Facilities Designated as Single-Gender  

All employees have the right to use single-gender facilities, such as restrooms, consistent with their gender. To the extent possible, the Company will provide single-occupancy restrooms and/or private space within multi-user facilities for individuals with privacy concerns, but will not require use of a single-occupancy bathroom because an individual is transgender or gender non-conforming.  

##### Dress Code  

The Company’s dress code and grooming standards are gender neutral, meaning they do not differentiate or impose restrictions or requirements based on gender or sex.  

##### Reporting and Anti-Retaliation  

Employees with questions or concerns regarding their safety, gender discrimination and/or a request for a reasonable accommodation or who feel they have been subjected to discrimination or improperly denied an accommodation, should contact their Human Resources representative. The Company prohibits and does not tolerate retaliation against employees who report issues or concerns of gender discrimination pursuant to this policy in good faith.  

#### Sexual Harassment (New York)  

ConstructConnect is committed to providing a work environment that is free of unlawful sexual harassment.  The Company strictly prohibits sexual harassment by or against any individuals involved in our operations, including employees (regardless of position), applicants, interns (paid or unpaid), vendors, contractors, sub-contractors, consultants and any other third party involved in our operations.   If such harassment is committed in the workplace by someone not employed by the Company, the reporting and complaint procedure in this policy should still be followed. The workplace includes:  actual worksites, any setting in which work-related business is being conducted (whether during or after normal business hours), online and electronic interactions with company employees and third parties involved in our operations, company-sponsored events, and company owned/controlled property.  

##### Sexual Harassment Defined  

Sexual harassment is unwelcome verbal or physical behavior based upon a person’s gender/sex and includes unwanted verbal or physical sexual advances, requests for sexual favors or visual, verbal or physical conduct of a sexual nature when:  

- Submission to such conduct is made a term or condition of employment; or  
- Submission to, or rejection of, such conduct is used as a basis for employment decisions affecting the individual; or  
- Such conduct has the purpose or effect of unreasonably interfering with an individual's work performance or creating an intimidating, hostile or offensive working environment, even if the individual making the report is not the intended target of such conduct.  

The following is a non-exhaustive list of the types of conduct prohibited by this policy:  

- Unwanted sexual advances or propositions (including repeated and unwelcome requests for dates);   
- Offers of employment benefits in exchange for sexual favors;  
- Making or threatening reprisals after a negative response to sexual advances;  
- Visual conduct: leering, making sexual gestures, displaying of pornographic or sexually suggestive images, objects, pictures, cartoons, graffiti, posters or websites on computers, emails, cell phones, bulletin boards, etc.;  
- Verbal conduct: making or using sexist remarks or derogatory comments based on gender, innuendos, epithets, slurs, sexually explicit jokes, whistling, suggestive or insulting sounds or lewd or sexual comments about an individual’s appearance, body, dress, sexuality or sexual experience;   
- Verbal and/or written abuse of a sexual nature, graphic verbal and/or written sexually degrading commentary about an individual's body or dress, sexually suggestive or obscene letters, notes, invitations, emails, text messages, tweets or other social media postings;  
- Physical conduct: unwelcome or inappropriate touching of employees or customers, physical violence, intimidation, assault or impeding or blocking normal movements;  
- Hostile actions taken against an individual because of that individual’s sex, sexual orientation, gender identity or the status of being transgender, such as:  
	- Interfering with, destroying or damaging a person’s workstation, tools or equipment, or otherwise interfering with the individual’s ability to perform the job;  
	- Sabotaging an individual’s work; and  
	- Bullying, yelling, name-calling.  
- Retaliation for making reports or threatening to report sexual harassment.  

Sexual harassment can occur regardless of the gender of the person committing it or the person exposed to it. Harassment on the basis of an individual’s sexual orientation, self-identified gender, perceived gender, or transgender status are all forms of prohibited sexual harassment.  

Individuals who observe conduct that may violate this policy are encouraged, but not required, to communicate to the offending person that the conduct is offensive and unwelcome.  Individuals who observe any behavior directed at others that may violate this policy are encouraged to take reasonable action to defuse such behavior, if possible, such as intervening directly, alerting a supervisor or Human Resources to assist or making a report under this policy.  

##### Protection Against Retaliation  

Retaliation is prohibited against any person covered by this policy who, in good faith: makes a complaint of sexual harassment, either internally or with a government agency, using the complaint procedures described below; objects to, opposes or speaks out against sexual harassment; participates in a sexual harassment investigation;  encourages another person to report harassment; or files, testifies, assists or participates in any manner in any investigation, proceeding or hearing conducted by a governmental enforcement agency. Prohibited retaliation includes, but is not limited to, termination, demotion, suspension, failure to hire or consider for hire, failure to give equal consideration in making employment decisions, failure to make employment recommendations impartially, adversely affecting working conditions or otherwise denying any employment benefit. Retaliation is unlawful and a form of misconduct that will result in disciplinary action, up to and including termination of employment.  

Individuals who believe that they or any other individual have been subjected to retaliation should report this concern using the complaint procedure set forth below.  

##### Complaint Procedure  

Individuals who believe that they or another individual have been subjected to sexual harassment, should, as soon as possible, report it to their manager, equal employment opportunity officer, or any Human Resources Representative.  Employees are not required to make the report to their immediate supervisor, manager or person who has engaged in the complained of conduct. Reports of sexual harassment can be made verbally or in writing. To submit a complaint in writing, individuals can use the complaint form attached to this supplement but are not required to do so.  

After a report is received or the Company otherwise becomes aware of a possible violation of this policy, a fair, timely, thorough and objective investigation will be undertaken if needed and will reach reasonable conclusions based on the information collected. The Company will maintain confidentiality surrounding the investigation to the extent possible, consistent with a thorough and objective investigation, and to the extent permitted or required under applicable law.  Both the person(s) raising the complaint and the person(s) about whom the complaint was made will be permitted to provide information that may be relevant to the investigation. The Company also will gather information and speak with witnesses, as applicable. Once the investigation is completed and a determination is made, the complaining party will be advised that the investigation has been completed and may be informed of the resolution.  The individual about whom the complaint was made will be informed of the outcome and, if the Company determines that this policy has been violated, will be subject to disciplinary action. The Company expects all employees to fully cooperate with any investigation conducted by the Company into a complaint of sexual harassment.  

##### Supervisory Responsibilities  

All supervisors or managers who receive a complaint or information about suspected sexual harassment, observe behavior that may violate this policy or for any other reason suspect that sexual harassment is occurring, are required to report such suspected sexual harassment to Chief People Officer.  

In addition to being subject to discipline for engaging in sexually harassing conduct themselves, supervisors and managers will be subject to discipline (up to and including termination) for failing to report suspected sexual harassment or otherwise knowingly allowing sexual harassment to continue. Supervisors and managers will also be subject to discipline for engaging in prohibited retaliation.  

##### Discipline  

If the Company determines that this policy has been violated, including in the event that a manager knowingly allows the policy to be violated without reporting it, prompt remedial action will be taken, commensurate with the severity of the offense, up to and including termination of employment. Appropriate action will also be taken to deter any such conduct in the future.  

##### Good Faith Reporting  

The initiation of a good faith complaint of sexual harassment or retaliation will not be grounds for disciplinary or other retaliatory action, even if the allegations cannot be substantiated or the employee was mistaken about aspects of the complaint.  Any individual who makes a complaint that is demonstrated to be intentionally false may be subject to discipline, up to and including termination.  

##### Other Information  

Sexual harassment is illegal under the New York State Human Rights Law, Title VII of the federal Civil Rights Act of l964, and some local laws including the New York City Human Rights Law.  Employees may file a complaint with the federal Equal Employment Opportunity Commission, the New York State Division of Human Rights, the New York City Commission on Human Rights, another enforcement agency (if applicable) or in certain courts of law. Agencies accept and investigate charges of sexual harassment.  The Equal Employment Opportunity Commission has district, area and regional offices and may be contacted by visiting [EEOC](www.eeoc.gov), emailing info@eeoc.gov or by telephone at 1-800-669-4000 (TTY 1;800-669-6820).  The [New York State Division of Human Rights](www.dhr.ny.gov) may be contacted by visiting www.dhr.ny.gov, by telephone at 718-741-8400, or by mail to One Fordham Plaza, Fourth Floor, Bronx, New York 10458. The [New York City Commission on Human Rights](http://www.nyc.gov/html/cchr/html/home/home.shtml) can be contacted by visiting http://www.nyc.gov/html/cchr/html/home/home.shtml or by telephone at (212) 306-7450. Employees subjected to unlawful harassment may be entitled to certain remedies, including monetary damages, civil penalties, and injunctive relief (such as an order that certain action be taken or certain behavior stop).  

A Stop Sexual Harassment Act Fact Sheet is attached to this handbook supplement.  

#### Political Opinions  

The Company will not tolerate intimidation, threats or impeding the voting activities of employees to influence them to vote or refrain from voting for a particular candidate or proposition. Additionally, the Company will not threaten or attempt to influence the political opinions of its employees by placing any political material within an employee’s pay envelope.  

#### Pregnancy Accommodation  

Employees and applicants for employment in New York City may request a reasonable accommodation for pregnancy, childbirth and related medical conditions (including lactation).  The Company will provide a requested reasonable accommodation that would enable the employee or applicant to perform the essential functions of her job unless the accommodation would impose an undue hardship on the Company's business operations.  

Employees may be required to provide medical or other information that is necessary for the Company's consideration of a reasonable accommodation. Such medical information will be kept confidential.  

Employees or applicants for employment who have questions about this policy or who wish to request a reasonable accommodation under this policy should contact their Human Resources representative. Employees who need reasonable break time to express breast milk for their child should consult the Company’s Lactation Accommodation policy and can discuss those arrangements with their supervisor, a Human Resources representative.  Human Resources will communicate with the employee and engage in good faith in a cooperative dialogue (written and/or oral) concerning the employee’s accommodation needs.  At the conclusion of this dialogue, the Company will provide an employee who requested an accommodation and participated in the dialogue with a final written determination identifying any accommodation granted or denied.  The Company will not retaliate against or tolerate retaliation against employees who request an accommodation in accordance with this policy.  

#### Accommodation for Victims of Domestic Violence, Sex Offenses or Stalking (New York City)  

The Company will provide reasonable accommodations to employees working in New York City who are victims of domestic violence, sex offenses or stalking, unless providing the accommodation would cause an undue hardship on the Company’s business.  

The Company may request that an employee provide proof that they are a victim of domestic violence, sex offenses or stalking, such as documentation from a victim’s services agency, lawyer, clergy, medical provider, court or the police.  

Employees who wish to request an accommodation under this policy should contact their Human Resources representative. Human Resources will communicate with the employee and engage in good faith in a cooperative dialogue (written and/or oral) concerning the employee’s accommodation needs.  At the conclusion of this dialogue, the Company will provide an employee who requested an accommodation and participated in the dialogue with a final written determination identifying any accommodation granted or denied.  

The Company will not refuse to hire, terminate or discriminate against any employee because the employee is, or is perceived to be, a victim of domestic violence, sex offenses or stalking and will not retaliate against any employee who requests an accommodation in accordance with this policy.  

#### Reasonable Accommodations for Victims of Domestic Violence, Sexual Assault or Stalking (Westchester County)  

The Company will provide reasonable accommodations for employees and applicants who are the victim of domestic violence, sexual assault or stalking, unless providing the accommodation would impose an undue hardship on the Company's business.  

Reasonable accommodations may include, but are not limited to, modified work schedules, acquisition or modification of equipment, job restructuring and forms of protection or security measures.  

In order to assist the Company in assessing the situation or if otherwise necessary to provide a reasonable accommodation, the Company may seek certification establishing an employee’s status as a victim of domestic violence, sexual abuse or stalking.  Acceptable documentation can include a police report, court order or documentation from a medical professional, domestic violence advocate, health care provider, member of the clergy or counselor. The employee must provide a copy of the requested certification within a reasonable period after the request is made.  

The Company will maintain information received from victims of domestic violence, stalking or sexual abuse in the strictest confidence and will disclose such information only if necessary to provide a reasonable accommodation or otherwise required by applicable law.  

The Company will not refuse to hire an applicant, discharge an employee or otherwise discriminate against an individual because of the person’s actual or perceived status as a victim of domestic violence, sexual abuse, or stalking or because the individual requests or uses an accommodation in accordance with this policy.  

Employees who have questions about this policy or who wish to request a reasonable accommodation under this policy should contact their Human Resources representative.  

#### Cooperative Dialogue about Accommodation Needs (New York City)  

ConstructConnect will make reasonable accommodations for employees who seek them for reasons related to pregnancy, religion, disability or status as a victim of domestic violence, sex offenses or stalking—when those accommodations are possible and in accordance with applicable law and Company policy.  

As is set forth more fully in those policies, the Company will engage in good faith in a dialogue (written or oral) concerning an employee’s accommodation needs and potential accommodations for those needs.  The Company will engage in this cooperative dialogue within a reasonable time after an employee requests an accommodation or the Company becomes aware of the possible need for an accommodation.  At the conclusion of the dialogue, the Company will provide the participating employee with a written final determination identifying any accommodation granted or denied.  The Company will engage (or attempt to engage) in this cooperative dialogue before ever reaching the conclusion that a reasonable accommodation cannot be found. The Company will not retaliate against or tolerate retaliation against employees who request accommodations in accordance with Company policy.  

Employees who wish to request an accommodation should contact Human Resources.  

### GENERAL EMPLOYMENT PRACTICES  

#### Requests for Schedule Changes (New York City)  

All employees (whether full-time, part-time or temporary) who work 80 or more hours per calendar year in New York City and who have worked for the Company for at least 120 days are eligible for two temporary schedule changes per year for certain personal events.  

##### Temporary Schedule Changes for Qualifying Personal Events  

Upon request, the Company will grant two temporary schedule changes per year for up to one (i) business day per request. or, with Company approval, two business days for one request. The Company may, in its discretion, grant one temporary schedule change that impacts two business days, in which case the employee will not be entitled to a second temporary schedule change in the same calendar year.  The Company’s calendar year is January 1 through December 31.  

For purposes of this policy, a temporary schedule change is a limited alteration to an employee’s usual schedule, including hours, times or work location.  Alterations may include, but are not limited to: (i) using available paid time off; (ii) working remotely; (iii) swapping or shifting work hours; or (iv) using short term unpaid leave.  

The Company may require employees to take unpaid leave in lieu of the employee’s requested temporary schedule change and the unpaid leave will be counted as one of the employee’s allotted schedule changes.  Leave granted as a temporary schedule change will generally be unpaid for non-exempt employees.  However, employees are allowed, but not required, to use any available, accrued paid leave.  

Employees can request temporary schedule changes for the following personal events:  

- To care for a minor child for whom the employee provides direct and ongoing care;  
- To care for a disabled individual (a “care recipient”) who is the employee’s family member or resides in the employee’s household and for whom the employee provides direct and ongoing care to meet the needs of daily living;  
- To attend a legal proceeding or hearing for subsistence benefits to which the employee, a family member or care recipient is a party; or   
- For reasons specified in the Company’s Paid Safe and Sick Time (New York City) policy.  


Eligible family members include an employee's current or former spouse or registered domestic partner; parent; child (including biological, adopted, step or foster child, a legal ward or a child of an employee standing in loco parentis); sibling (including a biological, half, adopted or step-sibling); parent or child of an employee’s spouse or registered domestic partner; grandchild; grandparent; an individual related to the employee by blood; and an individual whose close association with the employee is the equivalent of a family relationship.  

##### Requesting Temporary Scheduling Changes  

Employees who wish to request temporary schedule changes under this policy must notify Human Resources as soon as they are aware of the need for a temporary schedule change.  This initial notification can be made orally, but must indicate that the requested change is due to a personal event and must describe the requested temporary schedule change, unless the employee is only seeking leave without pay.  The Company will respond to this initial request for a temporary schedule change as soon as possible.  

If an employee’s initial request was not in writing, the employee must, as soon as practicable and no later than the second business day after returning to work following the conclusion of the temporary schedule change, also submit the schedule change request in writing to Human Resources, indicating the date for which the change was requested and that it was due to the employee’s personal event.  

##### Other Schedule Change Requests  

Employees are also allowed to request schedule changes (i.e., changes to the times, days and/or locations they are expected to work) in addition to those temporary schedule changes described above.  The Company will, in its discretion, grant or deny the request.  Employees who wish to make additional schedule change requests should follow the procedure described above.  

##### Effect on Other Rights and Policies  

The Company may provide other types of accommodation and other forms of leave under certain federal, state and municipal laws. In certain situations, time off allowed under this policy may run at the same time as leave available under another policy or under another federal, state or municipal law, provided eligibility requirements are met. However, unpaid leave provided as one or both of the two temporary schedule changes described in this policy is in addition to and will not run concurrently with leave provided under the Company’s New York City Paid Sick and Safe Time policy.  Employees are not required to exhaust their New York City Paid Sick and Safe Time before requesting a temporary schedule change in accordance with this policy.  

The Company is committed to complying with all applicable laws. Employees should contact Human Resources for information about other federal, state and municipal leave rights or workplace accommodations.  

##### Retaliation  

The Company prohibits retaliation against an employee for requesting a schedule change, filing a complaint, communicating with others about the law, participating in an investigation or proceeding regarding an alleged violation of the law, mistakenly invoking rights under the law or otherwise exercising their rights under the law, even if the employee does not specifically reference the Temporary Schedule Change Law and even if the employee is not entitled to a schedule change.  

### PAY PRACTICES  

#### Meal Breaks  

Employees  working at least a six-hour workday, which extends over the noon meal period (11 a.m. to 2 p.m.), are entitled to a 30-minute meal break to be taken between 11 a.m. and 2 p.m. Employees who start their workday before 11 a.m. and continue after 7 p.m. are entitled to a 30-minute noon meal break and an additional 20-minute break between 5 p.m. and 7 p.m.  

Employees who work more than six hours in their workday starting between the hours of 1 p.m. and 6 a.m. are entitled to a meal break of at least 45 minutes in the middle of their workday.  

An uninterrupted meal break lasting 30 minutes or more will be unpaid for nonexempt employees.  

Employees may not take a shorter meal break or skip a meal break to leave early.  

#### Lactation Accommodation  

The Company will provide a reasonable amount of break time to accommodate an employee desiring to express breast milk for the employee’s child. Generally, a reasonable amount of break time for purposes of this policy will be at least 20 minutes in every three hour period, if requested by the employee. Longer break times will be provided when the room designated for expression of breast milk is not in close proximity to the employee’s work station. The Company will provide this break time for up to three years following the birth of a child.  

Nursing mothers can elect to take time to express breast milk during their regularly scheduled meal and rest breaks. If the break time cannot run concurrently with the meal and rest breaks already provided to the employee, the break time will be unpaid for nonexempt employees. Where additional breaks are required, employees should work with their supervisor regarding scheduling. A nonexempt employee can elect to work before or after her normal shift to make up the amount of time used during unpaid break time for expression of breast milk, so long as the additional time requested falls within the Company’s normal work hours.  

Employees are required to provide reasonable notice to the Company that they intend to take breaks for expressing breast milk upon returning to work. The Company will make reasonable efforts to provide employees with the use of a private location, other than a toilet stall, in close proximity to their work area, for the employee to express breast milk. Employees should discuss with their supervisor, a Human Resources representative the location to express their breast milk and for storage of expressed milk and to make any other arrangements under this policy.  

The Company will not demote, terminate or otherwise take adverse action against an employee who requests or makes use of the accommodations and break time described in this policy.  

#### Lactation Accommodation (New York City)  

The Company will provide a reasonable amount of break time to accommodate an employee desiring to express breast milk for the employee’s child. The Company will provide this break time for up to three years following the birth of a child.  

Nursing mothers can elect to take time to express breast milk during their regularly scheduled meal and rest breaks. If the break time cannot run concurrently with the meal and rest breaks already provided to the employee, the break time will be unpaid for nonexempt employees. Where additional breaks are required, employees should work with their supervisor regarding scheduling. A nonexempt employee can elect to work before or after her normal shift to make up the amount of time used during unpaid break time for expression of breast milk, so long as the additional time requested falls within the Company’s normal work hours.  

Employees are required to provide reasonable notice to the Company that they intend to take breaks for expressing breast milk upon returning to work.  

##### Lactation Room  

Employees have the right to request a lactation room for purposes of expressing breast milk. The lactation room will be a sanitary place, other than a restroom, that is shielded from view, free from intrusion and in reasonable proximity to the employee’s work area. The lactation room will include an electrical outlet, a chair, a surface on which to place a breast pump and other personal items and nearby access to running water. A refrigerator suitable for breast milk storage will also be available in reasonable proximity to the employee’s work area.  

To request use of a lactation room, employees should complete a Lactation Room Request Form and submit the form to Human Resources. The Company will respond to the employee’s request within a reasonable amount of time. Employees should contact their supervisor or Human Resources with any follow-up inquiries.  

A room identified for use as a lactation room may also be used for other purposes. However, during times when an employee is using the room as a lactation room, that will be its sole function. When two or more employees need to use the room for lactation purposes or in connection with other accommodations, they should work with their supervisor to schedule room usage cooperatively and in a way that accommodates all affected employees. Employees who have questions or concerns related to lactation room scheduling conflicts can also contact Human Resources.  

If providing the requested lactation room will place an undue hardship on the Company’s operations, the Company will engage in good faith in a cooperative dialogue with the employee concerning the employee’s accommodation needs.  

The Company will not demote, terminate or otherwise take adverse action or retaliate against an employee who requests or makes use of the accommodations and break time described in this policy.  

##### Discussion of Wages  

No employee is prohibited from inquiring about, discussing or disclosing his or her wages or the wages of another employee, if voluntarily disclosed by that employee. Employees are not required to disclose their wages to anyone.  

This policy does not apply to disclosure of other employees’ wage information by an employee who has access to such information solely as part of his or her essential job functions and who, while acting on behalf of the Company, makes unauthorized disclosure of that information. Company representatives may disclose employees’ wages in response to a complaint or charge, or in furtherance of an investigation, proceeding, hearing or action under state law.  

### TIME OFF AND LEAVES OF ABSENCE  

#### Jury Duty Leave  

The Company encourages all employees to fulfill their civic responsibilities and to respond to jury service summons or subpoenas, attend court for prospective jury service or serve as a juror. Under no circumstances will employees be terminated, threatened, coerced, or penalized because they request or take leave provided they notify Human Resources prior to their service.  

Employees must provide their supervisor with notice of any jury summons or subpoena within a reasonable amount of time after receipt and before their appearance is required. Verification from the court clerk of having served may also be required.  

Leave under this policy will be unpaid, except that employees will be paid up to $40.00 of their pay for the first three days of jury duty and exempt employees will not incur any reduction in pay for a partial week's absence due to jury duty.  

#### Time Off To Vote  

The Company encourages all employees to fulfill their civic responsibilities and to vote in public elections.  

The Company provides employees who are registered voters with up to two hours of paid time off to vote, if they do not have sufficient time outside of their scheduled working hours in which to vote. Additional time off will be without pay, except that exempt employees may receive pay, as required by applicable law. Four consecutive hours between the opening of the polls and the start of the employee's work shift or between the end of the employee's work shift and the closing of the polls will be considered sufficient time outside of work to vote.  

Time off to vote will be provided only at the beginning or end of the employee’s shift, unless the Company and the employee mutually agree to different timing.  Employees intending to take leave to vote must inform their supervisor at least two, but not more than 10, working days prior to Election Day.  The employee’s supervisor will designate when the leave should be taken (e.g., at the beginning or end of the shift). 
Proof of having voted may be required.  

#### Mandatory Time Off/Day of Rest  

ConstructConnect will provide employees with at least 24 consecutive hours of rest in any calendar week.  

#### Volunteer Emergency Responder Leave  

Eligible employees will be allowed time off from work to perform duties as a volunteer firefighter or member of a volunteer ambulance service during a declared state of emergency, unless providing the leave would impose an undue hardship on the company’s business operations.  

To be eligible for leave under this policy, employees must have previously provided the Company with written documentation from the volunteer fire department or ambulance service notifying the Company of the employee’s status as a volunteer firefighter or volunteer ambulance service member, and the employee’s volunteer duties must be related to the declared emergency.  

Leave under this policy will be unpaid except that employees may elect to use any other applicable paid leave to which they are entitled.  

The Company may request certification of the need for leave in the form of a notarized statement from the head of the fire department or ambulance service certifying the time period that the employee’s volunteer services were required.  

#### Adoptive Parents Leave  

Employees who are adoptive parents will be permitted to take leave under the same terms as leave provided to biological parents, for the adoption of a child upon the start of the parent-child relationship. Leave will only be granted to employees who adopt children of preschool age or younger, or who adopt children under the age of 18 who are considered “hard to place” or handicapped under New York law.    

For further information or to request leave under this policy, contact a Human Resources representative.  

#### Military Leave  

In addition to the military leave rights set forth in the National Handbook, regular full-time and part-time employees will be granted time off from work for military training or active duty in accordance with New York law. Employees returning from active duty or military training will be reinstated to the same position, or to a position of like seniority, status and pay, unless the Company's circumstances have changed such that it is impossible or unreasonable to do so. To be eligible for reinstatement, employees must:  

- Receive a certificate of completion duly executed by an officer of the applicable force or militia;  
- Be qualified to perform their former job duties; and  
- Apply for reemployment within 90 days of discharge from duty, except that employees returning from training or school must reapply within 10 days and employees returning from initial full-time training duty or initial active duty training with or in the United States Armed Forces must apply for reemployment within 60 days of the end of such training.  

Any employee who is reinstated to his or her previous position after training or active duty will not be terminated without cause in the year following reinstatement.  

#### Military Spouse Leave  

Employees who work an average of 20 or more hours per week, who are the spouse of a member of the United States armed forces, national guard, or reserves who has been deployed during a period of military conflict (to a combat zone of operations or a combat theater) may be allowed up to 10 days of unpaid leave to use when their spouse is on leave. Employees who seek leave under this policy may be required to provide documentation to support their request.  

For purposes of this policy, “period of military conflict” means a period of war declared by the United States Congress, or in which a member of a reserve component of the armed forces is ordered to active duty.  

The Company will not retaliate or tolerate retaliation or harassment against employees for requesting or taking military spouse leave.  

#### Blood Donation Leave  

Employees who work an average of 20 or more hours per week will be granted an unpaid leave of absence if they seek to donate blood. Eligible employees will be granted up to three hours of leave per calendar year to donate blood off-site or will be provided with an opportunity to donate blood on-site during work hours (such as through a blood drive) at a convenient time and place. Time spent donating blood on-site will be paid. Time spent donating blood off-site will be unpaid for nonexempt employees.  

Except in emergencies, employees who seek leave under this policy must give reasonable notice to their supervisor of at least three working days prior to taking leave for blood donation off-premises and two working days for on-site and other alternative blood donation drives. Employees must also provide documentation to their supervisor immediately after such leave is taken.  

The Company will not retaliate or tolerate retaliation against an employee for requesting or taking blood donation leave.  

#### Bone Marrow Donation Leave  

Employees who work an average of 20 or more hours per week will be granted a leave of absence if they seek to undergo a medical procedure to donate bone marrow. The total length of the leave for each employee will be determined by a physician, but may not be longer than 24 work hours without company approval. An employee who seeks leave under this policy must provide verification from a physician of the purpose and length of the leave. Leave under this policy will be unpaid except that exempt employees will receive pay when required by applicable federal or state law.  

The Company will not retaliate or tolerate retaliation against an employee for requesting or taking bone marrow donation leave.  

#### Crime Victim Leave  

Eligible employees may take time off from work to comply with a subpoena to (1) testify in a criminal proceeding (including time off to consult with the district attorney); (2) give a statement at a sentencing proceeding; (3) give a victim impact statement at a pre-sentencing proceeding; or (4) give a statement at a parole board hearing.  

Time off under this policy is unpaid except that exempt employees will not incur any reduction in pay for a partial week's absence for leave under this policy.  

##### Leave Eligibility  

Employees are eligible for time off under this policy if they are:  

- The victim of the crime at issue in the proceedings;  
- The victim’s next of kin;  
- The victim’s representative (a person who represents or stands in the place of another person, including an agent, attorney, guardian, conservator, executor, heir or parent of a minor) if the victim is deceased as a result of the offense;  
- A good Samaritan (someone who acts in good faith to: (1) apprehend a person who has committed a crime in his or her presence; (2) prevent a crime or an attempted crime from occurring: or (3) aid a law enforcement officer in effecting an arrest); or  
- Pursuing an application or the enforcement of an order of protection as provided under relevant law.  

##### Notice and Certification  

Employees must notify their supervisor of the need to take a leave under this policy no later than the day before the absence. In addition, employees must provide their supervisor with verification of their service upon request.  

##### No Retaliation  

The Company will not retaliate, or tolerate retaliation, against any employee who seeks or obtains leave under this policy.  

#### Leave for Victims of Domestic Violence  

The Company will reasonably accommodate employees who are the victim of domestic violence who need a reasonable amount of time off for the following reasons, unless providing such accommodation would result in an undue hardship:  

- Seek medical attention for injuries caused by domestic violence, including for a child who is a victim of domestic violence;  
- Obtain services from a domestic violence shelter, program, or rape crises center;  
- Obtain psychological counseling related to domestic violence incidents, including for a child who is a victim of domestic violence;  
- Participate in safety planning or other actions to increase safety from future incidents of domestic violence; or  
- Obtain legal services, assist in the prosecution of an offense or appear in court in relation to an incident of domestic violence.  

Employees must give the Company reasonable advance notice of their intention to take leave for this purpose unless such advance notice is not feasible.  employee who cannot give reasonable advance notice must provide certification supporting the need for leave within a reasonable time after the absence.  Acceptable forms of certification include:  

- A police report indicating the employee or his or her child is a victim of domestic violence;  
- A court order protecting or separating the employee or his or her child from the perpetrator of domestic violence;  
- Other evidence from the court or prosecuting attorney that the employee appeared in court; or  
- Documentation from a medical professional, domestic violence advocate, health care provider, or counselor that the employee or his or her child was undergoing counseling or treatment for physical or mental injuries or abuse resulting from an act of domestic violence.  

When taking leave under this policy, an employee must use any available paid leave. Otherwise, leave will be unpaid. During the leave, the Company will maintain any health insurance coverage being provided in the same manner as if the employee had not taken leave.  

Except as otherwise required by law, the Company will maintain the confidentiality of any information regarding an employee’s status as a victim of domestic violence.  

The Company will not discriminate or retaliate against an employee because the employee is a victim of domestic violence or requests leave in accordance with this policy.  

#### Bereavement Leave  

For New York employees, the Company’s bereavement leave policy is expanded to provide leave for the death of an employee’s same-sex committed partner or the child, parent or other relative of the committed partner. "Same-sex committed partners" are defined as those who are financially and emotionally interdependent in a manner commonly presumed of spouses.  

#### New York Paid Family Leave Benefits  

In accordance with the New York Paid Family Leave Benefits Law (“PFLBL”), eligible employees are entitled to a leave of absence to care for a family member with a serious health condition, bond with a new child, or assist with obligations that arise when a spouse, domestic partner, child or parent is called into active military service. Employees are also eligible to receive partial wage replacement benefits during the leave through a state-mandated Paid Family Leave Benefits program.  

##### Employee Eligibility  

New York employees who work 20 hours or more per week for 26 weeks are eligible for Paid Family Leave (PFL) under the PFLBL.  Employees who work fewer than 20 hours per week are eligible for PFL after completing 175 days of employment.  

##### Length of Paid Family Leave  

Employees will be eligible for up to a maximum of 12 weeks of PFL in a consecutive 52-week period. Employees are limited to the maximum amount of PFL in a consecutive 52-week period, even if they begin employment with a different covered employer during that 52-week period.  

##### Qualifying Reasons for Leave  

PFL may be taken for the following reasons:  

- To provide physical or psychological care (including, for example, emotional support, visitation, assistance in treatment, transportation, arranging for a change in care, assistance with essential daily living matters and personal attendant services) for a family member because of the family member’s serious health condition;   
- To bond with the employee’s child during the first 12 months after the child’s birth, or during the first 12 months after placement of the child for adoption or foster care; or   
- For a “qualifying exigency,” as defined under the federal Family and Medical Leave Act (“FMLA”), arising from the active duty military service (or notification of an impending call to active duty) of an employee’s spouse, domestic partner, child or parent.  

For purposes of this policy, a “family member” includes a child, parent, grandparent, grandchild, spouse or domestic partner. A “child” includes a biological, adopted or foster child, stepchild, the child of a domestic partner, a legal ward or someone to whom the eligible employee stands in loco parentis, meaning in the place of a parent.  A “parent” includes a biological parent, foster parent, adoptive parent, stepparent, parent-in-law, parent of a domestic partner, legal guardian of the employee or an individual who stood in loco parentis to the employee when the employee was a child.  

PFL may not be taken for the employee’s own disability or health condition. Employees taking PFL to provide care to a family member with a serious health condition must be present at the same location as the family member or engaged in reasonable travel related to providing care during the majority of the employment period for which the employee takes leave.  

The Company is not required to provide PFL to two employees at the same time to care for the same family member.  If time off is provided to both employees, the PFL taken by each employee will be counted towards that employee’s PFL entitlement.  

##### Intermittent Leave  

Employees can take PFL on an intermittent basis. Employees seeking intermittent leave must notify the Company [include if not self-insured: and the insurance carrier] of the schedule for intermittent leave.  

Employees taking PFL in weekly increments will be eligible for the maximum number of weeks of leave in any 52 consecutive week period.  Employees can take PFL in daily increments.  The number of days of PFL available will be based on the average number of days the employee works per week.  For example, an employee who works an average of three days per week will receive the equivalent of three days per week for twelve weeks, up to a maximum of 36 days.  

##### Wage Replacement Benefits  

Eligible employees can receive wage replacement benefits through a state-mandated PFL benefits program for PFL.  PFL benefits are administered by [SELECT: a third party provider, the New York State Insurance Fund or the Company].

The PFL benefit amount is 67% of an employee’s average weekly wage or the State’s average weekly rate, whichever is lower.  The State’s average weekly wage is determined and periodically adjusted by the State of New York.  

An employee cannot receive both New York state disability benefits and PFL benefits for the same period of time.  An eligible employee may opt to receive both disability and PFL benefits during a post-partum/baby bonding period, but may not receive both benefits at the same time.  In addition, an employee who is eligible for both disability and PFL benefits during the same 52-week period cannot receive more than 26 total weeks of disability and PFL benefits combined during that time period.  

##### Payroll Deductions  

PFL benefits are funded by employee contributions made through payroll deductions. The amount of an employee’s contribution depends on the employee’s average weekly wage.  The maximum deduction amount will be adjusted periodically by the State of New York. The amount of any deduction taken will be reflected on an employee’s paystub.  

Employees who are not eligible for PFL benefits because they are not scheduled to meet the eligibility criteria regarding weeks or days worked can sign a waiver of benefits that relieves them from making the PFL benefits contribution. Ineligible employees who wish to complete such a waiver should contact Human Resources.  If, after signing the waiver, an employee’s schedule changes such that the employee is scheduled to meet the eligibility requirements, the waiver will be deemed revoked within eight weeks of the schedule change. Once the waiver is revoked, the employee will be required to make PFL contributions, including a retroactive amount that covers contributions since the time of hire.  

##### Requesting Leave  

Employees seeking PFL must provide at least 30 days advance notice to the Company when the need for leave is foreseeable.  If the need for leave is unforeseeable, employees must provide notice as soon as practicable.  The notice should include the timing and duration of the leave and identify the type of family leave needed. Failure to provide timely notice may result in a partial denial or delay in an employee’s receipt of PFL.  Employees must advise the Company as soon as practicable if the dates of a scheduled leave change or are extended.  

Employees seeking PFL benefits will also be required to submit a Request for Paid Family Leave Form and required certifications.  Employees must submit proof of the need for PFL within 30 days of the commencement of leave.  The Company will complete its portion of the Request for Paid Family Leave Form and return it to the employee within three business days.  

If the dates for PFL, including any intermittent use of PFL, are not specified on the Request for Paid Family Leave, payment of benefits may be withheld until the information is provided.  An employee must request payment for a previously unspecified day of PFL within thirty days of the leave.  

Employees are also required to provide additional documentation supporting the need for leave.  Required documentation may include, for example, a birth certificate or adoption paperwork for bonding leave or a medial certification from a healthcare provider for leave to care for a family member’s serious health condition.  

##### Benefits  

The Company will continue making contributions to employee group health benefits during the leave on the same terms as if the employee had continued to actively work. This means that if employees want their benefits coverage to continue during the PFL, they must also continue to make the same premium payments that they are now required to make for themselves or their dependents.  Failure to make timely payments may result in termination of health insurance coverage.  

##### Effect on Other Rights and Paid Leave  

When leave qualifies as protected family leave under both the PFLBL and the FMLA, leave entitlements under both laws will run concurrently.   Employees will not receive more than the maximum family leave available under either the PFLBL or the FMLA, as applicable.  

Where time off qualifies as both PFL and FMLA leave, employees are required to use available vacation, paid sick time and other available paid time off in accordance with the provisions of the FMLA and the Company’s FMLA policy.  When PFL does not qualify as FMLA leave, employees can choose, but are not required, to use available vacation, paid sick leave or other paid time off to receive full salary or wages during some or all of the PFL.  

##### Return from Leave  

Under most circumstances, employees who return to work as scheduled at the end of PFL will be reinstated to the same position they held at the time of the leave or to a comparable position with comparable benefits, pay and other terms and conditions of employment. Employees are not entitled under the PFLBL to accrue employment benefits or obtain seniority during any period of PFL, nor are they entitled to any right, benefit or position to which they would have been entitled absent the PFL.  

##### Fraudulent Use of PFL Prohibited  

Employees who fraudulently obtain PFL from the Company are not protected by the PFL’s job restoration or maintenance of health benefits provisions and may be subject to disciplinary action, up to and including termination of employment.  

##### Protected Rights  

The Company takes its PFL obligations very seriously and will not interfere, restrain or deny the exercise of any right protected under the PFLBL. The Company will not terminate or otherwise discriminate against any individual because that person uses or attempts to use PFL. If an employee believes that his or her PFLBL rights have been violated in any way, they should immediately report the matter to Human Resources.  

Employees may also contact Human Resources with questions.  

#### Paid Sick and Safe Leave (New York)  

The Company provides eligible employees with paid sick and safe leave in accordance with the requirements of New York’s Sick and Safe Leave Law (“NYSSLL”).  

##### Eligibility  

New York employees are eligible to accrue paid sick and safe leave.  

##### Accrual and Use of Sick and Safe Leave  

Eligible employees will begin to accrue paid sick and safe leave on September 30, 2020 or their date of hire, whichever occurs later. Sick and safe leave accrues at a rate of one hour for every 30 hours worked, up to a maximum accrual of 56 hours in a single calendar year. Sick and safe leave can be used beginning on January 1, 2021 and, after January 1, 2021, can be used as it accrues.  

For each use of paid sick and safe leave, employees must take a minimum of four hours of leave per day. Eligible employees may use up to 56 hours of paid sick and safe leave in any calendar year.  

Upon returning to work following sick and safe leave taken in accordance with this policy, employees will be restored to the position they held before taking leave, with the same pay and other terms and conditions of employment.  

##### Reasons Sick and Safe Leave May be Used  

Eligible employees may use paid sick and safe leave for the following reasons:  

- For an eligible employee's mental or physical illness, injury or health condition or need for medical diagnosis, care or treatment of a mental or physical illness, injury or health condition or need for preventive medical care;  
- To care for an eligible family member who needs medical diagnosis, care or treatment of a mental or physical illness, injury or health condition or who needs preventive medical care; or  
- If the employee or an eligible family member is the victim of domestic violence, a family offense matter, sexual offense, stalking or human trafficking and time off is needed to:   
- Obtain services from a domestic violence shelter, rape crisis center or other services program;  
- Participate in safety planning, temporarily or permanently relocate, or take other actions to increase the safety of the employee or family member;  
- Meet with an attorney or other social service provider to obtain information and advice on, and prepare for or participate in, any criminal or civil proceeding;  
- File a complaint or domestic incident report with law enforcement;  
- Meet with a district attorney’s office;  
- Enroll children in a new school; or  
- Take other actions necessary to ensure the health or safety of the employee or family member or to protect those who associate or work with the employee.  

Eligible family members include an employee's spouse or domestic partner; parent, including a biological, foster, step- or adoptive parent, a parent-in-law or parent of a domestic partner, a legal guardian or a person who stood in loco parentis when the employee was a minor child; child or child of a domestic partner, including a biological, adopted or foster child, a stepchild, a legal ward or a child of an employee standing in loco parentis; sibling; grandchild; or grandparent.  

##### Requesting Sick and Safe Time/Documentation  

To provide notice of the need to use sick and safe leave, employees should contact their Human Resources representative.  

In accordance with the NYSSLL, the Company does not require, as a condition of providing sick and safe leave, the disclosure of: confidential information relating to a mental or physical illness, injury or health condition of an employee or employee’s family member; or information relating to absence from work due to domestic violence, a sexual offense, stalking or human trafficking.  

##### Rate of Pay  

Sick and safe leave is paid at the employee’s regular rate of pay or the applicable state minimum wage rate, whichever is greater.  

##### Leave Carryover  

Accrued, unused paid sick and safe leave can be carried over from year to year. However, employees may not use more than 56 hours of sick and safe leave in a calendar year.  

The Company does not offer pay in lieu of actual sick and safe leave.  

##### Effect on Other Rights and Policies  

The Company may provide other forms of leave for employees to care for medical conditions or for issues related to domestic violence, stalking or sex offenses under certain federal, state and municipal laws. In certain situations leave under this policy may run at the same time as leave available under another federal, state or local law, provided eligibility requirements for that law are met. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other federal, state and municipal medical, victim or family leave rights.  

##### Separation from Employment  

Compensation for accrued and unused paid sick and safe leave is not provided upon separation from employment for any reason.  

##### Retaliation  

Employees have the right to request and use sick and safe leave in a manner consistent with the NYSSLL. The Company will not discriminate or retaliate, or tolerate discrimination or retaliation, against any employee who seeks or obtains sick and safe leave under this policy or who otherwise exercises their rights under the NYSSLL.  

#### Paid Sick and Safe Time (New York City)  

The Company provides eligible employees with paid sick and safe time in accordance with the requirements of the New York City Earned Sick and Safe Time Act (ESSTA).  

##### Eligibility  

All employees (whether full-time, part-time, or temporary) who work in New York City are eligible to accrue paid sick and safe time. The Company's calendar year starts on January 1 of each year.  

##### Accrual of Sick and Safe Time  

Eligible employees will begin to accrue paid sick and safe time on the employee's date of hire. Sick and safe time is accrued at a rate of one hour for every 30 hours worked in New York City, up to a maximum accrual of 56 hours in a single calendar year. Time off for sick, vacation or other paid time off is not included in actual hours worked. Salaried exempt employees will be assumed to work 40 hours in a week unless the employee's regular work week is less than forty 40 hours, in which case sick and safe time accrues based upon that regular workweek. Sick and safe time can be used as it accrues.  

Paid sick and safe  time may be used in an initial increment of four hours and then in half-hour increments thereafter. Eligible employees may use up to 56 hours of paid sick and safe time in any calendar year.  

##### Reasons Sick and Safe Time May be Used  

Eligible employees may use paid sick and safe time for the following reasons:  

- For an eligible employee's mental or physical illness, injury or health condition or need for medical diagnosis, care or treatment of a mental or physical illness, injury or health condition or need for preventive medical care (e.g., screenings, checkups, patient counseling to prevent health problems);  
- To care for an eligible family member who needs medical diagnosis, care or treatment of a mental or physical illness, injury or health condition or who needs preventive medical care; or   
- If the employee’s workplace is closed by order of a public official due to a public health emergency, or the employee needs to care for a child whose school or childcare provider has been closed by order of a public official due to a public health emergency. Such emergency must be declared by the New York City Mayor's office or the New York City Commissioner of Health.   
- If the employee or an eligible family member is the victim of  domestic violence, a family offense matter, sexual offense, stalking or human trafficking and time off is needed to:   
	- Obtain services from a domestic violence shelter, rape crisis center or other shelter or services program;  
	- Participate in safety planning, temporarily or permanently relocate, or take other actions to increase the safety of the employee or family member;  
	- Meet with a civil attorney or other social service provider to obtain information and advice on, and prepare for or participate in, any criminal or civil proceeding, including but not limited to matters related to a family offense matter, sexual offense, stalking, human trafficking, custody, visitation, matrimonial issues, orders of protection, immigration, housing, discrimination in employment, housing or consumer credit;  
	- File a complaint or domestic incident report with law enforcement;  
	- Meet with a district attorney’s office;  
	- Enroll children in a new school; or  
	- Take other actions necessary to maintain, improve, or restore the physical, psychological, or economic health or safety of the employee or family member or to protect those who associate or work with the employee.  
A “family offense matter” is actual or threatened disorderly conduct, harassment, aggravated harassment, sexual misconduct, forcible touching, sexual abuse, stalking, criminal mischief, menacing, reckless endangerment, strangulation, criminal obstruction of breathing or blood circulation, assault, identity theft, coercion or grand larceny, between spouses, former spouses, a parent and child or between members of the same family or household.  

Eligible family members include an employee's spouse or registered domestic partner; parent, parent-in-law or parent of a domestic partner; child or child of a domestic partner, including a biological, adopted or foster child, a stepchild, a legal ward or a child of an employee standing in loco parentis; sibling, including a half-sibling, step-sibling and sibling related through adoption; grandchild; grandparent; an individual related to the employee by blood and an individual whose close association with the employee is the equivalent of a family relationship.  

Paid sick and safe time may not be used as additional vacations days. Additionally, paid sick and safe time may not to be used to extend employment or to delay a termination date. An employee who uses sick and safe time for purposes other than those permitted by this policy will be subject to disciplinary action, up to and including termination from employment.  

##### Requesting Sick and Safe Time/Documentation  

Employees must provide seven days' advanced written notice if the need for sick and safe time is foreseeable (i.e., expected or planned leave). When the need for sick and safe time is unforeseeable, the Company does not require advance written notice, but employees may be required to document their request for sick time and/or provide written confirmation that they used sick time for purposes permitted under this policy. To provide notice of the need to use sick and safe time, employees should contact their Human Resources representative.  

If sick and safe time is for more than three consecutive work days, the Company may request that employees provide supporting documentation establishing the need for and duration of sick and safe time. Work days are the days or parts of days employees would have worked had they not used sick and safe time.  

The documentation should not disclose the nature of an employee’s illness, injury or health condition or specify the details of a family offense matter, sexual offense, stalking or human trafficking. If requested, such documentation must be provided within seven (7) days of returning to work.  

Failure to provide required medical documentation may result in discipline.  

Employees are not required to search for or find a replacement worker to cover the hours during which such employee is utilizing sick and safe leave.  

If sick and safe time is for fewer than three consecutive days, the Company may request that employees provide written confirmation that they used the time for a permissible purpose.  

##### Rate of Pay and Overtime  

Sick and safe time is paid based on the employee's regular rate of pay at the time the sick and safe time is taken. Sick and safe time is not considered time worked for the purpose of calculating overtime for the week in which the sick and safe time was taken. Employees will not receive overtime pay for sick and safe leave.  

##### Leave Carryover  

Employees who have accrued time remaining at the end of the year may carry over up to 56 hours of the accrued and unused time to the next calendar year. However, employees may not use more than 56 hours of sick and safe time in a calendar year.  

The Company does not offer pay in lieu of actual sick and safe time.  

##### Confidentiality  

In accordance with the ESSTA, the Company will keep confidential the health information of the employee or employee's family member, as well as information related to the employee’s or family  member’s status or perceived status as a victim of domestic violence, family offenses, sexual offenses, stalking or human trafficking. When such information is provided solely for the purposes of using paid sick and safe time, it will not be disclosed except by the affected employee, with the written permission of the affected employee or as required by law. The Company reserves the right to consider this information in connection with a request for sick and safe time or in connection with a request for a reasonable accommodation for a victim of domestic violence, stalking or a sex offense.  

##### Effect on Other Rights and Policies  

The Company may provide other forms of leave for employees to care for medical conditions or issues related to domestic violence, stalking or sex offenses under certain federal, state and municipal laws. In certain situations leave under this policy may run at the same time as leave available under another federal or state law, provided eligibility requirements for that law are met. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other federal, state and municipal medical, victim or family leave rights.  

##### Separation from Employment  

Compensation for accrued and unused paid sick and safe time is not provided upon separation from employment for any reason. If an employee is rehired by the Company within six months of separation from employment, previously accrued but unused sick and safe time will be immediately reinstated.  

##### Retaliation  

Employees have the right to request and use sick and safe time. The Company will not retaliate, or tolerate retaliation, against any employee who seeks or obtains sick and safe time under this policy or who makes a good faith complaint about a possible ESSTA violation, communicates with any person about such a violation or otherwise exercises any right afforded by the ESSTA. In addition, the Company will not retaliate against any employee who informs another person about the rights under the ESSTA.  

#### Paid Safe Leave (Westchester County)  

The Company provides eligible employees with paid safe leave in accordance with the requirements of the Westchester County Safe Time Leave Law (“STLL”).  

##### Eligibility  

All employees (including full-time and part-time) who are hired to work in Westchester County, New York for more than 90 days in a calendar year are eligible for paid safe leave in accordance with this policy. Eligible employees do not include: (i) employees hired to work less than 90 days in calendar year in Westchester County, New York; (ii) participants in a work experience program established by a social services district; (ii) individuals performing work pursuant to a federal work study program; or (iii) individuals compensated by or through qualified scholarships.  

##### Use of Safe leave  

Eligible employees who are victims of domestic violence or human trafficking (as defined by applicable law) may take up to 40 hours of paid safe leave in any calendar year in order to:  

- Attend or testify in criminal or civil court proceedings related to domestic violence or human trafficking; or  
- Move to a safe location.  

Paid safe leave may not be used as additional vacations days or to extend employment or to delay a termination date. An employee who uses safe leave for purposes other than those permitted by this policy will be subject to disciplinary action, up to and including termination from employment.  

Paid safe leave may be used in full or partial day increments.  

Employees are not required to search for or find a replacement worker to cover the hours during which they are using safe leave.  

The Company does not offer pay in lieu of actual safe leave.  

##### Requesting Safe Leave/Documentation  

When the need for paid safe leave is foreseeable, employees must make a good faith effort to provide advance notice of the need to use it.  When possible, the employee’s request must include the expected duration of the employee’s absence. Employees must also make a reasonable effort to schedule leave in a manner that does not unduly disrupt the Company’s operations. To provide notice of the need to use safe leave, employees should contact Human Resources.  

The Company may require employees to provide reasonable documentation that leave was used for a covered purpose. Such documentation may include any of the following:  

- A court appearance ticket or subpoena;  
- A copy of a police report;  
- An affidavit from an attorney involved in the court proceeding relating to the issue of domestic violence and/or human trafficking; or  
- An affidavit from an authorized person from a reputable organization known to provide assistance to victims of domestic violence and human trafficking.  

The Company will not count employees' use of safe leave in compliance with this policy as an absence when evaluating absenteeism. Therefore, any such use of safe leave will not lead to or result in discipline, discharge, demotion, or suspension.  

##### Rate of Pay  

Safe leave is paid based on the employee's normal hourly pay rate in effect at the time the safe leave is taken and will not be less than the New York State minimum wage. Safe time is not considered time worked for the purpose of calculating overtime for the week in which the safe time was taken. Employees will not receive overtime pay for safe leave.  

##### Confidentiality  

The Company will keep confidential information about an employee that is obtained solely for the purpose of using safe leave and will not disclose such information except with the written permission of the affected employee or as otherwise required by law.  

##### Effect on Other Rights and Policies  

Employees who are the victims of domestic violence or human trafficking may be entitled to additional leave under certain federal, state and/or local laws.  Paid safe leave provided in accordance with this policy is in addition to and does not run concurrently with paid sick leave provided in accordance with the Company’s Paid Sick Leave (Westchester County) policy or Westchester County’s Earned Sick Leave Law.  In certain situations leave under this policy may run at the same time as leave available under another federal, state or local law, provided eligibility requirements for that law are met. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other federal, state and municipal leave and accommodation rights.  

Employees who are the victims of domestic violence may also be entitled to reasonable accommodations under the Company’s reasonable accommodation policies or applicable law and should consult Human Resources for additional information.  

##### Integration with Other Benefits  

If an employee elects to integrate paid safe leave with other paid benefits (such as temporary disability insurance or paid family leave insurance), the Company will integrate all paid benefits such that an employee will not be paid more than his or her regular compensation at any time.  

##### Retaliation  

Employees have the right to request and use safe leave. The Company will not take retaliatory personnel action, discriminate or tolerate discrimination or retaliation, against any employee who seeks or obtains safe leave under this policy or who makes a good faith complaint about a possible violation of the SLL or this policy or who communicates with any person about such a violation. The Company will not interfere with or punish an employee for participating in or assisting with an investigation, proceeding or hearing under the SLL.  In addition, the Company will not retaliate or discriminate against an employee for informing others of their rights under the SLL.  

### SAFETY AND SECURITY  

#### Smoke-Free Workplace  

The Company prohibits smoking in the workplace and in company-owned vehicles. For purposes of this policy, smoking includes vaping (i.e., the use of electronic cigarettes). Employees wishing to smoke (including vaping) must do so outside the company’s facilities during scheduled work breaks.  

Employees that observe other individuals smoking in the workplace in violation of this policy have a right to object and should report the violation to their supervisor or another member of management. Employees will not be disciplined or retaliated against for reporting smoking that violates New York law or this policy.  

Employees who violate this policy may be subject to disciplinary action up to and including termination of employment.  

#### Cell Phone Use/Texting While Driving  

As is set forth in the National Handbook, the Company prohibits employees from using handheld cellular phones for business reasons while driving or for any reason while driving for work-related purposes or driving a company-owned vehicle. Employees should also be aware that New York law prohibits all drivers from using handheld mobile telephones and portable electronic devices while driving.  

New York law allows use of such devices under the following circumstances:  

1. When the driver is using a hands-free mobile telephone, which allows the user to communicate without the use of either hand, a handheld electronic device that is affixed to a vehicle surface, or a Global Positioning System (GPS) device that is attached to the vehicle;  
1. When the purpose of the call is to communicate an emergency to a police or fire department, a hospital or physician’s office or an ambulance corps; or  
1. When operating an authorized emergency vehicle in the performance of official duties.  

### Additional Documents  

---
#### EMPLOYEE COMPLAIMT FORM  

Please provide the requested information so that the Company may investigate and resolve your complaint.  You are not limited to the space provided and may attach additional pages.  Once you have completed this form, please provide a copy of it to any member of the Human Resources team.  

NAME:

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  

TITLE:  

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  

DEPARTMENT/lOCATION:  

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  

SUPEERVISOR:

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  

A.	Complaint(s) – Please describe your complaint(s), including the name of the person(s) about whom you are complaining.  If your complaint involves specific comments, please include a description of the comments.





B.	Related Material – Please list, and if possible, provide copies of, any emails, text messages, letters, notes, memos, diary entries, calendars, reports, or other items that relate to your complaint(s):




C.	Persons With Information  –  Please list any individuals who you believe may have information about your complaint(s):




D.	Prior Report(s) – Have you reported your concerns to anyone else at the Company?  If so, please provide the name and position of the person to whom you reported the concerns, and the date of the report.

**I understand that if I become aware of additional relevant information, I must promptly provide such information to Human Resources.  I also am aware that the Company prohibits retaliation against me for filing this complaint, and I agree that I will immediately report any incident I believe is retaliatory using the Company’s procedures for reporting retaliation.**  

Employee Signature:

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  

Date completed by Employee:  

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  

HR or Manager Signature:  

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  

Date Received from Employee:

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  

---  

[MODEL LACTATION ROOM REQUEST FORM](https://www1.nyc.gov/assets/cchr/downloads/pdf/lactationroomrequestform.docx)  

---  

[STOP SEXUAL HARASSMENT ACT FACTSHEET](https://www1.nyc.gov/site/cchr/media/sexual-harassment-act-factsheet.page)  

---  

## Ohio  

### GENERAL INFORMATION  

#### About This Ohio Supplement  

ConstructConnect is committed to workplace policies and practices that comply with federal, state and local laws. For this reason, Ohio employees will receive the Company’s national handbook (“National Handbook”) and the Ohio Supplement to the National Handbook (“Ohio Supplement”) (together, the “Employee Handbook”).  

The Ohio Supplement applies only to Ohio employees. It is intended as a resource containing specific provisions derived under Ohio law that apply to the employee’s employment. It should be read together with the National Handbook and, to the extent that the policies in the Ohio Supplement are different from, or more generous than those in the National Handbook, the policies in the Ohio Supplement will apply.  

The Ohio Supplement is not intended to create a contract of continued employment or alter the at-will employment relationship. **Only the President/Owner of the Company or his or her authorized representative has the authority to enter into an agreement that alters the at-will employment relationship and any such agreement must be in writing signed by the President/Owner of the Company or his or her authorized representative.**  

If employees have any questions about these policies, they should contact their Human Resources representative.  

### COMMITMENT TO DIVERSITY  

#### Equal Employment Opportunity  

As set forth in the National Handbook, ConstructConnect is committed to equal employment opportunity and to compliance with federal antidiscrimination laws. We also comply with Ohio law, which prohibits discrimination and harassment against employees or applicants for employment based on race, color, religion, national origin, military status, sex (including pregnancy, childbirth and related medical conditions), disability (including HIV) or age (40 and over) or ancestry. The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law.  

### GENERAL EMPLOYMENT PRACTICES

#### Access to Wage Information and Medical Records  

##### Wage Information for Nonexempt Employees  

Nonexempt employees may obtain records regarding their pay rate, hours worked and amount paid by submitting a written request to Human Resources. Certain individuals, including the employee’s attorney, parent, guardian, legal custodian or collective bargaining representative may also request this information on the employee’s behalf. A request to provide the information to a person acting on the employee’s behalf must be signed by the employee whose information is being requested and should reasonably specify the particular information being requested.  

Upon receipt of the request, the Company will provide the information within 30 business days, unless doing so would cause a hardship for the Company or unless the Company and the employee or the person acting on behalf of the employee agree to an alternative time. If providing the information within 30 business days presents an hardship for the Company, it will provide the requested information as soon as practicable.  

##### Medical Records  

Upon written request, employees or their designated representatives will be allowed to access any of their medical records retained by the Company, including any medical reports by physicians or healthcare professionals and any hospital or laboratory tests related to physical examinations or tests required as a condition of employment or arising out of a work-related injury or disease.  

The Company will provide copies of medical records covered under this policy, but may require employees to pay the cost of furnishing the copies, up to twenty-five cents for each page of a report.  

### TIME OFF AND LEAVES OF ABSENCE  

#### Family Military Leave  

Once per calendar year, the Company will allow employees up to 10 days or 80 hours (whichever is less) of time off from work for family military leave, if all of the following conditions are satisfied:  

- The employee has been employed with the Company for at least 12 consecutive months and for at least 1,250 hours in the 12 months immediately preceding commencement of the leave.  
- The employee is the parent, spouse or a person who has or had legal custody of a person who is a member of the uniformed services and is called into active duty in the uniformed services for a period longer than 30 days or is injured, wounded or hospitalized while serving on active duty in the uniformed services.  
- The employee provides notice of the need for leave under this policy at least 14 days prior to taking the leave, if the leave is being taken because of a call to active duty, or at least two days prior to taking the leave if the leave is being taken because of a uniformed servicemember’s injury, wound or hospitalization. If the employee receives notice from a representative of the uniformed services that the injury, wound or hospitalization is of a critical or life-threatening nature, the employee may take leave under this policy without providing notice to the Company.  
- The dates on which the employee takes leave under this policy occur no more than two weeks prior to or one week after the deployment date of the employee's spouse, child or ward or former ward, in the case of a call to active duty.  
- The employee does not have any other leave available except sick leave or disability leave, meaning that the employee does not have military family leave available under the FMLA policy set forth in the National Handbook    

Employees may be required to provide certification from the appropriate military authority to verify that the employee satisfies the above leave eligibility criteria.  

The Company will continue to provide benefits to employees during any period of leave under this policy. Employees will be responsible for the same proportion of the cost of the benefits as they regularly pay during periods of time when not on leave. Leave under this policy is unpaid, except that exempt employees may receive pay for partial day absences, as required by applicable law.  

Upon the completion of the leave, employees will be restored to the position they held prior to taking the leave or a position with equivalent seniority, benefits, pay and other terms and conditions of employment.
Employees should contact Human Resources as to any questions they may have about this policy.  

#### Military Leave—Rights to Reinstatement  

Ohio employees who are members of the uniformed services or Ohio organized militia are entitled to the same reinstatement and reemployment rights as are set forth in the federal Uniformed Services Employment and Reemployment Rights Act (USERRA) and described more fully in the Military Leave policy set forth in the National Handbook.  

#### Jury and Witness Duty Leave  

The Company encourages all employees to fulfill their civic responsibilities and to respond to jury service summons or subpoenas, attend court for prospective jury service, serve as a juror or respond to a subpoena to appear before a grand jury, in any proceeding in a criminal case or in any proceeding before a juvenile court. Under no circumstances will employees be terminated, threatened, coerced, or penalized because they take or request leave in accordance with this policy.  

Employees must provide their supervisor with reasonable advance notice of any jury summons or witness subpoena. Verification from the court clerk of having served may be required.  

Time spent engaged in attending court for prospective jury service or witness duty is not compensable except that exempt employees will not incur any reduction in pay for a partial week's absence due to jury or witness duty. Employees will not be required to use sick leave or vacation for time spent responding to a summons and/or subpoena, for participating in the jury selection process or for serving on a jury.  

#### Crime Victim Leave  

Eligible employees who are crime victims or family members of crime victims may take time off from work to participate, upon the prosecutor’s request, in preparation for a criminal or delinquency proceeding or for attending such a proceeding in response to a subpoena where attendance is reasonably necessary to protect a crime victim’s interests.  

Employees are eligible for time off under this policy if they are: (1) the victim of the crime at issue in the proceeding; (2) the spouse, child, stepchild, sibling, parent, stepparent, grandparent or other relative of a victim of the crime at issue in the proceeding; or (3) a representative of the crime victim.  

Employees who are the victims of, or represent a victim of, an act for which a child has been committed to the legal custody of the department of youth services, will not be terminated, disciplined or otherwise retaliated against for participating in a hearing before the release authority pertaining to the release of the child.  

Employees will not be eligible for leave under this policy if they are charged with, convicted of or adjudicated to be a delinquent child for the crime or delinquent acts against the victim or another crime or delinquent act arising from the same episode or conduct.  

Time off under this policy will be without pay except that exempt employees will receive pay when required under applicable law.  

#### Time Off to Vote  

The Company encourages all employees to fulfill their civic responsibilities and to vote in all public elections. Most employees’ schedules provide sufficient time to vote either before or after working hours.  

The Company will not terminate or threaten to terminate an employee for taking a reasonable amount of time off to vote on Election Day. Time off will be without pay for nonexempt employees.  

The Company asks that employees provide reasonable advance notice of the need for time off to vote so that the time off can be scheduled to minimize disruption to normal work schedules. Proof of having voted may be required.  

#### Election Official Leave  

Employees will be allowed time off to serve as an election official on any registration or Election Day.  

Time off for this purpose will be without pay, except that exempt employees may receive compensation for partial day absences, as required by applicable law.  

The Company asks that employees provide reasonable advance notice of the need for time off to serve as an election official, so that the time off can be scheduled to minimize disruption to normal work schedules.  

Proof of having served as an election official may be required.  

#### Volunteer Emergency Responder Leave  

Employees who serve as volunteer firefighters or volunteer providers of emergency medical services may arrive late to work or be absent from work in order to respond to an emergency occurring prior to the employee’s reporting time. For purposes of this policy, responding to an emergency includes going to, attending to, or coming from a fire, hazardous or toxic materials spill and cleanup, medical emergency or other situation that poses an imminent threat of loss of life or property to which the fire department or provider of emergency medical services has been or later could be dispatched.  

Time off under this policy will be without pay, except that exempt employees may be paid for partial day absences, as required by law.  

Employees must submit a written notification to Human Resources within 30 days of being certified as a volunteer firefighter or volunteer emergency services provider and must provide notice of any change in their status as a volunteer.  

Employees must make every effort to notify the Company when they will be late to work or absent from work to provide emergency services. If notification is not possible, employees must provide the Company with a written explanation from the chief of the volunteer fire department or the medical director or chief administrator of the cooperating physician advisory board of the emergency medical service organization with which the employee serves, to explain why prior notice was not given.  

Employees who take time off to respond to an emergency may be required to provide a written statement from the chief of the volunteer fire department or the medical director or chief administrator of the cooperating physician advisory board of the emergency medical service organization, stating that the employee responded to an emergency and listing the time and date of that response.  

### SAFETY AND SECURITY  

#### Smoke-Free Workplace  

The Company prohibits smoking in the workplace and in company-owned vehicles. Employees wishing to smoke must do so outside the company’s facilities during scheduled work breaks.  

Employees that observe other individuals smoking in the workplace in violation of this policy have a right to object and should report the violation to their supervisor or another member of management. Employees will not be disciplined or retaliated against for reporting smoking that violates Ohio law or this policy.  

Employees who violate this policy may be subject to disciplinary action up to and including termination of employment.  

#### Weapons in the Workplace  

In the interest of maintaining a workplace that is safe and free of violence, and in accordance with the policy set forth in the National Handbook, the Company generally prohibits the presence or use of firearms and other weapons on the Company's property, regardless of whether or not the person is licensed to carry the weapon. In compliance with Ohio law, however, the Company permits employees with a valid concealed handgun license to transport or store their firearms or ammunition inside their privately owned vehicles in the Company's parking lots or other parking areas provided by the Company, as long as the vehicle is in a location where it is permitted to be  and so long as any such firearm or ammunition is locked within the trunk, glove box or other enclosed compartment or container within the vehicle when the licensed employee is not physically present inside it.  Such lawfully possessed firearms and ammunition may not be removed from the employees' personal vehicle or displayed to others, except in accordance with this policy.  

#### Cell Phone Use / Texting While Driving  

As is set forth in the National Handbook, the Company prohibits employees from using cellular phones for business reasons while driving or for any reason while driving for work-related purposes or driving a company-owned vehicle. Employees should also be aware that using a handheld electronic wireless communications device to write, send or read a text-based communication is a violation of Ohio law, in addition to being a violation of Company policy.  


## Oregon  


### GENERAL INFORMATION  

#### About This Oregon Supplement  

ConstructConnect is committed to workplace policies and practices that comply with federal, state and local laws. For this reason, Oregon employees will receive the Company’s national handbook (“National Handbook”) and the Oregon Supplement to the National Handbook (“Oregon Supplement) (together the “Employee Handbook”).  

The Oregon Supplement applies only to Oregon employees. It is intended as a resource containing specific provisions derived under Oregon law that apply to the employee’s employment. It should be read together with the National Handbook and, to the extent that the policies in the Oregon Supplement are different from or more generous than those in the National Handbook, the policies in the Oregon Supplement will apply.  

The Oregon Supplement is not intended to create a contract of continued employment or alter the at-will employment relationship. **Only the President/Owner of the Company or that person’s authorized representative has the authority to enter into an agreement that alters the at-will employment relationship and any such agreement must be in writing signed by the President/Owner of the Company or an authorized representative.**  

If employees have any questions about these policies, they should contact their Human Resources representative.  

### COMMITMENT TO DIVERSITY  

#### Equal Employment Opportunity  

As set forth in the National Handbook, ConstructConnect is committed to equal employment opportunity and to compliance with federal antidiscrimination laws. We also comply with Oregon law which prohibits discrimination and harassment against any employees, applicants for employment or interns based on race, color, religion, sex (including pregnancy, childbirth or related medical conditions), national origin or ancestry, citizenship, physical or mental disability, genetic information, age (18 and over), veteran status, uniform servicemember status, unemployment status, sexual orientation, gender identity (including gender-related identity and gender expression), marital status family status, or having been a victim of sexual abuse, including domestic abuse, sexual assault or stalking. The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law.  

#### Sexual and Other Unlawful Harassment  

ConstructConnect is committed to providing a work environment free of harassment. The Company complies with Oregon law and maintains a strict policy prohibiting sexual harassment and harassment against employees, applicants for employment or interns based on race, color, national origin or ancestry, citizenship, religion, sex (including pregnancy, childbirth or related medical conditions), gender identity (including gender-related identity and gender expression), sexual orientation, marital status, family status, physical or mental disability, genetic information, age (18 and over), veteran status, uniform servicemember status, unemployment status, expunged juvenile record, or having been a victim of sexual abuse, including domestic abuse, sexual assault or stalking. The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law. Sexual harassment prohibited under this policy includes sexual assault, which is unwanted conduct of a sexual nature that is inflicted upon a person or compelled through the use of physical force, manipulation, threat or intimidation.  

All employees are expected to comply with the Company's Sexual and Other Unlawful Harassment policy, which is included in the National Handbook. While the Sexual and Other Unlawful Harassment policy sets forth the Company's goals of promoting a workplace that is free of harassment, the policy is not designed or intended to limit the Company's authority to discipline or take remedial action for workplace conduct that we deem unacceptable, regardless of whether that conduct satisfies the definition of unlawful harassment.  

Any employee who believes they have been harassed or discriminated against should provide a written or verbal report to their supervisor, another member of management, to Human Resources as soon as possible. All employees are encouraged to document any incidents involving discrimination, harassment or sexual assault as soon as possible.  

ConstructConnect will not tolerate retaliation against any employee for raising a good faith concern, for providing information related to a concern, or for otherwise cooperating in an investigation of a reported violation of this policy. Any employee who retaliates against anyone involved in an investigation is subject to disciplinary action, up to and including dismissal.  

##### Time Limitations  

Nothing in this policy precludes any person from filing a formal grievance in accordance with the Oregon Bureau of Labor and Industries’ Civil Rights Division or the Equal Employment Opportunity Commission. Note that Oregon state law requires that any legal action taken on alleged discriminatory conduct (specifically that prohibited by ORS 659A.030, 659A.082 or 659A.112) commence no later than five years after the occurrence of the violation. Other applicable laws may have a shorter time limitation on filing.  

##### Nondisclosure and Nondisparagement Agreements  

The Company will not require or coerce an employee or prospective employee to enter into any agreement as a condition of employment, continued employment, promotion, compensation or the receipt of benefits, that contains a nondisclosure provision, nondisparagement provision or any other provision that has the purpose or effect of preventing the individual from disclosing or discussing unlawful employment discrimination or harassment (including sexual assault). An employee claiming to be aggrieved by discrimination, harassment, or sexual assault may, however, voluntarily request to enter into a settlement, separation, or severance agreement that contains a nondisclosure, nondisparagement, or no-rehire provision (as defined below) and will have at least seven days to revoke any such agreement.  

Under this policy, a nondisclosure agreement is any agreement by which one or more parties agree not to discuss or disclose information regarding any complaint of work-related harassment, discrimination, or sexual assault. A nondisparagement agreement is any agreement by which one or more parties agree not to discredit or make negative or disparaging written or oral statements about any other party or the Company. A no-rehire provision is an agreement that prohibits an employee from seeking reemployment with the Company and allows the Company to not rehire that individual in the future.  

#### Pregnancy Accommodation  

The Company will provide employees and applicants with a reasonable accommodation for limitations related to pregnancy, childbirth or a related medical condition, including lactation, unless doing so would impose an undue hardship on the Company’s business. Reasonable accommodations may include, but are not limited to: acquisition or modification of equipment or devices; more frequent or longer break periods or periodic rest; assistance with manual labor; or modification of work schedules or job assignments.  

The Company will not require a pregnant employee or applicant to accept a reasonable accommodation if the employee does not have a known pregnancy-related limitation or require reasonable accommodation to perform essential job duties, nor will the Company require a pregnant employee to take a leave of absence instead of providing a reasonable accommodation.   

The Company prohibits discrimination against employees and applicants on the basis of pregnancy, childbirth or related condition. The Company will not take an adverse employment action or in any manner discriminate or retaliate against an applicant or employee because they request, inquire about or use reasonable accommodations in accordance with this policy. Employees with questions or concerns regarding this policy or who would like to request an accommodation should contact Human Resources.  

### GENERAL EMPLOYMENT PRACTICES  

#### Personal and Family Relationships  

Employee’s relatives and friends may be eligible for employment and, if employed, may be eligible for transfers, promotions, etc., to positions for which they are qualified. However, relatives are not entitled to preferential consideration and will not be hired or promoted into a position that creates a conflict of interest.  

The term “relatives,” includes but is not limited to, the following relationships: spouse, domestic partner, child, parent, brother, sister, brother-in-law, sister-in-law, son-in-law, daughter-in-law, mother-in-law, father-in-law, aunt, uncle, niece, nephew, stepparent and stepchild.  

#### Access to Personnel Files and Time and Pay Records  

Employees may inspect their personnel file (except any records and other material exempt from disclosure under state law) and/or their time and pay records by contacting Human Resources to arrange a mutually-convenient time. A supervisor or a Human Resources representative may be present while employees review their file.  

Upon written request, a certified copy of an employee’s personnel records and/or time and pay records will be provided to the employee within 45 days. In situations in which the records are not readily available, the Company may ask an employee to agree to extend this time. Employees may be asked to reimburse the company an amount reasonably calculated to recover the actual cost of providing the certified copy.  

### PAY PRACTICES  

#### Meal and Rest Breaks  

Employees working at least six-hours will receive an unpaid meal break of 30 minutes approximately midway through the day. If the work period is at least six but less than seven hours, the meal period must be taken between the second and fifth hours worked. If the work period is more than seven hours, the meal period must be taken between the third and sixth hours worked.  

An uninterrupted meal break lasting 30 minutes or more will be unpaid for nonexempt employees.  

Employees may not take a shorter meal break or skip a meal break to leave early.  

Employees who work at least two hours and one minute will also receive a paid 10-minute rest break and an additional rest break for every four hours worked thereafter.  

Rest breaks will be in addition to any meal breaks and cannot be taken at the beginning or end of a shift or combined with a meal break.  

#### Lactation Accommodation  

The Company will provide a reasonable amount of break time to accommodate an employee desiring to express breast milk for the employee’s infant child who is 18 months of age or younger. The Company will provide a reasonable rest period to express milk each time the employee has a need to do so.  

If possible, nursing mothers should take time to express breast milk during their regular meal and/or rest breaks. If the break time cannot run concurrently with meal and/or rest breaks already provided to the employee, the break time will be unpaid for nonexempt employees. Where these additional unpaid breaks are required, employees should work with their supervisor regarding scheduling.  

The Company will make reasonable efforts to provide employees with the use of a private location, other than a toilet stall, for the employee to express milk. Employees should discuss with their supervisor, a Human Resources representative the location to express their breast milk and for storage of expressed milk and to make any other arrangements under this policy.  

When possible, employees should provide reasonable notice to the Company that they intend to take breaks for expressing breast milk upon returning to work.  

The Company will not demote, terminate or otherwise take adverse action against an employee who requests or makes use of the accommodations and break time described in this policy.  

#### Discussion of Wages  

No employee is prohibited from inquiring about, discussing or disclosing his or her own wages or those of another employee. The Company will not terminate, demote, suspend,  or otherwise discriminate or retaliate against any employee on the basis of such a disclosure or because the employee files a complaint or charge or otherwise institutes an investigation, proceeding or hearing based on the disclosure of wage information.  

This policy does not apply to disclosure of other employees’ wage information by employees who have access to such information solely as part of their essential job functions and who, while acting on behalf of the company, make unauthorized disclosure of that information. Company representatives may disclose employees’ wages in response to a complaint or charge, or in furtherance of an investigation, proceeding, hearing or action under state law.  

### TIME OFF AND LEAVES OF ABSENCE  

#### Paid Sick and Safe Time  

##### Eligibility  

All employees (including full-time, part-time and temporary employees) are eligible to accrue paid sick and safe time.  

##### Annual Accrual of Paid Sick and Safe Time  

Eligible employees begin to accrue paid sick and safe time on January 1, 2016, or upon their first day of employment, whichever is later.  

Paid sick and safe time accrues at a rate of one hour for every 30 hours worked, up to a maximum accrual cap of 40 hours per calendar year. Employees will not accrue paid sick and safe time during periods of paid or unpaid leave.  

Salaried exempt employees will be assumed to work 40 hours in a week unless the employee's regular workweek is less than 40 hours, in which case sick time accrues based upon that regular workweek.  

Eligible employees hired on or before January 1, 2016 may use paid sick and safe time as it accrues beginning on January 1, 2016. Employees hired after January 1, 2016 may use accrued paid sick and safe time beginning on their 91st calendar day of employment.  

An employee's use of paid sick and safe time is limited to 40 hours per calendar year. For qualifying absences, employees must use paid sick and safe time on the first day, and on each subsequent date, until all time is used. Paid sick and safe time can be used in one-hour increments.  

Employees are not required to find an employee to cover their work when they take paid sick and safe time and are not required to work an alternate shift to make up for the use of such time.  

##### Reasons Sick and Safe Time May be Used  

Employees may use paid sick and safe time for the following reasons:  

- For the diagnosis, care or treatment of the employee or the employee's family member's mental or physical illness, injury or health condition, including preventive medical care;  
- For reasons specified in the Company's Family and Medical Leave policy;, including:   
	- To care for a spouse, same-gender domestic partner, child, parent, parent-in-law, grandparent, or grandchild of an employee (or of an employee's same-sex domestic partner) or a person with whom the employee is or was in a relationship of in loco parentis with a serious health condition;  
	- For an employee's own serious health condition;  
	- To care for a sick child of the employee or of a same-gender domestic partner who does not have a serious health condition but requires home care;  
	- To be with or care for a child of the employee or of a same-gender domestic partner after birth, placement for adoption or foster care (or certain comparable situations). This parental leave may include any time necessary for the legal process required for adoption or foster care if the child is under age 18 or is incapable of self-care because of a physical or mental disability. (This type of leave must be completed within one year of the birth or placement for adoption);  
	- For an employee's own disability due to pregnancy, childbirth or related medical condition or for absence for prenatal care. Pregnancy disability leave is available only if the employee is unable to perform any job duties that the Company is able to offer, except that leave for prenatal care is covered without regard to disability; and  
	- To make arrangements necessitated by the death of a family member, to attend the family member's funeral or memorial service, and/or to grieve the death of a family member.  
- For reasons related to domestic violence, harassment, sexual assault or stalking against the employee or the employee's minor child or dependent, including:  
	- To seek legal or law enforcement assistance or remedies to ensure the health and safety of the employee or the employee's minor child or dependent, including preparing for and participating in protective order proceedings or other civil or criminal legal proceedings related to domestic violence, harassment, sexual assault or stalking;  
	- To seek medical treatment for or to recover from injuries caused by domestic violence or sexual assault to or harassment or stalking of the employee or the employee's minor child or dependent;  
	- To obtain, or to assist a minor child or dependent in obtaining, counseling from a licensed mental health professional related to an experience of domestic violence, harassment, sexual assault or stalking;  
	- To obtain services from a victim services provider for the eligible employee or the employee's minor child or dependent; or  
	- To relocate or take steps to secure an existing home to ensure the health and safety of the employee or the employee's minor child or dependent.  
- If either the employee's place of business or the employee's child's school or day care is closed by order of a public official due to a public health emergency or if the employee is excluded from work for health reasons; and  
- If a family member's presence in the community jeopardizes the health of others as determined by a lawful public health authority or by a health care provider.  

For purposes of this policy, "family members" include a:  

- Spouse or same-gender domestic partner;   
- Biological, adopted or foster child;  
- Biological, adoptive, step, custodial, non-custodial or foster parent;  
- Grandparent or grandchild;   
- Parent-in-law or parent of a same-gender domestic partner; and  
- Person with whom the employee was or is in a relationship in loco parentis, meaning a relationship in which a person assumes the role of parent for someone who is not their legal or biological child.  

##### Requesting Paid Sick and Safe Time/Documentation  

When the need for paid sick and safe time is foreseeable (i.e., expected or planned leave), employees must provide reasonable advance notice, including, when possible, the expected duration of the leave, and must make reasonable attempts to schedule the use of paid sick and safe time in a manner that does not unduly disrupt the company's business operations. When the need for paid sick and safe time is unforeseeable, employees must provide notice before the start of the employee’s shift or, when circumstances prevent such notice, as soon as practicable. To provide notice of the need to use paid sick and safe time, employees should contact their supervisor.  

Employees must inform the company of any change in the expected duration of the sick and safe time as soon as practicable.  

If sick and safe time is for more than three scheduled workdays, not including scheduled days off, employees must provide supporting documentation establishing the need for and duration of sick and safe time. Examples of documentation include, but are not limited to:  

- Documentation signed by a licensed health care provider; or  
- Documentation for victims of domestic violence, such as a copy of a police report or protective order.  


If the need for sick and safe time is foreseeable and is projected to last more than three scheduled workdays, the verification must be provided before the sick and safe time begins or as soon as otherwise practicable. If an employee begins leave without providing required prior notice, medical verification (e.g., for an employee’s or family member’s illness, injury or health care or preventative care) must be provided within 15 calendar days after the Company requests verification, and certification for absences relating to domestic violence, harassment, sexual assault or stalking must be provided within a reasonable time after the Company requests certification.  

The Company will pay any costs associated with any out-of-pocket expenses incurred by employees (that is not paid by insurance) in obtaining required verification from a licensed health care provider.  

The Company may require documentation prior to the third consecutive workday if it suspects an employee is abusing leave.  

Failure to provide required medical documentation may result in the delay or denial of payment for sick and safe time.  

##### Rate of Pay and Overtime  

Sick and safe time is paid based on the employee's regular rate of pay. Sick and safe time is not considered time worked for the purpose of calculating overtime for the week in which the time was taken.  

##### Leave Carryover  

Employees who have accrued time remaining at the end of the calendar year may carry over up to 40 hours of accrued and unused time to the next year. However, employees may not use more than 40 hours of sick and safe time in a calendar year. At no time will an employee have more than 80 hours of accrued sick and safe time.  

The Company does not offer pay in lieu of actual sick and safe time.  

##### Effect on Other Rights and Policies  

The Company may provide other forms of leave for employees to care for medical conditions or for issues related to domestic violence under certain federal, state and municipal laws. In certain situations, leave under this policy may run at the same time as leave available under another federal, state or municipal law, provided eligibility requirements for that law are met. The Company is committed to complying with all applicable laws. Employees should contact their Human Resources representative for information about other federal, state and municipal medical or family leave rights.  

##### Separation from Employment  

Compensation for accrued and unused paid sick and safe time is not provided upon separation from employment for any reason. If an employee is rehired by the Company within 180 days of separation from employment, previously accrued but unused sick and safe time will be immediately reinstated. If separation occurs before an employee's 91st day of employment and the employee is rehired within 180 days, the accrued sick and safe time balance will be restored and may be used once the employee has worked a combined total of 90 calendar days.   

##### Retaliation  

Employees have the right to request and use sick and safe time. The Company will not retaliate or discriminate, or tolerate retaliation or discrimination, against any employee who seeks or obtains sick and safe time under this policy, inquires about or invokes the provisions of the OSTL, or participates in an investigation, proceeding or hearing related to the OSTL.  

##### Confidentiality and Nondisclosure  

The Company will treat as confidential sick and safe time-related health information pertaining to the employee or employee's family member. Records and information concerning requests for or use of sick and safe time for reasons related to being the victim of domestic violence, harassment, sexual assault or stalking, will be treated as confidential and will not be released without the employee's express permission, unless otherwise required by law.  

#### Family and Medical Leave of Absence  

We recognize that employees may need to be absent from work for an extended period of time for family, medical or bereavement-related reasons. Accordingly, the Company will grant time off to employees in accordance with the requirements of the federal Family and Medical Leave Act (Fed-FMLA) and the Oregon Family Leave Act (OFLA). The OFLA applies to employees who work in Oregon. Where both the Fed-FMLA and the OFLA apply, the leave provided by each will count against the employee’s entitlement under both laws and must be taken concurrently. An employee who is eligible for leave under only one of these laws will receive benefits in accordance with that law only.  

The following policy addresses employee family and medical leave rights under the OFLA. Employees should refer to the National Handbook for additional details regarding the Fed-FMLA. Questions concerning this policy should be directed to Human Resources.  

##### OFLA Eligibility   

To be eligible for family leave under the OFLA, employees must:  

- Have worked for the Company for at least 180 days immediately before the date the leave begins; and  
- Have averaged at least 25 hours a week during the 180 days immediately before the date on which OFLA leave begins. The requirement of 25 hours or more a week does not apply to OFLA leave for parental leave purposes. For parental leave, eligible employees are those who have worked for the company for at least 180 days.  

##### Reasons for OFLA Leave  

OFLA leave may be granted for these reasons or purposes:  

- To care for a spouse, same-sex domestic partner, child, parent, parent-in-law, grandparent, or grandchild of an employee (or of an employee’s same-sex domestic partner) with a serious health condition;  
- For an employee’s own serious health condition;  
- To care for a sick child of the employee or of a same-sex domestic partner who does not have a serious health condition but requires home care, if no other family member is available to care for the child;  
- To be with or care for a child of the employee or of a same-sex domestic partner after birth, placement for adoption or foster care (or certain comparable situations). This parental leave may include any time necessary for the legal process required for adoption or foster care if the child is under age 18 (or is incapable of self-care because of a physical or mental disability. (This type of leave must be completed within one year of the birth or placement for adoption);  
- To care for a child of the employee or of a same-sex domestic partner whose school or child care provider has been closed in conjunction with a statewide public health emergency declared by a public health official;  
- For an employee’s own disability due to pregnancy, childbirth or related medical condition or for absence for prenatal care. Pregnancy disability leave is available only if the employee is unable to perform any job duties that the Company is able to offer, except that leave for prenatal care is covered without regard to disability;  
- To make arrangements necessitated by the death of a family member, to attend the family member’s funeral or memorial service, and/or to grieve the death of a family member (Bereavement Leave).  

A “serious health condition” means:  

- An illness, injury, impairment or physical or mental condition that involves an overnight stay in a hospital or similar facility;  
- An illness, disease or condition that the treating health care provider believes poses an imminent danger of death, is terminal in prognosis with a reasonable possibility of death in the near future, or is a mental or physical condition requiring constant care;   
- Any period of absence due to pregnancy-related disability or for prenatal care; or  
- Any period of absence for the donation of a body part, organ or tissue, including preoperative or diagnostic services, surgery, post-operative treatment and recovery.  

##### Length of Leave  

Eligible employees are entitled to up to 12 workweeks (and up to an additional 12 weeks for any pregnancy-related disability) in any leave year. Absences due to a compensable, disabling on-the-job injury do not count against the 12 workweeks under OFLA except when the employee refuses a suitable offer of light-duty or modified work. Where applicable, OFLA leave will run concurrently with leave provided under the Oregon Military Family Leave Act.  

Parents who use all 12 of their workweeks for parental leave to care for a newborn, newly adopted or newly placed foster child are also entitled to take up to 12 workweeks to care for a child with an illness or an injury that is not a serious health condition if no other family member is available to care for the child — that is, to take sick-child leave. A female employee may take up to 12 weeks of OFLA pregnancy disability leave, up to 12 weeks of parental leave, and up to another 12 weeks of OFLA sick child leave, for a total of up to 36 weeks of OFLA leave. Additional OFLA leave is not available in the case of birth, adoption or foster care placement of more than one child at the same time.  

The applicable leave year utilized by the Company is the rolling 12-month period measured backward from the date an employee uses his/her OFLA leave.  

If more than one qualifying family member works for the Company, two family members can only take leave at the same time if one needs to care for the other with a serious health condition, needs to care for a child with a serious health condition while another also has such a condition, both family members have a serious health condition at the same time or both family members are taking bereavement leave at the same time.  

Employees will be allowed up to two weeks of unpaid bereavement leave per death of a covered family member, not to exceed 12 weeks total per year. For purposes of bereavement leave, a “family member’ includes a family member includes the employee’s spouse (including same-sex spouse), same-sex domestic partner, parent, child, parent-in-law, grandparent and the parent or child of the employee’s same-sex domestic partner. Bereavement leave must be completed within 60 days of the date the eligible employee receives notice of the family member’s death.  

##### Pay  

OFLA leave is unpaid and employees must use all accrued vacation and sick pay (if eligible to use it) before going on unpaid status.  

##### Requesting OFLA Leave  

Except in very unusual or emergency situations, employees must request leave as soon as is practicable after he or she learns of the need for time away from work.  

If employees do not give timely notice, the Company may deny or delay the start of an employee’s leave and/or an employee may be subject to disciplinary action.  

If an employee knows of the need for the leave 30 or more days in advance, the employee must complete the leave of absence forms at least 30 days before leave is to begin.  

In unusual or emergency situations (for example, if an employee is injured in an accident, suddenly becomes ill, gives premature birth, experiences a death in their family, etc.), the employee must make an oral request to his or her manager for leave within 24 hours (or as soon thereafter as is practicable). All oral requests for leave must then be confirmed in writing as soon as is practicable and in no event later than three calendar days after returning to work.  

In the case of Bereavement Leave, prior notice is not required, but oral notice must be provided by the employee or someone on the employee’s behalf to the Company within 24 hours of beginning leave. Written confirmation of such notice must then be provided to Human Resources within three days of returning to work.  

In the case of leave to care for a child whose school or child care provider has been closed in conjunction with a public health emergency, the Company may require verification of the need for leave.  

As long as employees are using some form of paid leave (for example, earned vacation or sick leave) to cover missed time, employees need only comply with the notice provisions of those policies.  

Employees should consult with Human Resources when scheduling intermittent or reduced leave for planned medical treatment or supervision so that time off can be scheduled to minimize disruption to the normal work schedule.  

If an employee is seeking to use paid (for example, earned vacation) or unpaid leave for a purpose that may qualify for FMLA and/or OFLA leave, he or she must notify the Company so that the employee will receive all of the benefits to which he or she is entitled. Failure to provide notification of reasons for any absence, whether a partial or full day, which might qualify, could result in the absence being counted against the employee for attendance and other purposes (e.g., pay increases, promotional opportunities, etc.).   

##### Medical Certification  

For leaves due to an employee’s own health condition, the employee’s health care provider must review the employee’s essential job functions and certify that the employee’s condition prevents the employee from performing at least one of them.  

If an employee’s insurance or other benefit plan does not cover the cost, the Company will pay for the medical certification.  

For leaves due to a family member’s health condition, the Company may require written certification from the treating health care provider(s), except for an Oregon employee whose child has an illness requiring home care that does not qualify as a serious health condition (i.e., sick child leave). In the case of sick child leave, the Company will only require medical verification after an employee has taken more than three days of leave for this purpose in a one-year period. The Company will pay for the cost of the certification to the extent it is not covered by the employee’s insurance or benefit plan. In appropriate situations, the Company may also require documentation of the individual’s relationship to the employee.  

Medical certification forms are available from Human Resources and must be fully completed and returned prior to the start of any leave for which the employee has provided 30 days’ advance notice. In other circumstances (for example, in emergencies or other unusual circumstances when it is impossible to foresee the need for leave that far in advance), employees must return the completed form as soon as is practicable but no later than within 15 calendar days of the date an employee’s absence began or within 15 calendar days of our request for certification or recertification. If an employee fails to provide a timely, fully completed certification or recertification, he or she may be denied continuation of the leave until complete and sufficient medical verification is received.  

Recertification of the medical need for continuing leave must be provided every 30 days, and more often when circumstances have changed significantly or the Company has received information casting doubt on the validity of the prior certification.  

The Company may request a second (or third) medical opinion (except for an Oregon employee who has a sick child requiring home care who does not have a serious health condition) at the Company’s expense. The Company may also initiate an unpaid leave of absence and/or require a medical or other professional examination at the Company’s expense in circumstances in which an employee’s performance, conduct or behavior, the nature of an employee’s job and/or his or her condition raises an issue as to fitness for duty or ability to safely perform regular job duties.  

An employee who is absent for three or more calendar days in any 30-day period because of an illness, injury or other condition (other than one returning from intermittent leave) may be required to provide a return-to-work release from an employee’s health care provider confirming that the employee is able to return to work. We may require such a release for absences of less than three calendar days as well.  

All medical releases must clearly explain an employee’s work abilities and any limitations or restrictions. Reinstatement may be delayed until the employee has provided a release meeting these standards. An employee who does not timely provide the required release is subject to disciplinary action up to and including termination.  

All medical information will be kept in the employee’s confidential medical file and will not be the basis for any personnel actions or decisions other than those related to family and medical leave, reasonable accommodation, or as otherwise allowed by law.  

##### Intermittent/Reduced Schedule Leave  

The Company may transfer an employee who takes OFLA leave on an intermittent or reduced work schedule basis or who is recovering from a serious health condition to an alternative position to accommodate the leave or recovery, but will do so only if the employee agrees to the transfer voluntarily, the transfer is temporary and the alternative job has equivalent pay and benefits.  

An employee will be allowed to take parental leave in two or more nonconsecutive periods only with Company approval, except in the case of parental leave to effectuate adoption or foster placement of a child. Such leave need not be taken in one uninterrupted period.  

##### Reinstatement  

Employees returning from OFLA leave will be reinstated to their former job if it exists. If the former job does not exist, reinstatement will be to a job with equivalent status, pay, benefits and other employment terms. For OFLA leave, reinstatement rights exist at the former site as well as any other location within 20 miles. If an employee is on leave due to his or her own health condition, he or she may be required to provide a return-to-work release.  

##### Benefit Coverage  

The Company will continue paying its share of the cost of an employee’s health coverage while the employee is on FMLA/OFLA leave on the same terms as if the employee were working, but the employee will be responsible for continuing to make any payments normally required of him or her. If the employee does not pay the cost of coverage during the leave and the employee does not return to employment after taking family leave, the Company may seek to recover the employee’s share of the cost of benefit coverage by deducting amounts the Company paid from any amounts owed to the employee or through other legal means.  

While an employee is on paid leave (i.e., using sick leave or vacation time), any required employee payments will continue to be deducted from his or her check as usual to the extent his or her pay is sufficient to cover the deduction.  

If an employee does not return to work at the end of his or her leave, the employee may have rights under the federal Consolidated Omnibus Budget Reconciliation Act (COBRA) to continue his or her health coverage by paying the full premium (plus a small administrative fee), and may also be able to obtain portability coverage under some state laws.  

##### Fraudulent Use of OFLA Prohibited  

Employees who fraudulently obtain leave under this policy may be subject to disciplinary action, up to and including termination.  

##### Retaliation  

The Company will not interfere, restrain or deny the exercise of any rights provided under this policy. If an employee believes that his or her OFLA rights have been violated in any way, he or she should immediately report the matter to Human Resources.  

#### Jury Duty Leave  

The Company encourages all employees to fulfill their civic responsibilities and to respond to jury service summons or witness subpoenas, attend court for prospective jury service or serve as a juror or witness. Under no circumstances will employees be terminated, threatened, coerced, or penalized because they request or take leave for jury duty or prospective jury service or to serve as a witness provided they notify Human Resources prior to their service.  

Employees must provide their supervisor with notice of any jury summons or witness subpoena within five days after receipt and before their appearance is required to allow the Company time to make arrangements to cover the absence.  

Time spent engaged in attending court for prospective jury service or for serving as a juror is not compensable except that exempt employees will not incur any reduction in pay for a partial week’s absence due to jury duty. Employees who are absent from work while participating in the jury selection process or while serving as a juror or a witness will not be asked or required to use any annual, vacation or sick leave during the absence, although employees may elect to do so.  

To the extent an employee participates in any company-sponsored health, disability, life or other insurance benefits, such coverage will continue during jury duty or witness service provided that the employee gives advance notice of the service.  

#### Domestic Violence, Harassment, Sexual Assault or Stalking Victim Leave  

Employees who are the victim, or the parent or guardian of a minor child or dependent who is a victim of harassment, domestic violence, sexual assault or stalking will be allowed time off in order to:  

- Seek legal or law enforcement assistance or remedies;  
- Seek medical treatment for or to recover from injuries caused by harassment, domestic violence, sexual assault, or stalking;  
- Obtain or assist a minor child or dependent in obtaining counseling related to an experience of harassment, domestic violence, sexual assault or stalking;  
- Obtain services from a victim services provider; or  
- Relocate or take steps to secure an existing home.  

Time off under this policy is unpaid except that exempt employees will be paid when required under applicable law. In addition, employees are allowed, but not required, to use any accrued paid vacation leave, sick leave, personal business leave or other paid leave during a leave under this policy.  Where applicable, time off under this policy will run concurrently with time off under the Oregon Family Leave Act (OFLA) and/or the federal Family and Medical Leave Act (FMLA).  

Employees must provide their supervisor reasonable advance notice of their intent to take leave under this policy, unless providing advance notice is not feasible. In cases of emergency, employees, or a person acting on behalf of an employee, must give notice as soon as practicable. The Company may also require certification that the employee or minor child is a victim and that the leave is being taken for a permissible purpose. Such certification may take the form of a police report, protective order, or documentation from a health care professional, clergy, attorney or law enforcement officer.  

The Company reserves the right to limit the amount of leave an eligible employee may take, if the leave creates an undue hardship on the Company’s business.  

Leave may be taken on an intermittent or reduced work schedule basis. The Company may transfer an employee on intermittent leave or a reduced work schedule to an alternate position that better accommodates the leave, so long as the transfer is temporary and voluntary and there is no other reasonable option available that would allow the employee to use intermittent or reduced schedule leave. Transferred employees will be returned to their former position upon providing notification of readiness to return.  

While on leave, employees must periodically report their status to their supervisor, including the date they intend to return to work.  

Upon request, the Company will provide reasonable safety accommodations needed because of actual or threatened domestic violence, harassment, sexual assault or stalking, unless such accommodations impose an undue hardship on the Company’s business. Such safety accommodations may include, but are not limited to: transfer, reassignment, a modified schedule, a changed work telephone number, a changed work station, an installed lock or implemented safety procedures or other adjustment to a job structure, workplace facility or work requirement.  

Employees who wish to request time off or an accommodation under this policy should promptly notify a supervisor or Human Resources.  

Confidentiality of the situation, including the employee’s request for reasonable safety accommodation or time off under this policy and any documentation provided, will be maintained to the greatest extent possible.  

The Company will not retaliate, or tolerate retaliation, against any employee who seeks or obtains leave or an accommodation under this policy.  

#### Crime Victim Leave  

Eligible employees will be allowed time off to attend a criminal proceeding, a juvenile proceeding or any other proceeding at which a crime victim has the right to be present.  

Eligible employees are those who:  

- Have worked an average of more than 25 hours per week during the 180 days immediately prior to the leave; and  
- Are a victim who has suffered financial, social, psychological or physical harm as a result of a personal felony or is the spouse, domestic partner, parent, sibling, child, stepchild or grandparent of the victim.  

Time off under this policy is unpaid except that exempt employees will be paid when required under applicable law. Employees will also be allowed to use available accrued vacation or other paid leave, though the Company may determine the order in which such leave must be used.  

The Company reserves the right to limit the amount of leave an eligible employee may take, if the leave creates an undue hardship on the Company’s business.  

Employees must provide reasonable advance notice of their intent to take leave under this policy, as well as a copy of any notices of scheduled criminal proceedings provided by a law enforcement agency.  

Confidentiality of the situation, including the employee’s request for the time off under this policy and any documentation provided, will be maintained to the greatest extent possible.  

#### Firefighter Leave  

Employees who serve as volunteer firefighters in a rural fire protection district or as a firefighter employed by a city or a private firefighting service will be allowed a leave of absence when called to an emergency. Employees must return to work when their release from service permits them to resume their job duties.  

Time off under this policy will be without pay, except that exempt employees will be paid when required under applicable law. Upon return, employees will be reinstated to the same or equivalent position with all of the same seniority and benefits following the leave.  

#### Search and Rescue Operation Leave  

Employees who serve as search and rescue volunteers will be allowed time off when accepted to participate in search and rescue activities by the sheriff.  

Time off under this policy will be without pay, except that exempt employees will be paid when required under applicable law.  

Upon return, employees will be reinstated to the same or an equivalent position without loss of seniority and benefits that had been earned before the leave commenced.  

The Company will not discriminate or retaliate against employees who request or take leave under this policy.  

#### Juvenile Court Appearance Leave  

Employees will be allowed time off when compelled to attend a juvenile court proceeding involving a child of which the employee is a parent or legal guardian.  

Time off under this policy will be without pay, except that exempt employees will receive pay when required by applicable law.  

The Company will not discriminate or retaliate against employees who seek or obtain leave under this policy.  

#### Legislative Leave  

Regular full-time and part-time employees that have been employed by the Company for at least 90 days will be allowed time off to serve in the Oregon Legislative Assembly. Leave will be granted for any regular or special sessions or for time needed to perform official duties as a member or prospective member of the Legislative Assembly. Time off under this policy will be without pay.  

Employees must provide notice of the need for leave under this policy at least 30 days before a regular session begins and as soon as possible when it is apparent that a special or emergency session will be called.  

Employees must return to work within 15 days after the adjournment of the Legislative Assembly following a regular session or within five days after any other assignment is completed. Upon return from leave, employees will be reinstated to the same or similar position without loss of seniority or benefits earned before the leave commenced.  

The Company reserves the right to deny reinstatement if a conflict of interest develops or if the circumstances of the Company change during the leave such that it would be impossible or unreasonable to reinstate the employee.  

#### Bone Marrow Donation Leave  

Eligible employees who undergo a medical procedure to donate bone marrow will be allowed to use already accrued paid leave to do so. Eligible employees are those who work an average of 20 or more hours per week. The leave can extend up to the amount of the employee’s accrued paid leave or 40 work hours, whichever is less, unless the Company agrees otherwise.  

Employees may be required to provide the Company with verification from a physician of the purpose and length of each leave. If there is a medical determination that the employee does not qualify as a bone marrow donor, the paid leave used prior to the determination is not affected.  

The Company will not retaliate against any employee for requesting or taking a leave under this policy.  

#### Military Leave  

In addition to the military leave rights set forth in the National Handbook, Oregon employees who are members of an organized militia will be granted an unpaid leave of absence to perform active state service if the militia is called into active service. The Company will grant leave until the employee is released from state service. Employees who take leave under this policy will be restored to their prior position or to an equivalent position and will not lose seniority, vacation credits, sick leave credits, service credits under a pension plan or any other employee benefit or right that had been earned at the time of the leave of absence. Employees who take leave under this policy must return to employment within seven calendar days in order to be entitled to reinstatement.  

#### Family Military Leave  

Employees working an average of at least 20 hours per week who have a spouse or domestic partner that is a member of the military and has been notified of an impending call, order to active duty, or has been deployed during a period of military conflict, will be granted a leave of up to 14 days for each deployment. Leave may be taken intermittently, in which case the total number of hours of leave available is the amount the employee regularly works per day multiplied by 14. The leave can be taken before and during deployment, as well as when the military spouse or domestic partner is on leave from deployment.  

Employees should let the Company know within five days of receiving an official notification of a call to duty if they intend to take leave, or as soon as practicable if official notice is received less than five days before the leave is to begin. An employee taking leave under this policy may be required to provide a photocopy of the service member’s orders.  

Leave under this policy is unpaid, but employees may elect to use accrued paid time off during the leave. For employees who are eligible for leave under the Oregon Family Leave Act (OFLA) and/or the federal Family and Medical Leave Act (FMLA) and have OFLA and/or FMLA leave time remaining, time off under this policy will be counted as part of the total amount of authorized OFLA and/or FMLA leave.  

#### Veterans’ Day Observance  

The Company will provide employees who are also qualified veterans unpaid time off on Veterans’ Day, provided that the employee gives at least 21 calendar days’ notice of the intent to take time off on Veterans’ Day, provides valid documentation showing he or she is a qualified veteran, and would otherwise be required to work on Veterans’ Day.  

Employees should contact their supervisor to make appropriate arrangements. The Company reserves the right to deny requested time off to all qualifying veterans under this policy if it determines that providing such time off would cause significant economic or operational disruption, or undue hardship.  

### SAFETY AND SECURITY  

#### Cell Phone Use/Texting While Driving  

As set forth in the National Handbook, the Company prohibits employees from using cellular phones for business reasons while driving, for any reason while driving for work-related purposes and while driving a company-owned vehicle. Employees should also be aware that using a mobile electronic communication device without a hands-free accessory, including for texting, voice communication, e-mail or navigation, while driving a motor vehicle is a violation of Oregon law, in addition to being a violation of company policy.  It is also a violation of Oregon law to hold a mobile electronic device in one’s hand while driving.  

Oregon law provides that drivers who are over the age of 18 may use a hands-free accessory to conduct cell phone communications. For the safety of employees and the safety of others, the Company strongly suggests that any and all cell phone communications, including those with hands-free accessories, be made only if careful attention to the road is possible.  

#### Smoke-Free Workplace  

The Company prohibits smoking in the workplace. Employees wishing to smoke must do so outside of company facilities during scheduled work breaks and must not smoke in outside areas that will allow circulation of smoke into the company’s facilities. For example, employees must not smoke within 10 feet of the Company’s facilities’ ventilation intakes, windows, entrances or exits.  For purposes of this policy, “smoking” includes the use of aerosolized or vaporized inhalants, such as nicotine, cannabinoids or other substances that are not FDA approved and marketed solely for a therapeutic purpose.  

Employees who observe other individuals smoking in the workplace in violation of this policy have a right to object and should report the violation to their supervisor or another member of management. Employees will not be disciplined or retaliated against for reporting smoking that violates Oregon law or this policy.  

Employees who violate this policy will be subject to disciplinary action up to and including termination of employment.  

#### Safety Committees  

The Company forms safety committees to allow management and employees to work together to monitor the overall safety of our operations and to recommend changes in policies, rules and practices in order to make this a safer place for all of us to work.  

Each business location has its own safety committee. The safety committee will be composed of an equal number of employer-selected members and employee-elected or volunteer members. If company management and employees agree, the safety committee may have more employee-elected or volunteer members.  

Management will select its representatives and employee representatives shall be selected by the employees. The names of the current safety committee members will be posted on the company’s bulletin board.  

The safety committee will:  

- Establish and/or maintain a system by which employees may make safety-related suggestions, report hazards, and present to the safety committee any pertinent information. Such suggestions, reports and information shall be reviewed at the next safety committee meeting.  
- Assist in evaluating the Company’s accident and illness prevention program and make written recommendations for improvement.  
- Establish procedures and conduct workplace inspections and make written recommendations for improvements.  
- Evaluate the Company’s accountability system and make recommendations to implement supervisor and employee accountability for safety and health.  
- Establish procedures for investigating all safety-related incidents including injury accidents, illnesses and deaths (the safety committee need not conduct the investigation itself).  

Safety committee members may use company facilities and staff assistance for the preparation of any required reports or recommendations. Safety committee reports and recommendations shall be delivered to Human Resources, and a copy will be posted except when posting would disclose confidential information (e.g., relating to an employee’s medical condition) and copies will be available upon request from Human Resources  


## Texas  

### GENERAL INFORMATION  

#### About This Texas Supplement  

ConstructConnect is committed to workplace policies and practices that comply with federal, state and local laws. For this reason, Texas employees will receive the Company’s national handbook (“National Handbook”) and the Texas Supplement to the National Handbook (“Texas Supplement”) (together, the “Employee Handbook”).  

The Texas Supplement applies only to Texas employees. It is intended as a resource containing specific provisions derived under Texas law that apply to the employee’s employment. It should be read together with the National Handbook and, to the extent that the policies in the Texas Supplement are different from, or more generous than those in the National Handbook, the policies in the Texas Supplement will apply.  

The Texas Supplement is not intended to create a contract of continued employment or alter the at-will employment relationship. **Only the President/Owner of the Company or that person’s authorized representative has the authority to enter into an agreement that alters the at-will employment relationship and any such agreement must be in writing signed by the President/Owner of the Company or an authorized representative.**  

If employees have any questions about these policies, they should contact their Human Resources representative.  

### COMMITMENT TO DIVERSITY  

#### Equal Employment Opportunity  

As set forth in the National Handbook, ConstructConnect is committed to equal employment opportunity and to compliance with federal antidiscrimination laws. We also comply with Texas law, which prohibits discrimination and harassment against any employees or applicants for employment based on race, color, religion, national origin, sex, disability, age (40 and over), genetic information or the refusal to submit to a genetic test. The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law.  

The Company also complies with the Texas law prohibiting sexual harassment of unpaid interns.  

### PAY PRACTICES  

#### Lactation Accommodation  

The Company has created a mother-friendly employee worksite lactation support program. The program provides a work environment that is supportive of lactating mothers. Lactation times will be established for any employee who wishes to express breast milk or breastfeed, based on the employee’s work schedule. If possible, the lactation time will run concurrently with any break time already provided. Lactation time beyond the regular break time is unpaid and will be negotiated between the employee and the Company.  

The Company will provide a private space, other than a bathroom, for lactating mothers to express milk. Where feasible, that space will be dedicated for the specific use of expressing milk by lactating mothers. Where dedicated space is not feasible, the Company will provide a mixed-use space where lactating mothers will have priority over all other users of the space. Access to a safe water source and a sink within a reasonable distance from the lactation space will be provided. Employees may store their expressed milk in their own personal coolers with an ice pack or in the shared break room refrigerator space, if available.  

The Company complies with applicable federal law regarding lactation accommodation. See the Lactation Accommodation policy in the National Handbook for additional information.  

#### Schedules and Hours (Euless)  

ConstructConnect complies with the City of Euless’s Fair Overtime and Scheduling Standards Ordinance (“FOSSO”). In accordance with that law, the Company adopts the policies and practices described below. These policies and practices apply to all non-exempt employees (including full-time, part-time, seasonal and temporary employees) who work for the Company within the geographic boundaries of the City of Euless.   

##### Advance Notice of Work Schedule  

At or before the time of hire, the Company will provide new covered employees with a written work schedule that runs through the last date of the currently posted schedule.  During employment, the Company will also post a written work schedule (including the employees’ shifts at the worksite, the specific start and end times for each employee’s regular shifts at the worksite, and whether or not they are scheduled to work that week) at least 10 days before the first day of any new schedule.  The work schedule will be posted. 

##### Schedule Changes  

In the event of a Company-initiated change to the posted work schedule, the Company will notify the affected employee of the need for the change.  Except in the case of an Emergency, employees can decline, without penalty, any hours or shifts in addition to those reflected in the posted work schedule.  For purposes of this policy, an “Emergency” is:  a fire, flood or natural disaster; severe weather that threatens an employee or public safety; threats to the Company or Company property; a State of emergency declared by the Governor of Texas; or a significant disruption or the risk of significant disruption to passenger air travel.   
	
Employees can voluntary consent to work additional hours or shifts by submitting a Schedule Change Consent Form for each day on which the employee agrees to work additional hours or shifts.  In the event of an Emergency, the Company may require an employee to work additional hours or shifts without obtaining voluntary consent. Any additional hours or shifts required in an Emergency without the employee’s voluntary consent will be paid at a rate of three times the employee’s regular hourly rate of pay.   

If the Company reduces an employee’s hours from what is included in the posted work schedule, the Company will pay the employee for one-half of the total hours reduced at their regular hourly rate of pay, unless the reduction of hours is due to an Emergency.   

##### Retaliation Prohibited  

The Company will not retaliate or tolerate retaliation against employees for exercising or attempting to exercise their rights protected under the FOSSO.  

### TIME OFF AND LEAVES OF ABSENCE  

#### Military Leave  

Employees that are called by the governor to active state duty as members of the Texas National Guard or state militia are entitled to the rights, privileges, benefits and protections with respect to employment that are set forth in the federal Uniformed Services Employment and Reemployment Rights Act (USERRA). Accordingly, if eligible employees are called to active duty, they are entitled to a leave of absence in accordance with the Military Leave Policy set forth in the National Handbook.  

#### Time Off to Vote  

The Company encourages all employees to fulfill their civic responsibilities and to vote in all public elections. Most employees’ schedules provide sufficient time to vote either before or after working hours.
If employees have less than two consecutive hours before or after work to vote, they will be allowed to reasonable time off to vote without loss of pay.
The Company asks that employees provide reasonable advance notice of the need for time off to vote so that the time off can be scheduled to minimize disruption to normal work schedules. Proof of having voted may be required.  

#### Political Leave  

The Company will not terminate or otherwise discriminate against employees if they take a leave from work in order to attend a precinct convention or attend a county, district or state convention as a delegate. Such leave is unpaid except that exempt employees will receive pay when required by applicable federal or state law.  

#### Jury Duty Leave  

The Company encourages all regular full-time and part-time employees to fulfill their civic responsibilities and to respond to jury service summons or subpoenas, attend court for prospective jury service or serve as a juror or grand juror. Under no circumstances will employees be terminated, threatened, intimidated coerced, or penalized because they request or take leave in accordance with this policy.  

Employees must provide their supervisor with notice of any jury summons, grand jury summons or subpoena within a reasonable amount of time after receipt and before their appearance is required. Verification from the court clerk of having served may also be required.  

Time spent engaged in attending court for prospective jury or grand jury service or for serving as a juror or grand juror is not compensable except that exempt employees will not incur any reduction in pay for a partial week’s absence due to jury duty or grand jury duty. Employees who are absent from work while participating in the jury selection process or while serving as a juror or grand juror will not be asked or required to use any annual, vacation or sick leave during the absence.  

#### Time Off to Appear In Court or Attend Proceedings  

Employees may take time off from work for the following reasons:  

- To attend juvenile court proceedings when required to do so as the parent or guardian of the juvenile; or  
- To comply with a valid subpoena to appear at a civil, criminal, legislative or administrative proceeding.  

Such leave is unpaid except that exempt employees will receive pay when required by applicable federal or state law.  

If employees give the Company notice of their intention to return to work after being released from a subpoena or attending a juvenile proceeding, they will usually be returned to the same position. Reemployment may be denied, however, if the Company’s circumstances have changed making reemployment impossible or unreasonable.  

#### Participation in Emergency Evacuations  

Employees will not be subject to termination, demotion, suspension or any other adverse employment action for leaving work in order to participate in a general public evacuation ordered under an emergency evacuation order.  

An emergency evacuation order is an official statement issued by the governing body of the state or of a political subdivision of the state recommending the evacuation of all or part of the population of an area stricken or threatened with a disaster.  

### SAFETY AND SECURITY  

#### Weapons in the Workplace  

In the interest of maintaining a workplace that is safe and free of violence, and in accordance with the policy set forth in the National Handbook, the Company generally prohibits the presence or use of firearms and other weapons on the Company’s property, regardless of whether or not the person is licensed to carry the weapon. In addition, in accordance with Section 30.06, Texas Penal, a person licensed under Subchapter H, Chapter 411, Government Code (handgun licensing law), may not enter company property with a concealed handgun. Also, pursuant to Section 30.07, Penal Code (trespass by license holder with an openly carried handgun), a person licensed under Subchapter H, Chapter 411, Government Code (handgun licensing law), may not enter company property with a handgun that is carried openly.  

For purposes of this policy, company property is defined as all company-owned or leased buildings and surrounding areas such as sidewalks, walkways, driveways and parking lots under the Company’s ownership or control, except as noted below. In addition, this policy applies to all company-owned or leased vehicles. Dangerous weapons include, but are not limited to, firearms, knives, explosives and other similar weapons.  

Notwithstanding any of the foregoing restrictions, the Company does not prohibit those who lawfully possess firearms or ammunition from storing their firearms or ammunition inside their locked, privately owned vehicles in parking lots or other parking areas provided by the Company. Such lawfully possessed firearms and ammunition may not be removed from the employees’ personal vehicle or displayed to others.  

To the extent that parking lots or other parking areas utilized by employees are not owned by the Company, the owners of such parking lots may have additional rules that impact the storage of firearms and ammunition. In such situations, employees must comply with the rules of both the Company and the property owner, in keeping with applicable law.  

Employees who violate this policy are subject to disciplinary action up to and including termination of employment. A visitor who violates this policy may be removed from the property and reported to authorities. This policy does not apply to law enforcement personnel or security personnel who are engaging in official duties.  

The Company reserves the right at any time and at its discretion to search all company-owned or leased vehicles and all vehicles, packages, containers, briefcases, purses, lockers, desks, enclosures and persons entering its property for the purpose of determining whether any weapon has been brought onto its property or premises in violation of this policy. If employees fail or refuse to promptly permit a search under this policy they will be subject to disciplinary action up to and including termination.  

Employees who have questions regarding this policy should contact their supervisor, another member of the management team or Human Resources.  

#### Smoke-Free Workplace  

The Company prohibits smoking in the workplace and most other enclosed areas. Employees wishing to smoke must do so outside company facilities during scheduled work breaks.  

Employees that observe other individuals smoking in violation of this policy have a right to object and should report the violation to their supervisor or to another member of management. Employees will not be disciplined or retaliated against for reporting smoking that violates this policy.  

Employees who violate this policy may be subject to disciplinary action up to and including termination of employment.   

#### Cell Phone Use / Texting While Driving  

As is set forth in the National Handbook, the Company prohibits employees from using cellular phones for business reasons while driving or for any reason while driving or for any reason while driving for work-related purposes or driving a company-owned vehicle. Employees should also be aware that using a cell phone to write, send, or read a text message while driving is a violation of Texas law, in addition to being a violation of Company policy.


## Utah  

### GENERAL INFORMATION  

#### About This Utah Supplement  

ConstructConnect is committed to workplace policies and practices that comply with federal, state and local laws.  For this reason, Utah employees will receive the Company’s national handbook (“National Handbook”) and the Utah Supplement to the National Handbook (“Utah Supplement”) (together, the “Employee Handbook”).  

The Utah Supplement applies only to Utah employees.  It is intended as a resource containing specific provisions derived under Utah law that apply to the employee’s employment.  It should be read together with the National Handbook and, to the extent that the policies in the Utah Supplement are different from, or more generous than those in the National Handbook, the policies in the Utah Supplement will apply.  

The Utah Supplement is not intended to create a contract of continued employment or alter the at-will employment relationship.  **Only the President/Owner of the Company or that person’s authorized representative has the authority to enter into an agreement that alters the at-will employment relationship and any such agreement must be in writing signed by the President/Owner of the Company or an authorized representative.**  

If employees have any questions about these policies, they should contact their Human Resources representative.  

### COMMITMENT TO DIVERSITY  

#### Equal Employment Opportunity  

As set forth in the National Handbook, ConstructConnect is committed to equal employment opportunity and to compliance with federal antidiscrimination laws. We also comply with Utah law, which prohibits discrimination and harassment against employees or applicants for employment based on race, color, religion, sex, sexual orientation, gender identity, pregnancy (including childbirth and pregnancy-related conditions, as well as breastfeeding or medical conditions related to breastfeeding ), age (40 and over), national origin, disability, genetic information and membership in the armed forces or reserves. The Company will not tolerate discrimination or harassment based upon these characteristics or any other characteristic protected by applicable federal, state or local law.  The Company also does not discriminate against employees due to their lawful expression or expressive activity outside the workplace of their religious, political or personal convictions, unless that expression or expressive activity is in conflict with the Company’s essential business-related interests.  

#### Accommodations for Pregnancy, Childbirth, Breastfeeding, or Related Conditions  

Employees may request a reasonable accommodation related to pregnancy, childbirth, breastfeeding, or a related condition. A reasonable accommodation will be provided unless it imposes an undue hardship on the Company’s operations.   
The Company may require an employee to provide a certification from a health care provider concerning the medical advisability of the requested accommodation.  The certification must include the date the accommodation becomes medically advisable; the probable duration of the accommodation; and an explanatory statement as to the medical advisability of the accommodation.  Medical certification will not be required if the accommodation request is limited to more frequent restroom, food, or water breaks.  

Employees who have questions about this policy or who wish to request a reasonable accommodation under this policy should contact a Human Resources representative.  

The Company will not require an employee to terminate employment or deny employment opportunities because of an employee’s need for a reasonable accommodation related to pregnancy, childbirth, breastfeeding, or related conditions, unless the accommodation would create an undue hardship on the Company’s operations.  

### TIME OFF AND LEAVES OF ABSENCE  

#### Jury Duty Leave  

The Company encourages all employees to fulfill their civic responsibilities and to respond to jury service subpoenas, attend court for prospective jury service or serve as a juror. Under no circumstances will employees be terminated, threatened, coerced, or penalized because they respond to a jury service subpoena, attend court for prospective jury service or serve as a juror.  

Employees must provide their supervisor with notice of any jury summons or subpoena within a reasonable amount of time after receipt and before their appearance is required.  Verification from the court clerk of having served may also be required.  

Time spent engaged in attending court for prospective jury service or for serving as a juror is not compensable except that exempt employees will not incur any reduction in pay for a partial week's absence due to jury duty. Employees who are absent from work while participating in the jury selection process or while serving as a juror will not be asked or required to use any annual, vacation or sick leave during the absence.  

#### Minor Child Court Appearance Leave  

Employees who are the parent, guardian or legal custodian of a minor child or ward who must appear in court will be allowed leave for the purpose of appearing in court with the child.  Employees seeking leave under this policy must request leave at least seven days in advance of the need for leave.  However, if an employee does not receive notice of the hearing seven or more days in advance, then the employee must request leave within 24 hours of receiving notice of the hearing.  

Leave under this policy will be unpaid except that exempt employees may be paid as required by applicable law.  

#### Time Off to Vote  

The Company encourages all employees to fulfill their civic responsibilities and to vote in all public elections. Most employees’ schedules provide sufficient time to vote either before or after working hours.  

Employees who have less than three hours outside of working hours to vote while the polls are open may take up to two hours off from work, without loss of pay, to vote. Any additional time off will be without pay for nonexempt employees.  

The Company asks that employees request time off to vote from their supervisor at least one day prior to Election Day so that the time off can be scheduled to minimize disruption to normal work schedules. The Company may specify the hours during which the employee may be absent. If, however, the employee requests leave at the beginning or end of the shift, the Company will grant the request.  

Proof of having voted may be required.  

#### Military Leave  

In addition to the military leave rights set forth in the National Handbook, Utah employees who are members of the United States military reserves are entitled under Utah law to an unpaid leave of absence for active duty, active duty for training, inactive duty training, or state active duty for a period of up to five years. Returning employees will be reinstated with seniority, status, pay, and vacation reinstated at the level the employee would have had if they had not been absent for military service.  

#### Emergency Responder Leave  

Employees who are emergency services volunteers will be allowed time off to respond to an emergency.  For purposes of this policy, an “Emergency Services Volunteer” means a volunteer firefighter, emergency medical services personnel, or a person mobilized as part of a posse comitatus.  

Employees must make a reasonable effort to notify the Company on each occasion that they will be late to or absent from work to respond to an emergency.  

The Company may require that employees who need time off to respond to an emergency as an Emergency Services Volunteer submit a written statement from the person who supervises them in the course of performing duties as an Emergency Services Volunteer, stating that the employee responded to an emergency and providing the time and date of the employee’s service.  

The Company will not terminate an employee solely for being an Emergency Services Volunteer or for being absent from or late to work as a result of responding to an emergency as an Emergency Services Volunteer.  

### WORKPLACE SAFETY AND SECURITY  

#### Cell Phone Use/Texting While Driving  

As set forth in the National Handbook, the Company prohibits employees from using cellular phones for business reasons while driving or for any reason while driving for work-related purposes or driving a company-owned vehicle. Employees should also be aware that Utah law prohibits all drivers from manually dialing a telephone number while driving or using a wireless telephone except in hands-free mode. Writing, sending or reading a written communication (including text message, instant message and electronic mail), dialing a phone number, accessing the internet, viewing or recording video or entering data into a handheld wireless communication devise while operating a motor vehicle on a highway are also prohibited under Utah law.  

Under Utah law “handheld wireless communication device” means a handheld device used for the transfer of information and includes: wireless telephones, text messaging devices, laptops, or any substantially similar communication device that is readily removable from the vehicle and is used to write, send or read text or data through manual input.  

#### Weapons in the Workplace  

In the interest of maintaining a workplace that is safe and free of violence, and in accordance with the policy set forth in the National Handbook, the Company generally prohibits the presence or use of firearms and other weapons on the Company’s property, regardless of whether or not the person is licensed to carry the weapon. In compliance with Utah law, the Company permits employees who lawfully possess firearms or ammunition to store or transport their firearms or ammunition inside their locked, privately-owned vehicles in the Company’s parking lots or other parking areas provided by the Company. Such lawfully possessed firearms and ammunition may not be removed from the employees' personal vehicle or displayed to others.  

The Company will not be liable for criminal or civil actions resulting from the theft of a firearm from an employee’s vehicle, and the Company will not provide additional security for employees.  

#### Smoke-Free Workplace  

The Company prohibits smoking, including the use of electronic cigarettes, in the workplace. Employees wishing to smoke must do so outside company facilities during scheduled work breaks. Smoking outside the workplace is prohibited within 25 feet of any entrance, window or ventilation intake of the workplace.  

Employees who observe other individuals smoking in the workplace in violation of this policy have a right to object and should report the violation to their supervisor or another member of management. Employees will not be disciplined or retaliated against for reporting smoking that violates Utah law or this policy.  

Employees who violate this policy will be subject to disciplinary action up to and including termination of employment.  

