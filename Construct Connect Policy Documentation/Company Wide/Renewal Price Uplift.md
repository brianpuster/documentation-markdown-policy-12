title: Renewal Price Uplift Policy
published: 2020-12-18
author: Jeff Cryder
updated: 2021-05-15


# Policy Level
Very Important  

# Approvers  
- Dave Storer, VP Controller 
- Jim Hill, EVP General Manager Trade Contractors 
- Howard Atkins, EVP General Manager Building Product Manufacturers 
- Jon Kost, EVP General Manager General Contractors 

# Location or Team Applicability
Company-wide

# Purpose

The purpose of this policy is to specifically outline the Company's renewal price uplift rules applied to all expiring contracts by Zuora (the Company's subscription management system). Zuora's application of these automated rules against expiring subscriptions results in the creation of renewal pricing quotes for the upcoming renewal term.  Renewal quotes are to be reviewed by assigned CSMs prior to submission to the expiring subscriber for consideration.  CSMs may make changes to Zuora's rule-based pricing based on subscriber's experience (occurs infrequently), changes in products sold or number of seat licenses. Discretionary modifications to Zuora's rule-based pricing are subject to team-specific leadership approvals.  See reference links below.

**Note - Prior to modifying any element of this Policy in Zuora, the proposed change must be approved by the Company's VP Controller, as well as, the requisite EVP General Manager(s) for the segment(s) impacted by the modification.**  

# Policy

Renewal Price Uplift Methods are set at the product level. The Price Uplift method assigned to each product can be found in this [Salesforce report](https://constructconnect.lightning.force.com/lightning/r/Report/00O0e000005Khka/view). All products are assigned to one of these five Renewal Run Uplift methods:  
- No Uplift  
- Flat Increase  
- Step Once to List  
- iSqFt Step Up, and  
- Step Up to List  

Renewal Price Uplift Rules work with the Methods and are the specific parameters applied by Zuora within during each renewal quote run.  They are enumerated in this [Confidential Document](https://constructionmd.sharepoint.com/:w:/r/sites/RenewalPriceUpliftPolicyTeam/_layouts/15/Doc.aspx?sourcedoc=%7BB5B2F02F-992F-417C-BE3C-0C45A264EE6C%7D&file=Renewal%20Price%20Uplift%20Policy.docx&action=default&mobileredirect=true).  The Rule Sets are organized by:
- Rule Set 1 - Trade Contractors  
- Rule Set 2 - Building Product Manufacturers  
- Rule Set 3 - General Contractors  
- Rule Set 4 - All Other Products  

**If you do not have permission to open the Confidential Document, please contact <jeff.cryder@constructconnect.com> or <dave.storer@constructconnect.com>.**

Discretionary Pricing and Discounting Guidebook reference links:  
- [Trade Contractors](/pages/policy/Construct%20Connect%20Policy%20Documentation/Trade%20Contractor/Pricing%20and%20Discounting)
- [General Contractors](/pages/policy/Construct%20Connect%20Policy%20Documentation/General%20Contractor/Pricing%20and%20Discounting
- [Building Product Manufacturers](/pages/policy/Construct%20Connect%20Policy%20Documentation/Building%20Product%20Manufacturer/Pricing%20and%20Discounting)  

## Revision History
2021-05-15 transferred more of policy details from protected Price Uplift Pricing Rules document into the publicly available policy guideline.  
