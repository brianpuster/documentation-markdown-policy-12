title: Sales Prospecting and Marketing Policy  
author: Steve Testa, VP Strategic Initiatives & Continuous Improvement  
published: 2020-01-02  
policy level:  Very Important  
approver(s): Jeff Cryder, Jim Hill, Jon Kost  
applicable location: All locations, all teams  
  


# Purpose  

The purpose of this policy is to ensure our organization is in compliance with all regulations and laws regarding data privacy, do not call, do not email, canned spam and telecommunications.  

# Email and call list creation  

All email and call lists created must account and adhere to all contact preferences, regulations and laws.  

# Email marketing and bulk email  

Bulk email (for any purpose) by any individual is prohibited. Any and all email marketing is a function of marketing and product teams and can only be done in and through those teams with full adherence to any and all contact preferences, regulations and laws.  

# Tools and technology Use  

Tools and technology used for email and phone is provided by the company. The use of any third party tools and technology for the purpose of email and/or telephone communications must go through and adhere to the Software Purchasing Policy and/or Consulting and Professional Services Policy. Unauthorized use of any tools and technology is prohibited.  

