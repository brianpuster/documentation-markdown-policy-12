title: 2021 Hourly Payroll Target Dates    
published: 2020-12-18     
author: Monica Brooksbank    


# Policy Level: 
Very Important

# Distribution:  
Company-wide

# Effective date:  
2021-01-01

# Approval  
Jeff Cryder, Executive VP

# 2021 - Semi Monthly Pay Schedule for Hourly Team Members

|No of Weeks|Hours Worked for the weeks of||Will be Paid on the Following Semi Monthly Pay Dates|   
|:-:|:-:|:-:|:-:|
||**Start Date**|**End Date**|| 
||12/21/2020|12/27/2020||
|1.5|12/28/2020|12/31/2020|Friday, January 15, 2021 Pay Date|  
||1/1/2021|1/3/2021||  
||1/4/2021|1/10/2021||  
||1/11/2021|1/17/2021||  
|3.5|1/18/2021|1/24/2021|Friday, January 29, 2021 Pay Date|  
||1/25/2021|1/31/2021||  
|2.0|2/1/2021|2/7/2021|Friday, February 12, 2021 Pay Date|  
||2/8/2021|2/14/2021||  
|2.0|2/15/2021|2/21/2021|Friday, February 26, 2021 Pay Date|  
||2/22/2021|2/28/2021||  
|2.0|3/1/2021|3/7/2021|Monday, March 15, 2021 Pay Date|  
||3/8/2021|3/14/2021||  
|2.0|3/15/2021|3/21/2021|Wednesday, March 31, 2021 Pay Date|  
||3/2/2021|3/28/2021||  
|2.0|3/29/2021|4/4/2021|Thursday, April 15, 2021 Pay Date|  
||4/5/2021|4/11/2021||  
||4/12/2021|4/18/2021||  
|3.0|4/19/2021|4/25/2021|Friday, April 30, 2021 Pay Date|  
||4/26/2021|5/2/2021||  
|2.0|5/3/2021|5/9/2021|Friday, May 15, 2021 Pay Date|  
||5/10/2021|5/16/2021||  
|2.0|5/17/2021|5/23/2021|Friday, May 28, 2021 Pay Date|  
||5/24/2021|5/30/2021||  
|2.0|5/31/2021|6/6/2021|Tuesday, June 15, 2021 Pay Date|  
||6/7/2021|6/13/2021||  
|2.0|6/14/2021|6/20/2021|Wednesday, june 30, 2021 Pay Date|  
||6/21/2021|6/27/2021||  
|2.0|6/28/2021|7/4/2021|Thursday, July 15, 2021 Pay Date|  
||7/5/2021|7/11/2021||  
||7/12/2021|7/18/2021||  
|3.0|7/19/2021|7/25/2021|Friday, July 30, 2021 Pay Date|  
||7/26/2021|8/1/2021||  
|2.0|8/2/2021|8/8/2021|Friday, August 13, 2021 Pay Date|  
||8/9/2021|8/15/2021||  
|2.0|8/16/2021|8/22/2021|Tesday, August 31, 2021 Pay Date|  
||8/23/2021|8/29/2021||  
|2.0|8/30/2021|9/5/2021|Wednesday, September 15, 2021 Pay Date|  
||9/6/2021|9/12/2021||  
|2.0|9/13/2021|9/19/2021|Thursday, September 30, 2021 Pay Date|  
||9/20/2021|9/26/2021||  
||9/27/2021|10/3/2021||  
|3.0|10/4/2021|10/10/2021|Friday, October 15, 2021 Pay Date|  
||10/11/2021|10/17/2021||  
|2.0|10/18/2021|10/24/2021|Friday, October 29, 2021 Pay Date|  
||10/25/2021|10/31/2021||  
|2.0|11/1/2021|11/7/2021|Monday, November 15, 2021 Pay Date|  
||11/8/2021|11/14/2021||  
|2.0|11/15/2021|11/21/2021|Tuesday, November 30, 2021 Pay Date|  
||11/22/2021|11/28/2021||  
|2.0|11/29/2021|12/5/2021|Wednesday, December 15, 2021 Pay Date|  
||12/6/2021|12/12/2021||  
|2.0|12/13/2021|12/19/2021|Thursday, December 31, 2021|  

