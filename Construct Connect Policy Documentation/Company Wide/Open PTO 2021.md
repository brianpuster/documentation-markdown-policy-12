title: Open Paid Time Off (PTO) Plan 2021       
published: 2021-01-06        
author: Julie Storm - Chief People Office    
status: draft   


# Our Policy

Our Open Paid Time Off policy is simple. Take the time you need. We assume you will take care of your responsibilities and customers before you leave and upon your return. We also assume you will manage your time away in a way that allows you meet the expectations of your specific role.  

# What It Means  

- Open Paid Time Off (PTO) is an all-purpose time off policy for eligible team members to use for vacation, illness or injury and personal business.  
- Because it is Open PTO, nothing accumulates, and nothing carries over in subsequent calendar years. Nothing will be owed to you if you leave.  

# How It Work   

You can request PTO as you see fit. Time off is yours to enjoy when you want it or when you need it so long as you follow a few courtesies to ensure the right people are notified.  

- Tell your supervisor, at least five days in advance of any **expected** PTO, that you would like to take time off. The more in advance you plan, the better.  
- Your manager has a good perspective on collective time off for the rest of your team and on the requirements of the team in meeting deliverable deadlines. It is your manager’s job to determine if s/he thinks your work can be appropriately covered by others.  
- Your manager will make reasonable efforts to grant your request. Please understand that due to staffing needs and when the business necessitates it, not all PTO requests can be approved. If it turns out that it cannot, courtesy goes to the person/people who planned in advance of you.  
- **Unexpected** PTO should be communicated to your manager by e-mail 2 hours before you would normally start work so that they are aware and can plan accordingly. Let them know why you are taking it and when you will return.  
- Every once in a while, you may want to take some **extended** PTO. We want you to be able to take advantage of rare opportunities that come your way. We consider 10 or more consecutive workdays to be extended time off. Advance planning with your manager will be needed for such requests and will only be approved for certain circumstances that are not common  

# How to Request PTO  

- Request your time off through e-mail to your direct manager.  
- Wait for a return e-mail from your manager approving the time off.  
- Put the time you will be away both on your Outlook calendar (show it as Out of Office or Busy, not Free) and add it to the Out of Office Master Calendar on Outlook.  

If a team member is unable to meet the expectations as outlined, ConstructConnect reserves the right to revoke the approved PTO request.  

If gross abuse of the Open PTO guidelines is observed, we reserve the right to have a serious discussion about it with you, and this may include being removed from the Open PTO Plan and returning to the standard PTO accrual method.  

# PTO and Leaves of Absence  

- In the event you must be off work for an extended period of time due to illness or injury, (Leave of Absence) Open PTO will apply for the first week. Short or Long-Term Disability must be utilized for the remainder of the time off. This time off will run in conjunction with FMLA, beginning on your first day off.  
- If the time off is for the birth of a child, time off will be taken as follows  
    - Maternity leave will be paid at 100% of base salary as follows  
        - 6 weeks of paid Maternity Leave followed by 2 weeks of Paternal Leave and then 2 weeks of Open PTO for a total of 10 weeks of maternity leave benefits  
    - Paternal Leave will be paid at 100% of base salary as follows  
        - 2 weeks of paid Paternal Leave plus a maximum of 1 additional week of Open PTO for a total of 3 weeks of paternal leave benefits.  
- All medical leaves will run in conjunction with FMLA from the first day off and all pertinent documentation of your leave will need to be completed before the actual leave occurs.  
- This Open PTO policy does not apply to FMLA (Family and Medical Leave), Military Leave, Military Caregiver Leave, Qualifying Exigency Leave, and extended personal or medical leaves when FMLA does not apply.  

# NOTE TO TEAM MEMBERS  

Where applicable state laws or local ordinances (including Paid Sick Leave laws or ordinances) provide greater benefits than those described in this policy, the Company will comply with the state law/local ordinance.  

This Policy supersedes time off policies as documented in the Employee Handbook or in other written or electronic versions referencing Paid Time Off that you may have previously received.  


☐  I elect to participate in the Open PTO program for 2020 and agree to the terms above.    

☐  I elect to participate in the standard PTO program for 2020.  
