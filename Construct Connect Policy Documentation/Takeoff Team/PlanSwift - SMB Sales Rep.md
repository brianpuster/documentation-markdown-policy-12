title: PlanSwift - SMB Sales Rep PIP  
published: 2019-09-11  
author: Jim Hill - EVP and GM Trade Contractor  
updated: 2021-01-01  


# Performance Improvement Plan Process

Sales Consultants’ performance will be reviewed by Sales Management on a monthly basis at the beginning of each month for the previous month’s performance against the individual’s established minimum monthly threshold. If a Sales Consultant does not meet their applicable threshold, the performance improvement plan process will begin as outlined below. Sales Consultants who have not yet completed the 90-day orientation period will not be subject to this process, but will be held responsible for a successful orientation period, which consists of:

# 90-Day Orientation Period Performance Guidelines

- Satisfactory attendance - outlined in the Employment Handbook
- Demonstrate a positive attitude that is open coaching and feedback
- Completion of any required certifications and trainings
- Build knowledge and skills by completing the *New Sales Consultant Competency Checklist*

*If team members are unable to demonstrate their ability to achieve a satisfactory level of performance during the orientation period, their employment could be terminated.*  
*Upon successful completion of the orientation period team members will enter the ongoing employment classification below:*  

# Required Minimum Performance (RMP)  

- 4-7 month tenured Sales Consultant: 40% of average team production  
- 8-11 month tenured Sales Consultant: 55% of average team production  
- 12+ month tenured Sales Consultant: 70% of average team production  


# Performance Improvement Process Post Orientation Period

- Verbal Warning: Below RMP for 1 month.  
- 1st Stage Improvement Plan: Below RMP for 2 consecutive months.  
- 2nd Stage Improvement Plan: Below RMP by 10% or more in a single month OR below RMP for 3 consecutive months.  
- While on 2nd Stage Improvement Plan: You MUST exceed RMP. Failure to do so will result in termination.  
- In order to be removed from a performance improvement plan you MUST maintain an average of 10% above RMP for a rolling 3-month period OR 2 consecutive months.  

# General  

- Minimum monthly threshold, as it pertains to the performance plan, will be pro-rated for approved FMLA or bereavement absences.  
- Sales Representatives with planned paid time off (PTO) scheduled, and approved, at least one month in advance for a minimum of 4 consecutive days will only be responsible for the pro-rated portion of the goal associated with the number of business days worked.  
- This is a guideline for a performance improvement plan and is subject to change at the discretion of management.  

# Acknowledgement

I have read, and acknowledge, the PlanSwift Sales Consultant Performance Improvement Plan Process to be the guideline to which I will be held accountable regarding my performance.
