title: IT Time Tracking/Reporting   
author: Radhika Santhanam, VP of IT Operations & Development PMO   
published: 2021-02-10    
policy level: Very Important     
approver(s): Bob Ven, CTO, Dana Oney, SVP of IT Operations, Jason Shawn, Senior Director of Cloud Engineering  
applicable locations: All locations, all teams within IT Operations   
effective date: 2021-03-01  

--- 

It is the responsibility of every team member (Engineers, Technical writers, Administrators) working within the IT Operations Scrum/Kanban teams (Development Operations, Security, Infrastructure) to specify time estimates planned for each of the tasks assigned to them. 

- Scrum team – Prior to the start of the sprint 
- Kanban team- Prior to pulling work into implementation (Tasks in To Do column within the Kanban board.   

The team members are required to maintain the time spent (actuals) on each of their assigned tasks and update Jira by every Friday 5 pm.   

The fields that are required to be filled for time estimates within Jira Tasks/Stories/Bugs include 

- Original Estimate (planned)   
- Time Spent (actual)   

The time reports (rolled up data) for the respective teams will be published to leadership on a biweekly basis.

--- 

 

 