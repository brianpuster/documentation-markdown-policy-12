title: Mid-Market BDR PIP  
published: 2019-09-11  
author: Jim Hill - EVP and GM Trade Contractor  
updated: 2019-10-01 


# Performance Improvement Plan Process

Business Development Representatives’ performance will be reviewed by Sales Management on a monthly basis at the beginning of each month for the previous month’s performance against the individual’s established minimum monthly threshold. If a business development representative does not meet their full threshold, the performance improvement plan process will begin as outlined below. Business Development Representatives who have not yet completed the 90-day orientation period will not be subject to this process, but will be held responsible for a successful orientation period, which consists of

# Orientation Period Performance Guidelines

- Satisfactory attendance - outlined in the Employment Handbook
- Attitude that is open coaching and feedback
- Achieve a minimum of 60% of the attended demo ramp metric goal each month
- Completion of all required Certifications

*If team members are unable to demonstrate their ability to achieve a satisfactory level of performance during their orientation period their employment could be terminated.*  

*Upon successful completion of the orientation period team members will enter the "regular" employment classification.*    

# Performance Improvement Process Post Orientation Period  

## Part 1 - Performance to ACV Goal

- Missed Goals below 60% of average quota for rolling two-month period: 1st Stage Improvement Plan
- Missed Goal below 60% for a rolling three month period: 2nd Stage Improvement Plan. Missed goal below 50%: up to and including termination.  
- While on 2nd Stage Improvement Plan you must hit minimum 65% for rolling three month period. Failure to do so will result in termination.  
- Stages run consecutively.  
- In order to be removed from performance improvement plan you must maintain 75% of goal for rolling three month period.  

## Part 2 - Connect & Sell Usage   

- Each BDR is required to have 65 conversations on the Connect & Sell application during a 5-day work week (prorated for PTO and company recognized holidays).
- Missed weekly 65 conversation requirement – 1st Offense: Written warning
- Missed weekly 65 conversation requirement – 2nd Offense: Terminatio

# Policy Changes

Management reserves the right to by-pass stages of the improvement process depending on the nature of the issue.  

Minimum monthly threshhold as it pertains to the performance plan will be pro-rated for approved FMLA or bereavement absences.  

BDRs’ with planned time off (PTO time) scheduled and approved at least a month in advance for a minimum of 3 consecutive days will only be responsible for the pro-rata portion of the goal associated with the number of business days worked.  

This is a guideline for a performance improvement plan and is subject to change at the discretion of the CRO or CPO of ConstructConnect. 

# Acknowledgement

I have read and acknowledge the Mid-Market Business Development Representatvie Performance Improvement Plan Process to be the process I will be held accountable to regarding my performance.
