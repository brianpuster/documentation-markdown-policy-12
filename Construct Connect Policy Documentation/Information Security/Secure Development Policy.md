title: Secure Development Policy
author: vCISO
published: 2020-09-28
policy level: Annual General
data classification: Internal
location or team applicability: All locations, all teams
approvers: Bob Ven - CTO/CIO, Dana Oney - Sr. VP of IT Operations, Radhika Santhanam - Director of Development PMO




# GENERAL

# Objective

The  purpose  of  this  policy is to guide the development of secure and resilient software at ConstructConnect. 

## Scope

This policy applies to all software developed in-house or by third party contractors as an extension of ConstructConnect Development and Release resources. 

## Responsibilities

- **Risk Committee** is responsible for reviewing high level metrics produced by the CISO and ensuring that all teams have the necessary budget and technologies to support the requirements outlined in this policy.

- **Corporate Information Security Office (CISO)** is responsible for oversight and general management of this policy. 

- **InfoSec Team** is responible for collecting metrics at various points throughout the ConstructConnect development lifecycle to measure the effectiveness of software security program. 

- **Managers** are responsible for ensuring that their respective teams follow the security requirements outlined in this policy.

- **IT Team** is responsible for setting up tooling inside of build and deployment environments to support security analysis and vulnerability management. 

- **Development and Release Teams** are responsible for developing and maintaining software in accordance with the requirements outlined in this policy.

## Review

This document must be reviewed for updates annually or whenever there is a material change in the Company's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CISO owns this Policy, but any update or change must be approved by the Risk Committee.

##### Environments

- **Use of Production Environments:** A production environment contains application or service deployments that is ready and approved for its intended use by end users or other systems. The production environment must meet all standards for compliance. 
- **Pre-Production Environments:** All environments that are not production are pre-production environments, including Development, QA, and Staging. Pre-production environments contain an application or service deployment undergoing verification steps such as quality assurance or security testing before final deployment into a production environment. Pre-production environments should meet all the same standards as their respective production environments due to the data classification of any testing data resident inside the environment. Production data should not be used for any purposes inside of Pre-Production environments.

##### Third Party Code

Application developers should ensure that third party code that is used during development is secure and vetted from a license perspective. 

###### Updates

Whenever possible, without breaking functionality, developers should ensure that all third party libraries are updated with the most recent version to protect against vulnerabilities. Exceptions to this policy must be handled according to the established Risk Exception Procedure documented here: (link).

###### Dynamic Inclusion

Languages and libraries that dynamically include other nested software at runtime must be documented and disclosed, including all dependencies.

###### Software Composition Analysis

Software composition analysis tooling should be set up and run regularly against all third party code to identify potential vulnerabilities, malicious code, or outdated libraries. 

##### Secure Coding

Secure software development practices should be incorporated into the developer’s workflow, ensuring that any systems, services, or features built are resilient to attacks such as those outlined in the OWASP Top 10 or OWASP ASVS. 

###### Input Validation

Developers should ensure that all untrusted (e.g., data originating from an outside source such as a user or another system) is validated. OWASP provides guidance on input validation. Only the data type and format expected should be allowed into the application and subsequently processed. 

###### Error Handling

Error messages should not include detailed system information, deny service, or impair security mechanisms in any way. Errors should be informative and relevant for the user experience where detailed information is captured and logged on the backend for debugging purposes. All exceptions must be logged and handled in code.

Additionally, there must be defined procedures for recovery, notification to user, or other appropriate actions to handle errors. There should be no unhandled errors.

###### Authentication

Wherever possible, authentication should occur through centralized services using protocols such as OAuth, SAML, or LDAP. Multi-factor authentication should also be enabled and enforced wherever possible. 

###### Least Privilege 

Developers and users of any system should adhere to the principles of least privilege; wherever feasible, providing access to resources based on role, association, or membership rather than by individual.  

###### Data Encryption

All data should be encrypted both in rest and in transit with protocols and key lengths appropriate for the data being protected. Data should be protected in accordance with the Data Management Policy.

###### Logging

Developers should make use of application logs to the extent possible and practical. Logs should be collected and managed in accordance with the Audit Logging and Monitoring Policy.

##### Security Assessments

###### Penetration Testing

Applications should be tested, on an annual basis at minimum for possible security issues. Third party penetration tests should be conducted with an appropriate scope of the network infrastructure, host server(s), and application components.

###### Automated Scanning

Automated security tests should be built into the development pipeline to occur prior to staging deploy, ideally before end of QA, if feasible. If this is not feasible, out-of-band automated security scans should be regularly conducted.

##### Security By Design

Whenever possible, for new products, new features, and major changes to existing functionality, the plan must include security requirements as part of the completion and acceptance criteria. 

##### Security Code Review

During the development process for new products, new features, and major changes to existing functionality, developers should ensure that their code is reviewed for quality and security issues, either as a peer code review before merge or before a build and deployment occurs. 

##### Tracking and Approval  

There  is  a  formal,  documented  process  for deployment of production systems such as testing to the production environment upon successful completion of quality assurance activities of those applications. Individuals with access to the testing environment should not have access to the production environment. Approvals  for  transfer  to  production  are  documented. The  approval  process  includes  appropriate representation from various groups, which may include:  Application, Database, Server/Operating System, Security and Network Versions should be tracked and the code should be stored in a secure code repository. Access to the code should be restricted to need-to-know or least privilege to ensure its security. 

Connected Products Security considerations should be placed on connected devices. Procedures should be in place to ensure authenticity of the parts obtained from vendors during procurement process. Procedures should be in place to restrict access to key connectivity components  or  products throughout the supply chain process, such as chain custody. Based on the risk associated with the device and the potential impact to the customer environment it may be used in, adequate security functionality should be enabled to prevent unintended and unauthorized usage.

##### Secure Development Training

Training Developers and those responsible for code review should participate in annual secure coding training. Secure coding training should incorporate defensive coding practices that align with commonly occurring issues such as the OWASP Top 10 as well as those which are frequently identified inside of ConstructConnect applications. 

##### REFERENCES 

###### Regulations.

- PCI-DSS

###### NIST CSF

- Identify
- Protect
- Detect

###### Roper CIS Controls

- Cybersecurity Governance and Risk Management
- Vulnerability Management
- Access Management
- Audit Logging and Monitoring
- Supply Chain Risk Management
- Secure System Development - SDLC
- Penetration Testing and Red Team Exercises

###### Related Policies, Plans, Procedures and Guidelines

- Written Information Security Program
- Policies
        - Access Control Policy
        - Audit Logging and Monitoring Policy
        - Data Management Policy
        - Patching and Vulnerability Management Policy