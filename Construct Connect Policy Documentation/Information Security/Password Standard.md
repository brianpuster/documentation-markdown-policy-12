title: Password Standard   
author: VerSprite vCISO  
published: 2021-04-02  
policy level: Annual General  
data classification: Internal  
approver(s): Bob Ven CTO/CIO, Dana Oney SVP IT Ops  
location or team applicability: All locations, all teams  



# GENERAL

## Objective

The  purpose  of  this  document  is  to  establish strong password standards and provide requirements and recommendations for the protection of accounts and passwords.

## Scope

This Standard applies to ConstructConnect employees, contractors, and vendors who have logical access to ConstructConnect information assets. Password standards must be applied to all ConstructConnect systems or any system that resides at any ConstructConnect facility, has access to a ConstructConnect network, or stores any non-public ConstructConnect information. 

## Responsibilities

- **Risk Committee** is responsible for ensuring that all ConstructConnect employees and systems adhere to this Standard. 
- **Corporate Information Security Office (CISO)** is responsible for oversight and general management of this policy. 
- **InfoSec Team** is responsible for communicating with employees, contractors, and vendors regarding password requirements and enforcing technical controls to ensure compliance. The InfoSec Team will collaborate with the IT Team to identify applicable systems and accounts, and will implement technical controls where feasible. 
- **IT Team** is responsible for implementing technical controls where feasible to ensure compliance with this Standard.
- **Managers** are responsible for ensuring that members of their respective teams are familiar with this Standard and know where to find it.
- **ConstructConnect Team Members** are responsible for familiarizing themselves with and implementing the contents of this Standard.

## Review

This document must be reviewed for updated annually or whenever there is a material change in the Company's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CISO owns this Standard, but any update or change must be approved by the Risk Committee.

# INTRODUCTION

Passwords are an important aspect of computer security. A poorly chosen password may result in unauthorized access and/or exploitation of ConstructConnect's resources. Try to create passwords that can be easily remembered but impossible to reasonably decipher by another person. One way to do this is create a password based on a song title, affirmation, or other phrase. For example, the phrase might be: "This May Be One Way To Remember" and the password could include: "TmB1w2R!" or "Tmb1W>r~" or some other variation.

## Password Requirements
Passwords must contain at least 15 alphanumeric characters
All passwords must be changed every 365 days
Password must NOT be stored in plaintext or flat files (CSV files, for example)
Accounts must be locked after 5 unsuccessful logon attempts

## Password and Account Protection Standards

 - **No Reuse Outside CC.** Always use different passwords for ConstructConnect accounts from other non-ConstructConnect access (e.g., personal ISP account, option trading, benefits, etc.). 
 - **No Duplication Within CC.** Always use different passwords for various ConstructConnect access needs whenever possible. For example, select one password for systems that use directory services (i.e. LDAP, Active Directory, etc.) for authentication and another for locally authenticated access. 
 - **No Sharing.** Do not share ConstructConnect passwords with anyone, including administrative assistants or secretaries. All passwords are to be treated as sensitive, confidential information. 
 - **Keep It Secret.** If you have reason to believe that someone else has seen, knows, or has access to one of your passwords, change it immediately.
 - **Keep It Hidden.** Never store a password where it could be found such as written down or typed/pasted/saved into an unapproved file (.txt, Word, Excel, Powerpoint).
 - **Keep It Safe.**  Passwords should never be stored in any non-approved location or application. Never use the "Remember Password" features of applications such as web browsers. 
 - **Don't Send.** Do not reveal a password in email, chat, or other electronic communication, even if it is to yourself.
 - **Deny Eavesdroppers.** Do not speak about a password in front of others. 
 - **No Hints.** Do not hint at the format of a password (e.g., "my family name") 
 - **Refuse if Asked.** If someone demands a password, refer them to this document and direct them to the Information Security Department. 
 - **MFA.** Enable multi-factor authentication whenever available.

## Onboarding

When onboarding, it may be necessary to send credentials to new teammember(s). When doing so:
 - **No Middleman.** The password should be provided by the account creator directly to the new team member. Do not go through an intermediary, such as the teammember's manager.
 - **Authorized Channel.** Credentials must only be provided via a pre-approved channel and/or using a pre-approved method. If you have a question about this, please contact the security team at SecurityReviews@constructconnect.com.
 - **Change Upon First Login.** Whenever logging into a user account for the first time, change your password. Wherever possible, this requirement should be enforced by a technical control.

# Exceptions 

Exceptions to this policy are not permitted unless properly documented and submitted via the Exceptions Procedure and have the required management approval.

# REFERENCES 

Escal Institute of Advanced Technologies, Inc. (SANS Institute)

## Regulations

PCI-DSS

## NIST

NIST 800-63B

## Relevant Roper CIS Domain(s)/Category(ies)/Subcategory(ies).

 - 11 Data Protection
 - 14 Security Awareness

## Related Policies, Plans, Procedures and Guidelines.

 - Written Information Security Program
 - Policies
    - Acceptable Use Policy
    - Access Control Policy
    - Data Management Policy
    - Personnel Security Policy