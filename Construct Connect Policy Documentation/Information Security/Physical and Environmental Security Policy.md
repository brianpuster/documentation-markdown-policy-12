title: Physical and Environmental Security Policy  
author: vCISO  
published: 2020-09-03  
policy level: Annual General
approver(s): Jeff Cryder - EVP, Julie Storm - CPO, Bob Ven - CTO/CIO, Dana Oney - SVP IT Ops  
location or team applicability: All locations, all teams  


The purpose of this policy is to protect digital and physical copies of sensitive or confidential information in all forms (e.g., paper, storage, devices, physical hardware) from the risk of unauthorized access, loss, or damage when unattended. This is inclusive of physical devices and workspaces such as data centers and office space. 

## Scope

This policy applies to all employees, temporary staff, contractors, physical computing devices, and physical work spaces of ConstructConnect. 

## Responsibilities


Risk Committee
: Responsible for ensuring that the appropriate resources, people, process, and technologies are deployed to enable and support this policy. 

Corporate Information Security Office (CISO)
: Responsible for designing, managing, and monitoring security controls as well as auditing compliance with this policy.

Managers
: Responsible for ensuring that personnel under their management protect information in accordance with information security policies related to their area of expertise and job responsibilities.

IT Team
: Responsible for implementing controls relating to digital and technology resources to support this policy.

Facilities Team
: Responsible for implementing physical controls to work areas to support this policy.

## Review

This document must be reviewed for updates annually or whenever there is a material change in the Company's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CISO owns this Policy, but any update or change must be approved by the Risk Committee.

# Clear Desk

- All sensitive or confidential documents, materials and portable media (e.g. USB Drives, CD or DVDs) must be removed from desks and locked in a drawer or cabinet when not in use. (Note: This also applies to post-it notes with passwords or other data written on them.)
- Any paper waste that contains sensitive or confidential Information must be disposed of in provided secure disposal bins.
- Display monitors at each workstation or laptop must be positioned to optimize data privacy.
- Computer screens must be locked or user logged out of the computer when temporarily unattended (e.g. user is still in the building but stepped away for a break or a meeting).
- Keys to cabinets and drawers mus not be left unattended.
- If an individual incurs an extended absence from work, such as holidays, vacation, etc., they must clear their desk of all items considered sensitive and confidential.
- Any printed documents with sensitive or confidential Information should be retrieved immediately from the printer and stored face-down when not in use.
- All unclaimed print jobs will be disposed of in provided secure disposal bins at the end of the day.
- All whiteboards will be erased of sensitive or confidential information when not in use.
- Walkthroughs should be conducted quarterly to identify any potential gap and to ensure that the Policy is implemented. 

# Physical Security

## Physical Security Perimeter

- Each office location and data center must include at least one of the following security measures: a reception desk, visitor log and escort, key card or video surveillance of entrances and exits.
- It is each building managers responsibility to alarm, monitor, and test all fire doors.
- Suitable intruder detection systems must be installed to national, regional or international standards and regularly tested.

## Physical Entry Controls

- Access to server rooms and data centers must be controlled and restricted to authorized persons only; authentication controls (e.g. key cards, key locks and key logs, etc.) must be used to authorize and validate all access; an audit trail of all access must be securely maintained.
- There should be periodic reviews of all physical spaces for possible security weaknesses and adherence to security policies such as not leaving doors propped open. 
- Access rights to server rooms, data centers, and other secured office spaces must be annually reviewed, updated, and revoked when necessary.
- Securing offices, rooms, and facilities: Account must be taken of relevant health and safety regulations and standards.
- If and when an Employee/ Contractor loses or has their physical access card stolen/ lost, they must let their manager know and fill out a security incident report and send to the ConstructConnect security team at [email address].
- All employees should be cognizant of individuals attempting to tailgate or otherwise gain unauthorized entry into ConstructConnect office space. All individuals entering a ConstructConnect workspace should either be an accompanied guest of a badged employee or an employee with a badge which is used upon each entry into the space. It is the responsibility of all employees to ensure physical security of ConstructConnect facilities. 

# External and Environmental Threats

- Building management for all office locations must provide fire protection systems that comply with the local fire codes. 
- Third-party data centers hosting ConstructConnect systems should be selected based on their ability to provide audit reports or some other form of assurance that they comply with this policy and its guidelines. 
- Inside of data centers or critical server rooms, temperature, humidity, water, and fire controls should be in place to ensure proper functioning of all equipment. There should also be monitoring mechanisms in place to generate notifications in the event that sub optimal conditions are reached or any equipment fails. 

# Physical Data Protection

- Related to printed data sources (on hard copy) or portable media (e.g. flash drives, tapes, DVDs etc.), data must be secured in a physical location that supports right of least privilege. Hard copy data sources should be shredded using shredder bins located on-premises in order to preserve confidentiality of hard copy data sources.
- Related to physical media (e.g. tapes, flash drives, DVDs, etc.) should be not left open to where unauthorized individuals could grab them and view their contents.

# REFERENCES

## Regulations.

- PCI-DSS

## NIST CSF

- Protect
- Detect

## Roper CIS Controls

- 1 Cybersecurity Governance and Risk Management
- 4 Access Management
- 7 Physical and Environmental Controls
- 11 Data Protection
- 17 Penetration Testing and Red Team Exercises]

## Related Policies, Plans, Procedures and Guidelines

- Written Information Security Program

- Policies
    - Access Control Policy
    - Acceptable Use Policy
    - Asset Management Policy
    - Data Management Policy
    - Personnel Security Policy

# Insert any procedure or process documentation (optional):

- Physical Security Walkthrough Checklist (to be created)
