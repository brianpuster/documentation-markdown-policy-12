title: Access Control Policy  
author: VerSprite vCISO  
published: 2020-06-03


#### Policy name:
Access Control Policy

#### Policy level:
Annual General

#### Author:
VerSprite vCISO

#### Approver(s)
Bob Ven CTO/CIO
Dana Oney SVP IT Ops

#### Location or Team Applicability
All locations, all teams

#### Effective date:
input date using YYYY-MM-DD format



# GENERAL

# Objective

The purpose of this policy is to ensure information security and data privacy by defining security requirements for accessing IT environments and handling of data storage media.

## Scope

This policy applies to anyone who has physical or logical access to Company locations or information assets (computers, networks, etc.) owned, used or administered by Company.

## Responsibilities

- The Steering Committee is responsible for ensuring the appropriate resource, process, people, and technology are deployed to enable this policy. 
- The Corporate Information Security Office (CISO) is responsible for auditing compliance with this policy in accordance with its guidelines. 
- Managers are responsible for: 
  - Determining proper authorization level according to business need using Role Based Access Control and Principle of Least Privilege; 
  - Requesting, approving or rejecting employee access according to role or group, rather than individually; 
  - Periodically reviewing access and permissions; 
  - Using separation of duties where possible for both functions within an application and access to multiple applications; 
  - Ensuring no single individual has the ability to control all stages of a business process; and 
  - Communicating the requirements of this policy. 
- IT Team is responsible for actual administration of the provisioning process and user account management. 
- Human Resources is responsible for:
  - [Issuance of badges/ID? for new employees, collection of badges from terminated employees; and replacement of badges as necessary].
  - [notifying IT for system access for new employees, re-provisioning access for employees changing roles, and revoking credentials for terminated employees.]

## Review

This document must be reviewed for updates annually or whenever there is a material change in the Company's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CISO owns this Policy, but any update or change must be approved by the Information Security Committee (Steering Committee?).

##### REQUIREMENTS

Rules for access need to be established, documented, and reviewed based on business and security requirements. 

- Least Privilege. All access granted, be it new or a revision of existing access, will be done using the Principle of Least Privilege for physical, logical and network access. 
- RBAC. Role Based Access Controls based on job function or business need will be created and must be used wherever possible with standard user access profiles providing minimum access for common job roles. 
- Separation of Duties. Submitting access requests, authorizing access and reviewing access rights must be segregated tasks. 
- Deprovisioning. Any access right must be removed immediately once it is no longer required. 
- MFA. Multi-Factor Authentication must be implemented for sensitive assets or information. 
- Approval Before Admin. CISO will verify admin access grants every 90 days, and report any discrepancies to Risk Committee.
- Admin and Standard User Accounts. Individuals granted administrative access will get that access as a second account to their standard user account. The administrative account will only be used for administrative tasks and the standard account for normal use. 
- No Shared Credentials. Multi-user or shared user accounts are strictly prohibited unless mandated by a specific system or process limitation. In this event a policy exception request must be submitted and authorized by the CISO or designated representative. Additionally, account access may be specifically monitored. 
- Intended Purpose. Service accounts must be specific to the use for which they are being created. They must only be used by the intended application, service, process, database or system and are not used for user login. 
- Track Provisioning. All user provisioning will be tracked using the designated ITSM system and records will maintained for audit and compliance. 
- Access Audits. Access rights must be periodically reviewed or audited to identify and remove surplus privileges. 

##### PHYSICAL ACCESS

Access shall be defined and granted according to job role/ functionality. 

Unauthorized access must be reported as an Information Security Incident. 

Access to areas with confidential information shall be restricted to authorized individuals only. 

Access rights to secure areas shall be regularly monitored and, reviewed and updated.

###### Employee Access

Physical access across Company office environment is via [badges?]. 

Lost Badge: Lost badges shall be immediately reported to the HR. HR shall create a ticket in BECA take the necessary steps to re-issue lost badges. 

Replacement of badge: A formal request shall be made to HR for replacement of badges in case of any modification, access level changes, etc. 

###### Visitor Access

Date and time of entry and departure of visitors shall be recorded. 

Visitors shall be granted access only for authorized purposes. 

Visitors shall be accompanied at all times by an employee. 

##### LOGICAL ACCESS

All users shall change their passwords for information systems at regular intervals in accordance with the Password Policy requirements in (see the Acceptable Use Policy). 

All users shall lock their screens when they leave their desks to reduce the risk of unauthorized access. 

All users shall keep their passwords confidential. 

###### Administrative Responsibilities

Periodic review of access rights with information systems owners shall be conducted. 

Managers of different departments shall review access rights on a regular basis and after any changes to employee role and responsibility. 

Formal documentation regarding of all privileges of user accounts shall be maintained. 

User accounts shall be disabled when it is no longer required (employee resignation termination or change of duties). 

Removal of accounts shall include removal of access rights. 

##### REMOTE ACCESS

Access to Company's internal information system shall be done via secure communication channel, e.g. VPN. 

##### REFERENCES

###### Regulations
- PCI-DSS
- US data privacy laws

###### NIST CSF
- Protect

###### Roper CIS Controls
- 2.1, 4.1, 4.2, 4.3, 4.4, 4.5, 7.1, 7.2, 8.6, 12.1, 12.2

###### Related Policies, Plans, Procedures and Guidelines
- Written Information Security Program
- Data Management Policy

#### Insert any procedure or process documentation (optional):

[insert text]
