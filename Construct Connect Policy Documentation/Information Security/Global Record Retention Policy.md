title: Global Records Retention
author: Roper, and adapted by VerSprite vCISO
published: 2020-01-29
effective date: 2020-01-29
policy level: Annual General
data classification: Internal
approver(s): Bob Ven CTO/CIO, Buck Brody CFO/EVP, Jeff Cryder EVP/CC-IGPC, Dana Oney SVP IT Ops.
location or team applicability: All locations, all teams


# GENERAL
A MESSAGE FROM ROPER
Roper Technologies, Inc. (“Roper”) recognizes that it is a good business practice to manage information in a consistent, systematic and reliable manner so that it can be retained for those periods required by law and for business purposes and retrieved as necessary for legal, regulatory, and/or operational reasons.

Why is Information Governance important?

- **Productivity.** Good information governance practices help you to be more productive at work and make finding the information you need easier.
- **Risk.** Good information governance practices also help to limit exposure to data breaches, unauthorized access, court verdicts, fines or penalties in legal or regulatory matters.
- **Cost.** The fewer records you keep (paper or electronic), the less it costs to store and manage them.
- **Security.** Protect important and sensitive company information for clients, employees, business continuity and disaster recovery. Information governance helps to protect the Roper image of good faith, reputation, responsiveness and consistency in our industries and with customers, courts and regulators alike.

This Information Governance (IG) Policy (the “Policy”) – (Roper Global Records Retention Policy) provides guidance and governs the creation, classification, storage and destruction of all information at Roper and its subsidiaries, regardless of format (electronic or paper) while enabling Roper to comply with applicable contractual, regulatory and legal recordkeeping requirements.

Please take the time to carefully read, understand, and actively apply this Policy in your day-today activities. It is important that we protect our records and information, maintain our company’s outstanding reputation, and comply with applicable laws and regulations.

## Objectives
The objectives of this Policy are to:

- Define Information Governance (“IG”) scope, ownership and guidelines for Roper that include the appropriate creation, classification, retention and disposition of UCMS Records, Business Records and Non-Records for the minimum periods required by applicable laws, regulations and business needs;
- Outline essential roles and responsibilities associated with maintenance of Roper’s Information Governance Program (“IG Program”);
- Provide for the disposition of Business Records in a timely, consistent and secure manner pursuant to the Data Management Policy and Record Retention Schedule (“Schedule” or “RRS”) [https://constructionmd.sharepoint.com/:x:/s/InformationGovernance/EXbikyLDmUBCnp4nOtfYB4oBbRrwrjRDDd-6Rp1VGQv9Ig?e=7gJIMm ] after there is no longer a legal or operational need to retain such Business Records; and
- Outline exceptions to general IG practices.

## Scope

- The Policy applies to Roper Headquarters, all Roper Companies and subsidiaries, partners, and related entities of Roper that manage information that is the legal responsibility of Roper. This policy applies to employees, temporary employees, contractors, consultants and anyone else who has access to, creates, has use of, or manages information for a reason affecting or relating to Roper’s operations (collectively, “Personnel”).
- Roper is committed to complying with applicable recordkeeping requirements, whether captured in law or regulation. Like any other Roper asset, information generated in the course of normal business operations should be used exclusively in serving the interests of Roper and its operations.
- Information generated during the course of Roper’s operations falls into one of Three categories – UCMS Records, Business Records and Non-Records.
  - "UCMS Records" are any document(s) or object(s) that contain(s) data and/or information relating to a specific construction project or proposal.  UCMS Records can exist in many forms and may include blueprints, technical designs and drawings, specifications, building plans, correspondence, addenda, government notices or other similar technical and project information for which a subscriber or any authorized user sends, discloses, uploads, posts, or otherwise provides to ConstructConnect by or through one of its customer-facing applications, postal mail, e-mail, or hand delivery.
  - “Business Records” are any document(s) or object(s) that contain(s) data and/or information and which relates to business activities or decisions, except for those records defined as UCMS Records. Business Records can exist in many forms, including writings, documents, data, software, samples, drawings, graphs, charts, maps, email, photographs, sound recordings, video, images, or other data compilations stored in any medium from which information can be obtained. 
  - **A special note on Business Records.** There is only one original Business Record – all other copies, drafts or versions are considered Non-Record material. Material retained for personal reference or use is not a Business Record.
  - By contrast, there are no minimum retention requirements for Non-Records. Non-Records must be destroyed in adherence to the Data Management Policy when they have been superseded and/or when related work-specific tasks have been completed. Non-Record material must not be kept longer than the corresponding Business Record. Non-Records do not qualify as Business Records, and may include items such as Convenience Copies, Working Papers, Drafts, Transitory Information and Reference Materials.
- This Policy provides for the management of information in the possession, custody or control of Roper and its Personnel, regardless of physical location or storage format (electronic, including email, or paper). This includes information maintained in individual workspaces, offices, common areas, and within the control of third-party vendors or at any offsite storage location. Business Records maintained in nonUS/Canada facilities or on Non-US/Canada systems could be subject to applicable country and/or provincial/local laws and regulations.
- **A special note on email**: email may qualify as a Business Record or as a Non-Record depending on the content. The retention period for email messages is determined by content, not the media by which the content was transmitted. Consequently, email that reflects any of the Record Classes listed within the Schedule must be maintained according to the corresponding retention period. If the subject matter of the email pertains to multiple record classes, the retention period that is the longest shall be applied. Attachments to emails are considered separate documents that must be independently reviewed to determine their retention period. In some instances, email may be the only record documenting communication about a specific issue, development, undertaking or completion of an administrative or business transaction. That said all Roper employees must recognize the significance of the email messages they create and receive, and endeavor to prudently use email to accomplish Roper’s business objectives and not to unnecessarily inflate the size and content of their electronic mailboxes. Employees should continually endeavor to follow this Records Retention Policy so that emails are not kept any longer than necessary.

## Policy

- Roper will manage the retention, storage, and disposition of its information, whether it is in paper, electronic, media or other formats, in accordance with all applicable laws, regulations, contracts, accounting, tax, audit, legal or other requirements and/or business practices. All Roper Personnel must retain Business Records in accordance with the Schedule.
- This Policy applies to all information regardless of storage media or storage location (e.g., laptop, hard drive, flash drive, compact disc, cloud-based storage, network drive, etc.) or format (e.g., paper, electronic, audio, video, physical or other formats).
- Roper Personnel who create and manage Business Records must retain them for the minimum period of time specified in the Schedule for that record class (the “Retention Period”). Unless there is a legal requirement or approved business purpose to maintain them longer than the specified Retention Period (e.g., a Legal Hold Notice described under the “Legal Hold” section), Business Records should be destroyed promptly after the expiration of the applicable Retention Period (at least annually).
- Information should be managed in accordance with other Roper policies.
- Personnel should take all reasonable measures to ensure the integrity, authenticity, confidentiality and availability of the Business Records they create, use and maintain.

## Records Ownership

- With the exception of UCMS records or unless otherwise specified by legal agreement, all information created or received in the course or conduct of Roper’s business by any individual subject to the Policy are the exclusive property of Roper, and they are not the property of the record’s author, creator, or custodian. No Personnel has any personal or property right to any Business Record(s), including those Business Records that they drafted or helped create.
- Intellectual property rights in all Business Records belong to Roper and are owned by Roper in perpetuity. Intellectual property includes, among other things, registered trademarks, service marks, patents, registered designs, applications for and the right to apply for any such rights, inventions, unregistered trademarks, trade and business names, copyrights, unregistered design rights, databases and rights in databases, and all other similar proprietary rights that may exist in any part of the world. Intellectual property rights also extend to all renewals, extensions, revivals, consents, and applications related to the above property types.

## Records Retention

- The Schedule groups Business Records into record classes and includes a retention period – how long the Business Records must be retained. Because the Schedule cannot explicitly list every Business Record, it does not represent an exhaustive list of record classes. Rather, Roper Personnel should use common sense, experience and good judgment in identifying the most appropriate record category and applicable Retention Period when making retention decisions. When Personnel have questions with respect to identifying the most appropriate record class, or where multiple record classes may apply to the same document, Roper Personnel should check with their immediate supervisor or department head for guidance.
- Retention Periods are assigned to record classes based on legal, regulatory and business requirements. Retention Periods are either fixed-period (e.g., 7 Years), or event-based (e.g., Expiration of Contract + 7 Years). Please refer to the Schedule for any exceptions.
- Non-Records are not subject to formal minimum retention requirements. For that reason, Retention Periods for Non-Records do not appear in the Schedule. Earlier drafts of documents and duplicates of Business Records should only be retained so long as an immediate business need exists, and no longer than the Business Record upon which it is based is retained. Except when subject to a Legal Hold Notice, Non-Records should be promptly destroyed.
- Storage and disposition of Business Records must follow the Schedule and any other applicable corporate policies and procedures. Consider the following:
  - **Customer and Supplier agreements**: Roper may enter into agreements, cooperative agreements, contracts, etc. (“Governing Documents”), with various entities with whom Roper does business. These Governing Documents may contain terms and conditions addressing the retention and destruction and/or access to certain Business Records, and/or records of the counterparty under Roper’s control. To the extent it is determined that such Governing Documents create more stringent retention requirements than those identified in the Schedule, the retention periods set forth in the Governing Documents shall prevail.
  - **Other applicable laws**: To the extent Roper is subject to other country’s federal, state/provincial or local laws governing access to Business Records, the mandates of those laws shall apply in conjunction with this Record Retention Policy and the Schedule. Questions about whether specific applicable laws supersede this IG Policy and Schedule should be directed to ConstructConnect's Information Governance Program Coordinator (the CC-IGPC) at [INSERT E-MAIL ADDRESS], who will confer with Roper’s Legal Department as necessary.

## Legal Hold

- As legally required based on the facts and circumstances, the Roper Legal Department or a particular Roper Company’s Legal Department will issue a “Legal Hold Notice” – a formal notification to suspend routine destruction of records that are potentially relevant to a legal matter – to the appropriate Roper Personnel outlining the general nature of the matter and the relevant Business Records and Non-Records that should be preserved until the matter is resolved. All Roper Personnel who receive a Legal Hold Notice shall acknowledge it as required by the Legal Department.
- A Legal Hold Notice suspends the normal Retention Period of all relevant Business Records and Non-Records regardless of format – including hard copy and electronic documents. Roper Personnel are required to preserve relevant Business Records and Non-Records subject to a Legal Hold Notice even if they are no longer needed for business purposes or are due for destruction under the Schedule and shall preserve them until the Legal Department issues a written notice lifting the Legal Hold Notice. No Business Record or Non-Record subject to a Legal Hold Notice should be disposed of under any circumstances.
- The Legal Department will notify appropriate Roper Personnel when a Legal Hold Notice is lifted and no longer applicable. At that time, any Business Records and Non-Records preserved under the Legal Hold Notice will once again be subject to the terms of this Policy and shall be managed according to the Schedule.
- Failure to obey a Legal Hold Notice may result in sanctions or penalties being imposed against Roper and the violating Personnel. Violations include, but are not limited to, destroying, altering, modifying, spoliating, or otherwise making inaccessible relevant Business Records and Non-Records that are subject to a Legal Hold Notice.

## Responsibilities

### All Personnel

- All Personnel who create, receive, use, or manage Roper information are required to comply with the Policy. Failure to comply with the Policy may lead to disciplinary action, ***up to and including termination of employment as well as legal action.***
- All Personnel are responsible for ensuring proper classification and retention of Business Records in accordance with this IG Policy, and for compliance with Legal Hold requirements when applicable.
- All Roper personnel are required to participate in initial web training when hired as well as the designated Roper periodic Information Governance (IG) training sessions.

### Companies

- Roper Companies are responsible for establishing an implementation plan to ensure compliance with this Policy and the Schedule. The Program Manager and the Program Coordinators will help to facilitate this effort. Each Roper Company must nominate an employee to serve as its Program Coordinator.

### IG Program Manager

- The IG Program Manager is responsible for implementing this Policy. In addition, the IG Program Manager is responsible for the following:
  - Reporting to the General Counsel (“GC”) and bringing IG issues of importance to the GC or the Chief Compliance Officer, as appropriate;
  - Implementing the IG Program throughout Roper;
  - Providing day to day oversight of the IG Program;
  - Approving any changes to the Schedule;
  - Facilitating the creation, maintenance and update of standards, policies, the Schedule and any training materials to assist Roper in complying with known recordkeeping requirements;
  - Working with and supporting the CC-IGPC to assist departments and programs in meeting IG Program responsibilities;
  - With the assistance of CC-IGPC, monitoring and assessing records management practices among Roper Personnel, as well as practices of third parties that hold, store or manage Roper Business information; and
  - Execute the strategic vision for the IG Program aligned with Roper’s business strategy and needs.
  - Working with the CC-IGPC to develop, deliver and monitor IG training.
  - Providing legal advice regarding any portion of this Policy and the Schedule as it applies to any specific situation;
  - Reviewing and providing legal advice regarding updates of this Policy;
  - Reviewing and providing legal advice regarding updates to the Schedule, as needed in response to changing legal, regulatory or business requirements;
  - Providing legal advice regarding applicable laws and regulations to the IG Committee and/or departmental management.

### General Counsel (“GC”)

- The GC, with input from representatives from Finance, HR, IT, and outside legal experts as he or she sees fit, coordinates the following IG activities:
  - Overall responsibility for establishing and maintaining organization-wide IG policies, procedures, standards, and guidelines;
  - Set the strategic direction for the IG Program;
  - Review and approve off-site storage vendors and facilities;
  - Work to resolve escalated information governance and records management related issues; and
  - Provide guidance on and final approval of all IG Program documentation prior to distribution to Roper Personnel.

### ConstructConnect Information Governance Program Coordinator ("CC-IGPC")

- CC-IGPC facilitates the implementation of this Policy and the Schedule within their Roper Company, in coordination the IG Program Manager. The responsibilities of the CC-IGPC include:
  - Proposing revisions and additions to the Schedule;
  - Communicating the process of packing, shipping, retrieving and disposing of inactive physical Business Records;
  - Communicating the process of managing electronic records and records disposition to the Roper Personnel at your company;
  - Leading clean-up efforts in the department or program;
  - Coordinating audit and compliance activities with the IG Program;
  - Facilitating all IG Program-related activities, as directed by the IG Program Manager; and
  - Promoting the IG Program and Policy to the departments and/or programs within their scope of responsibility.
- CC-IGPC must:
  - Hold a position within any of the following functions: legal, finance, compliance or IT;
  - Notify the IG Program Manager when the responsibilities of Program Coordinator change from one person to another.

### Information Technology

- Expertise found within each Roper Company’s Information Technology Department is needed to implement policy settings on electronic information, and to automate records disposition where appropriate. Information Technology personnel are also responsible for the following:
  - Supporting enterprise technology solutions necessary for the proper functioning of Roper’s IG Program;
  - Assisting business areas with requirements scoping, selection and purchase of technology solutions that complement the objectives of the IG Program; and
  - Providing advanced notice of proposed technology changes that could potentially impact the creation, classification, retention or destruction of Business Records.

## Changes, Exceptions and Violations

- After extended consideration of the goals of this Policy and the Schedule, some Roper Companies may possess a legitimate business need to hold particular Business Records longer than proscribed under this Policy. In those instances, requests for changes or exceptions to this Policy or the Schedule should be submitted by CC-IGPC and will be reviewed for approval by the GC as needed.
- Effective administration of this policy requires the support of all Roper Personnel and others who deal with business-related information and/or information storage systems. Therefore, it is the responsibility of all Roper Personnel and other authorized users to understand and adhere to this Policy.
- Personnel who learn of a violation of this Policy should promptly report any such violations to CC-IGPC or the GC. 

## Compliance During Transition

- Roper is working towards compliance with the requirements of this Policy. Roper will take a phased approach for implementation of this Policy and the Records Retention Schedule that reflects each Roper Company’s unique business and regulatory profile. When all departments/functions have the processes, infrastructure and governance in place to adhere to the Policy and Schedule, they will become effective enterprise wide.
- Roper auditors will begin assessing compliance with this Policy and the Schedule in 2021.

## Additional Resources

Who to contact for help with this Policy
- Talk to your supervisor
- Talk to CC-IGPC
- E-mail to [INSERT E-MAIL FOR CC-IGPC HERE] 

# Appendix A
## Definitions
**Business Record** – Any document or object that contains data and/or information and which relates to business activities or decisions. Business Records can exist in many forms, including writings, documents, data, software, samples, drawings, graphs, charts, maps, email, photographs, sound recordings, video, images, or other data compilations stored in any medium from which information can be obtained. There is only one original Business Record. All other copies, drafts or versions are not considered Business Records. Material retained for personal reference or use is not a Business Record.

**Convenience Copy** – Information created for convenient reference that duplicates an original Business Record, which may or may not be kept in the department of record. Convenience Copies are not considered Business Records and are not required to be kept according to the Schedule, unless additional information is added to them that changes their classification to a Business Record. Convenience Copies should be disposed of as soon as they are no longer needed for operational business purposes and must not be kept longer than the corresponding Business Record.

**Draft** – The preliminary form of a document before it becomes a Business Record. In most cases, Drafts must be disposed of once a new draft is created, or the final version of a Business Record is completed. Drafts must not be kept longer than the corresponding Business Record.

**Electronically Stored Information (“ESI”)** – Includes any Business Records created or stored in electronic format, as opposed to physical format (e.g. paper).

**Information** – Data that has been given value through analysis, interpretation, or compilation in a meaningful form.

**Legal Hold** – A communication issued by either the Legal Department of either Roper or a Roper Company as a result of current or anticipated litigation, audit, government investigation or other such matter that suspends the normal disposition or processing of relevant Business Records and Disposable Information.

**Non-Records** - Documents, files and other information not required to be retained by the Schedule. Non-Records include Convenience Copies, Drafts, Working Papers, and Transitory Information, and must be destroyed as soon as it is no longer needed for operational business purposes.

**Record Class** – A grouping of Business Records according to their particular function and assigned a common Retention Period by the Schedule.

**Records Retention Schedule** – A Records Retention Schedule (RRS) showing Record Class titles, descriptions and retention requirements for distinct types of business records and information adopted by Roper. The RRS is grouped by Functional Areas. Within the Functional Areas, Record Classes are defined, with descriptions and example document or file names. These are typically organized into groups of related subjects or business processes. 

**Retention Period** – The period of time a Business Record is required to be kept retained by the Schedule, whether for regulatory, legal, or business purposes.

**Transitory Information** – Short-term information including reference materials and notes, outof-office replies, routine system messages and log files, and correspondence and/or email with no ongoing business value. Transitory information must be disposed of promptly.

**Vital Records** – Records identified as essential for the business continuation or survival of Roper if a disaster strikes (for example, fire, flood, theft, etc.). Vital Records are necessary to establish Roper’s legal and financial status and to determine the rights and obligations of employees, clients, customers, partners, or other stakeholders.

**Working Documents** – Short-term information created as part of a project or in conjunction with working on a task. This information could include notes, reference materials, and other documents associated with the preparation and creation of Business Records. Working Documents are not considered Business Records under the Schedule and must be disposed of as soon as they are no longer needed for operational business purposes.

# REFERENCES
## RELEVANT REGULATIONS

- PCI-DSS
- US data privacy laws

## RELEVANT NIST CSF DOMAIN(S)/CATEGORY(IES)/SUBCATEGORY(IES).

- Identify

## RELEVANT ROPER CIS DOMAIN(S)/CATEGORY(IES)/SUBCATEGORY(IES).

- 1.3
- 8.6
- 10.2
- 11.1

## RELATED POLICIES, PLANS, PROCEDURES AND GUIDELINES.

- Written Information Security Program
- Access Control Policy
- Data Management Policy
- Roper Record Retention Schedule https://constructionmd.sharepoint.com/:x:/s/InformationGovernance/EXbikyLDmUBCnp4nOtfYB4oBbRrwrjRDDd-6Rp1VGQv9Ig?e=7gJIMm