title: Exception Management Procedure    
author: VerSprite vCISO   
published: 2021-04-29  
policy level: Annual General   
data classification: Internal   
approver(s): Buck Brody, EVP Finance, Jennifer Johnson, Chief Experience Officer, Dana Oney, SVP IT Ops  
location or team applicability: All locations, all teams  


# GENERAL

## Objective

The objective of  this  procedure  is  to  establish a process for requesting and approving exceptions to ConstructConnect policies and procedures. Exceptions are used to document information technology risks or policy violations that, for one reason or another, cannot effectively be mitigated, transferred, avoided, or accepted. Unlike risk acceptance, exceptions automatically expire after a period of time and must be resubmitted for approval. This Procedure also describes the ongoing responsibilities related to approved exceptions.

## Scope

This  Procedure  applies  to  all ConstructConnect employees, contractors, and vendors who are subject to ConstructConnect policies and procedures.

## Responsibilities

- **Risk Committee** is responsible for reviewing and approving all Exception requests.

- **Corporate Information Security Office (CISO)** is responsible for oversight and general management of this Procedure. 

- **InfoSec Team** is responsible for communicating with employees, contractors, and vendors regarding the process for requesting exceptions and, when possible, identifying situations when an exception should be requested.

- **IT Team** is responsible for ensuring ConstructConnect systems maintain compliance with Company policies and procedures unless an approved exception exists. The IT Team will also validate exceptions when applicable. 

- **Managers** are responsible for ensuring that members of their respective teams are familiar with this Procedure and know where to find it.

- **ConstructConnect Team Members** are responsible for familiarizing themselves with and implementing the contents of this Procedure.

## Review

This document must be reviewed for updates annually or whenever there is a material change in the Company's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CISO owns this Policy, but any update or change must be approved by the Risk Committee.

# Step 1: Requesting an Exception

A written Exception Request is submitted to the IT Team and the affected business unit leader(s). An Exception Request Form can be found here: [INSERT LINK TO EXCEPTION REQUEST FORM]. 

Exception Requests must contain the following information:

- Person requesting the exception (the “Requestor”);
- Email address of the Requestor;
- Date of the request;
- Description of the Exception being requested, including all relevant technology(ies), process(es), policy(ies), procedure(s), standard(s), and/or guideline(s);
- Explanation of the business case for the exception, including the reason ConstructConnect or the relevant team cannot adhere to existing policy(ies), procedure(s), guideline(s), or standard(s);
- Description of the technical, administrative, or physical controls mitigating relevant risks;
- Detailed list of the potentially affected data (fields, classifications);
- Detailed list of other personnel, teams, or processes that rely on the potentially affected data; and
- Expected length of the exception and plan for remediation, if applicable.

# Exception Request Validation

The IT Team and affected business unit leader(s) must review the Exception Request in a timely manner. If the request is unclear or missing information, the IT Team and/or business unit leader(s) must:

- clarify or add the missing information; and/or
- conduct a follow-up with the Requestor as needed.

# Exception Request Review

After the Exception Request is validated, the IT Team or the business unit leader(s) must forward the request to the Risk Committee for review. The Risk Committee decides whether the exception is:

- Approved as drafted;
- Conditionally approved once certain additional risk treatments are applied; or
- Denied. 

The Exception Request form, no matter the outcome, must be signed by either the Chief Technology Officer, the Chief Financial Officer, or the Chief Executive Officer. The decision must be presented in writing to the Requestor, the IT Team, and/or the relevant business unit leader(s).

# Record and Track

Once a decision is reached, the CISO will record the decision and perform the following:

- If the Exception Request is approved, the CISO sends the document to the InfoSec team for secure storage.
- If the Exception Request is conditionally approved, the CISO sends the request and the response to the InfoSec team for secure storage, and monitors the required risk treatments until complete.
- If the Exception Request is denied, the CISO sends the document to the InfoSec team for secure storage of the Exception Request and denial. 
- The CISO will also record the relevant expiration date. 

# Periodic Review

At least quarterly, all approved Exceptions will be reviewed by the CISO so that these risks can be:

- continually monitored;
- accounted for when planning initiatives or changes; and
- considered for interaction with other risks when determining impact and likelihood.

# Expiration

Generally, all Exceptions should automatically expire based on approval date and risk rating, as determined by the Risk Committee.

- All Exceptions for Critical risks expire after 6 months.
- All Exceptions for high risks expire after 12 months. 
- All Exceptions for medium or low risks expire after 24 months.


# REFERENCES 

## Relevant Roper CIS Domain(s)/Category(ies)/Subcategory(ies).

 - 14 Security Awareness

## Related Policies, Plans, Procedures and Guidelines.

 - Written Information Security Program
 - All Policies, Plans, Procedures, and Guidelines